﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAddRemarkRequestFromChecker
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RemarkNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.RemarkNameTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.RemarkStatusCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.SuspendLayout()
        '
        'RemarkNoTextBox
        '
        Me.RemarkNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.RemarkNoTextBox.CustomButton.Image = Nothing
        Me.RemarkNoTextBox.CustomButton.Location = New System.Drawing.Point(76, 2)
        Me.RemarkNoTextBox.CustomButton.Name = ""
        Me.RemarkNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.RemarkNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.RemarkNoTextBox.CustomButton.TabIndex = 1
        Me.RemarkNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.RemarkNoTextBox.CustomButton.UseSelectable = True
        Me.RemarkNoTextBox.CustomButton.Visible = False
        Me.RemarkNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.RemarkNoTextBox.Lines = New String(-1) {}
        Me.RemarkNoTextBox.Location = New System.Drawing.Point(310, 219)
        Me.RemarkNoTextBox.MaxLength = 32767
        Me.RemarkNoTextBox.Name = "RemarkNoTextBox"
        Me.RemarkNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.RemarkNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.RemarkNoTextBox.SelectedText = ""
        Me.RemarkNoTextBox.SelectionLength = 0
        Me.RemarkNoTextBox.SelectionStart = 0
        Me.RemarkNoTextBox.Size = New System.Drawing.Size(104, 30)
        Me.RemarkNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.RemarkNoTextBox.TabIndex = 75
        Me.RemarkNoTextBox.UseSelectable = True
        Me.RemarkNoTextBox.UseStyleColors = True
        Me.RemarkNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.RemarkNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(211, 219)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(79, 20)
        Me.MetroLabel1.TabIndex = 74
        Me.MetroLabel1.Text = "Remark no."
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(852, 35)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 77
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft501
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(777, 35)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 76
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Save50
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'RemarkNameTextBox
        '
        '
        '
        '
        Me.RemarkNameTextBox.CustomButton.Image = Nothing
        Me.RemarkNameTextBox.CustomButton.Location = New System.Drawing.Point(523, 2)
        Me.RemarkNameTextBox.CustomButton.Name = ""
        Me.RemarkNameTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.RemarkNameTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.RemarkNameTextBox.CustomButton.TabIndex = 1
        Me.RemarkNameTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.RemarkNameTextBox.CustomButton.UseSelectable = True
        Me.RemarkNameTextBox.CustomButton.Visible = False
        Me.RemarkNameTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.RemarkNameTextBox.Lines = New String(-1) {}
        Me.RemarkNameTextBox.Location = New System.Drawing.Point(310, 255)
        Me.RemarkNameTextBox.MaxLength = 32767
        Me.RemarkNameTextBox.Name = "RemarkNameTextBox"
        Me.RemarkNameTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.RemarkNameTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.RemarkNameTextBox.SelectedText = ""
        Me.RemarkNameTextBox.SelectionLength = 0
        Me.RemarkNameTextBox.SelectionStart = 0
        Me.RemarkNameTextBox.Size = New System.Drawing.Size(551, 30)
        Me.RemarkNameTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.RemarkNameTextBox.TabIndex = 79
        Me.RemarkNameTextBox.UseSelectable = True
        Me.RemarkNameTextBox.UseStyleColors = True
        Me.RemarkNameTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.RemarkNameTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(211, 255)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(95, 20)
        Me.MetroLabel2.TabIndex = 78
        Me.MetroLabel2.Text = "Remark name"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(211, 303)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(45, 20)
        Me.MetroLabel3.TabIndex = 80
        Me.MetroLabel3.Text = "Status"
        '
        'RemarkStatusCheckBox
        '
        Me.RemarkStatusCheckBox.AutoSize = True
        Me.RemarkStatusCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RemarkStatusCheckBox.Location = New System.Drawing.Point(310, 303)
        Me.RemarkStatusCheckBox.Name = "RemarkStatusCheckBox"
        Me.RemarkStatusCheckBox.Size = New System.Drawing.Size(137, 20)
        Me.RemarkStatusCheckBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.RemarkStatusCheckBox.TabIndex = 81
        Me.RemarkStatusCheckBox.Text = "MetroCheckBox1"
        Me.RemarkStatusCheckBox.UseSelectable = True
        '
        'FrmAddRemarkRequestFromChecker
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1028, 635)
        Me.Controls.Add(Me.RemarkStatusCheckBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.RemarkNameTextBox)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.AddMetroTile)
        Me.Controls.Add(Me.RemarkNoTextBox)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Name = "FrmAddRemarkRequestFromChecker"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Add remark (request from checker for approve)"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RemarkNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents RemarkNameTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RemarkStatusCheckBox As MetroFramework.Controls.MetroCheckBox
End Class
