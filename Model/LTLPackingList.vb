﻿Public Class LTLPackingList
    Public Property InvoiceNo As String
    Public Property TruckPackingNo As String
    Public Property BaleBarcode As String
    Public Property BaleNo As Nullable(Of Integer)
    Public Property Supplier As String
    Public Property Green As String
    Public Property WeightBuy As Nullable(Of Decimal)
    Public Property LoadUser As String
    Public Property LoadDate As Nullable(Of Date)
    Public Property TruckNo As String
    Public Property BaleBarcodeFromReplace As String
    Public Property Remark As String
    Public Property ReceivedFlag As Nullable(Of Boolean)
    Public Property BatchGroup As Nullable(Of Integer)
    Public Property CampainID As Nullable(Of Integer)
    Public Property FarmerID As String
    Public Property StockMvtInDate As Nullable(Of Date)
    Public Property QualityLTLCode As String
    Public Property Price As Nullable(Of Decimal)
    Public Property StockOutNo As String
    Public Property PurchaserID As String
    Public Property Type As String
End Class
