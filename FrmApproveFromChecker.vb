﻿Public Class FrmApproveFromChecker
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim XRcNo As String
    Dim XRequestType As String
    Dim XApproveStatus As Boolean
    Dim XRequestNo As Integer
    Dim XGreen As String
    Dim XClassify As String
    Dim XBuyingWeight As Double
    Dim XReceivingWeight As Double

    Private Sub FrmApproveFromChecker_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            RequestTypeComboBox.SelectedIndex = 0
            ApproveStatusComboBox.SelectedIndex = 0

            XRequestType = RequestTypeComboBox.Text
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If

            If XRequestType <> "" Then
                Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, ReceivingModule._defaultCrop, XRequestType, XApproveStatus)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    
    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub RequestApproveMetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles RequestApproveMetroGrid.CellMouseClick
        Try
            XRequestNo = RequestApproveMetroGrid.Item(10, RequestApproveMetroGrid.CurrentCell.RowIndex).Value
            XRcNo = RequestApproveMetroGrid.Item(2, RequestApproveMetroGrid.CurrentCell.RowIndex).Value
            If IsDBNull(RequestApproveMetroGrid.Item(3, RequestApproveMetroGrid.CurrentCell.RowIndex).Value) = False Then
                XBaleBarcode = RequestApproveMetroGrid.Item(3, RequestApproveMetroGrid.CurrentCell.RowIndex).Value
            End If

            ' ให้ Fill ลง sp_Receiving_SEL_MatByRcNo เพื่อ Get ค่า Green , Classify , BuyingWeight, ReceivingWeight
            Dim XbRow As ReceivingDataSet.sp_Receiving_SEL_MatByRcNoRow
            Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, XRcNo)
            XbRow = Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.FindBybc(XBaleBarcode)
            If Not XbRow Is Nothing Then
                XGreen = XbRow.green
                XClassify = XbRow.classify
                XBuyingWeight = XbRow.weightbuy
                XReceivingWeight = XbRow.weight
            End If

        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            XRequestType = RequestTypeComboBox.Text
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If

            If XRequestType <> "" Then
                Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, ReceivingModule._defaultCrop, XRequestType, XApproveStatus)
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RequestTypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RequestTypeComboBox.SelectedIndexChanged
        Try
            XRequestType = RequestTypeComboBox.Text
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If

            If XRequestType <> "" Then
                Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, ReceivingModule._defaultCrop, XRequestType, XApproveStatus)
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ApproveStatusComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ApproveStatusComboBox.SelectedIndexChanged
        Try
            XRequestType = RequestTypeComboBox.Text
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If

            If XRequestType <> "" Then
                Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, ReceivingModule._defaultCrop, XRequestType, XApproveStatus)
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Save_Click(sender As Object, e As EventArgs) Handles Save.Click
        Try
            If XRequestType = "" Then
                MessageBox.Show("กรุณาเลือก Request type", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If


            If XRequestNo <= 0 Then
                MessageBox.Show("กรุณา Click เลือกเลขรายการที่ต้องการจากตาราง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If XRequestType = "Edit" Then 'ถ้าเป็นการApprove เรื่อง Edit
                Dim result As DialogResult
                result = MessageBox.Show("ต้องการApprove ให้สามารถแก้ไขข้อมูลใช่หรือไม่? ", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    'ให้ update approvestatus
                    db.sp_Receiving_UPD_ManagerApprove(ReceivingModule._defaultCrop, XRequestNo, XRcNo, True, _username)
                    MessageBox.Show("ทำการApproveเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    XRequestType = RequestTypeComboBox.Text
                    If ApproveStatusComboBox.SelectedIndex = 0 Then
                        XApproveStatus = False
                    ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                        XApproveStatus = True
                    End If
                    Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, ReceivingModule._defaultCrop, XRequestType, XApproveStatus)
                ElseIf result = DialogResult.No Then
                    Me.Dispose()
                End If
            ElseIf XRequestType = "Delete" Then  'ถ้าเป็นการApprove เรื่อง Delete
                Dim result As DialogResult
                result = MessageBox.Show("ต้องการลบ barcode " & XBaleBarcode & " นี้ใช่หรือไม่? ", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    'ให Delete ข้อมูลใน mat & cpa 
                    db.sp_Receiving_DEL_MatCPA(XBaleBarcode, XRcNo, XGreen, XClassify, XBuyingWeight, XReceivingWeight, "Delete from ApproveFromCheckerRequest", _username)
                    'ให้ update approvestatus
                    db.sp_Receiving_UPD_ManagerApprove(ReceivingModule._defaultCrop, XRequestNo, XRcNo, True, _username)
                    MessageBox.Show("ทำการลบเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    XRequestType = RequestTypeComboBox.Text
                    If ApproveStatusComboBox.SelectedIndex = 0 Then
                        XApproveStatus = False
                    ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                        XApproveStatus = True
                    End If
                    Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, ReceivingModule._defaultCrop, XRequestType, XApproveStatus)
                ElseIf result = DialogResult.No Then
                    Me.Dispose()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    
End Class