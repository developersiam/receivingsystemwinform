﻿Public Class FrmChangeUser
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmChangeUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ReceivingDataSet.security' table. You can move, or remove it, as needed.
        Try
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub UsernameTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles UsernameTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                Dim XUsernameApprove As String
                XUsernameApprove = Trim(UsernameTextBox.Text)

                'Get baleBarcodeRow detail 
                Dim baleBarcodeRow As ReceivingDataSet.securityRow
                Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
                baleBarcodeRow = Me.ReceivingDataSet.security.FindByuname(XUsernameApprove)
                If baleBarcodeRow Is Nothing Then
                    MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    UsernameTextBox.Text = ""
                    UsernameTextBox.Focus()
                    Return
                ElseIf Not baleBarcodeRow Is Nothing Then
                    If baleBarcodeRow.uname = XUsernameApprove And baleBarcodeRow.receiving = True Then
                        _username = XUsernameApprove
                        FrmReceivingWeight.UsernameLabel.Text = _username
                        FrmCallReceivingNo.UsernameMetroLabel.Text = _username
                        FrmMenu.UserButton.Text = _username
                        FrmLogIn.UsernameTextBox.Text = _username
                        Me.Dispose()
                    Else
                        MessageBox.Show("Username ไม่ถูกต้องหรือไม่มีสิทธิ์ในการใช้งานในการรับเข้าใบยา , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


End Class