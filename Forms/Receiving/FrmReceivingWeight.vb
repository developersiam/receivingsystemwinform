﻿Imports System.IO.Ports
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Threading
Imports FastMember

Public Class FrmReceivingWeight
    Inherits MetroFramework.Forms.MetroForm

    Dim db As New ReceivingDataClassesDataContext

    Public _matrc As sp_Receiving_SEL_MatRC_Result
    Public _matSubtypeCompany As sp_Receiving_SEL_MatSubtypeCompany_Result
    Public _subType As String
    Public _transportationDocument As sp_GetTransportationDocumentByTransportationCode_Result
    Public _LTLBaleBarcodeFromLoadBale As String
    Public _LTLRemarkReasonResult As String
    Public _LTLResultType As Boolean
    Dim _LTLCartonStatus As Boolean
    Dim _LTLCartonCount As Integer

    Dim _start As Boolean
    Dim _company As sp_Receiving_SEL_Company_Result
    Dim _companies As List(Of sp_Receiving_SEL_Company_Result)
    Dim _suppliers As List(Of supplier)
    Dim _farmers As List(Of sp_Receiving_SEL_Farmer_Result)
    Dim _greenGrades As List(Of sp_Receiving_SEL_Green_Result)
    Dim _classifyGrades As List(Of classify)
    Dim _matList As List(Of sp_Receiving_SEL_MatByRcNo_Result)
    Dim _matSelected As sp_Receiving_SEL_MatByRcNo_Result
    Dim _batchNo As String
    Dim _cpa_group As String
    Dim _baleType As Boolean
    Dim _batchGroupLTL As Integer
    Dim _buyerList As List(Of sp_Receiving_SEL_BuyerClassifier_Result)
    Dim _classifierList As List(Of sp_Receiving_SEL_BuyerClassifier_Result)
    Dim _unfinishBatchNo As List(Of UnfinishBatchNo)

    Dim buffer As String
    Dim readText(2) As String
    'Delagate เพื่อใช้ในการชี้ตำแหน่งของ Function หรือ Sub
    Private Delegate Sub UpdateTextBox(ByVal recvData As String)
    Private Shared Function Num(ByVal value As String) As Integer
        Dim returnVal As String = String.Empty
        Dim collection As MatchCollection = Regex.Matches(value, "\d+")
        For Each m As Match In collection
            returnVal += m.ToString()
        Next
        Return Convert.ToInt32(returnVal)
    End Function

    Private Function ExtractNumberFromString(str As String) As String
        Dim myChars() As Char = str.ToCharArray()
        Dim result As String = ""
        For Each ch As Char In myChars
            If Char.IsDigit(ch) Then
                result = result + ch
            End If
        Next
        Return result
    End Function

    'Sub สร้างไว้เพื่อใช้ในการ update textbox ที่ต้องการ
    Private Sub UpdateTextBoxHandler(ByVal readInfo As String)
        Try
            'May 4 , 2018 ยา Burley เปลี่ยนหักกระสอบ จาก 2.3 เป็น 2.0 ให้เริ่มมีผลกับ Receiving ตั้งแต่วันจันทร์ ที่ 7 พ.ค. 2018

            'METTLER    TOLEDO IND221   9600,8,N,1   => Line 3       {Receiving,Regrade}
            'COMMANDO   TIGER           9600,8,N,1   => Line 4       {Receiving}
            'MELLTER    SPIDER2         9600,8,E,1   => Line 1,2     {Receiving}

            'Close bale หมายถึง ยาที่ห่อกระสอบ
            'Open bale หมายถึง ยาฟ่อน(ไม่ห่อกระสอบ)

            'Hessian weight 2.2 Kg per bale. STEC set 2.3 Kg per bale. 
            'Open bale string weight 0.1 Kg per bale.
            'Update at 2021-Jan-19

            If RcLinesLabel.Text = "3" Then
                readInfo = Microsoft.VisualBasic.Left(readInfo, 1000)

                Dim splitStr = readInfo.Split(vbCr)

                'Sending message of RS232 example
                'readInfo = ChrW(2) & "+0    209    00" & vbCr & ChrW(2) & "+0    209    00" & vbCr & ChrW(2) & "+0    209    00"

                'ถ้าข้อมูลตัดออกมาด้วย vbCr แล้วได้ array น้อยกว่า 2 ให้จบการทำงานของฟังก์ชัน
                If splitStr.Count < 2 Then
                    Return
                End If

                'เอา array(0) ที่ split มาแบ่งออกเป็น array ย่อยอีกครั้งโดยใช้ " " เป็นตัวแบ่ง array
                Dim splitter = Microsoft.VisualBasic.Right(splitStr(0), 15).Split(" ")

                'สร้างตัวแปล r เอาไว้เก็บข้อมูลประกอบด้วย
                'r(0) = prefix คือ 2 ตัวแรกของชุดข้อมูลได้แก่ +0,+1,+2,+3 โดยที่ +3 คือข้อมูลที่ติดลบ
                'r(1) = weight คือ น้ำหนักที่จะแสดงบนหน้าจอ
                'r(2) = tare คือ น้ำหนักที่เกิดจากการ tare

                'รูปแบบข้อมูลที่จะนำมาแบ่งออกเป็น Array ย่อยคือ
                'prefix     weight      tare
                '+0         210         210

                Dim r(2) As String
                Dim index = 0

                For Each i In splitter
                    If i.Length > 1 Then
                        r(index) = i
                        index = index + 1
                    End If
                Next

                'กรณีชุดข้อมูลไม่ครบ 3 ชุด ให้จบการทำงานของฟังก์ชัน
                If r(2) = Nothing Then
                    Return
                End If

                Dim weight
                Dim tare

                weight = If(Val(r(1)) > 0, Val(r(1)) / 10, 0)
                tare = If(Val(r(2)) > 0, Val(r(2)) / 10, 0)

                If r(0) = "+3" Then
                    weight = weight * -1
                End If

                leftValue.Text = weight
                rightValue.Text = tare

                If BaleTypeComboBox.SelectedIndex = 1 Then
                    weight = weight - 2.3
                Else
                    If _matrc.type = "FC" Then
                        weight = weight - 0.1
                    End If
                End If

                WeightFromDigitalScaleTextBox.Text = weight

            ElseIf RcLinesLabel.Text = "4" Then
                'COMMANDO
                readInfo = Microsoft.VisualBasic.Left(readInfo, 11).Replace(" ", "")

                readText = readInfo.Split(New Char() {"."c})

                If readText.Length < 2 Then
                    Return
                End If

                leftValue.Text = Microsoft.VisualBasic.Right(readText(0), 2)
                rightValue.Text = Microsoft.VisualBasic.Left(readText(1), 1)

                readInfo = ExtractNumberFromString(leftValue.Text) + "." + rightValue.Text

                If BaleTypeComboBox.SelectedIndex = 1 Then 'close bale
                    If _matrc.type = "BU" Then
                        readInfo = Val(readInfo) - 2.3
                    Else
                        readInfo = Val(readInfo) - 2.3
                    End If
                Else
                    If _matrc.type = "FC" Then
                        readInfo = Val(readInfo) - 0.1
                    End If
                End If

                WeightFromDigitalScaleTextBox.Text = Val(readInfo)
                Return

            ElseIf RcLinesLabel.Text = "5" Then
                'Azano
                Dim value = readInfo.Split({ControlChars.CrLf}, StringSplitOptions.None)
                If value.Length < 1 Then
                    WeightFromDigitalScaleTextBox.Text = Val("999.9")
                    Return
                End If

                Dim array = value(0).Split(" ")
                Dim result = array(array.Length - 1).Split(",")
                Dim finalResult = result(0)
                leftValue.Text = finalResult
                rightValue.Text = finalResult

                If BaleTypeComboBox.SelectedIndex = 1 Then 'close
                    If _matrc.type = "BU" Then
                        finalResult = val(finalResult) - 2.3
                    Else
                        finalResult = val(finalResult) - 2.3
                    End If
                Else 'open
                    If _matrc.type = "FC" Then
                        finalResult = val(finalResult) - 0.1
                    End If
                End If

                WeightFromDigitalScaleTextBox.Text = val(finalResult)

            ElseIf (RcLinesLabel.Text = "1") Or (RcLinesLabel.Text = "2") Then
                '--MELLTER TOREDO SPIDER2
                readInfo = Microsoft.VisualBasic.Left(readInfo, 15)

                leftValue.Text = Microsoft.VisualBasic.Left(readInfo, 8)
                rightValue.Text = Val(Microsoft.VisualBasic.Right(readInfo, 7))

                If BaleTypeComboBox.SelectedIndex = 1 Then 'close
                    readInfo = Val(rightValue.Text)

                    'หากเป็นยา Burley และไม่ใช่ยา TTM จะหักกระสอบ 2.0 
                    If _matrc.type = "BU" Then
                        readInfo = Val(readInfo) - 2.3
                    Else
                        readInfo = Val(readInfo) - 2.3
                    End If

                Else 'open
                    If _matrc.type = "FC" Then
                        readInfo = Val(rightValue.Text) - 0.1
                    End If
                End If

                WeightFromDigitalScaleTextBox.Text = Val(readInfo)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ReceivingDataGridDataBinding() ' Sub แสดงข้อมูล countOFbaleBarcode, SumOfBuyingweight,SumOfReceivingweight
        Try
            _matList = _businessLayerService.ReceivingBL().GetMatByRcno(_matrc.rcno)
            TotalBaleLabel.Text = _matList.Count

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_matList)
            dt.Load(reader)

            ReceivingDataGrid.DataSource = dt

            Dim totalBuyingWeight As Decimal = _matList.Sum(Function(x) x.weightbuy)
            Dim totalReceivingWeight As Decimal = _matList.Sum(Function(x) x.weight)
            TotalBuyingWeightLabel.Text = totalBuyingWeight.ToString("N2")
            TotalReceivedWeightLabel.Text = totalReceivingWeight.ToString("N2")
            DiffWeightLabel.Text = (totalReceivingWeight - totalBuyingWeight).ToString("N2")

            'ถ้าใน Rcno นี้เป็นการ replacebale ให้ auto เช็คถูกและไม่ให้แก้อีก 
            If (_matList.Where(Function(x) x.ReplaceBales = True).Count > 0) Then
                ReplaceBalesCheckbox.Checked = True
                ReplaceBalesCheckbox.Enabled = False
            End If

            GetTransportationRemaining()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClassifyAndGreenGradeDataBinding()
        Try
            If _company.name = "LTL" Then
                _classifyGrades = _businessLayerService.ClassifyBL().GetLTLClassifyByType(_matrc.type)
            Else
                _classifyGrades = _businessLayerService.ClassifyBL().GetClassifyByType(_matrc.type)
            End If

            Dim classifyGradeData As New AutoCompleteStringCollection()
            For Each item As classify In _classifyGrades
                classifyGradeData.Add(item.classify1)
            Next

            ClassifyGradeComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource
            ClassifyGradeComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            ClassifyGradeComboBox.AutoCompleteCustomSource = classifyGradeData

            _greenGrades = _businessLayerService.GreenBL().GetGreens(_defaultCrop, _matrc.type)

            Dim greenGradeData As New AutoCompleteStringCollection()
            For Each item As sp_Receiving_SEL_Green_Result In _greenGrades
                greenGradeData.Add(item.green)
            Next

            GreenGradeComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource
            GreenGradeComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            GreenGradeComboBox.AutoCompleteCustomSource = greenGradeData
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub IsNTRMInspection(baleBarcode As String)
        'เพิ่มเช็ค ถ้ามีข้อมูล NTRM ที่ buying หรือ stecdbms ให้โชว์ตัวหนังสือสีแดง user สามารถตรวจสอบได้จากการกดปุ่ม NTRM
        If _businessLayerService.ReceivingBL().GetNTRMInspectionByBaleBarcode(baleBarcode).Count > 0 Then
            NTRMFromBuyingAlertLabel.Visible = True
        Else
            NTRMFromBuyingAlertLabel.Visible = False
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            RefreshReceivingDocument()
            RefreshForm()
            CallDigitalScaleSetting()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshReceivingDocument()
        Try
            _matSubtypeCompany = _businessLayerService.ReceivingDocumentBL().GetMatSupTypeCompanyByRcno(_matrc.rcno)
            _companies = _businessLayerService.CompanyBL().GetCompanies()
            _company = _companies.Where(Function(x) x.name = _matSubtypeCompany.Company).SingleOrDefault()

            CompanyLabel.Text = _company.name
            UsernameLabel.Text = _username

            If _company.name = "TTM" Then
                ManualInputCheckBox.Checked = True
            End If

            If _company.name = "LTL" Then
                SupplierComboBox.Text = "LTL"
                SupplierComboBox.Enabled = False
            End If

            _suppliers = _businessLayerService.SupplierBL().GetSuppliers() '.OrderBy(Function(x) x.code)
            _buyerList = _businessLayerService.ReceivingDocumentBL().GetClassifiers()
            _classifierList = _businessLayerService.ReceivingDocumentBL().GetClassifiers()

            BuyerComboBox.DataSource = _buyerList
            ClassifierComboBox.DataSource = _classifierList

            If _matrc.InvoiceNo <> "" Then
                'กรณียา LTL ให้แสดงข้อมูลจำนวนห่อยาทั้งหมดใน LTL Invoice
                TotalBaleOnTruckLabel.Text = _businessLayerService.ReceivingDocumentBL().GetBalesByInvoiceNo(_matrc.InvoiceNo).Count()
            ElseIf _matrc.TransportationDocumentCode <> "" Then
                'กรณี STEC BU ให้แสดงจำนวนห่อยาทั้งหมดในการขนส่งครั้งนั้น (TransportationDocument)
                TotalBaleOnTruckLabel.Text = _businessLayerService.BurleyBuyingSystemBL().GetTransportationDocumentByID(_matrc.TransportationDocumentCode).TotalBale
            Else
                TransportationDocumentCodeLabel.Text = ""
            End If

            BaleTypeComboBox.SelectedIndex = 1
            BaleTypeComboBox.BackColor = Color.LimeGreen
            BaleTypeComboBox.ForeColor = Color.White

            'Read text from a text file 
            'สำหรับ Set ค่า Receiving line  เพื่อจัดกลุ่ม BatchNo
            Using sr As New StreamReader("C:\Program Files\Common Files\RCLines\RCLines.txt")
                Dim line As String
                line = sr.ReadToEnd()
                RcLinesLabel.Text = line
            End Using

            If _matrc.type = "BU" Then
                ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
            End If

            rcnoLabel.Text = _matrc.rcno
            CropLabel.Text = _matrc.crop
            TypeLabel.Text = _matrc.type
            CompanyLabel.Text = _company.name '_matSubtypeCompany.Company
            RcfromLabel.Text = _matrc.rcfrom
            PlaceLabel.Text = _matrc.place
            ClassifierComboBox.Text = _matrc.classifier
            BuyerComboBox.Text = _matrc.buyer
            TransportationDocumentCodeLabel.Text = _matrc.TransportationDocumentCode
            InvoiceNoLabel.Text = _matrc.InvoiceNo
            TrucknoLabel.Text = _matrc.truckno

            If _matSubtypeCompany.Company = "NPI" Then
                NPIFarmerCodeTextBox.Enabled = True
            End If

            Dim supplierData As New AutoCompleteStringCollection()
            For Each item As supplier In _suppliers
                supplierData.Add(item.code)
            Next

            SupplierComboBox.AutoCompleteSource = AutoCompleteSource.CustomSource
            SupplierComboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            SupplierComboBox.AutoCompleteCustomSource = supplierData

            '_farmers = _businessLayerService.ReceivingBL().GetFcvNpiFarmers()
            _farmers = _businessLayerService.ReceivingBL().GetFcvNpiFarmersV2(_matrc.crop)
            Dim fcvNpiFarmerData As New AutoCompleteStringCollection()

            'For Each item As NPI_Farmer In _farmers
            '    fcvNpiFarmerData.Add(item.FarmerCode)
            'Next

            For Each item As sp_Receiving_SEL_Farmer_Result In _farmers
                fcvNpiFarmerData.Add(item.FarmerCode)
            Next

            NPIFarmerCodeTextBox.AutoCompleteSource = AutoCompleteSource.CustomSource
            NPIFarmerCodeTextBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            NPIFarmerCodeTextBox.AutoCompleteCustomSource = fcvNpiFarmerData

            ClassifyAndGreenGradeDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshForm()
        Try
            BaleBarcodeTextBox.Text = ""
            BaleNumberTextBox.Text = ""
            BaleNumberTextBox.Enabled = False
            BaleNumberTextBox.ReadOnly = True

            NPIFarmerCodeTextBox.Text = ""
            SupplierComboBox.Text = ""
            GreenGradeComboBox.Text = ""
            ClassifyGradeComboBox.Text = ""

            NPIFarmerCodeTextBox.Enabled = False
            SupplierComboBox.Enabled = False
            GreenGradeComboBox.Enabled = False
            ClassifyGradeComboBox.Enabled = False

            BuyingWeightTextBox.Text = ""
            ReceivingWeightTextBox.Text = ""

            BuyingWeightTextBox.Enabled = False
            'ReceivingWeightTextBox.Enabled = False

            BuyingWeightTextBox.ReadOnly = True
            'ReceivingWeightTextBox.ReadOnly = True

            DigitalWeightPanel.Visible = False

            If ChkFromLTLCarton.Checked = False Then
                _LTLRemarkReasonResult = Nothing
                _LTLResultType = True
            End If

            Save.Visible = False
            SaveMetroLabel.Visible = False

            _LTLBaleBarcodeFromLoadBale = Nothing

            ReceivingDataGridDataBinding()
            BaleBarcodeTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearForm()
        BaleBarcodeTextBox.Text = ""
        BaleNumberTextBox.Text = ""
        SupplierComboBox.SelectedIndex = -1
        GreenGradeComboBox.SelectedIndex = -1
        ClassifyGradeComboBox.SelectedIndex = -1
        BuyingWeightTextBox.Text = ""
        ReceivingWeightTextBox.Text = ""
        FarmerLabel.Text = ""

        Save.Visible = False
        SaveMetroLabel.Visible = False

        _LTLRemarkReasonResult = Nothing
        _LTLBaleBarcodeFromLoadBale = Nothing
        _LTLCartonCount = 0
        _LTLCartonStatus = False
        _LTLResultType = True
        ChkFromLTLCarton.Checked = False

        BaleNumberTextBox.Enabled = True
        GreenGradeComboBox.Enabled = True
        ClassifyGradeComboBox.Enabled = True
        BuyingWeightTextBox.Enabled = True

        BaleBarcodeTextBox.Focus()
    End Sub

    Private Sub CallDigitalScaleSetting()
        Try
            'ถ้ามีการเซ็ตเครื่องชั่งไว้ก่อนหน้านี้แล้วให้ดึงค่าที่เซ็ตไว้มาใช้ได้เลย()
            If My.Settings.ComPort <> "" Then
                '16-01-2017


                SerialPort1.Close()
                SerialPort1.Dispose()


                ' Set the read/write timeouts
                SerialPort1.ReadTimeout = 500
                SerialPort1.WriteTimeout = 500
                'thread sleep
                Thread.Sleep(20)
                'dicardinbuffer
                If (SerialPort1.IsOpen = True) Then SerialPort1.DiscardInBuffer()
                '---------------

                If SerialPort1.IsOpen Then SerialPort1.Close()

                'Get a list of serial port names.
                Dim ports As String() = IO.Ports.SerialPort.GetPortNames()
                If ports.Count < 1 Then
                    MessageBox.Show("The COM Port on your computer is not found! Please install COM port on your computer" & vbNewLine &
                                "ไม่พบ COM Port บนเครื่องคอมพิวเตอร์ของท่าน โปรดติดตั้ง COM Port ลงบนเครื่องคอมพิวเตอร์นี้ก่อน", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    DigitalWeightCheckBox.Checked = False
                    Return
                End If

                With SerialPort1
                    .PortName = My.Settings.ComPort
                    .BaudRate = My.Settings.BourdRate
                    .Parity = IIf(My.Settings.Parity = "None", System.IO.Ports.Parity.None, IIf(My.Settings.Parity = "Odd", System.IO.Ports.Parity.Odd, IIf(My.Settings.Parity = "Even", System.IO.Ports.Parity.Even, IIf(My.Settings.Parity = "Mark", System.IO.Ports.Parity.Mark, System.IO.Ports.Parity.Space))))
                    .DataBits = My.Settings.Databit
                    .StopBits = IIf(My.Settings.StopBits = "None", System.IO.Ports.StopBits.None, IIf(My.Settings.StopBits = "One", System.IO.Ports.StopBits.One, IIf(My.Settings.StopBits = "Two", System.IO.Ports.StopBits.Two, System.IO.Ports.StopBits.OnePointFive)))
                    .Handshake = Handshake.None
                    .RtsEnable = True
                End With
                SerialPort1.Open()
                DigitalWeightCheckBox.Checked = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub GetTransportationRemaining()
        Try
            If _matrc.type = "BU" And _company.name = "STEC" Then
                RemainingTruckReceivingLabel.Text = _businessLayerService.ReceivingBL() _
                    .GetRemainingBurleyReceivingBaleFromTransportationCode(_matrc.TransportationDocumentCode, _matrc.crop, _matrc.rcno).Count.ToString("N0")
            ElseIf _company.name = "LTL" And _matrc.InvoiceNo <> "" Then
                RemainingTruckReceivingLabel.Text = _businessLayerService.ReceivingBL().GetRemainingLTLReceivingBaleFromInvoiceNo(_matrc.InvoiceNo).Count.ToString("N0")
            Else
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub DigitalWeightCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles DigitalWeightCheckBox.CheckedChanged
        Try
            If DigitalWeightCheckBox.Checked = True Then
                Dim ports As String() = IO.Ports.SerialPort.GetPortNames()
                If ports.Count < 1 Then
                    MessageBox.Show("The COM port on your computer is not found! Please install COM port on your computer" & vbNewLine &
                                    "ไม่พบ COM Port บนเครื่องคอมพิวเตอร์ของท่าน โปรดติดตั้ง COM Port ลงบนเครื่องคอมพิวเตอร์นี้ก่อน", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    DigitalWeightCheckBox.Checked = False
                    Return
                End If
                'ถ้ามีการ config เครื่องชั่งไว้ก่อนหน้านี้แล้วให้เปิด Serial port
                If Not My.Settings.ComPort Is Nothing Then
                    If SerialPort1.IsOpen = False Then
                        SerialPort1.Open()
                    End If
                Else
                    Dim setupDigitalScale As New FrmSetDigitalScaleParameter
                    setupDigitalScale.ShowDialog()
                End If
                Save.Visible = False
                SaveMetroLabel.Visible = False
            Else
                SerialPort1.Close()
                SerialPort1.Dispose()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BaleBarcodeTextBox_Click(sender As Object, e As EventArgs) Handles BaleBarcodeTextBox.Click
        Try
            BaleBarcodeTextBox.Text = ""
            BaleNumberTextBox.Text = ""
            GreenGradeComboBox.Text = ""
            ClassifyGradeComboBox.Text = ""
            BuyingWeightTextBox.Text = ""
            ReceivingWeightTextBox.Text = ""
            FarmerLabel.Text = ""
            LTLRemarkResultLabel.Visible = False
            LTLRemarkResultLabel.Text = ""

            If ChkFromLTLCarton.Checked = False Then
                _LTLRemarkReasonResult = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub GetFreeOrReplaceBale()
        Try
            'เช็คว่าจะต้องใช้ป้ายสีน้ำตาลเท่านั้น 05
            If Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text), 2) <> "05" Then
                MessageBox.Show("ยาฟรีหรือป้าย Replace ต้องใช้ป้ายสีน้ำตาลเท่านั้น", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            BaleNumberTextBox.Text = ""
            BaleNumberTextBox.Enabled = True
            SupplierComboBox.SelectedIndex = -1
            SupplierComboBox.Enabled = True
            GreenGradeComboBox.SelectedIndex = -1
            GreenGradeComboBox.Enabled = True
            ClassifyGradeComboBox.SelectedIndex = -1
            BuyingWeightTextBox.Text = ""
            ReceivingWeightTextBox.Text = ""
            FarmerLabel.Text = ""
            BaleNumberTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Function GetReceivedBale(baleBarcode As String) As sp_Receiving_SEL_Mat_Result
        Return _businessLayerService.ReceivingBL().GetMatByCrop(_defaultCrop, baleBarcode)
    End Function

    'Private Sub GetDataFromFCVBuyingSystem(baleBarcode As String)
    '    Try
    '        '2018-02-05 ถ้า supplier= NPI NPIT NPIS ไม่ต้องเช็คใน buyingsystem
    '        'และ Baleno จะget ค่า baleno+1 ในแต่ละ supplier
    '        If (ManualNPI.Checked = True) Then
    '            If Len(BaleBarcodeTextBox.Text) <> 17 Then
    '                MessageBox.Show("ระบบอนุญาตให้บันทึกข้อมูลยา FCV ได้เฉพาะป้ายบาร์โค้ดที่มีตัวเลขจำนวน 17 หลักเท่านั้น !!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If (Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text), 2) <> "06" And Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text), 2) <> "05") Then
    '                MessageBox.Show("ระบบอนุญาตให้บันทึกข้อมูลย FCV ได้เฉพาะป้ายสีส้ม(รหัส06) และป้ายสีน้ำตาล(รหัส05) เท่านั้น !!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If (SupplierComboBox.SelectedItem = -1) Then
    '                MessageBox.Show("โปรดระบุ supplier ที่ต้องการและกดปุ่ม Enter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                SupplierComboBox.Enabled = True
    '                SupplierComboBox.Focus()
    '                Return
    '            End If

    '            BaleNumberTextBox.Text = ""
    '            FarmerLabel.Text = ""

    '        Else 'ถ้าsuppliercode ไม่ใช่ NPI NPIT NPIS ให้เช็คจาก buyingsystem 
    '            'Get balebarcode detail from FCbuying database
    '            Dim fcvBuying = _businessLayerService.FCVBuyingBL().GetBaleBarcodeDetailByBarcode(baleBarcode)
    '            If fcvBuying Is Nothing Then 'ไมพบข้อมูลหมายเลขบาร์โค้ดจาก FCbuyingSystem
    '                MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม FCV Buying  กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If fcvBuying.FinalGrade Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล FinalGrade
    '                MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If fcvBuying.FinalRejectReason IsNot Nothing Then 'ถ้ามีข้อมูลแต่เป็นเกรด - 
    '                MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม FC Buying เนื่องจาก " & fcvBuying.FinalRejectReason, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If fcvBuying.ThirdFinishStatus = False Then 'ถ้ามีข้อมูลแล้วแต่ยังไม่ได้ Approve จากFC buying
    '                MessageBox.Show("ห่อยานี้ยังไม่ได้รับการApproveจากBuyer กรุณาตรวจสอบจากโปรแกรม FC Buying", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            BaleNumberTextBox.Text = fcvBuying.BaleNumber
    '            SupplierComboBox.Text = fcvBuying.CurerCode
    '            ClassifyGradeComboBox.Text = fcvBuying.FinalGrade
    '            ClassifyGradeComboBox.Text = fcvBuying.FinalGrade
    '            BuyingWeightTextBox.Text = fcvBuying.Weight
    '            If DigitalWeightCheckBox.Checked = True Then
    '                ReceivingWeightTextBox.Text = ""
    '            ElseIf DigitalWeightCheckBox.Checked = False Then
    '                ReceivingWeightTextBox.Text = fcvBuying.Weight
    '            End If
    '            FarmerLabel.Text = ""
    '            ClassifyGradeComboBox.Focus()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '    End Try
    'End Sub
    'Private Sub GetDataFromLoadBaleFunction(baleBarcode As String)
    '    Try
    '        'ถ้าไม่ใช่ป้ายสีน้ำตาลให้ค้นหาจาก LoadBales
    '        'แต่ถ้าเป็นป้ายสีน้ำตาลให้คีย์เองทั้งหมด และอนุญาตให้ Save ได้
    '        If Microsoft.VisualBasic.Left(Trim(baleBarcode), 2) <> "05" Then
    '            'Get balebarcode detail from LoadBales
    '            Dim model = _businessLayerService.ReceivingBL().GetLTLLoadBaleByBaleBarcode(baleBarcode)
    '            If model Is Nothing Then 'ไมพบข้อมูลหมายเลขบาร์โค้ดจาก LoadBales
    '                MessageBox.Show("ห่อยานี้ยังไม่ได้ Load ลงระบบฐานข้อมูล กรุณาตรวจสอบข้อมูลกับเจ้าหน้าที่ checker", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If model.InvoiceNo <> _matrc.InvoiceNo Then
    '                MessageBox.Show("ห่อยานี้ Packlist Invoice no.ไม่ตรงกัน ไม่อนุญาตให้บันทึกข้อมูล หากต้องการบันทึกจะต้องใช้ป้ายบาร์โค้ดสีน้ำตาลและติดต่อ Checker ให้ทำการปรับปรุงข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If model.Supplier Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล suppplier
    '                MessageBox.Show("ห่อยานี้ไม่มีข้อมูล Supplier", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            If model.WeightBuy Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูลน้ำหนัก
    '                MessageBox.Show("ห่อยานี้ไม่มีข้อมูลน้ำหนัก", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '                Return
    '            End If

    '            BaleNumberTextBox.Text = model.BaleNo
    '            SupplierComboBox.Text = model.Supplier
    '            GreenGradeComboBox.Text = model.Green
    '            BuyingWeightTextBox.Text = model.WeightBuy

    '            If DigitalWeightCheckBox.Checked = True Then
    '                ReceivingWeightTextBox.Text = ""
    '            ElseIf DigitalWeightCheckBox.Checked = False Then
    '                ReceivingWeightTextBox.Text = model.WeightBuy
    '            End If

    '            FarmerLabel.Text = ""
    '            ClassifyGradeComboBox.Focus()
    '        Else
    '            BaleNumberTextBox.Text = ""
    '            BaleNumberTextBox.Enabled = True
    '            SupplierComboBox.Text = CompanyLabel.Text
    '            SupplierComboBox.Enabled = True
    '            GreenGradeComboBox.SelectedIndex = -1
    '            GreenGradeComboBox.Enabled = True
    '            ClassifyGradeComboBox.SelectedIndex = -1
    '            BuyingWeightTextBox.Text = ""
    '            ReceivingWeightTextBox.Text = ""
    '            FarmerLabel.Text = ""

    '            'Show LTL remark popup
    '            Dim window As New FrmLTLRemarkBrownTag()
    '            window.Show()

    '            '***************
    '            '***************
    '            '***************
    '            '***************
    '            ' อาจจะต้องมีการนำรหัส BaleBarcode ของป้ายสีน้ำตาลมาใส่ไว้ใน BaleBarcodeTextBox ในหน้า receiving ด้วยก็เป็นได้
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '    End Try
    'End Sub
    'Private Sub GetDataFromTTM2(baleBarcode As String)
    '    Try
    '        BaleNumberTextBox.Enabled = True
    '        SupplierComboBox.Enabled = True
    '        GreenGradeComboBox.Enabled = True
    '        ClassifyGradeComboBox.Enabled = True
    '        BuyingWeightTextBox.Enabled = True
    '        ReceivingWeightTextBox.Enabled = True
    '        FarmerLabel.Enabled = True

    '        BaleNumberTextBox.Text = ""
    '        SupplierComboBox.Text = ""
    '        GreenGradeComboBox.SelectedIndex = -1
    '        ClassifyGradeComboBox.SelectedIndex = -1
    '        BuyingWeightTextBox.Text = ""
    '        ReceivingWeightTextBox.Text = ""
    '        FarmerLabel.Text = ""
    '        BaleNumberTextBox.Focus()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '    End Try
    'End Sub
    'Private Sub GetDataFromBuBuyingSystem(baleBarcode As String)
    '    Try
    '        'Get  balebarcode detail from BuyingSystem database
    '        Dim model = _businessLayerService.BurleyBuyingSystemBL().GetBuyingDetailByBaleBarcode(baleBarcode)
    '        If model Is Nothing Then 'ไมพบข้อมูลหมายเลขบาร์โค้ดจาก BU BuyingSystem
    '            MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม BU Buying  กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If
    '        If model.Grade Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล Grade
    '            MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If
    '        If model.RejectReason IsNot Nothing Then 'ถ้ามีข้อมูลแต่เป็นเกรด - 
    '            MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม BU Buying เกรดถูกระบุเป็น " & model.RejectReason & " กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If
    '        If model.Weight Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ยังไม่ได้ชั่งน้ำหนัก
    '            MessageBox.Show("ห่อยานี้ไม่ได้บันทึกน้ำหนักจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If
    '        If model.TransportationDocumentCode Is Nothing Then
    '            MessageBox.Show("ห่อยานี้ไม่ได้บันทึกการโหลดTruckจากโปรแกรม BUBuying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If
    '        If model.TransportationDocumentCode <> TransportationDocumentCodeLabel.Text Then
    '            MessageBox.Show("ห่อยานี้Transportation document no. คือ " & model.TransportationDocumentCode & "ซึ่งไม่ตรงกับที่ระบุใน Receiving numberนี้ กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If
    '        If model.IsFinish = False Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล FinishedDate
    '            MessageBox.Show("ห่อยานี้ยังไม่ได้ปิดการขาย (IsFinish is null) กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '            Return
    '        End If

    '        BaleNumberTextBox.Text = model.Baleno
    '        SupplierComboBox.Text = _businessLayerService.SupplierBL().GetSupplierFromBUBuyingSystemByBaleBarcode(baleBarcode)
    '        ClassifyGradeComboBox.Text = model.Grade
    '        ClassifyGradeComboBox.Text = model.Grade
    '        BuyingWeightTextBox.Text = model.Weight

    '        If DigitalWeightCheckBox.Checked = True Then
    '            ReceivingWeightTextBox.Text = ""
    '        ElseIf DigitalWeightCheckBox.Checked = False Then
    '            ReceivingWeightTextBox.Text = model.Weight
    '        End If

    '        CountMoistureLabel.Text = _businessLayerService.BurleyBuyingSystemBL().GetMoistureSummaryByFarmer(model.FarmerCode)
    '        FarmerLabel.Text = model.FarmerCode & " " & model.FirstName & " " & model.LastName
    '        ClassifyGradeComboBox.Focus()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    '    End Try
    'End Sub

    Private Sub BindingControlFromLTLLoadBale(model As sp_Receiving_SEL_LoadBaleByBaleBarcode_Result)
        Try
            BaleNumberTextBox.Text = model.BaleNo
            SupplierComboBox.Text = model.Supplier
            GreenGradeComboBox.Text = model.Green
            BuyingWeightTextBox.Text = Convert.ToDecimal(model.WeightBuy).ToString("N2")

            BaleNumberTextBox.Enabled = False
            SupplierComboBox.Enabled = False
            GreenGradeComboBox.Enabled = False
            BuyingWeightTextBox.ReadOnly = True
            ClassifyGradeComboBox.Enabled = True

            If DigitalWeightCheckBox.Checked = True Then
                ReceivingWeightTextBox.Text = ""
            ElseIf DigitalWeightCheckBox.Checked = False Then
                ReceivingWeightTextBox.Text = Convert.ToDecimal(model.WeightBuy).ToString("N2")
            End If

            ClassifyGradeComboBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BaleBarcodeTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles BaleBarcodeTextBox.KeyDown
        Try
            If e.KeyCode <> Keys.Enter Then
                Return
            End If

            If BaleBarcodeTextBox.Text = "" Then
                MessageBox.Show("โปรดสแกนหรือกรอกข้อมูลหมายเลขบาร์โค้ต", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                BaleBarcodeTextBox.Focus()
                Return
            End If

            Dim baleBarcode = BaleBarcodeTextBox.Text.Replace(" ", "")

            If GetReceivedBale(baleBarcode) IsNot Nothing Then
                MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้เนื่องจากห่อยานี้รับเข้าแล้วใน Rc no. " & _matrc.rcno & "  กรุณาตรวจสอบข้อมูล", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning)
                BaleBarcodeTextBox.Text = ""
                BaleBarcodeTextBox.Focus()
                Return
            End If

            If _company.name = "LTL" Then

                '******************************************************************
                'LTL receiving.
                '******************************************************************

                SupplierComboBox.Text = "LTL"

                'เช็คว่าใช้ brown tag หรือไม่?
                'ถ้าไม่ใช่ป้ายสีน้ำตาลให้ค้นหาจาก LoadBales
                'แต่ถ้าเป็นป้ายสีน้ำตาลให้คีย์เองทั้งหมด และอนุญาตให้ Save ได้
                If Microsoft.VisualBasic.Left(Trim(baleBarcode), 2) = "05" Then
                    'Show a using brown tag reason popup.
                    'Get the result from that popup (reason and LTL barcode to replace).
                    _LTLBaleBarcodeFromLoadBale = Nothing
                    _LTLRemarkReasonResult = Nothing

                    LTLRemarkResultLabel.Text = _LTLRemarkReasonResult
                    LTLRemarkResultLabel.Visible = True

                    Dim window As New FrmLTLRemarkBrownTag()
                    window._invoiceNo = _matrc.InvoiceNo

                    Dim result = window.ShowDialog()
                    If result = DialogResult.OK Then
                        _LTLBaleBarcodeFromLoadBale = window._LTLBarcode
                        _LTLRemarkReasonResult = window._remarkResult
                        _LTLResultType = window._resultType
                        LTLRemarkResultLabel.Text = _LTLRemarkReasonResult
                        LTLRemarkResultLabel.Visible = True
                    Else
                        MessageBox.Show("กรณีใช้ป้ายสีน้ำตาล (05) ท่านจะต้องระบุสาเหตุของการใช้ป้าย จากนั้นให้กด Save หากต้องการใช้ป้ายสีน้ำตาล กรุณาลองใหม่อีกครั้ง", "การแจ้งเตือน",
                                        MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If

                    'When user close this popup. We can get the result and check the result again.
                    If _LTLBaleBarcodeFromLoadBale IsNot Nothing Then
                        'The result from popup is cannot read a barcode and find barcode from load bale found.
                        Dim model = _businessLayerService.ReceivingBL().GetLTLLoadBaleByBaleBarcode(_LTLBaleBarcodeFromLoadBale)
                        BindingControlFromLTLLoadBale(model)
                        ClassifyGradeComboBox.Focus()
                        Return
                    Else
                        'When the result don't have a bale barcode from Load bale. 
                        'It is mean another reason (not "can not read a barcode"). 
                        BaleNumberTextBox.Text = ""
                        BuyingWeightTextBox.Text = ""
                        ReceivingWeightTextBox.Text = ""

                        SupplierComboBox.Text = "LTL"
                        GreenGradeComboBox.Text = ""
                        ClassifyGradeComboBox.Text = ""

                        BaleNumberTextBox.Enabled = True
                        BaleNumberTextBox.ReadOnly = False
                        SupplierComboBox.Enabled = True
                        GreenGradeComboBox.Enabled = True
                        ClassifyGradeComboBox.Enabled = True
                        ReceivingWeightTextBox.Enabled = True
                        BuyingWeightTextBox.Enabled = True
                        BuyingWeightTextBox.ReadOnly = False

                        BaleNumberTextBox.Focus()
                        Return
                    End If
                Else
                    'In case not use brown tag.
                    'Get data from load bale and binding to form controls.
                    Dim model = _businessLayerService.ReceivingBL().GetLTLLoadBaleByBaleBarcode(baleBarcode)
                    If model Is Nothing Then
                        MessageBox.Show("ห่อยานี้ยังไม่ได้ Load ลงระบบฐานข้อมูล กรุณาตรวจสอบข้อมูลกับเจ้าหน้าที่ checker", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    Else
                        'Find data from load bale found.
                        If model.InvoiceNo <> _matrc.InvoiceNo Then
                            MessageBox.Show("ห่อยานี้ Packlist Invoice no.ไม่ตรงกัน ไม่อนุญาตให้บันทึกข้อมูล หากต้องการบันทึกจะต้องใช้ป้ายบาร์โค้ดสีน้ำตาลและติดต่อ Checker ให้ทำการปรับปรุงข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If
                        If model.Supplier Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล suppplier
                            MessageBox.Show("ห่อยานี้ไม่มีข้อมูล Supplier", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If
                        If model.WeightBuy Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูลน้ำหนัก
                            MessageBox.Show("ห่อยานี้ไม่มีข้อมูลน้ำหนัก", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If
                        BindingControlFromLTLLoadBale(model)
                        FarmerLabel.Text = ""
                        ClassifyGradeComboBox.Focus()
                    End If
                End If
            ElseIf _matrc.type = "FC" And _company.name = "NPI" And _matrc.rcfrom = "Purchasing" Then

                '******************************************************************
                'STEC(NPI) FCV Receiving
                '******************************************************************
                '2020-04-21 มีการซื้อยา H และ L จากคุณอดุลย์สันกำแพงเพื่อมาทำยา CUTRAG
                'ไม่ต้องตรวจสอบข้อมูลจากโปรแกรม Buying ถ้า supplier = SKP, type = FC

                If IsCUTRAGCheckBox.Checked = True Then
                    'Other the receiving type (คีย์เองทั้งหมดนะ) 
                    'กรณี company = NPI, type = FC, rcfrom = Purchasing, IsCUTRAG = true
                    'NPIFarmerCodeTextBox.Enabled = True
                    BaleNumberTextBox.Text = ""
                    BuyingWeightTextBox.Text = ""
                    ReceivingWeightTextBox.Text = ""

                    BaleNumberTextBox.ReadOnly = False
                    BuyingWeightTextBox.ReadOnly = False
                    ReceivingWeightTextBox.ReadOnly = False

                    BaleNumberTextBox.Enabled = True
                    BuyingWeightTextBox.Enabled = True
                    ReceivingWeightTextBox.Enabled = True

                    'SupplierComboBox.Text = ""
                    GreenGradeComboBox.Text = ""
                    ClassifyGradeComboBox.Text = ""

                    SupplierComboBox.Enabled = True
                    GreenGradeComboBox.Enabled = True
                    ClassifyGradeComboBox.Enabled = True

                    BaleNumberTextBox.Focus()
                    Return
                Else
                    Dim fcvBuying = _businessLayerService.FCVBuyingBL().GetBaleBarcodeDetailByBarcodeV2(baleBarcode)
                    If fcvBuying Is Nothing Then
                        MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleNumberTextBox.Focus()
                        Return
                    Else
                        If fcvBuying.Grade Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล FinalGrade
                            MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            BaleBarcodeTextBox.Text = ""
                            BaleBarcodeTextBox.Focus()
                            Return
                        End If
                        If fcvBuying.RejectTypeName IsNot Nothing Then 'ถ้ามีข้อมูลแต่เป็นเกรด - 
                            MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม FC Buying เนื่องจาก " & fcvBuying.RejectTypeName, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            BaleBarcodeTextBox.Text = ""
                            BaleBarcodeTextBox.Focus()
                            Return
                        End If
                        If fcvBuying.FinishStatus = False Then 'ถ้ามีข้อมูลแล้วแต่ยังไม่ได้ Approve จากFC buying
                            MessageBox.Show("ห่อยานี้ยังไม่ได้รับการ Approve จาก Buyer กรุณาตรวจสอบจากโปรแกรม FC Buying", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            BaleBarcodeTextBox.Text = ""
                            BaleBarcodeTextBox.Focus()
                            Return
                        End If

                        BaleNumberTextBox.Text = fcvBuying.BaleNumber
                        NPIFarmerCodeTextBox.Text = fcvBuying.FarmerCode

                        If fcvBuying.ExtensionAgentCode.Contains("CR") Then
                            SupplierComboBox.Text = "NPICR"
                        ElseIf fcvBuying.ExtensionAgentCode.Contains("CM") Then
                            SupplierComboBox.Text = "NPICM"
                        ElseIf fcvBuying.ExtensionAgentCode.Contains("PR") Then
                            SupplierComboBox.Text = "NPIPR"
                        Else
                            SupplierComboBox.Text = fcvBuying.ExtensionAgentCode
                        End If

                        GreenGradeComboBox.Text = fcvBuying.Grade
                        BuyingWeightTextBox.Text = fcvBuying.Weight

                        BaleNumberTextBox.Enabled = False
                        SupplierComboBox.Enabled = False
                        GreenGradeComboBox.Enabled = False
                        BuyingWeightTextBox.ReadOnly = True

                        ClassifyGradeComboBox.Enabled = True
                        ClassifyGradeComboBox.Focus()
                        ClassifyGradeComboBox.Text = fcvBuying.Grade
                        ClassifyGradeComboBox.SelectAll()
                        Return
                    End If
                End If

            ElseIf _matrc.type = "FC" And _company.name = "STEC" And _matrc.rcfrom = "Purchasing" Then

                '******************************************************************
                'STEC(TW, AOI) FCV Receiving
                '******************************************************************

                'Check data from FCV Buying system.
                'If meet the result.Binding the result to form controls.
                'If not meet the result. Maybe this bale is from the NPI tobacco.
                Dim fcvBuying = _businessLayerService.FCVBuyingBL().GetBaleBarcodeDetailByBarcodeV2(baleBarcode)
                If fcvBuying Is Nothing Then
                    MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม FCV Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    BaleBarcodeTextBox.Text = ""
                    BaleNumberTextBox.Focus()
                    Return
                Else
                    If fcvBuying.Grade Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล FinalGrade
                        MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If
                    If fcvBuying.RejectTypeID IsNot Nothing Then 'ถ้ามีข้อมูลแต่เป็นเกรด - 
                        MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม FC Buying เนื่องจาก " & fcvBuying.RejectTypeName, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If
                    If fcvBuying.FinishStatus = False Then 'ถ้ามีข้อมูลแล้วแต่ยังไม่ได้ Approve จากFC buying
                        MessageBox.Show("ห่อยานี้ยังไม่ได้รับการ Approve จาก Buyer กรุณาตรวจสอบจากโปรแกรม FC Buying", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If

                    BaleNumberTextBox.Text = fcvBuying.BaleNumber
                    SupplierComboBox.Text = fcvBuying.ExtensionAgentCode
                    GreenGradeComboBox.Text = fcvBuying.Grade
                    BuyingWeightTextBox.Text = fcvBuying.Weight

                    BaleNumberTextBox.Enabled = False
                    SupplierComboBox.Enabled = False
                    GreenGradeComboBox.Enabled = False
                    BuyingWeightTextBox.ReadOnly = True

                    'If DigitalWeightCheckBox.Checked = True Then
                    '    ReceivingWeightTextBox.Text = ""
                    'ElseIf DigitalWeightCheckBox.Checked = False Then
                    '    ReceivingWeightTextBox.Text = fcvBuying.Weight
                    'End If

                    ClassifyGradeComboBox.Enabled = True
                    ClassifyGradeComboBox.Text = fcvBuying.Grade
                    ClassifierComboBox.SelectAll()
                    ClassifyGradeComboBox.Focus()
                    Return
                End If
            ElseIf _company.name = "STEC" And _matrc.type = "BU" And _matrc.rcfrom = "Purchasing" Then

                '********************************************************
                'ใช้เฉพาะการรับซื้อใบยาเพิ่มเติมโตยตรงจากคุณอุลย์และคุณเกียรติในปี 2022 เท่านั้น
                '********************************************************
                If DateTime.Now.Year = 2022 And DateTime.Now.Month = 8 Then
                    BaleNumberTextBox.Text = ""
                    BuyingWeightTextBox.Text = ""
                    ReceivingWeightTextBox.Text = ""

                    BaleNumberTextBox.ReadOnly = False
                    BuyingWeightTextBox.ReadOnly = False
                    ReceivingWeightTextBox.ReadOnly = False

                    BaleNumberTextBox.Enabled = True
                    BuyingWeightTextBox.Enabled = True
                    ReceivingWeightTextBox.Enabled = True

                    SupplierComboBox.Text = ""
                    GreenGradeComboBox.Text = ""
                    ClassifyGradeComboBox.Text = ""

                    SupplierComboBox.Enabled = True
                    GreenGradeComboBox.Enabled = True
                    ClassifyGradeComboBox.Enabled = True

                    FarmerLabel.Text = ""
                    BaleNumberTextBox.Focus()
                    Return
                End If

                '******************************************************************
                ' STEC Burley Receiving *******************************************
                '******************************************************************

                'Check data from Burley buying system.
                'If meet the result.Binding the result to form controls.
                Dim model = _businessLayerService.BurleyBuyingSystemBL().GetBuyingDetailByBaleBarcode(baleBarcode)
                If model Is Nothing Then
                    MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    BaleBarcodeTextBox.Text = ""
                    BaleBarcodeTextBox.Focus()
                    Return
                Else
                    If model.Grade Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล Grade
                        MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If

                    If model.RejectReason IsNot Nothing Then
                        MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม BU Buying เกรดถูกระบุเป็น " & model.RejectReason & " กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If

                    If model.Weight Is Nothing Then
                        MessageBox.Show("ห่อยานี้ไม่ได้บันทึกน้ำหนักจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If

                    If model.TransportationDocumentCode Is Nothing Then
                        MessageBox.Show("ห่อยานี้ไม่ได้บันทึกการโหลด Truck จากโปรแกรม BUBuying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If

                    If model.TransportationDocumentCode <> TransportationDocumentCodeLabel.Text.Trim() Then
                        MessageBox.Show("ห่อยานี้ Transportation document no. คือ " & model.TransportationDocumentCode & " ซึ่งไม่ตรงกับที่ระบุใน Receiving number นี้ กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        BaleBarcodeTextBox.Text = ""
                        BaleBarcodeTextBox.Focus()
                        Return
                    End If

                    If model.IsFinish = False Then
                        MessageBox.Show("ใบซื้อของห่อยานี้ยังไม่ได้ถูกปิดการขาย (IsFinish = false) กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If

                    If model.MovingBy <> Nothing Then
                        MessageBox.Show("ยาห่อนี้เป็นยาในโครงการของทางฝ่ายไร่ โปรดแยกยาห่อนี้ไปเก็บไว้ต่างหาก", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                    Dim incidentFarmerList = _businessLayerService.BurleyBuyingSystemBL().GetIncidentFarmertByBaleBarcode(baleBarcode)
                    If incidentFarmerList.Count() > 0 Then
                        MessageBox.Show("มีการบันทึกข้อมูล famer incident ของชาวไร่รายนี้ จำนวน " + incidentFarmerList.Count().ToString() + " รายการ", "Info",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                    'Use for buying system in CY 2021
                    If model.ChangeGradeUser <> Nothing Then
                        MessageBox.Show("ชาวไร่รายนี้ได้รับผลกระทบจากพายุ!!!", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If

                    BaleNumberTextBox.Text = model.Baleno
                    SupplierComboBox.Text = model.Supplier
                    GreenGradeComboBox.Text = model.Grade
                    BuyingWeightTextBox.Text = model.Weight

                    BaleNumberTextBox.Enabled = False
                    SupplierComboBox.Enabled = False
                    GreenGradeComboBox.Enabled = False
                    ClassifyGradeComboBox.Enabled = True

                    BaleNumberTextBox.ReadOnly = True
                    BuyingWeightTextBox.ReadOnly = True
                    ReceivingWeightTextBox.ReadOnly = False

                    If DigitalWeightCheckBox.Checked = True Then
                        ReceivingWeightTextBox.Text = ""
                    ElseIf DigitalWeightCheckBox.Checked = False Then
                        ReceivingWeightTextBox.Text = model.Weight
                    End If

                    CountMoistureLabel.Text = _businessLayerService.BurleyBuyingSystemBL().GetMoistureSummaryByFarmer(model.FarmerCode)
                    FarmerLabel.Text = model.FarmerCode & " " & model.FirstName & " " & model.LastName
                    ClassifyGradeComboBox.Focus()
                End If
            ElseIf _matrc.rcfrom = "Free" Or ReplaceBalesCheckbox.Checked = True Then

                '******************************************************************
                ' Free or Replace bale barcode Receiving **************************
                '******************************************************************

                'เช็คว่าจะต้องใช้ป้ายสีน้ำตาลเท่านั้น 05
                If Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text), 2) <> "05" Then
                    MessageBox.Show("ยาฟรีหรือป้าย Replace ต้องใช้ป้ายสีน้ำตาลเท่านั้น", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    BaleNumberTextBox.Text = ""
                    BaleNumberTextBox.Focus()
                    Return
                End If

                BaleNumberTextBox.Text = ""
                SupplierComboBox.Text = "MX102"
                GreenGradeComboBox.SelectedIndex = -1
                ClassifyGradeComboBox.SelectedIndex = -1
                BuyingWeightTextBox.Text = ""
                ReceivingWeightTextBox.Text = ""

                BaleNumberTextBox.Enabled = True
                BaleNumberTextBox.ReadOnly = False
                SupplierComboBox.Enabled = True
                GreenGradeComboBox.Enabled = True
                BuyingWeightTextBox.ReadOnly = True

                FarmerLabel.Text = ""
                BaleNumberTextBox.Focus()
            Else
                '******************************************************************
                'Other a receiving type (คีย์เองทั้งหมดนะ) TSC and TTM Receiving *******
                '******************************************************************

                BaleNumberTextBox.Text = ""
                BuyingWeightTextBox.Text = ""
                ReceivingWeightTextBox.Text = ""

                BaleNumberTextBox.ReadOnly = False
                BuyingWeightTextBox.ReadOnly = False
                ReceivingWeightTextBox.ReadOnly = False

                BaleNumberTextBox.Enabled = True
                BuyingWeightTextBox.Enabled = True
                ReceivingWeightTextBox.Enabled = True

                SupplierComboBox.Text = ""
                GreenGradeComboBox.Text = ""
                ClassifyGradeComboBox.Text = ""

                SupplierComboBox.Enabled = True
                GreenGradeComboBox.Enabled = True
                ClassifyGradeComboBox.Enabled = True

                FarmerLabel.Text = ""
                BaleNumberTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BaleNumberTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles BaleNumberTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
                e.KeyChar = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BaleNumberTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles BaleNumberTextBox.KeyDown
        Try
            If e.KeyCode <> Keys.Enter Then
                Return
            End If

            If _company.name = "LTL" And Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text.Replace(" ", "")), 2) = "05" Then
                SupplierComboBox.Text = "LTL"
                GreenGradeComboBox.Focus()
            ElseIf _matrc.rcfrom = "Free" Then
                'เช็คว่าคีย์ baleno ซ้ำหรือเปล่า หากซ้ำจะไม่อนุญาตให้บันทึก
                If _businessLayerService.ReceivingBL().GetDupplicateBaleNumberBySupplier(_matrc.crop, _matrc.type, SupplierComboBox.Text,
                                                                                         BaleNumberTextBox.Text.Replace(" ", "")).Count > 0 Then
                    MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้เนื่องจาก Baleno นี้ซ้ำกับที่มีใน Supplier กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    BaleNumberTextBox.Text = ""
                    BaleNumberTextBox.Focus()
                    Return
                End If
                GreenGradeComboBox.Focus()
            ElseIf _company.name = "NPI" And _matrc.type = "FC" And IsCUTRAGCheckBox.Checked = False Then
                GreenGradeComboBox.Focus()
            Else
                'In case TSC, TTM, NPI and STEC FCV receiving.
                SupplierComboBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub GreenGradeComboBox_KeyDown(sender As Object, e As KeyEventArgs) Handles GreenGradeComboBox.KeyDown
        If e.KeyCode <> Keys.Enter Then
            Return
        End If

        If _matrc.rcfrom = "Free" Then
            ClassifyGradeComboBox.Text = GreenGradeComboBox.Text

            If DigitalWeightCheckBox.Checked = True Then
                DigitalWeightPanel.Visible = True
                WeightFromDigitalScaleTextBox.Focus()
            Else
                ReceivingWeightTextBox.Enabled = True
                ReceivingWeightTextBox.ReadOnly = False
                ReceivingWeightTextBox.Text = ""
                ReceivingWeightTextBox.Focus()
            End If
        Else
            ClassifyGradeComboBox.Text = GreenGradeComboBox.Text
            ClassifyGradeComboBox.Focus()
        End If
    End Sub

    Private Sub BWeightTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles BuyingWeightTextBox.KeyDown
        Try
            If e.KeyCode <> Keys.Enter Then
                Return
            End If

            If BuyingWeightTextBox.Text = "" Then
                MessageBox.Show("ไม่มีข้อมูล Buying weight กรุณาตรวจสอบข้อมูล!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                BuyingWeightTextBox.Focus()
                Return
            End If

            If DigitalWeightCheckBox.Checked = True Then
                DigitalWeightPanel.Visible = True
                WeightFromDigitalScaleTextBox.Focus()
            Else
                ReceivingWeightTextBox.Enabled = True
                ReceivingWeightTextBox.ReadOnly = False
                ReceivingWeightTextBox.Text = ""
                ReceivingWeightTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ShowUnfinishBatchNo(list As List(Of UnfinishBatchNo))
        Try
            XBatchLabel.Text = ""
            XCountLabel.Text = ""
            LXBatchLabel.Text = ""
            LXCountLabel.Text = ""
            CBatchLabel.Text = ""
            CCountLabel.Text = ""
            BTBatchLabel.Text = ""
            BTCountLabel.Text = ""

            If list.Count <= 0 Then
                Return
            End If

            Dim batchX = list.SingleOrDefault(Function(x) x.ClassifyGroup = "X")
            Dim batchLX = list.SingleOrDefault(Function(x) x.ClassifyGroup = "LX")
            Dim batchC = list.SingleOrDefault(Function(x) x.ClassifyGroup = "C")
            Dim batchBT = list.SingleOrDefault(Function(x) x.ClassifyGroup = "BT")

            If batchX IsNot Nothing Then
                XBatchLabel.Text = batchX.BatchNo
                XCountLabel.Text = batchX.Bales
            End If
            If batchLX IsNot Nothing Then
                LXBatchLabel.Text = batchLX.BatchNo
                LXCountLabel.Text = batchLX.Bales
            End If
            If batchC IsNot Nothing Then
                CBatchLabel.Text = batchC.BatchNo
                CCountLabel.Text = batchC.Bales
            End If
            If batchBT IsNot Nothing Then
                BTBatchLabel.Text = batchBT.BatchNo
                BTCountLabel.Text = batchBT.Bales
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SaveData(baleBarcode As String)
        Try
            _start = False

            If baleBarcode = "" Then
                MessageBox.Show("ไม่มีข้อมูล Bale barcode กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'ตรวจสอบว่าห่อยาหมายเลขบาร์โค้ตนี้ ถูกรับเข้าระบบแล้วหรือไม่
            'If GetReceivedBale(baleBarcode) IsNot Nothing Then
            '    MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้เนื่องจากห่อยานี้รับเข้าแล้วใน Rc no. " & _matrc.rcno & "  กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Return
            'End If

            '****************************************
            ' เงื่อนไขในการกำหนดค่า weight ก่อนการบันทึกข้อมูล
            '****************************************
            If DigitalWeightCheckBox.Checked = True Then
                ReceivingWeightTextBox.Text = WeightFromDigitalScaleTextBox.Text
                If _company.name = "LTL" And _LTLResultType = False Then
                    BuyingWeightTextBox.Text = ReceivingWeightTextBox.Text
                End If
            End If

            If _greenGrades.Where(Function(x) x.green = GreenGradeComboBox.Text).Count <= 0 Then
                MessageBox.Show("green เกรดที่คุณบันทึก ไม่ตรงกับที่มีในระบบ!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                GreenGradeComboBox.Focus()
                Return
            End If

            If _classifyGrades.Where(Function(x) x.classify1 = ClassifyGradeComboBox.Text).Count <= 0 Then
                MessageBox.Show("classify เกรดที่คุณบันทึก ไม่ตรงกับที่มีในระบบ!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ClassifyGradeComboBox.Focus()
                Return
            End If

            If _matrc.rcno = "" Then
                MessageBox.Show("ไม่มีข้อมูล Receiving no. กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _matSubtypeCompany.Company = "" Then
                MessageBox.Show("ไม่มีข้อมูล Company กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _matrc.place = "" Then
                MessageBox.Show("ไม่มีข้อมูล Place กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _matrc.truckno = "" Then
                MessageBox.Show("ไม่มีข้อมูล Truck No. กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If Len(BaleNumberTextBox.Text) > 5 Then
                MessageBox.Show("Bale number จะต้องไม่เกิน 5 ตัว!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If SupplierComboBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Supplier !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                SupplierComboBox.Focus()
                Return
            End If

            If _suppliers.Where(Function(x) x.code = SupplierComboBox.Text).Count < 1 Then
                MessageBox.Show("ไม่พบข้อมูล Supplier นี้ในระบบ!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                SupplierComboBox.Focus()
                Return
            End If

            If GreenGradeComboBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Green grade !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                GreenGradeComboBox.Focus()
                Return
            End If

            If ClassifyGradeComboBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Classify grade !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ClassifyGradeComboBox.Focus()
                Return
            End If

            If BuyingWeightTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Buying weight !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                BuyingWeightTextBox.Focus()
                Return
            End If

            If ReceivingWeightTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Receiving weight !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ReceivingWeightTextBox.Focus()
                Return
            End If

            If Convert.ToDouble(BuyingWeightTextBox.Text) <= 0 Then
                MessageBox.Show("น้ำหนัก Buying weight ต้องมากกว่า 0 !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                BuyingWeightTextBox.Focus()
                Return
            End If

            If Convert.ToDouble(ReceivingWeightTextBox.Text) <= 0 Then
                MessageBox.Show("น้ำหนัก Receiving weight ต้องมากกว่า 0 !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                ReceivingWeightTextBox.Focus()
                Return
            End If

            If _matrc.rcfrom = "Free" And Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text), 2) <> "05" Then
                MessageBox.Show("ยาฟรีหรือป้าย Replace ต้องใช้ป้ายสีน้ำตาลเท่านั้น", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _matrc.rcfrom = "Free" And GreenGradeComboBox.Text <> ClassifyGradeComboBox.Text Then
                MessageBox.Show("กรุณาตรวจสอบ Green และ Classify grade เนื่องจากระบบกำหนดให้เกรดของยาฟรีหรือป้าย Replace จะต้องเป็นเกรดเดียวกัน !!!", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            '2020-04-21 มีการซื้อยา H และ L จากคุณอดุลย์สันกำแพงเพื่อมาทำยา CUTRAG
            'เลยเพิ่ม IsCUTRAGCheckBox เข้าไปอีก 1 เงื่อนไข
            If (_company.name = "STEC" Or _company.name = "NPI") And
                _matrc.type = "FC" And
                Microsoft.VisualBasic.Left(baleBarcode, 2) <> "06" And
                IsCUTRAGCheckBox.Checked = False Then 'กรณี ยา FCV เชียงรายและ NPI
                MessageBox.Show("ระบบอนุญาตให้บันทึกข้อมูลยา FCV ได้เฉพาะป้ายสีส้ม (รหัส 06) เท่านั้น !!!", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If


            '*****************************************************
            'การตรวจสอบในกรณี STEC FCV NPI *************************
            '2020-04-21 มีการซื้อยา H และ L จากคุณอดุลย์สันกำแพงเพื่อมาทำยา CUTRAG
            'ไม่มีข้อมูล farmer ในระบบ Buying จึงไม่ต้องเช็ค farmercode
            '*****************************************************

            If _company.name = "NPI" And _matrc.type = "FC" And IsCUTRAGCheckBox.Checked = False Then
                If _farmers.Where(Function(x) x.FarmerCode = NPIFarmerCodeTextBox.Text).Count <= 0 Then
                    MessageBox.Show("Farmer Code ที่คุณบันทึก ไม่ตรงกับที่มีในระบบ!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    NPIFarmerCodeTextBox.Focus()
                    Return
                End If
            End If


            '*****************************************************
            'การตรวจสอบในกรณี STEC FCV *****************************
            '*****************************************************

            If _company.name = "STEC" And _matrc.type = "FC" And _company.FromBuyingSystem = True And _matrc.rcfrom = "Purchasing" Then
                Dim fcvBale = _businessLayerService.FCVBuyingBL().GetBaleBarcodeDetailByBarcodeV2(baleBarcode)
                If fcvBale Is Nothing Then 'ไมพบข้อมูลหมายเลขบาร์โค้ดจาก FC BuyingSystem
                    MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
                If fcvBale.Grade Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล FinalGrade
                    MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
                If fcvBale.RejectTypeName IsNot Nothing Then 'ถ้ามีข้อมูลแต่เป็นเกรด - 
                    MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม FC Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
                If fcvBale.FinishStatus = False Then 'ถ้ามีข้อมูลแล้วแต่ยังไม่ได้ Approve จากFC buying
                    MessageBox.Show("ห่อยานี้ยังไม่ได้รับการ Approve จาก Buyer กรุณาตรวจสอบจากโปรแกรม FC Buying", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If


            '*****************************************************
            'การตรวจสอบในกรณี STEC BU ******************************
            'มีการแก้ไขเพื่อใช้สำหรับรับยาซื้อพิเศษจากคุณอดุลและคุณเกียรติปี 2020 
            '*****************************************************

            If _company.name = "STEC" And
                _matrc.type = "BU" And
                _company.FromBuyingSystem = True And
                _matrc.rcfrom = "Purchasing" Then

                Dim buBale = _businessLayerService.BurleyBuyingSystemBL().GetBuyingDetailByBaleBarcode(baleBarcode)

                If buBale Is Nothing Then 'ไมพบข้อมูลหมายเลขบาร์โค้ดจาก BU BuyingSystem
                    MessageBox.Show("ห่อยานี้ยังไม่ได้รับซื้อจากโปรแกรม BU Buying  กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.Grade Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล Grade
                    MessageBox.Show("ห่อยานี้ไม่ได้บันทึกเกรดรับซื้อจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.Grade <> GreenGradeComboBox.Text Then
                    MessageBox.Show("grade ซื้อ และ green ไม่ตรงกัน โปรดลองสแกนบาร์โค้ตใหม่และตรวจสอบข้อมูลอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.RejectReason IsNot Nothing Then 'ถ้ามีข้อมูลแต่เป็นเกรด - 
                    MessageBox.Show("ห่อยานี้ถูกปฏิเสธการรับซื้อจากโปรแกรม BU Buying เกรดถูกระบุเป็น - กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.Weight Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ยังไม่ได้ชั่งน้ำหนัก
                    MessageBox.Show("ห่อยานี้ไม่ได้บันทึกน้ำหนักจากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.IsFinish = False Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล FinishedDate
                    MessageBox.Show("ห่อยานี้ยังไม่ได้ปิดการขาย (IsFinish = false) กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.TransportationDocumentCode Is Nothing Then
                    MessageBox.Show("ห่อยานี้ไม่ได้บันทึกการโหลด Truck จากโปรแกรม BU Buying กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If buBale.TransportationDocumentCode <> TransportationDocumentCodeLabel.Text.Trim() Then
                    MessageBox.Show("ห่อยานี้มี Transportation document no. คือ " & buBale.TransportationDocumentCode &
                                    " ซึ่งไม่ตรงกับที่ระบุใน Receiving number นี้ กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If

            '*********************************************************
            ' เงื่อนไขในการ check diff over not accept ก่อนการบันทึกข้อมูล
            '*********************************************************
            Dim _buyingWeight As Double
            Dim _receivingWeight As Double

            _buyingWeight = Val(BuyingWeightTextBox.Text)
            _receivingWeight = Val(ReceivingWeightTextBox.Text)

            'ถ้าเป็นยาฟรี หรือยา replace หรือเป็นยาลาวที่ใช้ป้ายสีน้ำตาล ให้น้ำหนักซื้อเท่ากับน้ำหนักรับอัตโนมัติ
            If _matrc.rcfrom = "Free" Or
                ReplaceBalesCheckbox.Checked = True Or
                _matrc.type = "TSC" Or
                (_company.name = "LTL" And
                Microsoft.VisualBasic.Left(Trim(baleBarcode), 2) = "05" And
                _LTLRemarkReasonResult IsNot Nothing And
                _LTLBaleBarcodeFromLoadBale Is Nothing) Then
                _buyingWeight = _receivingWeight
            End If

            ' หักน้ำหนักกระสอบกรณี user เลือกที่จะคีย์ข้อมูลเอง ไม่ผ่านเครื่องชั่งดิจิตอล
            If ManualInputCheckBox.Checked = True And DigitalWeightCheckBox.Checked = False Then
                If BaleTypeComboBox.Text = "Close bales" Then
                    _receivingWeight = _receivingWeight - 2.3
                Else
                    _receivingWeight = _receivingWeight
                End If
            End If


            If _company.name = "LTL" Then
                'Lao alert ที่ 5 kgs. ยกเว้นที่ใช้ป้ายน้ำตาลและระบุว่าน้ำหนักของลาวกับที่รับต่างกัน 5 โลขึ้นไป
                If _LTLRemarkReasonResult <> "Diff. weight greater than or equal to 5 Kgs. " And
                    _LTLRemarkReasonResult <> "Carton Boxs" Then
                    If (Math.Abs(_buyingWeight - _receivingWeight) >= 5) Then
                        MessageBox.Show("ห่อยานี้น้ำหนักต่างกัน 5 กก.ขึ้นไป ระบบไม่อนุญาตให้บันทึกข้อมูล กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If

                ElseIf (Math.Abs(_buyingWeight - _receivingWeight) >= 2) Then
                    If MessageBox.Show("น้ำหนักต่างกัน 2 กก.ขึ้นไป ท่านต้องการบันทึกข้อมูลหรือไม่? ", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) =
                        DialogResult.No Then
                        Return
                    End If
                End If

            ElseIf _company.name = "STEC" And _matrc.type = "FC" Then
                If (Math.Abs(_buyingWeight - _receivingWeight) >= 1.5) Then
                    MessageBox.Show("ห่อยานี้น้ำหนักต่างกัน 1.5 กก.ขึ้นไป ระบบไม่อนุญาตให้บันทึกข้อมูล กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

            ElseIf _company.name = "NPI" And _matrc.type = "FC" Then
                If (Math.Abs(_buyingWeight - _receivingWeight) >= 1.5) Then
                    MessageBox.Show("ห่อยานี้น้ำหนักต่างกัน 1.5 กก.ขึ้นไป ระบบไม่อนุญาตให้บันทึกข้อมูล กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                'Edit condition CY2024 (2024-03-25)
                'Thantip only can be accept a difference weight.
            ElseIf _company.name = "STEC" And _matrc.type = "BU" And Not Environment.UserName.ToLowerInvariant().Contains("thantip") Then
                If (Math.Abs(_buyingWeight - _receivingWeight) >= 1.5) Then
                    MessageBox.Show("ห่อยานี้น้ำหนักต่าง 1.5 กก.ขึ้นไป ระบบไม่อนุญาตให้บันทึกข้อมูล กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

            End If


            'เพิ่มเช็คก่อนว่าเป็นการบันทึกใหม่หรือแก้ไข
            Dim baleInfo = _businessLayerService.ReceivingBL().GetMatByBaleBarcode(baleBarcode)

            '*************************************************
            'In case insert new record ***********************
            '*************************************************
            If baleInfo Is Nothing Then 'ถ้าไม่มีข้อมูล barcode นี้ให้ insert เนื่องจากมองว่ายังไม่เคยรับยาห่อนี้เข้าระบบมาก่อน
                If _company.name = "LTL" Then
                    If Microsoft.VisualBasic.Left(Trim(baleBarcode), 2) = "05" Then
                        ' กรณีน้ำหนักต่างกันเกิน 5 kg จะต้องมีการระบุ Barcode เติมใน packing list ด้วย
                        If _LTLResultType = True And
                            _LTLBaleBarcodeFromLoadBale Is Nothing And
                            ReplaceBalesCheckbox.Checked = False Then
                            MessageBox.Show("กรุณาคีย์บาร์โค้ดเดิมของ LTL ที่ติดมากับห่อยานี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If
                        ' กรณีใช้ป้าย 05 ทุกกรณี จะต้องมีการระบุเหตุผลของการใช้ป้ายเสมอ
                        If _LTLRemarkReasonResult Is Nothing And
                            ReplaceBalesCheckbox.Checked = False Then
                            MessageBox.Show("กรุณาเลือกสาเหตุที่ต้องใช้ป้ายน้ำตาลของ STEC", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If

                        ' กรณีป้ายสแกนไม่ได้ หรือน้ำหนักต่างกัน 5 กิโลขึ้นไป ให้ทำการ update ข้อมูลใน Packing list เพื่อบอกว่ามีการใช้ป้ายแทน
                        ' กรณีอื่นๆ ให้ทำการ insert ข้อมูลป้ายแทนเข้าไปใน Packing list
                        If _LTLResultType = True And ReplaceBalesCheckbox.Checked = False Then
                            db.sp_Receiving_UPD_LoadBales(_LTLBaleBarcodeFromLoadBale, BaleBarcodeTextBox.Text, _LTLRemarkReasonResult, 0)
                        ElseIf _LTLResultType = False And ReplaceBalesCheckbox.Checked = False Then
                            db.sp_Receiving_INS_LoadBales(InvoiceNoLabel.Text,
                                                          "",
                                                          "",
                                                          BaleNumberTextBox.Text,
                                                          SupplierComboBox.Text,
                                                          GreenGradeComboBox.Text,
                                                          BuyingWeightTextBox.Text,
                                                          _username,
                                                          TrucknoLabel.Text,
                                                          baleBarcode,
                                                          _LTLRemarkReasonResult,
                                                          0,
                                                          "",
                                                          Now,
                                                          ClassifyGradeComboBox.Text,
                                                          0,
                                                          InvoiceNoLabel.Text,
                                                          "000",
                                                          _matrc.type)
                        End If
                    Else
                        ' กรณีเป็นบาร์โค้ตลาวปกติ ที่ไม่ใช้ป้าย 05 และไม่ใช่ยากล่อง ให้อีพเดทข้อมูลใน LoadBale
                        If ChkFromLTLCarton.Checked = False Then
                            db.sp_Receiving_UPD_LoadBales(baleBarcode, "", "", 0)
                        Else
                            _LTLCartonCount = _LTLCartonCount + 1
                            If _LTLCartonCount > Val(TxtLTLCartons.Text) Then
                                MessageBox.Show("กรุณาตรวจสอบข้อมูล เนื่องจากจำนวนป้ายลาวที่บันทีกกรณีเป็น Carton box เกินจากจำนวนที่ระบุไว้ในช่อง LTL จำนวน หากครบตามจำนวนแล้วให้ทำการกดปุ่ม Complete  ",
                                                "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                Return
                            End If
                            If _LTLCartonCount = 1 Then
                                _batchGroupLTL = _businessLayerService.ReceivingBL().GetLTLBatchGroup()
                            End If

                            'ถ้าเป็น cartonBox ให้ทำการ updte ใน loadbale ด้วย
                            db.sp_Receiving_UPD_LoadBales(baleBarcode, "", _LTLRemarkReasonResult, _batchGroupLTL)
                        End If
                    End If
                End If

                If _company.name = "NPI" And _matrc.type = "FC" Then
                    _businessLayerService.ReceivingBL().FcvNpiReceivingAdd(_matrc.rcno,
                                                                        _matrc.crop,
                                                                        _matrc.type,
                                                                        _matSubtypeCompany.Subtype,
                                                                        1,'ถ้าเป็นยา FCV ของ NPI ทั้งหมด ให้ company เป็นของ STEC (code 1)
                                                                        _matrc.rcfrom,
                                                                        baleBarcode,
                                                                        SupplierComboBox.Text,
                                                                        Convert.ToInt16(BaleNumberTextBox.Text),
                                                                        NPIFarmerCodeTextBox.Text,
                                                                        GreenGradeComboBox.Text,
                                                                        ClassifyGradeComboBox.Text,
                                                                        Convert.ToDouble(_receivingWeight),
                                                                        Convert.ToDouble(_buyingWeight),
                                                                        _baleType,
                                                                        _matrc.place,
                                                                        False,
                                                                        _username,
                                                                        "",
                                                                        Now,
                                                                        "",
                                                                        ClassifierComboBox.Text,
                                                                        ReplaceBalesCheckbox.Checked)

                Else
                    ' ถ้าเป็น BU ก่อนบันทึกให้ตรวจสอบว่ามี Batchno ที่ยังไม่ได้ finish ค้างอยู่หรือไม่
                    ' ถ้าไม่มีให้สร้าง Batchno ใหม่ขึ้นมาเพื่อบันทึกให้กับยาห่อที่จะรับใหม่นี้
                    '

                    '*****************************************************
                    'การตรวจสอบในกรณี STEC BU ******************************
                    'มีการแก้ไขเพื่อใช้สำหรับรับยาซื้อพิเศษจากคุณอดุลและคุณเกียรติปี 2020 
                    '*****************************************************

                    Dim _cpaBatch As New UnfinishBatchNo
                    If _company.name = "STEC" And _matrc.type = "BU" And _matrc.rcfrom = "Purchasing" Then
                        _cpa_group = _classifyGrades.SingleOrDefault(Function(x) x.classify1 = ClassifyGradeComboBox.Text).cpa_group

                        If _cpa_group = Nothing Then
                            MessageBox.Show("ไม่พบ cpa group ของเกรด " & ClassifyGradeComboBox.Text & " ที่ใช้จำแนกกลุ่มของการตรวจ CPA โปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบ", "warning",
                                            MessageBoxButtons.OK, MessageBoxIcon.Warning)
                            Return
                        End If

                        _cpaBatch = _businessLayerService.CPABatchBL.
                                GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text).
                                SingleOrDefault(Function(x) x.ClassifyGroup = _cpa_group)

                        If _cpaBatch Is Nothing Then
                            _businessLayerService.CPABatchBL.Add(_matrc.crop, _cpa_group, RcLinesLabel.Text, _matrc.rcno)
                            _cpaBatch = _businessLayerService.CPABatchBL.
                                GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text).
                                SingleOrDefault(Function(x) x.ClassifyGroup = _cpa_group)
                        End If

                        _batchNo = _cpaBatch.BatchNo
                    End If

                    db.sp_Receiving_INS_MatCPA(_matrc.rcno,
                                               _defaultCrop,
                                               _matrc.type,
                                               _matSubtypeCompany.Subtype,
                                               _company.code,
                                               _matrc.rcfrom,
                                               baleBarcode,
                                               SupplierComboBox.Text,
                                               BaleNumberTextBox.Text,
                                               GreenGradeComboBox.Text,
                                               ClassifyGradeComboBox.Text,
                                               Convert.ToDouble(_receivingWeight),
                                               Convert.ToDouble(_buyingWeight),
                                               _baleType,
                                               _matrc.place,
                                               0,
                                               _username,
                                               _batchNo,
                                               "",
                                               Now,
                                               "",
                                               ClassifierComboBox.Text,
                                               ReplaceBalesCheckbox.Checked)

                    ' แสดงข้อมูล แต่ละ batch หลังจากที่บันทึกข้อมูลห่อยาเรียบร้อยแล้ว
                    If _company.name = "STEC" And _matrc.type = "BU" And _matrc.rcfrom = "Purchasing" Then
                        Dim list = _businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text).ToList()

                        ShowUnfinishBatchNo(list)

                        _cpaBatch = list.SingleOrDefault(Function(x) x.ClassifyGroup = _cpa_group)

                        If _cpaBatch.NBales = _cpaBatch.Bales Then
                            PrintBatchNo(_cpaBatch.BatchNo)
                            _businessLayerService.CPABatchBL.Finish(_cpaBatch.BatchNo, _username)
                        End If
                    End If

                End If

            Else
                '*************************************************
                'In case update record ***************************
                '*************************************************

                If _matrc.checkerlocked = True Then
                    MessageBox.Show("Checker ได้ทำการ lock rcno นี้แล้ว ไม่สามารถแก้ไขข้อมูลได้", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    ClearForm()
                    Return
                End If
                If MessageBox.Show("ต้องการแก้ไข barcode นี้ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                    ClearForm()
                    Return
                End If

                If _company.name = "NPI" And _matrc.type = "FC" Then
                    _businessLayerService.ReceivingBL().FcvNpiReceivingEdit(baleBarcode,
                                                                            SupplierComboBox.Text,
                                                                            BaleNumberTextBox.Text,
                                                                            NPIFarmerCodeTextBox.Text,
                                                                            GreenGradeComboBox.Text,
                                                                            ClassifyGradeComboBox.Text,
                                                                            Convert.ToDouble(_receivingWeight),
                                                                            Convert.ToDouble(_buyingWeight),
                                                                            baleInfo.green,
                                                                            baleInfo.classify,
                                                                            Convert.ToDouble(_receivingWeight),
                                                                            baleInfo.weightbuy,
                                                                            _username,
                                                                            "From FrmReceivingWeight แก้ไขข้อมูล mat",
                                                                            "",
                                                                            Now,
                                                                            _username,
                                                                            ClassifierComboBox.Text,
                                                                            ReplaceBalesCheckbox.Checked)
                Else
                    db.sp_Receiving_UPD_MatCPA(_matrc.rcno,
                                           _matrc.crop,
                                           _matrc.type,
                                           _matSubtypeCompany.Subtype,
                                           _company.code,
                                           _matrc.rcfrom,
                                           baleInfo.bc,
                                           SupplierComboBox.Text,
                                           BaleNumberTextBox.Text,
                                           GreenGradeComboBox.Text,
                                           ClassifyGradeComboBox.Text,
                                           Convert.ToDouble(_receivingWeight),
                                           Convert.ToDouble(_buyingWeight),
                                           _baleType,
                                           _matrc.place,
                                           0,
                                           _username,
                                           baleInfo.BatchNo,
                                           baleInfo.green,
                                           baleInfo.classify,
                                           baleInfo.weightbuy,
                                           baleInfo.weight,
                                           "From FrmReceivingWeight แก้ไขข้อมูล mat",
                                           "",
                                           Now,
                                           "",
                                           ClassifierComboBox.Text,
                                           ReplaceBalesCheckbox.Checked)
                    MessageBox.Show("แก้ไขข้อมูลเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
            RefreshForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub DigitalWeightBackTile_Click(sender As Object, e As EventArgs) Handles DigitalWeightBackTile.Click
        Try
            DigitalWeightPanel.Visible = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SaveFromDigitalTile_Click(sender As Object, e As EventArgs) Handles SaveFromDigitalTile.Click
        Try
            SaveData(BaleBarcodeTextBox.Text.Replace(" ", ""))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Save_Click(sender As Object, e As EventArgs) Handles Save.Click
        Try
            SaveData(BaleBarcodeTextBox.Text.Replace(" ", ""))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmReceivingWeight_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If e.KeyChar = ">" Then  'CLOSE BALES
                BaleTypeComboBox.SelectedIndex = 1
                BaleTypeComboBox.BackColor = Color.LimeGreen
                BaleTypeComboBox.ForeColor = Color.White
                BaleBarcodeTextBox.Focus()
            ElseIf e.KeyChar = "<" Then  'OPEN BALES
                BaleTypeComboBox.SelectedIndex = 0
                BaleTypeComboBox.BackColor = Color.Green
                BaleTypeComboBox.ForeColor = Color.White
                BaleBarcodeTextBox.Focus()
            ElseIf e.KeyChar = "/" Then  'กดปุ่ม / ให้เคลียร์ข้อมูล
                BaleBarcodeTextBox.Text = ""
                BaleNumberTextBox.Text = ""
                GreenGradeComboBox.SelectedIndex = -1
                ClassifyGradeComboBox.SelectedIndex = -1
                BuyingWeightTextBox.Text = ""
                ReceivingWeightTextBox.Text = ""
                BaleBarcodeTextBox.Focus()
            ElseIf e.KeyChar = "+" Then 'กดปุ่ม + ให้เปลี่ยน receiving user
                FrmChangeUser.ShowDialog()
                UsernameLabel.Text = _username
                'BaleBarcodeTextBox.Focus()
            ElseIf e.KeyChar = Chr(27) Then
                BaleBarcodeTextBox.Focus()
                RefreshForm()
            Else
                '*******************
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RWeightTextBox_KeyDown(sender As Object, e As KeyEventArgs)
        Try
            If e.KeyCode = Keys.Enter Then
                'ถ้ารับน้ำหนักจากเครื่องชั่ง
                If DigitalWeightCheckBox.Checked = True Then
                    DigitalWeightPanel.Visible = True
                    DigitalWeightPanel.Location = New Point(180, 294)
                    WeightFromDigitalScaleTextBox.Text = "0"
                    SaveFromDigitalTile.Focus()
                Else
                    If (Microsoft.VisualBasic.Left(Trim(BaleBarcodeTextBox.Text), 2) = "05" And _company.name <> "TTM") Then
                        BuyingWeightTextBox.Text = ReceivingWeightTextBox.Text
                    End If
                    Save.Enabled = True
                    Save.Visible = True
                    Save.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RWeightTextBox_Click(sender As Object, e As EventArgs)
        Try
            If BaleTypeComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Bale type!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If DigitalWeightCheckBox.Checked = True Then
                DigitalWeightPanel.Visible = True
                DigitalWeightPanel.Location = New Point(180, 294)
                WeightFromDigitalScaleTextBox.Text = "0"
                SaveFromDigitalTile.Focus()
            Else
                DigitalWeightPanel.Visible = False
                'Save.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RWeightTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ReceivingWeightTextBox.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
    End Sub

    Private Sub ClassifyComboBox_KeyDown(sender As Object, e As KeyEventArgs)
        Try
            If e.KeyCode = Keys.Enter Then
                ReceivingWeightTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SerialPort1_DataReceived(sender As Object, e As SerialDataReceivedEventArgs) Handles SerialPort1.DataReceived
        Try
            'Dim sp As SerialPort = CType(sender, SerialPort)
            'Dim indata As String = sp.ReadLine.Substring(17, 5) 'Microsoft.VisualBasic.Right(sp.ReadLine, 5)
            'WeightTextBox2.Text = Convert.ToDouble(indata) * 1000

            'METTLER TOLEDO IND221 --------------------
            buffer = buffer & SerialPort1.ReadExisting

            'TRANSCALL TI-50E -------------------------
            'buffer = buffer & SerialPort1.ReadExisting

            'Commander Tiger --------------------------
            'buffer = buffer & SerialPort1.ReadExisting

            'DEGREDATION QC DEG100 --------------------
            'buffer = buffer & SerialPort1.ReadLine

            Dim iret As Integer
            iret = InStr(1, buffer, vbCr)

            If (iret <> 0) Then

                'ตรวจสอบ object Textbox1 ว่าขณะนี้ต้อง invork กลับไป MainThread หรือไม่
                If WeightFromDigitalScaleTextBox.InvokeRequired Then
                    'สร้าง Delagate และส่งชื่อ Sub ที่ต้องการจะชี้ให้ไปทำงาน
                    Dim d As New UpdateTextBox(AddressOf UpdateTextBoxHandler)
                    'ทำการเรียก Sub และส่ง string value
                    Me.Invoke(d, New Object() {buffer})
                    'ถ้าอยู่ใน MainThread
                Else
                    'ทำการ เรียก Sub เพื่อ update ได้เลย
                    UpdateTextBoxHandler(buffer)
                End If

                buffer = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub LXCloseTile_Click(sender As Object, e As EventArgs) Handles LXCloseTile.Click
        Try
            If LXBatchLabel.Text <> "" Then
                If MessageBox.Show("ต้องการ Close LX ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
                    Return
                End If
                db.sp_Receiving_UPD_CPABatch(LXBatchLabel.Text, _username)
                MessageBox.Show("Close LX เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                PrintBatchNo(LXBatchLabel.Text)
                ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
                'BatchDataBinding()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub XCloseTile_Click(sender As Object, e As EventArgs) Handles XCloseTile.Click
        Try
            If XBatchLabel.Text <> "" Then
                If MessageBox.Show("ต้องการ Close X ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
                    Return
                End If
                db.sp_Receiving_UPD_CPABatch(XBatchLabel.Text, _username)
                MessageBox.Show("Close X เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                PrintBatchNo(XBatchLabel.Text)
                ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
                'BatchDataBinding()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub CCloseTile_Click(sender As Object, e As EventArgs) Handles CCloseTile.Click
        Try
            If CBatchLabel.Text <> "" Then
                If MessageBox.Show("ต้องการ Close C ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
                    Return
                End If
                db.sp_Receiving_UPD_CPABatch(CBatchLabel.Text, _username)
                MessageBox.Show("Close C เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                PrintBatchNo(CBatchLabel.Text)
                ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
                'BatchDataBinding()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BTCloseTile_Click(sender As Object, e As EventArgs) Handles BTCloseTile.Click
        Try
            If BTBatchLabel.Text <> "" Then
                If MessageBox.Show("ต้องการ Close BT ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
                    Return
                End If
                db.sp_Receiving_UPD_CPABatch(BTBatchLabel.Text, _username)
                MessageBox.Show("Close BT เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                PrintBatchNo(BTBatchLabel.Text)
                ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
                'BatchDataBinding()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub PrintBatchNo(ByVal batchNo As String)
        Try
            Dim _supplierForPrint As String = ""
            For Each item In _businessLayerService.ReceivingBL().GetSupplierGrouByBatchNo(batchNo)
                _supplierForPrint = _supplierForPrint + "  " + item.supplier
            Next

            '================================ TSC Printer ===============================
            Call openport("TSC TTP-247")
            Call setup("100", "65", "3.0", "2", "0", "0", "0")
            Call sendcommand("GAP 4 mm,0")
            Call sendcommand("DIRECTION 1")
            Call clearbuffer
            Call windowsfont(100, 80, 30, 0, 0, 0, "arial", "Lines :" & RcLinesLabel.Text)
            Call windowsfont(500, 80, 30, 0, 0, 0, "arial", "Date :" & Format(Now, "dd/MM/yyyy"))
            Call barcode("250", "200", "128", "80", "0", "0", "2", "2", batchNo)     'Barcode
            Call windowsfont(225, 133, 40, 0, 2, 0, "arial", "CPA Sampling Batch")
            Call sendcommand("BAR 80,130,630,4")      'เส้น Header บน
            Call sendcommand("BAR 80,170,630,4")      'เส้น Header ล่าง
            Call sendcommand("BAR 80,130,4,180")      'เส้นซ้าย
            Call sendcommand("BAR 710,130,4,180")      'เส้นขวา
            Call sendcommand("BAR 80,310,634,4")      'เส้นล่าง
            Call windowsfont(100, 330, 120, 0, 0, 0, "arial", batchNo)      '6 digit
            Call windowsfont(50, 460, 30, 0, 0, 0, "arial", "RC no." & _matrc.rcno)
            Call windowsfont(400, 460, 30, 0, 0, 0, "arial", _supplierForPrint)
            Call printlabel("1", "1")
            Call closeport
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Private Sub LXPrintTile_Click(sender As Object, e As EventArgs) Handles LXPrintTile.Click
        Try
            If LXBatchLabel.Text <> "" Then
                PrintBatchNo(LXBatchLabel.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub XPrintTile_Click(sender As Object, e As EventArgs) Handles XPrintTile.Click
        Try
            If XBatchLabel.Text <> "" Then
                PrintBatchNo(XBatchLabel.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub CPrintTile_Click(sender As Object, e As EventArgs) Handles CPrintTile.Click
        Try
            If CBatchLabel.Text <> "" Then
                PrintBatchNo(CBatchLabel.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BTPrintTile_Click(sender As Object, e As EventArgs) Handles BTPrintTile.Click
        Try
            If BTBatchLabel.Text <> "" Then
                PrintBatchNo(BTBatchLabel.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ReplaceBalesCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles ReplaceBalesCheckbox.CheckedChanged
        Try
            If ReplaceBalesCheckbox.Checked = True Then
                If MessageBox.Show("คุณต้องการ Replace Bale barcode ใช่หรือไม่ ?   ", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.Yes Then
                    Save.Visible = True
                    SaveMetroLabel.Visible = True
                    '  ถ้าหากเลือก Replaceแล้ว ใน RCno นี้จะต้อง Replace ทั้งหมด ไม่สามารถมีการ Purchase หรือ Free  ได้
                    ReplaceBalesCheckbox.Enabled = False
                    BaleBarcodeTextBox.Focus()
                End If
            Else
                Save.Visible = False
                SaveMetroLabel.Visible = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FindTruck_Click(sender As Object, e As EventArgs)
        Try
            If _matrc.type = "BU" And _company.name = "STEC" Then
                Dim window As New FrmTransportationDetail()
                window.ShowDialog()
            ElseIf _company.name <> "STEC" And _matrc.InvoiceNo <> "" Then
                Dim window As New FrmInvoiceDetail()
                window.ShowDialog()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ChkFromLTLCarton_Click(sender As Object, e As EventArgs) Handles ChkFromLTLCarton.Click
        Try
            If ChkFromLTLCarton.Checked = True Then
                _LTLRemarkReasonResult = "Carton Boxs"
                _LTLResultType = False
                TxtLTLCartons.Enabled = True
                TxtLTLCartons.BackColor = Color.Tomato
                TxtLTLCartons.Text = ""
                TxtLTLCartons.Focus()
            Else
                TxtLTLCartons.Enabled = False
                TxtLTLCartons.BackColor = Color.White
                TxtLTLCartons.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnLTLStart_Click(sender As Object, e As EventArgs) Handles BtnLTLStart.Click
        Try
            If ChkFromLTLCarton.Checked = True Then
                If TxtLTLCartons.Text = "" Then
                    MessageBox.Show("ให้ใส่จำนวนป้าย LTL และกดปุ่ม Start", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
                _LTLRemarkReasonResult = "Carton Boxs"
                _LTLResultType = False
                _batchGroupLTL = 0
                _LTLCartonCount = 0
                _LTLCartonStatus = True
                BaleBarcodeTextBox.Focus()
            Else
                MessageBox.Show("ให้ทำการเช็คถูกที่ LTL cartons ก่อนกดปุ่มStart", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub TxtLTLCartons_KeyDown(sender As Object, e As KeyEventArgs) Handles TxtLTLCartons.KeyDown
        Try
            If (e.KeyCode = Keys.Enter) Then
                BtnLTLStart.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnLTLComplete_Click(sender As Object, e As EventArgs) Handles BtnLTLComplete.Click
        Try
            If ChkFromLTLCarton.Checked = True Then
                If _batchGroupLTL = 0 Then
                    MessageBox.Show("ไม่สามารถcomplete เพื่อถัวเฉลี่ยน้ำหนักได้เนื่องจากค่า LTLBatchCountเป็นศูนย์  ให้ทำการคีย์จำนวนป้ายและกดปุ่ม Start ")
                    Exit Sub
                End If

                db.sp_Receiving_UPD_LTLAVGCARTONS(_batchGroupLTL)
                MessageBox.Show("ทำการ update น้ำหนักรับโดยใช้วิธีหาค่าเฉลี่ยเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

                'Refresh mat datagrid.
                ReceivingDataGridDataBinding()

                _batchGroupLTL = 0
                _LTLCartonCount = 0
                _LTLCartonStatus = False
                ChkFromLTLCarton.Checked = False
                TxtLTLCartons.Enabled = False
                TxtLTLCartons.BackColor = Color.White
                TxtLTLCartons.Text = ""

                BaleBarcodeTextBox.Focus()

            ElseIf (ChkFromLTLCarton.Checked = False) Then
                MessageBox.Show("ให้ทำการเช็คถูกที่ LTL cartons ก่อนกดปุ่มStart หรือ Complete", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ManualInputCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles ManualInputCheckBox.CheckedChanged
        Try
            If (_company.name = "STEC" And _matrc.type = "BU") Then
                MessageBox.Show("ยา STEC BU ไม่อนุญาตให้ใช้ฟังก์ชันนี้!!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            If (ManualInputCheckBox.Checked = True) Then
                DigitalWeightCheckBox.Checked = False
                ClearForm()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ChangeStaffUserButton_Click(sender As Object, e As EventArgs) Handles ChangeStaffUserButton.Click
        Try
            FrmChangeUser.ShowDialog()
            UsernameLabel.Text = _username
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RejectButton_Click(sender As Object, e As EventArgs) Handles RejectButton.Click
        Try
            If BaleBarcodeTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุ Bale Barcode", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim buyingBale = _businessLayerService.BurleyBuyingSystemBL().GetBuyingDetailByBaleBarcode(BaleBarcodeTextBox.Text)
            If buyingBale Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูลหมายเลข Bale Barcode ที่ระบุ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                BaleBarcodeTextBox.Focus()
                Return
            End If

            ' ถ้ามีการหักชำระหนี้ค่าเมล็ดพันธุ์และ PPE Kits ในใบเวาเชอร์ของยาห่อนี้ให้ทำการตรวจสอบยอดเงินคงเหลือหลังจากแบ็คยาห่อนี้ว่าพอหักหรือไม่
            ' จากนั้นให้แจ้งเตือนไปยัง user เพื่อให้ทราบและแจ้งต่อไปยังแผนกบัญขีและผู้เกี่ยวข้องอื่นๆ ต่อไป
            'If buyingBale.HasPayment Then
            '    Dim voucherAmount = buyingBale.SoldPrice - buyingBale.BalePrice - buyingBale.TotalDebt

            '    If voucherAmount < 0 Then
            '        MessageBox.Show("เมื่อทำการแบ็คยาห่อนี้แล้ว ส่งผลให้ชาวไร่รายนี้เงินที่ได้จากการขายไม่พอหักชำระหนี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '        BaleBarcodeTextBox.Focus()
            '        Return
            '    End If
            'End If

            Dim recievingBale = _businessLayerService.ReceivingBL().GetMatByBaleBarcode(BaleBarcodeTextBox.Text)
            If recievingBale IsNot Nothing Then
                MessageBox.Show("ห่อยานี้ถูกรับเข้าระบบแล้ว ไม่สามารถ Reject ได้ โปรดลบข้อมูลออกจากระบบ receiving ก่อน", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                BaleBarcodeTextBox.Focus()
                Return
            End If

            _approveType = "BUBuyingRejection"
            _aproveRcno = _matrc.rcno

            Dim window As New FrmPasswordApprove()
            window._baleBarcode = BaleBarcodeTextBox.Text
            window.ShowDialog()

            BaleBarcodeTextBox.Text = ""
            BaleNumberTextBox.Text = ""
            GreenGradeComboBox.Text = ""
            ClassifyGradeComboBox.Text = ""
            BaleBarcodeTextBox.Focus()

            RefreshReceivingDocument()
            ReceivingDataGridDataBinding()
            ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FinishButton_Click(sender As Object, e As EventArgs) Handles FinishButton.Click
        Try
            If TotalBaleLabel.Text = "" Or Val(TotalBaleLabel.Text) <= 0 Then
                MessageBox.Show("ไม่มีข้อมูล barcode ใน Rc no.นี้ กรุณาตรวจสอบข้อมูล !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'ถ้าเป็น BU และเป็นการ Purchasing จะต้องเช็คว่าก่อน Finished ต้องทำการ Close CPA BatchNo ให้หมดก่อน
            If _matrc.type = "BU" And _company.name = "STEC" And _matrc.rcfrom = "Purchasing" Then
                If LXBatchLabel.Text <> "" Or XBatchLabel.Text <> "" Or CBatchLabel.Text <> "" Or BTBatchLabel.Text <> "" Then
                    MessageBox.Show("ยังมีบาง BatchNo ที่คุณยังไม่ได้ทำการ Close กรุณาv Close ก่อนทำการ Finished !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If

            If MessageBox.Show("ต้องการ Finish เลข RC no. นี้ใช่หรือไม่? หากใช่หลังจาก Finish แล้วจะไม่สามารถแก้ไขหรือลบข้อมูลได้อีก", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = DialogResult.No Then
                Return
            End If

            db.sp_Receiving_UPD_RCBFinished(_matrc.rcno, True)

            '----------- 2018-02-05 แก้ไขโปรแกรมโดยหาก suppliercode เป็น NPI  NPIT NPIS ให้ทำการ run company ใหม่
            '----------- โดย NPI = 8  , NPIT  = 2  , NPIS = 3
            If (_matrc.type = "FC") Then
                'db.sp_Receiving_UPD_COMPANYFORNPI(ReceivingNoLabel.Text)
                db.sp_Receiving_UPD_COMPANYFORNPI(_matrc.rcno)
            End If
            '---------

            '-----------------------------------------
            'ถ้าเป็นการ ReplaceBale ให้ทำการ AddDocNo และ Checker approve เลย
            If ReplaceBalesCheckbox.Checked = True Then
                '******************************
                'Add Docno 
                '******************************

                If _matList.Where(Function(x) x.docno IsNot Nothing).Count > 0 Then
                    MessageBox.Show("ไม่สามารถ Add Docno ได้เนื่องจากมีการระบุ docno ในข้อมูลชุดนี้แล้ว", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                'Get all checker locked already.
                'Get matrc by current rcno.
                If _matrc.checkerlocked = True Then
                    MessageBox.Show("ไม่สามารถ Add Docno ได้เนื่องจาก Checker locked ข้อมูลแล้ว", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                Dim matByRcnoForAddDocnoList = _businessLayerService.ReceivingDocumentBL().GetMatForAddDocno(_matrc.rcno)

                For Each row As sp_Receiving_SEL_MatbyRcNoGroupByField_Result In matByRcnoForAddDocnoList
                    db.sp_Receiving_UPD_DocNo(_defaultCrop, _matrc.rcno, row.supplier, row.subtype, row.company)
                Next

                'Update checker Locked
                db.sp_Receiving_UPD_CheckerLocked(_matrc.rcno, _username, True, True)
                MessageBox.Show("ทำการ Finish เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            RefreshReceivingDocument()
            RefreshForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        Try
            ClassifyAndGreenGradeDataBinding()
            ReceivingDataGridDataBinding()
            ShowUnfinishBatchNo(_businessLayerService.CPABatchBL.GetUnfinishCPABatchByRCLine(_matrc.rcno, RcLinesLabel.Text))
            'BatchDataBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As EventArgs) Handles DeleteButton.Click
        Try
            If BaleBarcodeTextBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Barcode ที่ต้องการลบจากตารางด้านล่าง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            _approveType = "DeleteBaleBarcode"
            _aproveRcno = _matrc.rcno

            Dim window As New FrmPasswordApprove()
            window._baleBarcode = BaleBarcodeTextBox.Text
            window.ShowDialog()
            window.Dispose()

            BaleBarcodeTextBox.Text = ""
            BaleNumberTextBox.Text = ""
            GreenGradeComboBox.Text = ""
            ClassifyGradeComboBox.Text = ""
            BaleBarcodeTextBox.Focus()

            ReceivingDataGridDataBinding()
            RefreshReceivingDocument()
            RefreshForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SetupDigitalScaleButton_Click(sender As Object, e As EventArgs) Handles SetupDigitalScaleButton.Click
        Try
            ' Get a list of serial port names.
            Dim ports As String() = IO.Ports.SerialPort.GetPortNames()
            If ports.Count < 1 Then
                MessageBox.Show("The COM port on your computer is not found! Please install COM port on your computer" & vbNewLine &
                                "ไม่พบ COM Port บนเครื่องคอมพิวเตอร์ของท่าน โปรดติดตั้ง COM Port ลงบนเครื่องคอมพิวเตอร์นี้ก่อน", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim window As New FrmSetDigitalScaleParameter()
            window.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub NTRMInspectionButton_Click(sender As Object, e As EventArgs) Handles NTRMInspectionButton.Click
        Try
            If (BaleBarcodeTextBox.Text = "") Then
                MessageBox.Show("กรุณาคีย์บาร์โค้ดที่ต้องการดูข้อมูลหรือบันทึก NTRM", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                BaleBarcodeTextBox.Focus()
                Return
            End If

            Dim window As New FrmNTRMInput()
            window._baleBarcode = BaleBarcodeTextBox.Text.Replace(" ", "")
            window.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub VieeTransportDetailButton_Click(sender As Object, e As EventArgs) Handles VieeTransportDetailButton.Click
        Try
            If _matrc.type = "BU" And _company.name = "STEC" Then
                Dim window As New FrmTransportationDetail()
                window._matrc = _matrc
                window.ShowDialog()
            ElseIf _company.name <> "STEC" And _matrc.InvoiceNo <> "" Then
                Dim window As New FrmInvoiceDetail()
                window.ShowDialog()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClassifyGradeComboBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ClassifyGradeComboBox.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub

    Private Sub ClassifyGradeComboBox_KeyDown(sender As Object, e As KeyEventArgs) Handles ClassifyGradeComboBox.KeyDown
        Try
            If e.KeyCode <> Keys.Enter Then
                Return
            End If

            If BuyingWeightTextBox.Text = "" Then
                BuyingWeightTextBox.Enabled = True
                BuyingWeightTextBox.ReadOnly = False
                BuyingWeightTextBox.Focus()
            Else
                If DigitalWeightCheckBox.Checked = True Then
                    DigitalWeightPanel.Visible = True
                    WeightFromDigitalScaleTextBox.Focus()
                Else
                    ReceivingWeightTextBox.Enabled = True
                    ReceivingWeightTextBox.ReadOnly = False
                    If _matrc.type = "FC" Then
                        ReceivingWeightTextBox.Text = BuyingWeightTextBox.Text
                    Else
                        ReceivingWeightTextBox.Text = ""
                    End If
                    ReceivingWeightTextBox.SelectAll()
                    ReceivingWeightTextBox.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub WeightFromDigitalScaleTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles WeightFromDigitalScaleTextBox.KeyPress
        If e.KeyChar = Chr(13) Then
            SaveData(BaleBarcodeTextBox.Text.Replace(" ", ""))
        End If
    End Sub

    Private Sub SupplierComboBox_KeyDown(sender As Object, e As KeyEventArgs) Handles SupplierComboBox.KeyDown
        If e.KeyCode = Keys.Enter Then
            GreenGradeComboBox.Focus()
        End If
    End Sub

    Private Sub SupplierComboBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles SupplierComboBox.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub

    Private Sub GreenGradeComboBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles GreenGradeComboBox.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub

    Private Sub ReceivingWeightTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles ReceivingWeightTextBox.KeyDown
        If e.KeyCode <> Keys.Enter Then
            Return
        End If

        If ReceivingWeightTextBox.Text = "" Then
            MessageBox.Show("โปรดกรอกน้ำหนักห่อยาที่ต้องการรับ!")
            Return
        End If

        If _matrc.rcfrom = "Free" Then
            BuyingWeightTextBox.Text = ReceivingWeightTextBox.Text
        End If

        SaveData(BaleBarcodeTextBox.Text.Replace(" ", ""))
    End Sub

    Private Sub ReceivingDataGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles ReceivingDataGrid.CellMouseClick
        Try
            If e.RowIndex < 0 Then
                Return
            End If

            Dim baleBarcode = ReceivingDataGrid.SelectedRows(0).Cells(0).Value
            _matSelected = _matList.SingleOrDefault(Function(x) x.bc = baleBarcode)

            If _matSelected Is Nothing Then
                Return
            End If

            If _matSelected.locked = True Then
                MessageBox.Show("ไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก rcno นี้ได้ถูกเปลี่ยนสถานะเป็น finish ไปก่อนหน้านี้แล้ว โปรดแจ้ง checker เพื่อปลดล็อคก่อนดำเนินการใดๆ", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            BaleBarcodeTextBox.Text = _matSelected.bc
            NPIFarmerCodeTextBox.Text = _matSelected.farmer_code
            BaleNumberTextBox.Text = _matSelected.baleno
            SupplierComboBox.Text = _matSelected.supplier
            GreenGradeComboBox.Text = _matSelected.green
            ClassifyGradeComboBox.Text = _matSelected.classify
            BuyingWeightTextBox.Text = Format(_matSelected.weightbuy, "##0.00")
            ReceivingWeightTextBox.Text = Format(_matSelected.weight, "##0.00")
            ClassifierComboBox.Text = _matSelected.classifier

            If _matSelected.ReplaceBales = True Then
                ReplaceBalesCheckbox.Checked = True
            Else
                ReplaceBalesCheckbox.Checked = False
            End If

            IsNTRMInspection(BaleBarcodeTextBox.Text.Replace(" ", ""))

            '2018-02-07  ถ้าเป็นยา NPI NPIT NPIS ให้แก้ไข green bweight ได้
            '2018-08-15 ถ้าเป็นยา TTM ให้แก้ไข green bweight ได้
            '--------------
            If ManualInputCheckBox.Checked = True Then
                BaleNumberTextBox.Enabled = True
                SupplierComboBox.Enabled = True
                GreenGradeComboBox.Enabled = True
                ClassifyGradeComboBox.Enabled = True
                BuyingWeightTextBox.Enabled = True
                ReceivingWeightTextBox.Enabled = True

                BaleNumberTextBox.ReadOnly = False
                BuyingWeightTextBox.ReadOnly = False
                ReceivingWeightTextBox.ReadOnly = False
            End If
            '----------

            If _company.name = "NPI" Then
                NPIFarmerCodeTextBox.Enabled = True
                BaleNumberTextBox.Enabled = True
                SupplierComboBox.Enabled = True
                GreenGradeComboBox.Enabled = True
                ClassifyGradeComboBox.Enabled = True
                BuyingWeightTextBox.Enabled = True
                ReceivingWeightTextBox.Enabled = True

                BaleNumberTextBox.ReadOnly = False
                BuyingWeightTextBox.ReadOnly = False
                ReceivingWeightTextBox.ReadOnly = False
            End If

            ClassifyGradeComboBox.Enabled = True
            ' ClassifyGradeComboBox.Focus()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub NPIFarmerCodeTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles NPIFarmerCodeTextBox.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub

    Private Sub NPIFarmerCodeTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles NPIFarmerCodeTextBox.KeyDown
        If e.KeyCode <> Keys.Enter Then
            Return
        End If
        BaleNumberTextBox.Focus()
        AutoReplaceSupplierByFarmerID(NPIFarmerCodeTextBox.Text)
    End Sub

    Private Sub AutoReplaceSupplierByFarmerID(farmerID As String)
        If farmerID.Contains("MS") Then
            SupplierComboBox.Text = "NPIMS"
        ElseIf farmerID.Contains("PK") Then
            SupplierComboBox.Text = "NPIPK"
        ElseIf farmerID.Contains("MJ") Then
            SupplierComboBox.Text = "NPIMJ"
        ElseIf farmerID.Contains("CR") Then
            SupplierComboBox.Text = "NPICR"
        ElseIf farmerID.Contains("PR") Then
            SupplierComboBox.Text = "NPIPR"
        Else
            SupplierComboBox.Text = "TW"
        End If
    End Sub

    Private Sub ReceivingWeightTextBox_Click(sender As Object, e As EventArgs) Handles ReceivingWeightTextBox.Click
        If DigitalWeightCheckBox.Checked = True Then
            DigitalWeightPanel.Visible = True
            WeightFromDigitalScaleTextBox.Focus()
        Else
            ReceivingWeightTextBox.Enabled = True
            ReceivingWeightTextBox.ReadOnly = False
            ReceivingWeightTextBox.Text = ""
            ReceivingWeightTextBox.Focus()
        End If
    End Sub

    Private Sub FrmReceivingWeight_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        SerialPort1.Close()
        SerialPort1.Dispose()
    End Sub
End Class
