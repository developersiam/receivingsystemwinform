﻿Imports System.ComponentModel
Imports System.Reflection
Imports FastMember

Public Class FrmCallReceivingNo
    Inherits MetroFramework.Forms.MetroForm
    Dim matrcSelected As sp_Receiving_SEL_MatRC_Result
    Dim _matRcList As List(Of sp_Receiving_SEL_MatRC_Result)

    Private Sub MatRcDataGridBinding()
        Try
            _matRcList = _businessLayerService.ReceivingDocumentBL().GetMatRcByCropAndType(_defaultCrop, TypeComboBox.Text, CheckerLockedCheckBox.Checked)

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_matRcList)
            dt.Load(reader)
            MatRcDataGrid.DataSource = dt

            DeleteDocumentButton.Enabled = False
            ReceivingButton.Enabled = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmCallReceivingNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            '_username = "IT"
            '_defaultCrop = 2019
            UsernameMetroLabel.Text = _username
            CheckerLockedCheckBox.Checked = False
            TypeBindingSource.DataSource = _businessLayerService.TypeBL().GetTypes()
            MatRcDataGridBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TypeComboBox.SelectedIndexChanged
        MatRcDataGridBinding()
    End Sub
    Private Sub CheckerLockedCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles CheckerLockedCheckBox.CheckedChanged
        MatRcDataGridBinding()
    End Sub
    Private Sub CreateDocumentButton_Click(sender As Object, e As EventArgs) Handles CreateDocumentButton.Click
        Try
            Dim window As New FrmAddReceivingNo
            window._type = TypeComboBox.SelectedValue.ToString()
            window.ShowDialog()
            MatRcDataGridBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub DeleteDocumentButton_Click(sender As Object, e As EventArgs) Handles DeleteDocumentButton.Click
        Try
            If MatRcDataGrid.SelectedRows.Count < 1 Then
                MessageBox.Show("กรุณา Click เลือกเลข Receiving no. ที่ต้องการจากตาราง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim matrcRow As sp_Receiving_SEL_MatRCByRcNo_Result
            matrcRow = _businessLayerService.ReceivingDocumentBL().GetMatRcByID(matrcSelected.crop, matrcSelected.type, matrcSelected.rcno)

            If matrcRow Is Nothing Then
                MessageBox.Show("ไม่พบเลข rcno นี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If matrcRow.checkerlocked = True Then
                MessageBox.Show("เลข rcno นี้ checker ได้ทำการ lock ข้อมูลแล้วจึงไม่สามารถลบข้อมูลได้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If matrcRow.COUNTOFBALES > 0 Then
                MessageBox.Show("เลข receiving number นี้ได้ทำการรับเข้าใบยาแล้ว กรุณาลบในหน้าชั่งน้ำหนักห่อยา", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim window As New FrmCancelRcno()
            window.ShowDialog()
            MatRcDataGridBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        MatRcDataGridBinding()
    End Sub
    Private Sub ReceivingButton_Click(sender As Object, e As EventArgs) Handles ReceivingButton.Click
        Try
            If matrcSelected Is Nothing Then
                MessageBox.Show("กรุณา Click เลือกเลข rcno. ที่ต้องการจากตาราง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If matrcSelected.CancleRcNoStatus = True Then
                MessageBox.Show("rcno นี้ถูกยกเลิกการใช้งานแล้ว ไม่สามารถบันทึกข้อมูลได้อีก", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If matrcSelected.checkerlocked = True Then
                MessageBox.Show("เลข rcno นี้ checker ได้ทำการ lock ข้อมูลแล้วไม่สามารถแก้ไขหรือเพิ่มข้อมูลได้ โปรดตรวจสอบข้อมูลจาก report", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            Dim window As New FrmReceivingWeight()
            window._matrc = matrcSelected
            window.ShowDialog()
            MatRcDataGridBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub ReportButton_Click(sender As Object, e As EventArgs) Handles ReportButton.Click
        Try
            Dim xfrmReport As New FrmRPT_RCB005
            xfrmReport._rcno = matrcSelected.rcno
            xfrmReport._type = matrcSelected.type
            xfrmReport.ShowDialog()
            MatRcDataGridBinding()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub MatRcDataGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MatRcDataGrid.CellMouseClick
        Try
            Dim rowIndex = MatRcDataGrid.SelectedCells.Item(0).RowIndex

            If rowIndex < 0 Then
                Return
            End If

            Dim rcno = MatRcDataGrid.SelectedRows(0).Cells(0).Value
            matrcSelected = _matRcList.FirstOrDefault(Function(x) x.rcno = rcno)

            If matrcSelected Is Nothing Then
                Return
            End If

            DeleteDocumentButton.Enabled = True
            ReceivingButton.Enabled = True
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class