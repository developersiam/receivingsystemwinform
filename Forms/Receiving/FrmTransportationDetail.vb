﻿Imports FastMember

Public Class FrmTransportationDetail
    Inherits MetroFramework.Forms.MetroForm

    Public _matrc As sp_Receiving_SEL_MatRC_Result

    Private Sub RefreshData()
        Try
            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_businessLayerService.ReceivingDocumentBL().GetNotReceiveBalesFromBuyingSystem(_matrc.TransportationDocumentCode, _matrc.crop, _matrc.rcno))
            dt.Load(reader)

            TransportationDetailDataGridView.DataSource = dt
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


    Private Sub FrmTransportationDetail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RefreshData()
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        RefreshData()
    End Sub
End Class