﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAddReceivingNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TruckNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.CompanyComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.CompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TransportDocNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.TotalBalesTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.InvoiceCombobox = New MetroFramework.Controls.MetroComboBox()
        Me.SpReceivingSELInvoiceDocNoResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReplaceBalesCheckbox = New MetroFramework.Controls.MetroCheckBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SubtypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SubTypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RcFromComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.PlaceComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpReceivingSELMatWHResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuyerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.ClassifierComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ClearButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SpReceivingSELBuyerClassifierResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.CompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpReceivingSELInvoiceDocNoResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SubTypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpReceivingSELMatWHResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.SpReceivingSELBuyerClassifierResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TruckNoTextBox
        '
        '
        '
        '
        Me.TruckNoTextBox.CustomButton.Image = Nothing
        Me.TruckNoTextBox.CustomButton.Location = New System.Drawing.Point(130, 2)
        Me.TruckNoTextBox.CustomButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TruckNoTextBox.CustomButton.Name = ""
        Me.TruckNoTextBox.CustomButton.Size = New System.Drawing.Size(17, 19)
        Me.TruckNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextBox.CustomButton.TabIndex = 1
        Me.TruckNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextBox.CustomButton.UseSelectable = True
        Me.TruckNoTextBox.CustomButton.Visible = False
        Me.TruckNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckNoTextBox.Lines = New String(-1) {}
        Me.TruckNoTextBox.Location = New System.Drawing.Point(2, 100)
        Me.TruckNoTextBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TruckNoTextBox.MaxLength = 32767
        Me.TruckNoTextBox.Name = "TruckNoTextBox"
        Me.TruckNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        'Me.TruckNoTextBox.PromptText = "Truck no (STEC-FC-BU, LTL and Other)"
        Me.TruckNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextBox.SelectedText = ""
        Me.TruckNoTextBox.SelectionLength = 0
        Me.TruckNoTextBox.SelectionStart = 0
        Me.TruckNoTextBox.ShortcutsEnabled = True
        Me.TruckNoTextBox.Size = New System.Drawing.Size(200, 28)
        Me.TruckNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.TruckNoTextBox.TabIndex = 6
        Me.TruckNoTextBox.UseCustomBackColor = True
        Me.TruckNoTextBox.UseSelectable = True
        Me.TruckNoTextBox.UseStyleColors = True
        Me.TruckNoTextBox.Visible = False
        Me.TruckNoTextBox.WaterMark = "Truck no (STEC-FC-BU, LTL and Other)"
        Me.TruckNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'CompanyComboBox
        '
        Me.CompanyComboBox.DataSource = Me.CompanyBindingSource
        Me.CompanyComboBox.DisplayMember = "name"
        Me.CompanyComboBox.FormattingEnabled = True
        Me.CompanyComboBox.ItemHeight = 23
        Me.CompanyComboBox.Location = New System.Drawing.Point(2, 2)
        Me.CompanyComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CompanyComboBox.Name = "CompanyComboBox"
        Me.CompanyComboBox.PromptText = "Company"
        Me.CompanyComboBox.Size = New System.Drawing.Size(188, 29)
        Me.CompanyComboBox.TabIndex = 129
        Me.CompanyComboBox.UseSelectable = True
        Me.CompanyComboBox.ValueMember = "code"
        '
        'CompanyBindingSource
        '
        Me.CompanyBindingSource.DataSource = GetType(ReceivingSystem.company)
        '
        'TransportDocNoTextBox
        '
        Me.TransportDocNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TransportDocNoTextBox.CustomButton.Image = Nothing
        Me.TransportDocNoTextBox.CustomButton.Location = New System.Drawing.Point(130, 2)
        Me.TransportDocNoTextBox.CustomButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TransportDocNoTextBox.CustomButton.Name = ""
        Me.TransportDocNoTextBox.CustomButton.Size = New System.Drawing.Size(17, 19)
        Me.TransportDocNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TransportDocNoTextBox.CustomButton.TabIndex = 1
        Me.TransportDocNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TransportDocNoTextBox.CustomButton.UseSelectable = True
        Me.TransportDocNoTextBox.CustomButton.Visible = False
        Me.TransportDocNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TransportDocNoTextBox.Lines = New String(-1) {}
        Me.TransportDocNoTextBox.Location = New System.Drawing.Point(2, 2)
        Me.TransportDocNoTextBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TransportDocNoTextBox.MaxLength = 32767
        Me.TransportDocNoTextBox.Name = "TransportDocNoTextBox"
        Me.TransportDocNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        'Me.TransportDocNoTextBox.PromptText = "Transport code (STEC-BU)"
        Me.TransportDocNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TransportDocNoTextBox.SelectedText = ""
        Me.TransportDocNoTextBox.SelectionLength = 0
        Me.TransportDocNoTextBox.SelectionStart = 0
        Me.TransportDocNoTextBox.ShortcutsEnabled = True
        Me.TransportDocNoTextBox.Size = New System.Drawing.Size(200, 28)
        Me.TransportDocNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.TransportDocNoTextBox.TabIndex = 2
        Me.TransportDocNoTextBox.UseCustomBackColor = True
        Me.TransportDocNoTextBox.UseSelectable = True
        Me.TransportDocNoTextBox.UseStyleColors = True
        Me.TransportDocNoTextBox.Visible = False
        Me.TransportDocNoTextBox.WaterMark = "Transport code (STEC-BU)"
        Me.TransportDocNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TransportDocNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TotalBalesTextBox
        '
        Me.TotalBalesTextBox.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TotalBalesTextBox.CustomButton.Image = Nothing
        Me.TotalBalesTextBox.CustomButton.Location = New System.Drawing.Point(130, 2)
        Me.TotalBalesTextBox.CustomButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TotalBalesTextBox.CustomButton.Name = ""
        Me.TotalBalesTextBox.CustomButton.Size = New System.Drawing.Size(17, 19)
        Me.TotalBalesTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TotalBalesTextBox.CustomButton.TabIndex = 1
        Me.TotalBalesTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TotalBalesTextBox.CustomButton.UseSelectable = True
        Me.TotalBalesTextBox.CustomButton.Visible = False
        Me.TotalBalesTextBox.Enabled = False
        Me.TotalBalesTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TotalBalesTextBox.Lines = New String(-1) {}
        Me.TotalBalesTextBox.Location = New System.Drawing.Point(2, 68)
        Me.TotalBalesTextBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TotalBalesTextBox.MaxLength = 32767
        Me.TotalBalesTextBox.Name = "TotalBalesTextBox"
        Me.TotalBalesTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        'Me.TotalBalesTextBox.PromptText = "Total bales (STEC-BU, LTL)"
        Me.TotalBalesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TotalBalesTextBox.SelectedText = ""
        Me.TotalBalesTextBox.SelectionLength = 0
        Me.TotalBalesTextBox.SelectionStart = 0
        Me.TotalBalesTextBox.ShortcutsEnabled = True
        Me.TotalBalesTextBox.Size = New System.Drawing.Size(200, 28)
        Me.TotalBalesTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.TotalBalesTextBox.TabIndex = 8
        Me.TotalBalesTextBox.UseCustomBackColor = True
        Me.TotalBalesTextBox.UseCustomForeColor = True
        Me.TotalBalesTextBox.UseSelectable = True
        Me.TotalBalesTextBox.UseStyleColors = True
        Me.TotalBalesTextBox.Visible = False
        Me.TotalBalesTextBox.WaterMark = "Total bales (STEC-BU, LTL)"
        Me.TotalBalesTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TotalBalesTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'InvoiceCombobox
        '
        Me.InvoiceCombobox.DataSource = Me.SpReceivingSELInvoiceDocNoResultBindingSource
        Me.InvoiceCombobox.DisplayFocus = True
        Me.InvoiceCombobox.DisplayMember = "InvoiceNo"
        Me.InvoiceCombobox.FormattingEnabled = True
        Me.InvoiceCombobox.ItemHeight = 23
        Me.InvoiceCombobox.Location = New System.Drawing.Point(2, 34)
        Me.InvoiceCombobox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.InvoiceCombobox.Name = "InvoiceCombobox"
        Me.InvoiceCombobox.PromptText = "Packlist Invoice# (LTL)."
        Me.InvoiceCombobox.Size = New System.Drawing.Size(200, 29)
        Me.InvoiceCombobox.Style = MetroFramework.MetroColorStyle.Orange
        Me.InvoiceCombobox.TabIndex = 4
        Me.InvoiceCombobox.UseSelectable = True
        Me.InvoiceCombobox.ValueMember = "InvoiceNo"
        Me.InvoiceCombobox.Visible = False
        '
        'SpReceivingSELInvoiceDocNoResultBindingSource
        '
        Me.SpReceivingSELInvoiceDocNoResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_InvoiceDocNo_Result)
        '
        'ReplaceBalesCheckbox
        '
        Me.ReplaceBalesCheckbox.AutoSize = True
        Me.ReplaceBalesCheckbox.BackColor = System.Drawing.Color.White
        Me.ReplaceBalesCheckbox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.ReplaceBalesCheckbox.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold
        Me.ReplaceBalesCheckbox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ReplaceBalesCheckbox.Location = New System.Drawing.Point(2, 245)
        Me.ReplaceBalesCheckbox.Margin = New System.Windows.Forms.Padding(2, 8, 2, 2)
        Me.ReplaceBalesCheckbox.Name = "ReplaceBalesCheckbox"
        Me.ReplaceBalesCheckbox.Size = New System.Drawing.Size(171, 19)
        Me.ReplaceBalesCheckbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.ReplaceBalesCheckbox.TabIndex = 140
        Me.ReplaceBalesCheckbox.Text = "Replace bale barcode"
        Me.ReplaceBalesCheckbox.UseCustomBackColor = True
        Me.ReplaceBalesCheckbox.UseCustomForeColor = True
        Me.ReplaceBalesCheckbox.UseSelectable = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.CompanyComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.TypeComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.SubtypeComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.RcFromComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.PlaceComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.BuyerComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.ClassifierComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.ReplaceBalesCheckbox)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(2, 2)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(192, 266)
        Me.FlowLayoutPanel1.TabIndex = 141
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource
        Me.TypeComboBox.DisplayMember = "type1"
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 23
        Me.TypeComboBox.Location = New System.Drawing.Point(2, 35)
        Me.TypeComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.PromptText = "Type"
        Me.TypeComboBox.Size = New System.Drawing.Size(188, 29)
        Me.TypeComboBox.TabIndex = 126
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type1"
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataSource = GetType(ReceivingSystem.type)
        '
        'SubtypeComboBox
        '
        Me.SubtypeComboBox.DataSource = Me.SubTypeBindingSource
        Me.SubtypeComboBox.DisplayMember = "subtype1"
        Me.SubtypeComboBox.FormattingEnabled = True
        Me.SubtypeComboBox.ItemHeight = 23
        Me.SubtypeComboBox.Location = New System.Drawing.Point(2, 69)
        Me.SubtypeComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.SubtypeComboBox.Name = "SubtypeComboBox"
        Me.SubtypeComboBox.PromptText = "sub type"
        Me.SubtypeComboBox.Size = New System.Drawing.Size(188, 29)
        Me.SubtypeComboBox.TabIndex = 131
        Me.SubtypeComboBox.UseSelectable = True
        Me.SubtypeComboBox.ValueMember = "subtype1"
        '
        'SubTypeBindingSource
        '
        Me.SubTypeBindingSource.DataSource = GetType(ReceivingSystem.subtype)
        '
        'RcFromComboBox
        '
        Me.RcFromComboBox.DisplayMember = "type"
        Me.RcFromComboBox.FormattingEnabled = True
        Me.RcFromComboBox.ItemHeight = 23
        Me.RcFromComboBox.Items.AddRange(New Object() {"Purchasing", "Free"})
        Me.RcFromComboBox.Location = New System.Drawing.Point(2, 103)
        Me.RcFromComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.RcFromComboBox.Name = "RcFromComboBox"
        Me.RcFromComboBox.PromptText = "rcfrom"
        Me.RcFromComboBox.Size = New System.Drawing.Size(188, 29)
        Me.RcFromComboBox.TabIndex = 127
        Me.RcFromComboBox.UseSelectable = True
        Me.RcFromComboBox.ValueMember = "type"
        '
        'PlaceComboBox
        '
        Me.PlaceComboBox.DataSource = Me.SpReceivingSELMatWHResultBindingSource
        Me.PlaceComboBox.DisplayMember = "wh"
        Me.PlaceComboBox.FormattingEnabled = True
        Me.PlaceComboBox.ItemHeight = 23
        Me.PlaceComboBox.Location = New System.Drawing.Point(2, 137)
        Me.PlaceComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.PlaceComboBox.Name = "PlaceComboBox"
        Me.PlaceComboBox.PromptText = "place"
        Me.PlaceComboBox.Size = New System.Drawing.Size(188, 29)
        Me.PlaceComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.PlaceComboBox.TabIndex = 122
        Me.PlaceComboBox.UseSelectable = True
        Me.PlaceComboBox.ValueMember = "wh"
        '
        'SpReceivingSELMatWHResultBindingSource
        '
        Me.SpReceivingSELMatWHResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_MatWH_Result)
        '
        'BuyerComboBox
        '
        Me.BuyerComboBox.DisplayMember = "name"
        Me.BuyerComboBox.FormattingEnabled = True
        Me.BuyerComboBox.ItemHeight = 23
        Me.BuyerComboBox.Location = New System.Drawing.Point(2, 171)
        Me.BuyerComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.BuyerComboBox.Name = "BuyerComboBox"
        Me.BuyerComboBox.PromptText = "buyer"
        Me.BuyerComboBox.Size = New System.Drawing.Size(188, 29)
        Me.BuyerComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.BuyerComboBox.TabIndex = 142
        Me.BuyerComboBox.UseSelectable = True
        Me.BuyerComboBox.ValueMember = "name"
        '
        'ClassifierComboBox
        '
        Me.ClassifierComboBox.DisplayMember = "name"
        Me.ClassifierComboBox.FormattingEnabled = True
        Me.ClassifierComboBox.ItemHeight = 23
        Me.ClassifierComboBox.Location = New System.Drawing.Point(2, 205)
        Me.ClassifierComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.ClassifierComboBox.Name = "ClassifierComboBox"
        Me.ClassifierComboBox.PromptText = "classifier"
        Me.ClassifierComboBox.Size = New System.Drawing.Size(188, 29)
        Me.ClassifierComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ClassifierComboBox.TabIndex = 124
        Me.ClassifierComboBox.UseSelectable = True
        Me.ClassifierComboBox.ValueMember = "name"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.TransportDocNoTextBox)
        Me.FlowLayoutPanel2.Controls.Add(Me.InvoiceCombobox)
        Me.FlowLayoutPanel2.Controls.Add(Me.TotalBalesTextBox)
        Me.FlowLayoutPanel2.Controls.Add(Me.TruckNoTextBox)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(198, 2)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(204, 130)
        Me.FlowLayoutPanel2.TabIndex = 142
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.SaveButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.ClearButton)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(3, 277)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(106, 32)
        Me.FlowLayoutPanel6.TabIndex = 149
        '
        'SaveButton
        '
        Me.SaveButton.AutoSize = True
        Me.SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SaveButton.BackColor = System.Drawing.Color.DarkOrange
        Me.SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SaveButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveButton.FlatAppearance.BorderSize = 0
        Me.SaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SaveButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.ForeColor = System.Drawing.Color.Black
        Me.SaveButton.Location = New System.Drawing.Point(3, 3)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(46, 26)
        Me.SaveButton.TabIndex = 145
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'ClearButton
        '
        Me.ClearButton.AutoSize = True
        Me.ClearButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClearButton.BackColor = System.Drawing.Color.Gainsboro
        Me.ClearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClearButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearButton.FlatAppearance.BorderSize = 0
        Me.ClearButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ClearButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClearButton.ForeColor = System.Drawing.Color.Black
        Me.ClearButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ClearButton.Location = New System.Drawing.Point(55, 3)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(48, 26)
        Me.ClearButton.TabIndex = 146
        Me.ClearButton.Text = "Clear"
        Me.ClearButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ClearButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(2, 2)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(404, 270)
        Me.FlowLayoutPanel3.TabIndex = 150
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.Controls.Add(Me.FlowLayoutPanel3)
        Me.FlowLayoutPanel4.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(15, 60)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(738, 569)
        Me.FlowLayoutPanel4.TabIndex = 151
        '
        'SpReceivingSELBuyerClassifierResultBindingSource
        '
        Me.SpReceivingSELBuyerClassifierResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_BuyerClassifier_Result)
        '
        'FrmAddReceivingNo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(768, 645)
        Me.Controls.Add(Me.FlowLayoutPanel4)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmAddReceivingNo"
        Me.Padding = New System.Windows.Forms.Padding(15, 60, 15, 16)
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Create a new receiving no."
        CType(Me.CompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpReceivingSELInvoiceDocNoResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SubTypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpReceivingSELMatWHResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        CType(Me.SpReceivingSELBuyerClassifierResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TruckNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents CompanyComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TransportDocNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents TotalBalesTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents InvoiceCombobox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ReplaceBalesCheckbox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents SaveButton As Button
    Friend WithEvents ClearButton As Button
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents SubtypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents RcFromComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents PlaceComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ClassifierComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents CompanyBindingSource As BindingSource
    Friend WithEvents SpReceivingSELMatWHResultBindingSource As BindingSource
    Friend WithEvents TypeBindingSource As BindingSource
    Friend WithEvents SubTypeBindingSource As BindingSource
    Friend WithEvents SpReceivingSELBuyerClassifierResultBindingSource As BindingSource
    Friend WithEvents SpReceivingSELInvoiceDocNoResultBindingSource As BindingSource
    Friend WithEvents BuyerComboBox As MetroFramework.Controls.MetroComboBox
End Class
