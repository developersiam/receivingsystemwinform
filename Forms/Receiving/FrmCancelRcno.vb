﻿Public Class FrmCancelRcno
    Inherits MetroFramework.Forms.MetroForm
    Public matrc As sp_Receiving_SEL_MatRC_Result
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmCancelRcno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        rcnoLabel.Text = matrc.rcno
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If matrc Is Nothing Then
                MessageBox.Show("ไม่พบเลข receiving number นี้ กรุณาตรวจสอบอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If matrc.checkerlocked = True Then
                MessageBox.Show("เลข receiving number นี้ checker ได้ทำการ lock ข้อมูลแล้วจึงไม่สามารถลบข้อมูลได้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If matrc.CountOfBale > 0 Then
                MessageBox.Show("เลข receiving number นี้ได้ทำการรับเข้าใบยาแล้ว กรุณาลบในหน้าชั่งน้ำหนักห่อยา", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If CancelRcnoRemarkTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุหมายเหตุที่ต้องการยกเลิก Receiving number นี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                CancelRcnoRemarkTextBox.Focus()
                Return
            End If

            If MessageBox.Show("ต้องการ Delete receiving number " & matrc.rcno & " ใช่หรือไม่ ?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.No Then
                Return
            End If

            db.sp_Receiving_DEL_MatRC(matrc.rcno, "Confirm to delete Rc no :" & matrc.rcno, _username, CancelRcnoRemarkTextBox.Text)
            MessageBox.Show("ลบข้อมูล receiving number นี้แล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        CancelRcnoRemarkTextBox.Text = ""
        CancelRcnoRemarkTextBox.Focus()
    End Sub
End Class