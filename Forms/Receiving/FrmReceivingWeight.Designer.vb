﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmReceivingWeight
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.UsernameLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.LTLRemarkResultLabel = New MetroFramework.Controls.MetroLabel()
        Me.NTRMInspectionButton = New System.Windows.Forms.Button()
        Me.ChangeStaffUserButton = New System.Windows.Forms.Button()
        Me.RejectButton = New System.Windows.Forms.Button()
        Me.FinishButton = New System.Windows.Forms.Button()
        Me.SetupDigitalScaleButton = New System.Windows.Forms.Button()
        Me.ReplaceBalesCheckbox = New MetroFramework.Controls.MetroCheckBox()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.DeleteButton = New System.Windows.Forms.Button()
        Me.ChangeUserButton = New MetroFramework.Controls.MetroTile()
        Me.NTRMFromBuyingAlertLabel = New MetroFramework.Controls.MetroLabel()
        Me.DiffWeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel36 = New MetroFramework.Controls.MetroLabel()
        Me.TotalBuyingWeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.TotalReceivedWeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.TotalBaleLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.DigitalWeightCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.ManualInputCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.MetroLabel49 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel48 = New MetroFramework.Controls.MetroLabel()
        Me.InvoiceNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.TransportationDocumentCodeLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.ClassifierComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.RcLinesLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.BaleTypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.RcfromLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel21 = New MetroFramework.Controls.MetroLabel()
        Me.CompanyLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.PlaceLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.TrucknoLabel = New MetroFramework.Controls.MetroLabel()
        Me.TxtLTLCartons = New MetroFramework.Controls.MetroTextBox()
        Me.ChkFromLTLCarton = New MetroFramework.Controls.MetroCheckBox()
        Me.DigitalWeightPanel = New MetroFramework.Controls.MetroPanel()
        Me.rightValue = New MetroFramework.Controls.MetroLabel()
        Me.leftValue = New MetroFramework.Controls.MetroLabel()
        Me.DigitalWeightBackTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile5 = New MetroFramework.Controls.MetroTile()
        Me.WeightFromDigitalScaleTextBox = New System.Windows.Forms.TextBox()
        Me.SaveFromDigitalTile = New MetroFramework.Controls.MetroTile()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.ReceivingDataGrid = New MetroFramework.Controls.MetroGrid()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.farmer_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.baleno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.supplier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.green = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classify = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weightbuy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WeightDiffDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemarkChecked = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classifier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReplaceBales = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.LockedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.LeaflockedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DocnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiveduserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtrecordDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpReceivingSELMatByRcNoResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel17 = New MetroFramework.Controls.MetroLabel()
        Me.SaveMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel18 = New MetroFramework.Controls.MetroLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LXCountLabel = New MetroFramework.Controls.MetroLabel()
        Me.LXBatchLabel = New MetroFramework.Controls.MetroLabel()
        Me.LXPrintTile = New MetroFramework.Controls.MetroTile()
        Me.LXCloseTile = New MetroFramework.Controls.MetroTile()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.XCountLabel = New MetroFramework.Controls.MetroLabel()
        Me.XBatchLabel = New MetroFramework.Controls.MetroLabel()
        Me.XPrintTile = New MetroFramework.Controls.MetroTile()
        Me.XCloseTile = New MetroFramework.Controls.MetroTile()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CCountLabel = New MetroFramework.Controls.MetroLabel()
        Me.CBatchLabel = New MetroFramework.Controls.MetroLabel()
        Me.CPrintTile = New MetroFramework.Controls.MetroTile()
        Me.CCloseTile = New MetroFramework.Controls.MetroTile()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.BTCountLabel = New MetroFramework.Controls.MetroLabel()
        Me.BTBatchLabel = New MetroFramework.Controls.MetroLabel()
        Me.BTPrintTile = New MetroFramework.Controls.MetroTile()
        Me.BTCloseTile = New MetroFramework.Controls.MetroTile()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.FarmerLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel32 = New MetroFramework.Controls.MetroLabel()
        Me.CountMoistureLabel = New MetroFramework.Controls.MetroLabel()
        Me.RemainingTruckReceivingLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel50 = New MetroFramework.Controls.MetroLabel()
        Me.TotalBaleOnTruckLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel54 = New MetroFramework.Controls.MetroLabel()
        Me.BtnLTLStart = New System.Windows.Forms.Button()
        Me.BtnLTLComplete = New System.Windows.Forms.Button()
        Me.MetroLabel52 = New MetroFramework.Controls.MetroLabel()
        Me.Save = New MetroFramework.Controls.MetroTile()
        Me.ReceivingDetailGroupBox = New System.Windows.Forms.GroupBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.BuyerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel27 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel25 = New MetroFramework.Controls.MetroLabel()
        Me.TypeLabel = New MetroFramework.Controls.MetroLabel()
        Me.rcnoLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel22 = New MetroFramework.Controls.MetroLabel()
        Me.CropLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.FromLTLCartonGroupBox = New System.Windows.Forms.GroupBox()
        Me.SummaryReceivingGroupBox = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.VieeTransportDetailButton = New System.Windows.Forms.Button()
        Me.ClassifyGradeComboBox = New System.Windows.Forms.ComboBox()
        Me.GreenGradeComboBox = New System.Windows.Forms.ComboBox()
        Me.BaleBarcodeTextBox = New System.Windows.Forms.TextBox()
        Me.BaleNumberTextBox = New System.Windows.Forms.TextBox()
        Me.SupplierComboBox = New System.Windows.Forms.ComboBox()
        Me.BuyingWeightTextBox = New System.Windows.Forms.TextBox()
        Me.ReceivingWeightTextBox = New System.Windows.Forms.TextBox()
        Me.NPIFarmerCodeTextBox = New System.Windows.Forms.TextBox()
        Me.NPIFarmerCodeLabel = New MetroFramework.Controls.MetroLabel()
        Me.SpReceivingSELBuyerClassifierResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.IsCUTRAGCheckBox = New System.Windows.Forms.CheckBox()
        Me.MetroPanel1.SuspendLayout()
        Me.DigitalWeightPanel.SuspendLayout()
        CType(Me.ReceivingDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpReceivingSELMatByRcNoResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.ReceivingDetailGroupBox.SuspendLayout()
        Me.FromLTLCartonGroupBox.SuspendLayout()
        Me.SummaryReceivingGroupBox.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.SpReceivingSELBuyerClassifierResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'UsernameLabel
        '
        Me.UsernameLabel.AutoSize = True
        Me.UsernameLabel.Location = New System.Drawing.Point(43, 17)
        Me.UsernameLabel.Name = "UsernameLabel"
        Me.UsernameLabel.Size = New System.Drawing.Size(68, 19)
        Me.UsernameLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameLabel.TabIndex = 112
        Me.UsernameLabel.Text = "Username"
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.LTLRemarkResultLabel)
        Me.MetroPanel1.Controls.Add(Me.NTRMInspectionButton)
        Me.MetroPanel1.Controls.Add(Me.ChangeStaffUserButton)
        Me.MetroPanel1.Controls.Add(Me.RejectButton)
        Me.MetroPanel1.Controls.Add(Me.FinishButton)
        Me.MetroPanel1.Controls.Add(Me.SetupDigitalScaleButton)
        Me.MetroPanel1.Controls.Add(Me.ReplaceBalesCheckbox)
        Me.MetroPanel1.Controls.Add(Me.RefreshButton)
        Me.MetroPanel1.Controls.Add(Me.DeleteButton)
        Me.MetroPanel1.Controls.Add(Me.ChangeUserButton)
        Me.MetroPanel1.Controls.Add(Me.UsernameLabel)
        Me.MetroPanel1.Controls.Add(Me.NTRMFromBuyingAlertLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(17, 23)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1269, 58)
        Me.MetroPanel1.TabIndex = 116
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'MetroLabel4
        '
        Me.MetroLabel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel4.ForeColor = System.Drawing.Color.Red
        Me.MetroLabel4.Location = New System.Drawing.Point(642, 7)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(163, 16)
        Me.MetroLabel4.TabIndex = 182
        Me.MetroLabel4.Text = "LTL BrownTag Replace :>"
        Me.MetroLabel4.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.MetroLabel4.UseCustomBackColor = True
        Me.MetroLabel4.UseCustomForeColor = True
        '
        'LTLRemarkResultLabel
        '
        Me.LTLRemarkResultLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LTLRemarkResultLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.LTLRemarkResultLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.LTLRemarkResultLabel.ForeColor = System.Drawing.Color.Red
        Me.LTLRemarkResultLabel.Location = New System.Drawing.Point(801, 7)
        Me.LTLRemarkResultLabel.Name = "LTLRemarkResultLabel"
        Me.LTLRemarkResultLabel.Size = New System.Drawing.Size(454, 16)
        Me.LTLRemarkResultLabel.TabIndex = 181
        Me.LTLRemarkResultLabel.Text = "LTL BrownTag Replace"
        Me.LTLRemarkResultLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.LTLRemarkResultLabel.UseCustomBackColor = True
        Me.LTLRemarkResultLabel.UseCustomForeColor = True
        Me.LTLRemarkResultLabel.Visible = False
        '
        'NTRMInspectionButton
        '
        Me.NTRMInspectionButton.AutoSize = True
        Me.NTRMInspectionButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.NTRMInspectionButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.NTRMInspectionButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.NTRMInspectionButton.FlatAppearance.BorderSize = 0
        Me.NTRMInspectionButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.NTRMInspectionButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.NTRMInspectionButton.Font = New System.Drawing.Font("Tahoma", 7.0!, System.Drawing.FontStyle.Bold)
        Me.NTRMInspectionButton.ForeColor = System.Drawing.Color.Black
        Me.NTRMInspectionButton.Image = Global.ReceivingSystem.My.Resources.Resources.PurchaseOrder321
        Me.NTRMInspectionButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.NTRMInspectionButton.Location = New System.Drawing.Point(584, 3)
        Me.NTRMInspectionButton.Name = "NTRMInspectionButton"
        Me.NTRMInspectionButton.Size = New System.Drawing.Size(52, 51)
        Me.NTRMInspectionButton.TabIndex = 180
        Me.NTRMInspectionButton.Text = "NTRM"
        Me.NTRMInspectionButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.NTRMInspectionButton.UseVisualStyleBackColor = False
        '
        'ChangeStaffUserButton
        '
        Me.ChangeStaffUserButton.AutoSize = True
        Me.ChangeStaffUserButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ChangeStaffUserButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ChangeStaffUserButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ChangeStaffUserButton.FlatAppearance.BorderSize = 0
        Me.ChangeStaffUserButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ChangeStaffUserButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ChangeStaffUserButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.ChangeStaffUserButton.ForeColor = System.Drawing.Color.Black
        Me.ChangeStaffUserButton.Image = Global.ReceivingSystem.My.Resources.Resources.Collaborator32
        Me.ChangeStaffUserButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ChangeStaffUserButton.Location = New System.Drawing.Point(249, 3)
        Me.ChangeStaffUserButton.Name = "ChangeStaffUserButton"
        Me.ChangeStaffUserButton.Size = New System.Drawing.Size(57, 51)
        Me.ChangeStaffUserButton.TabIndex = 179
        Me.ChangeStaffUserButton.Text = "change"
        Me.ChangeStaffUserButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ChangeStaffUserButton.UseVisualStyleBackColor = True
        '
        'RejectButton
        '
        Me.RejectButton.AutoSize = True
        Me.RejectButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RejectButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.RejectButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RejectButton.FlatAppearance.BorderSize = 0
        Me.RejectButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.RejectButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RejectButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RejectButton.ForeColor = System.Drawing.Color.Black
        Me.RejectButton.Image = Global.ReceivingSystem.My.Resources.Resources.TopicMoved32
        Me.RejectButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.RejectButton.Location = New System.Drawing.Point(308, 3)
        Me.RejectButton.Name = "RejectButton"
        Me.RejectButton.Size = New System.Drawing.Size(52, 51)
        Me.RejectButton.TabIndex = 178
        Me.RejectButton.Text = "reject"
        Me.RejectButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.RejectButton.UseVisualStyleBackColor = True
        '
        'FinishButton
        '
        Me.FinishButton.AutoSize = True
        Me.FinishButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.FinishButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.FinishButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FinishButton.FlatAppearance.BorderSize = 0
        Me.FinishButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.FinishButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.FinishButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.FinishButton.ForeColor = System.Drawing.Color.Black
        Me.FinishButton.Image = Global.ReceivingSystem.My.Resources.Resources.Lock32
        Me.FinishButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.FinishButton.Location = New System.Drawing.Point(362, 3)
        Me.FinishButton.Name = "FinishButton"
        Me.FinishButton.Size = New System.Drawing.Size(52, 51)
        Me.FinishButton.TabIndex = 177
        Me.FinishButton.Text = "finish"
        Me.FinishButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.FinishButton.UseVisualStyleBackColor = True
        '
        'SetupDigitalScaleButton
        '
        Me.SetupDigitalScaleButton.AutoSize = True
        Me.SetupDigitalScaleButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SetupDigitalScaleButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SetupDigitalScaleButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SetupDigitalScaleButton.FlatAppearance.BorderSize = 0
        Me.SetupDigitalScaleButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SetupDigitalScaleButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SetupDigitalScaleButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.SetupDigitalScaleButton.ForeColor = System.Drawing.Color.Black
        Me.SetupDigitalScaleButton.Image = Global.ReceivingSystem.My.Resources.Resources.Scale32
        Me.SetupDigitalScaleButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.SetupDigitalScaleButton.Location = New System.Drawing.Point(527, 3)
        Me.SetupDigitalScaleButton.Name = "SetupDigitalScaleButton"
        Me.SetupDigitalScaleButton.Size = New System.Drawing.Size(55, 51)
        Me.SetupDigitalScaleButton.TabIndex = 176
        Me.SetupDigitalScaleButton.Text = "setting"
        Me.SetupDigitalScaleButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.SetupDigitalScaleButton.UseVisualStyleBackColor = True
        '
        'ReplaceBalesCheckbox
        '
        Me.ReplaceBalesCheckbox.AutoSize = True
        Me.ReplaceBalesCheckbox.BackColor = System.Drawing.Color.White
        Me.ReplaceBalesCheckbox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.ReplaceBalesCheckbox.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold
        Me.ReplaceBalesCheckbox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ReplaceBalesCheckbox.Location = New System.Drawing.Point(642, 35)
        Me.ReplaceBalesCheckbox.Name = "ReplaceBalesCheckbox"
        Me.ReplaceBalesCheckbox.Size = New System.Drawing.Size(171, 19)
        Me.ReplaceBalesCheckbox.Style = MetroFramework.MetroColorStyle.Lime
        Me.ReplaceBalesCheckbox.TabIndex = 128
        Me.ReplaceBalesCheckbox.Text = "Replace bale barcode"
        Me.ReplaceBalesCheckbox.UseCustomBackColor = True
        Me.ReplaceBalesCheckbox.UseCustomForeColor = True
        Me.ReplaceBalesCheckbox.UseSelectable = True
        '
        'RefreshButton
        '
        Me.RefreshButton.AutoSize = True
        Me.RefreshButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RefreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.RefreshButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshButton.FlatAppearance.BorderSize = 0
        Me.RefreshButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RefreshButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RefreshButton.ForeColor = System.Drawing.Color.Black
        Me.RefreshButton.Image = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.RefreshButton.Location = New System.Drawing.Point(416, 3)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(55, 51)
        Me.RefreshButton.TabIndex = 175
        Me.RefreshButton.Text = "refresh"
        Me.RefreshButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.RefreshButton.UseVisualStyleBackColor = True
        '
        'DeleteButton
        '
        Me.DeleteButton.AutoSize = True
        Me.DeleteButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DeleteButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.DeleteButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteButton.FlatAppearance.BorderSize = 0
        Me.DeleteButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.DeleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.DeleteButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.DeleteButton.ForeColor = System.Drawing.Color.Black
        Me.DeleteButton.Image = Global.ReceivingSystem.My.Resources.Resources.Cancel32
        Me.DeleteButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DeleteButton.Location = New System.Drawing.Point(473, 3)
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(52, 51)
        Me.DeleteButton.TabIndex = 174
        Me.DeleteButton.Text = "delete"
        Me.DeleteButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.DeleteButton.UseVisualStyleBackColor = True
        '
        'ChangeUserButton
        '
        Me.ChangeUserButton.ActiveControl = Nothing
        Me.ChangeUserButton.AutoSize = True
        Me.ChangeUserButton.BackColor = System.Drawing.Color.White
        Me.ChangeUserButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ChangeUserButton.Location = New System.Drawing.Point(9, 13)
        Me.ChangeUserButton.Name = "ChangeUserButton"
        Me.ChangeUserButton.Size = New System.Drawing.Size(28, 28)
        Me.ChangeUserButton.Style = MetroFramework.MetroColorStyle.White
        Me.ChangeUserButton.TabIndex = 111
        Me.ChangeUserButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ChangeUserButton.TileImage = Global.ReceivingSystem.My.Resources.Resources.icons8_contacts_32
        Me.ChangeUserButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ChangeUserButton.UseSelectable = True
        Me.ChangeUserButton.UseTileImage = True
        '
        'NTRMFromBuyingAlertLabel
        '
        Me.NTRMFromBuyingAlertLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.NTRMFromBuyingAlertLabel.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.NTRMFromBuyingAlertLabel.ForeColor = System.Drawing.Color.White
        Me.NTRMFromBuyingAlertLabel.Location = New System.Drawing.Point(820, 29)
        Me.NTRMFromBuyingAlertLabel.Name = "NTRMFromBuyingAlertLabel"
        Me.NTRMFromBuyingAlertLabel.Size = New System.Drawing.Size(132, 25)
        Me.NTRMFromBuyingAlertLabel.TabIndex = 158
        Me.NTRMFromBuyingAlertLabel.Text = "พบข้อมูล NTRM !!"
        Me.NTRMFromBuyingAlertLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.NTRMFromBuyingAlertLabel.UseCustomBackColor = True
        Me.NTRMFromBuyingAlertLabel.UseCustomForeColor = True
        Me.NTRMFromBuyingAlertLabel.Visible = False
        '
        'DiffWeightLabel
        '
        Me.DiffWeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DiffWeightLabel.Location = New System.Drawing.Point(114, 44)
        Me.DiffWeightLabel.Name = "DiffWeightLabel"
        Me.DiffWeightLabel.Size = New System.Drawing.Size(120, 23)
        Me.DiffWeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.DiffWeightLabel.TabIndex = 80
        Me.DiffWeightLabel.Text = "diff weight"
        Me.DiffWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel36
        '
        Me.MetroLabel36.AutoSize = True
        Me.MetroLabel36.Location = New System.Drawing.Point(6, 46)
        Me.MetroLabel36.Name = "MetroLabel36"
        Me.MetroLabel36.Size = New System.Drawing.Size(70, 19)
        Me.MetroLabel36.TabIndex = 79
        Me.MetroLabel36.Text = "diff weight"
        '
        'TotalBuyingWeightLabel
        '
        Me.TotalBuyingWeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalBuyingWeightLabel.Location = New System.Drawing.Point(114, 72)
        Me.TotalBuyingWeightLabel.Name = "TotalBuyingWeightLabel"
        Me.TotalBuyingWeightLabel.Size = New System.Drawing.Size(120, 23)
        Me.TotalBuyingWeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalBuyingWeightLabel.TabIndex = 78
        Me.TotalBuyingWeightLabel.Text = "total buying weight"
        Me.TotalBuyingWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(6, 72)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(90, 19)
        Me.MetroLabel12.TabIndex = 77
        Me.MetroLabel12.Text = "buying weight"
        '
        'TotalReceivedWeightLabel
        '
        Me.TotalReceivedWeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalReceivedWeightLabel.Location = New System.Drawing.Point(114, 100)
        Me.TotalReceivedWeightLabel.Name = "TotalReceivedWeightLabel"
        Me.TotalReceivedWeightLabel.Size = New System.Drawing.Size(120, 23)
        Me.TotalReceivedWeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalReceivedWeightLabel.TabIndex = 76
        Me.TotalReceivedWeightLabel.Text = "total received weight"
        Me.TotalReceivedWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TotalBaleLabel
        '
        Me.TotalBaleLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalBaleLabel.Location = New System.Drawing.Point(114, 16)
        Me.TotalBaleLabel.Name = "TotalBaleLabel"
        Me.TotalBaleLabel.Size = New System.Drawing.Size(120, 23)
        Me.TotalBaleLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalBaleLabel.TabIndex = 75
        Me.TotalBaleLabel.Text = "total bales"
        Me.TotalBaleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.Location = New System.Drawing.Point(6, 98)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(103, 19)
        Me.MetroLabel14.TabIndex = 74
        Me.MetroLabel14.Text = "receiving weight"
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.Location = New System.Drawing.Point(6, 20)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(39, 19)
        Me.MetroLabel16.TabIndex = 70
        Me.MetroLabel16.Text = "bales"
        '
        'DigitalWeightCheckBox
        '
        Me.DigitalWeightCheckBox.AutoSize = True
        Me.DigitalWeightCheckBox.FontWeight = MetroFramework.MetroCheckBoxWeight.Light
        Me.DigitalWeightCheckBox.Location = New System.Drawing.Point(110, 400)
        Me.DigitalWeightCheckBox.Name = "DigitalWeightCheckBox"
        Me.DigitalWeightCheckBox.Size = New System.Drawing.Size(81, 15)
        Me.DigitalWeightCheckBox.Style = MetroFramework.MetroColorStyle.Blue
        Me.DigitalWeightCheckBox.TabIndex = 75
        Me.DigitalWeightCheckBox.Text = "Digital scale"
        Me.DigitalWeightCheckBox.UseSelectable = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel1.Location = New System.Drawing.Point(631, 90)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(53, 15)
        Me.MetroLabel1.TabIndex = 74
        Me.MetroLabel1.Text = "Bale No."
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel7.Location = New System.Drawing.Point(1049, 90)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(48, 15)
        Me.MetroLabel7.TabIndex = 71
        Me.MetroLabel7.Text = "Rec kg."
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel8.Location = New System.Drawing.Point(266, 90)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(79, 15)
        Me.MetroLabel8.TabIndex = 70
        Me.MetroLabel8.Text = "Bale barcode"
        '
        'ManualInputCheckBox
        '
        Me.ManualInputCheckBox.AutoSize = True
        Me.ManualInputCheckBox.FontWeight = MetroFramework.MetroCheckBoxWeight.Light
        Me.ManualInputCheckBox.Location = New System.Drawing.Point(110, 377)
        Me.ManualInputCheckBox.Name = "ManualInputCheckBox"
        Me.ManualInputCheckBox.Size = New System.Drawing.Size(59, 15)
        Me.ManualInputCheckBox.Style = MetroFramework.MetroColorStyle.Blue
        Me.ManualInputCheckBox.TabIndex = 165
        Me.ManualInputCheckBox.Text = "Manual"
        Me.ManualInputCheckBox.UseSelectable = True
        '
        'MetroLabel49
        '
        Me.MetroLabel49.AutoSize = True
        Me.MetroLabel49.Location = New System.Drawing.Point(6, 205)
        Me.MetroLabel49.Name = "MetroLabel49"
        Me.MetroLabel49.Size = New System.Drawing.Size(71, 19)
        Me.MetroLabel49.TabIndex = 144
        Me.MetroLabel49.Text = "invoice no."
        '
        'MetroLabel48
        '
        Me.MetroLabel48.AutoSize = True
        Me.MetroLabel48.Location = New System.Drawing.Point(6, 178)
        Me.MetroLabel48.Name = "MetroLabel48"
        Me.MetroLabel48.Size = New System.Drawing.Size(96, 19)
        Me.MetroLabel48.TabIndex = 143
        Me.MetroLabel48.Text = "transport code"
        '
        'InvoiceNoLabel
        '
        Me.InvoiceNoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.InvoiceNoLabel.Location = New System.Drawing.Point(110, 205)
        Me.InvoiceNoLabel.Name = "InvoiceNoLabel"
        Me.InvoiceNoLabel.Size = New System.Drawing.Size(120, 23)
        Me.InvoiceNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.InvoiceNoLabel.TabIndex = 142
        Me.InvoiceNoLabel.Text = "invoice#"
        Me.InvoiceNoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TransportationDocumentCodeLabel
        '
        Me.TransportationDocumentCodeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TransportationDocumentCodeLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.TransportationDocumentCodeLabel.Location = New System.Drawing.Point(110, 178)
        Me.TransportationDocumentCodeLabel.Name = "TransportationDocumentCodeLabel"
        Me.TransportationDocumentCodeLabel.Size = New System.Drawing.Size(120, 23)
        Me.TransportationDocumentCodeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TransportationDocumentCodeLabel.TabIndex = 141
        Me.TransportationDocumentCodeLabel.Text = "transportCode"
        Me.TransportationDocumentCodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel3.Location = New System.Drawing.Point(6, 313)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(57, 19)
        Me.MetroLabel3.TabIndex = 140
        Me.MetroLabel3.Text = "classifier"
        Me.MetroLabel3.UseCustomBackColor = True
        Me.MetroLabel3.UseCustomForeColor = True
        '
        'ClassifierComboBox
        '
        Me.ClassifierComboBox.BackColor = System.Drawing.Color.White
        Me.ClassifierComboBox.DisplayMember = "name"
        Me.ClassifierComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.ClassifierComboBox.FormattingEnabled = True
        Me.ClassifierComboBox.ItemHeight = 19
        Me.ClassifierComboBox.Location = New System.Drawing.Point(110, 315)
        Me.ClassifierComboBox.Name = "ClassifierComboBox"
        Me.ClassifierComboBox.Size = New System.Drawing.Size(120, 25)
        Me.ClassifierComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ClassifierComboBox.TabIndex = 139
        Me.ClassifierComboBox.UseCustomBackColor = True
        Me.ClassifierComboBox.UseCustomForeColor = True
        Me.ClassifierComboBox.UseSelectable = True
        Me.ClassifierComboBox.ValueMember = "name"
        '
        'RcLinesLabel
        '
        Me.RcLinesLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RcLinesLabel.Location = New System.Drawing.Point(110, 259)
        Me.RcLinesLabel.Name = "RcLinesLabel"
        Me.RcLinesLabel.Size = New System.Drawing.Size(120, 23)
        Me.RcLinesLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.RcLinesLabel.TabIndex = 138
        Me.RcLinesLabel.Text = "rc lines"
        Me.RcLinesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(6, 259)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel9.TabIndex = 137
        Me.MetroLabel9.Text = "lines"
        '
        'MetroLabel19
        '
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.Location = New System.Drawing.Point(6, 286)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(63, 19)
        Me.MetroLabel19.TabIndex = 134
        Me.MetroLabel19.Text = "bale type"
        '
        'BaleTypeComboBox
        '
        Me.BaleTypeComboBox.BackColor = System.Drawing.Color.White
        Me.BaleTypeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.BaleTypeComboBox.FormattingEnabled = True
        Me.BaleTypeComboBox.ItemHeight = 19
        Me.BaleTypeComboBox.Items.AddRange(New Object() {"Open bales", "Close bales"})
        Me.BaleTypeComboBox.Location = New System.Drawing.Point(110, 286)
        Me.BaleTypeComboBox.Name = "BaleTypeComboBox"
        Me.BaleTypeComboBox.Size = New System.Drawing.Size(120, 25)
        Me.BaleTypeComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.BaleTypeComboBox.TabIndex = 133
        Me.BaleTypeComboBox.UseCustomBackColor = True
        Me.BaleTypeComboBox.UseCustomForeColor = True
        Me.BaleTypeComboBox.UseSelectable = True
        '
        'RcfromLabel
        '
        Me.RcfromLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RcfromLabel.Location = New System.Drawing.Point(110, 124)
        Me.RcfromLabel.Name = "RcfromLabel"
        Me.RcfromLabel.Size = New System.Drawing.Size(120, 23)
        Me.RcfromLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.RcfromLabel.TabIndex = 81
        Me.RcfromLabel.Text = "rcfrom"
        Me.RcfromLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel21
        '
        Me.MetroLabel21.AutoSize = True
        Me.MetroLabel21.Location = New System.Drawing.Point(6, 124)
        Me.MetroLabel21.Name = "MetroLabel21"
        Me.MetroLabel21.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel21.TabIndex = 80
        Me.MetroLabel21.Text = "rcfrom"
        '
        'CompanyLabel
        '
        Me.CompanyLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CompanyLabel.Location = New System.Drawing.Point(110, 97)
        Me.CompanyLabel.Name = "CompanyLabel"
        Me.CompanyLabel.Size = New System.Drawing.Size(120, 23)
        Me.CompanyLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CompanyLabel.TabIndex = 77
        Me.CompanyLabel.Text = "company"
        Me.CompanyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.Location = New System.Drawing.Point(6, 97)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(63, 19)
        Me.MetroLabel20.TabIndex = 76
        Me.MetroLabel20.Text = "company"
        '
        'PlaceLabel
        '
        Me.PlaceLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PlaceLabel.Location = New System.Drawing.Point(110, 151)
        Me.PlaceLabel.Name = "PlaceLabel"
        Me.PlaceLabel.Size = New System.Drawing.Size(120, 23)
        Me.PlaceLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.PlaceLabel.TabIndex = 73
        Me.PlaceLabel.Text = "place"
        Me.PlaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(6, 151)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(40, 19)
        Me.MetroLabel13.TabIndex = 72
        Me.MetroLabel13.Text = "place"
        '
        'TrucknoLabel
        '
        Me.TrucknoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TrucknoLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.TrucknoLabel.Location = New System.Drawing.Point(110, 232)
        Me.TrucknoLabel.Name = "TrucknoLabel"
        Me.TrucknoLabel.Size = New System.Drawing.Size(120, 23)
        Me.TrucknoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TrucknoLabel.TabIndex = 71
        Me.TrucknoLabel.Text = "truckNo"
        Me.TrucknoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxtLTLCartons
        '
        '
        '
        '
        Me.TxtLTLCartons.CustomButton.Image = Nothing
        Me.TxtLTLCartons.CustomButton.Location = New System.Drawing.Point(68, 1)
        Me.TxtLTLCartons.CustomButton.Name = ""
        Me.TxtLTLCartons.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.TxtLTLCartons.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxtLTLCartons.CustomButton.TabIndex = 1
        Me.TxtLTLCartons.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxtLTLCartons.CustomButton.UseSelectable = True
        Me.TxtLTLCartons.CustomButton.Visible = False
        Me.TxtLTLCartons.Enabled = False
        Me.TxtLTLCartons.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TxtLTLCartons.Lines = New String(-1) {}
        Me.TxtLTLCartons.Location = New System.Drawing.Point(144, 20)
        Me.TxtLTLCartons.MaxLength = 32767
        Me.TxtLTLCartons.Name = "TxtLTLCartons"
        Me.TxtLTLCartons.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxtLTLCartons.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxtLTLCartons.SelectedText = ""
        Me.TxtLTLCartons.SelectionLength = 0
        Me.TxtLTLCartons.SelectionStart = 0
        Me.TxtLTLCartons.ShortcutsEnabled = True
        Me.TxtLTLCartons.Size = New System.Drawing.Size(90, 23)
        Me.TxtLTLCartons.Style = MetroFramework.MetroColorStyle.Orange
        Me.TxtLTLCartons.TabIndex = 145
        Me.TxtLTLCartons.UseCustomBackColor = True
        Me.TxtLTLCartons.UseSelectable = True
        Me.TxtLTLCartons.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxtLTLCartons.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ChkFromLTLCarton
        '
        Me.ChkFromLTLCarton.AutoSize = True
        Me.ChkFromLTLCarton.BackColor = System.Drawing.Color.White
        Me.ChkFromLTLCarton.FontWeight = MetroFramework.MetroCheckBoxWeight.Light
        Me.ChkFromLTLCarton.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ChkFromLTLCarton.Location = New System.Drawing.Point(10, 23)
        Me.ChkFromLTLCarton.Name = "ChkFromLTLCarton"
        Me.ChkFromLTLCarton.Size = New System.Drawing.Size(82, 15)
        Me.ChkFromLTLCarton.Style = MetroFramework.MetroColorStyle.Lime
        Me.ChkFromLTLCarton.TabIndex = 137
        Me.ChkFromLTLCarton.Text = "LTL cartons "
        Me.ChkFromLTLCarton.UseCustomBackColor = True
        Me.ChkFromLTLCarton.UseCustomForeColor = True
        Me.ChkFromLTLCarton.UseSelectable = True
        '
        'DigitalWeightPanel
        '
        Me.DigitalWeightPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DigitalWeightPanel.Controls.Add(Me.rightValue)
        Me.DigitalWeightPanel.Controls.Add(Me.leftValue)
        Me.DigitalWeightPanel.Controls.Add(Me.DigitalWeightBackTile)
        Me.DigitalWeightPanel.Controls.Add(Me.MetroTile5)
        Me.DigitalWeightPanel.Controls.Add(Me.WeightFromDigitalScaleTextBox)
        Me.DigitalWeightPanel.Controls.Add(Me.SaveFromDigitalTile)
        Me.DigitalWeightPanel.HorizontalScrollbarBarColor = True
        Me.DigitalWeightPanel.HorizontalScrollbarHighlightOnWheel = False
        Me.DigitalWeightPanel.HorizontalScrollbarSize = 10
        Me.DigitalWeightPanel.Location = New System.Drawing.Point(266, 307)
        Me.DigitalWeightPanel.Name = "DigitalWeightPanel"
        Me.DigitalWeightPanel.Size = New System.Drawing.Size(845, 294)
        Me.DigitalWeightPanel.Style = MetroFramework.MetroColorStyle.Lime
        Me.DigitalWeightPanel.TabIndex = 121
        Me.DigitalWeightPanel.VerticalScrollbarBarColor = True
        Me.DigitalWeightPanel.VerticalScrollbarHighlightOnWheel = False
        Me.DigitalWeightPanel.VerticalScrollbarSize = 10
        Me.DigitalWeightPanel.Visible = False
        '
        'rightValue
        '
        Me.rightValue.AutoSize = True
        Me.rightValue.Location = New System.Drawing.Point(762, 178)
        Me.rightValue.Name = "rightValue"
        Me.rightValue.Size = New System.Drawing.Size(68, 19)
        Me.rightValue.TabIndex = 91
        Me.rightValue.Text = "Com port"
        '
        'leftValue
        '
        Me.leftValue.AutoSize = True
        Me.leftValue.Location = New System.Drawing.Point(760, 111)
        Me.leftValue.Name = "leftValue"
        Me.leftValue.Size = New System.Drawing.Size(68, 19)
        Me.leftValue.TabIndex = 90
        Me.leftValue.Text = "Com port"
        '
        'DigitalWeightBackTile
        '
        Me.DigitalWeightBackTile.ActiveControl = Nothing
        Me.DigitalWeightBackTile.AutoSize = True
        Me.DigitalWeightBackTile.BackColor = System.Drawing.Color.White
        Me.DigitalWeightBackTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DigitalWeightBackTile.Location = New System.Drawing.Point(764, 33)
        Me.DigitalWeightBackTile.Name = "DigitalWeightBackTile"
        Me.DigitalWeightBackTile.Size = New System.Drawing.Size(50, 48)
        Me.DigitalWeightBackTile.Style = MetroFramework.MetroColorStyle.White
        Me.DigitalWeightBackTile.TabIndex = 87
        Me.DigitalWeightBackTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DigitalWeightBackTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.DigitalWeightBackTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.DigitalWeightBackTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.DigitalWeightBackTile.UseSelectable = True
        Me.DigitalWeightBackTile.UseTileImage = True
        '
        'MetroTile5
        '
        Me.MetroTile5.ActiveControl = Nothing
        Me.MetroTile5.AutoSize = True
        Me.MetroTile5.Dock = System.Windows.Forms.DockStyle.Top
        Me.MetroTile5.Location = New System.Drawing.Point(0, 0)
        Me.MetroTile5.Name = "MetroTile5"
        Me.MetroTile5.Size = New System.Drawing.Size(843, 30)
        Me.MetroTile5.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroTile5.TabIndex = 69
        Me.MetroTile5.Text = "Digital weight"
        Me.MetroTile5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile5.UseSelectable = True
        '
        'WeightFromDigitalScaleTextBox
        '
        Me.WeightFromDigitalScaleTextBox.BackColor = System.Drawing.SystemColors.Control
        Me.WeightFromDigitalScaleTextBox.Font = New System.Drawing.Font("Tahoma", 109.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WeightFromDigitalScaleTextBox.Location = New System.Drawing.Point(20, 40)
        Me.WeightFromDigitalScaleTextBox.Name = "WeightFromDigitalScaleTextBox"
        Me.WeightFromDigitalScaleTextBox.Size = New System.Drawing.Size(736, 184)
        Me.WeightFromDigitalScaleTextBox.TabIndex = 70
        Me.WeightFromDigitalScaleTextBox.Text = "0.0"
        Me.WeightFromDigitalScaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'SaveFromDigitalTile
        '
        Me.SaveFromDigitalTile.ActiveControl = Nothing
        Me.SaveFromDigitalTile.AutoSize = True
        Me.SaveFromDigitalTile.BackColor = System.Drawing.Color.White
        Me.SaveFromDigitalTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveFromDigitalTile.Location = New System.Drawing.Point(328, 131)
        Me.SaveFromDigitalTile.Name = "SaveFromDigitalTile"
        Me.SaveFromDigitalTile.Size = New System.Drawing.Size(38, 35)
        Me.SaveFromDigitalTile.Style = MetroFramework.MetroColorStyle.White
        Me.SaveFromDigitalTile.TabIndex = 86
        Me.SaveFromDigitalTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveFromDigitalTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveFromDigitalTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SaveFromDigitalTile.UseSelectable = True
        Me.SaveFromDigitalTile.UseTileImage = True
        '
        'SerialPort1
        '
        '
        'ReceivingDataGrid
        '
        Me.ReceivingDataGrid.AllowUserToAddRows = False
        Me.ReceivingDataGrid.AllowUserToDeleteRows = False
        Me.ReceivingDataGrid.AllowUserToResizeRows = False
        Me.ReceivingDataGrid.AutoGenerateColumns = False
        Me.ReceivingDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.ReceivingDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.ReceivingDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ReceivingDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ReceivingDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.ReceivingDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReceivingDataGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ReceivingDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ReceivingDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bc, Me.farmer_code, Me.baleno, Me.CompanyDataGridViewTextBoxColumn, Me.supplier, Me.green, Me.classify, Me.weightbuy, Me.weight, Me.WeightDiffDataGridViewTextBoxColumn, Me.RemarkChecked, Me.DataGridViewTextBoxColumn1, Me.classifier, Me.ReplaceBales, Me.LockedDataGridViewTextBoxColumn, Me.LeaflockedDataGridViewTextBoxColumn, Me.DocnoDataGridViewTextBoxColumn, Me.ReceiveduserDataGridViewTextBoxColumn, Me.DtrecordDataGridViewTextBoxColumn})
        Me.ReceivingDataGrid.DataSource = Me.SpReceivingSELMatByRcNoResultBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ReceivingDataGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.ReceivingDataGrid.EnableHeadersVisualStyles = False
        Me.ReceivingDataGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.ReceivingDataGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ReceivingDataGrid.Location = New System.Drawing.Point(269, 143)
        Me.ReceivingDataGrid.Name = "ReceivingDataGrid"
        Me.ReceivingDataGrid.ReadOnly = True
        Me.ReceivingDataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReceivingDataGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.ReceivingDataGrid.RowHeadersVisible = False
        Me.ReceivingDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.ReceivingDataGrid.RowTemplate.Height = 24
        Me.ReceivingDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ReceivingDataGrid.Size = New System.Drawing.Size(844, 623)
        Me.ReceivingDataGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.ReceivingDataGrid.TabIndex = 122
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "bale barcode"
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 111
        '
        'farmer_code
        '
        Me.farmer_code.DataPropertyName = "farmer_code"
        Me.farmer_code.HeaderText = "farmer id"
        Me.farmer_code.Name = "farmer_code"
        Me.farmer_code.ReadOnly = True
        Me.farmer_code.Width = 88
        '
        'baleno
        '
        Me.baleno.DataPropertyName = "baleno"
        Me.baleno.HeaderText = "baleno"
        Me.baleno.Name = "baleno"
        Me.baleno.ReadOnly = True
        Me.baleno.Width = 74
        '
        'CompanyDataGridViewTextBoxColumn
        '
        Me.CompanyDataGridViewTextBoxColumn.DataPropertyName = "company"
        Me.CompanyDataGridViewTextBoxColumn.HeaderText = "company"
        Me.CompanyDataGridViewTextBoxColumn.Name = "CompanyDataGridViewTextBoxColumn"
        Me.CompanyDataGridViewTextBoxColumn.ReadOnly = True
        Me.CompanyDataGridViewTextBoxColumn.Width = 87
        '
        'supplier
        '
        Me.supplier.DataPropertyName = "supplier"
        Me.supplier.HeaderText = "supplier"
        Me.supplier.Name = "supplier"
        Me.supplier.ReadOnly = True
        Me.supplier.Width = 82
        '
        'green
        '
        Me.green.DataPropertyName = "green"
        Me.green.HeaderText = "green"
        Me.green.Name = "green"
        Me.green.ReadOnly = True
        Me.green.Width = 67
        '
        'classify
        '
        Me.classify.DataPropertyName = "classify"
        Me.classify.HeaderText = "classify"
        Me.classify.Name = "classify"
        Me.classify.ReadOnly = True
        Me.classify.Width = 76
        '
        'weightbuy
        '
        Me.weightbuy.DataPropertyName = "weightbuy"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.weightbuy.DefaultCellStyle = DataGridViewCellStyle2
        Me.weightbuy.HeaderText = "b kg."
        Me.weightbuy.Name = "weightbuy"
        Me.weightbuy.ReadOnly = True
        Me.weightbuy.Width = 60
        '
        'weight
        '
        Me.weight.DataPropertyName = "weight"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.weight.DefaultCellStyle = DataGridViewCellStyle3
        Me.weight.HeaderText = "r kg."
        Me.weight.Name = "weight"
        Me.weight.ReadOnly = True
        Me.weight.Width = 57
        '
        'WeightDiffDataGridViewTextBoxColumn
        '
        Me.WeightDiffDataGridViewTextBoxColumn.DataPropertyName = "WeightDiff"
        DataGridViewCellStyle4.Format = "N1"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.WeightDiffDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.WeightDiffDataGridViewTextBoxColumn.HeaderText = "diff kg."
        Me.WeightDiffDataGridViewTextBoxColumn.Name = "WeightDiffDataGridViewTextBoxColumn"
        Me.WeightDiffDataGridViewTextBoxColumn.ReadOnly = True
        Me.WeightDiffDataGridViewTextBoxColumn.Width = 73
        '
        'RemarkChecked
        '
        Me.RemarkChecked.DataPropertyName = "RemarkChecked"
        Me.RemarkChecked.HeaderText = "NTRM"
        Me.RemarkChecked.Name = "RemarkChecked"
        Me.RemarkChecked.ReadOnly = True
        Me.RemarkChecked.Visible = False
        Me.RemarkChecked.Width = 84
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "RemarkChecked"
        Me.DataGridViewTextBoxColumn1.HeaderText = "remark"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 93
        '
        'classifier
        '
        Me.classifier.DataPropertyName = "classifier"
        Me.classifier.HeaderText = "classifier"
        Me.classifier.Name = "classifier"
        Me.classifier.ReadOnly = True
        Me.classifier.Width = 85
        '
        'ReplaceBales
        '
        Me.ReplaceBales.DataPropertyName = "ReplaceBales"
        Me.ReplaceBales.HeaderText = "replace bale"
        Me.ReplaceBales.Name = "ReplaceBales"
        Me.ReplaceBales.ReadOnly = True
        Me.ReplaceBales.Width = 88
        '
        'LockedDataGridViewTextBoxColumn
        '
        Me.LockedDataGridViewTextBoxColumn.DataPropertyName = "locked"
        Me.LockedDataGridViewTextBoxColumn.HeaderText = "locked"
        Me.LockedDataGridViewTextBoxColumn.Name = "LockedDataGridViewTextBoxColumn"
        Me.LockedDataGridViewTextBoxColumn.ReadOnly = True
        Me.LockedDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LockedDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.LockedDataGridViewTextBoxColumn.Width = 72
        '
        'LeaflockedDataGridViewTextBoxColumn
        '
        Me.LeaflockedDataGridViewTextBoxColumn.DataPropertyName = "leaflocked"
        Me.LeaflockedDataGridViewTextBoxColumn.HeaderText = "leaflocked"
        Me.LeaflockedDataGridViewTextBoxColumn.Name = "LeaflockedDataGridViewTextBoxColumn"
        Me.LeaflockedDataGridViewTextBoxColumn.ReadOnly = True
        Me.LeaflockedDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LeaflockedDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.LeaflockedDataGridViewTextBoxColumn.Width = 95
        '
        'DocnoDataGridViewTextBoxColumn
        '
        Me.DocnoDataGridViewTextBoxColumn.DataPropertyName = "docno"
        Me.DocnoDataGridViewTextBoxColumn.HeaderText = "docno"
        Me.DocnoDataGridViewTextBoxColumn.Name = "DocnoDataGridViewTextBoxColumn"
        Me.DocnoDataGridViewTextBoxColumn.ReadOnly = True
        Me.DocnoDataGridViewTextBoxColumn.Width = 69
        '
        'ReceiveduserDataGridViewTextBoxColumn
        '
        Me.ReceiveduserDataGridViewTextBoxColumn.DataPropertyName = "receiveduser"
        Me.ReceiveduserDataGridViewTextBoxColumn.HeaderText = "receiveduser"
        Me.ReceiveduserDataGridViewTextBoxColumn.Name = "ReceiveduserDataGridViewTextBoxColumn"
        Me.ReceiveduserDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReceiveduserDataGridViewTextBoxColumn.Width = 112
        '
        'DtrecordDataGridViewTextBoxColumn
        '
        Me.DtrecordDataGridViewTextBoxColumn.DataPropertyName = "dtrecord"
        Me.DtrecordDataGridViewTextBoxColumn.HeaderText = "dtrecord"
        Me.DtrecordDataGridViewTextBoxColumn.Name = "DtrecordDataGridViewTextBoxColumn"
        Me.DtrecordDataGridViewTextBoxColumn.ReadOnly = True
        Me.DtrecordDataGridViewTextBoxColumn.Width = 84
        '
        'SpReceivingSELMatByRcNoResultBindingSource
        '
        Me.SpReceivingSELMatByRcNoResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_MatByRcNo_Result)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel11.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel11.Location = New System.Drawing.Point(983, 90)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(48, 15)
        Me.MetroLabel11.TabIndex = 126
        Me.MetroLabel11.Text = "Buy kg."
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel15.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel15.Location = New System.Drawing.Point(887, 90)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(47, 15)
        Me.MetroLabel15.TabIndex = 127
        Me.MetroLabel15.Text = "Classify"
        '
        'MetroLabel17
        '
        Me.MetroLabel17.AutoSize = True
        Me.MetroLabel17.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel17.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel17.Location = New System.Drawing.Point(791, 90)
        Me.MetroLabel17.Name = "MetroLabel17"
        Me.MetroLabel17.Size = New System.Drawing.Size(42, 15)
        Me.MetroLabel17.TabIndex = 128
        Me.MetroLabel17.Text = "Green"
        '
        'SaveMetroLabel
        '
        Me.SaveMetroLabel.AutoSize = True
        Me.SaveMetroLabel.Location = New System.Drawing.Point(1154, 114)
        Me.SaveMetroLabel.Name = "SaveMetroLabel"
        Me.SaveMetroLabel.Size = New System.Drawing.Size(117, 19)
        Me.SaveMetroLabel.TabIndex = 81
        Me.SaveMetroLabel.Text = "save (replace only)"
        Me.SaveMetroLabel.Visible = False
        '
        'MetroLabel18
        '
        Me.MetroLabel18.AutoSize = True
        Me.MetroLabel18.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel18.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel18.Location = New System.Drawing.Point(700, 90)
        Me.MetroLabel18.Name = "MetroLabel18"
        Me.MetroLabel18.Size = New System.Drawing.Size(38, 15)
        Me.MetroLabel18.TabIndex = 130
        Me.MetroLabel18.Text = "Supp."
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LXCountLabel)
        Me.GroupBox1.Controls.Add(Me.LXBatchLabel)
        Me.GroupBox1.Controls.Add(Me.LXPrintTile)
        Me.GroupBox1.Controls.Add(Me.LXCloseTile)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 86)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(166, 85)
        Me.GroupBox1.TabIndex = 145
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "LX"
        '
        'LXCountLabel
        '
        Me.LXCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LXCountLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.LXCountLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.LXCountLabel.Location = New System.Drawing.Point(92, 19)
        Me.LXCountLabel.Name = "LXCountLabel"
        Me.LXCountLabel.Size = New System.Drawing.Size(65, 25)
        Me.LXCountLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.LXCountLabel.TabIndex = 85
        Me.LXCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LXBatchLabel
        '
        Me.LXBatchLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LXBatchLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.LXBatchLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.LXBatchLabel.Location = New System.Drawing.Point(6, 19)
        Me.LXBatchLabel.Name = "LXBatchLabel"
        Me.LXBatchLabel.Size = New System.Drawing.Size(80, 25)
        Me.LXBatchLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.LXBatchLabel.TabIndex = 84
        Me.LXBatchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LXPrintTile
        '
        Me.LXPrintTile.ActiveControl = Nothing
        Me.LXPrintTile.AutoSize = True
        Me.LXPrintTile.BackColor = System.Drawing.Color.White
        Me.LXPrintTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LXPrintTile.Location = New System.Drawing.Point(101, 47)
        Me.LXPrintTile.Name = "LXPrintTile"
        Me.LXPrintTile.Size = New System.Drawing.Size(38, 35)
        Me.LXPrintTile.Style = MetroFramework.MetroColorStyle.White
        Me.LXPrintTile.TabIndex = 88
        Me.LXPrintTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LXPrintTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Print32
        Me.LXPrintTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LXPrintTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.LXPrintTile.UseSelectable = True
        Me.LXPrintTile.UseTileImage = True
        '
        'LXCloseTile
        '
        Me.LXCloseTile.ActiveControl = Nothing
        Me.LXCloseTile.AutoSize = True
        Me.LXCloseTile.BackColor = System.Drawing.Color.White
        Me.LXCloseTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LXCloseTile.Location = New System.Drawing.Point(32, 47)
        Me.LXCloseTile.Name = "LXCloseTile"
        Me.LXCloseTile.Size = New System.Drawing.Size(38, 35)
        Me.LXCloseTile.Style = MetroFramework.MetroColorStyle.White
        Me.LXCloseTile.TabIndex = 87
        Me.LXCloseTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LXCloseTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Shutdown32
        Me.LXCloseTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LXCloseTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.LXCloseTile.UseSelectable = True
        Me.LXCloseTile.UseTileImage = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.XCountLabel)
        Me.GroupBox2.Controls.Add(Me.XBatchLabel)
        Me.GroupBox2.Controls.Add(Me.XPrintTile)
        Me.GroupBox2.Controls.Add(Me.XCloseTile)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 177)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(166, 85)
        Me.GroupBox2.TabIndex = 146
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "X"
        '
        'XCountLabel
        '
        Me.XCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.XCountLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.XCountLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.XCountLabel.Location = New System.Drawing.Point(95, 19)
        Me.XCountLabel.Name = "XCountLabel"
        Me.XCountLabel.Size = New System.Drawing.Size(60, 25)
        Me.XCountLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.XCountLabel.TabIndex = 85
        Me.XCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'XBatchLabel
        '
        Me.XBatchLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.XBatchLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.XBatchLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.XBatchLabel.Location = New System.Drawing.Point(6, 19)
        Me.XBatchLabel.Name = "XBatchLabel"
        Me.XBatchLabel.Size = New System.Drawing.Size(80, 25)
        Me.XBatchLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.XBatchLabel.TabIndex = 84
        Me.XBatchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'XPrintTile
        '
        Me.XPrintTile.ActiveControl = Nothing
        Me.XPrintTile.AutoSize = True
        Me.XPrintTile.BackColor = System.Drawing.Color.White
        Me.XPrintTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XPrintTile.Location = New System.Drawing.Point(101, 47)
        Me.XPrintTile.Name = "XPrintTile"
        Me.XPrintTile.Size = New System.Drawing.Size(38, 35)
        Me.XPrintTile.Style = MetroFramework.MetroColorStyle.White
        Me.XPrintTile.TabIndex = 88
        Me.XPrintTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.XPrintTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Print32
        Me.XPrintTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.XPrintTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.XPrintTile.UseSelectable = True
        Me.XPrintTile.UseTileImage = True
        '
        'XCloseTile
        '
        Me.XCloseTile.ActiveControl = Nothing
        Me.XCloseTile.AutoSize = True
        Me.XCloseTile.BackColor = System.Drawing.Color.White
        Me.XCloseTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.XCloseTile.Location = New System.Drawing.Point(32, 47)
        Me.XCloseTile.Name = "XCloseTile"
        Me.XCloseTile.Size = New System.Drawing.Size(38, 35)
        Me.XCloseTile.Style = MetroFramework.MetroColorStyle.White
        Me.XCloseTile.TabIndex = 87
        Me.XCloseTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.XCloseTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Shutdown32
        Me.XCloseTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.XCloseTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.XCloseTile.UseSelectable = True
        Me.XCloseTile.UseTileImage = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CCountLabel)
        Me.GroupBox3.Controls.Add(Me.CBatchLabel)
        Me.GroupBox3.Controls.Add(Me.CPrintTile)
        Me.GroupBox3.Controls.Add(Me.CCloseTile)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 268)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(166, 85)
        Me.GroupBox3.TabIndex = 147
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "C"
        '
        'CCountLabel
        '
        Me.CCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CCountLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.CCountLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.CCountLabel.Location = New System.Drawing.Point(95, 16)
        Me.CCountLabel.Name = "CCountLabel"
        Me.CCountLabel.Size = New System.Drawing.Size(60, 25)
        Me.CCountLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CCountLabel.TabIndex = 85
        Me.CCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CBatchLabel
        '
        Me.CBatchLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CBatchLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.CBatchLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.CBatchLabel.Location = New System.Drawing.Point(6, 16)
        Me.CBatchLabel.Name = "CBatchLabel"
        Me.CBatchLabel.Size = New System.Drawing.Size(80, 25)
        Me.CBatchLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CBatchLabel.TabIndex = 84
        Me.CBatchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CPrintTile
        '
        Me.CPrintTile.ActiveControl = Nothing
        Me.CPrintTile.AutoSize = True
        Me.CPrintTile.BackColor = System.Drawing.Color.White
        Me.CPrintTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CPrintTile.Location = New System.Drawing.Point(101, 44)
        Me.CPrintTile.Name = "CPrintTile"
        Me.CPrintTile.Size = New System.Drawing.Size(38, 35)
        Me.CPrintTile.Style = MetroFramework.MetroColorStyle.White
        Me.CPrintTile.TabIndex = 88
        Me.CPrintTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CPrintTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Print32
        Me.CPrintTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CPrintTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.CPrintTile.UseSelectable = True
        Me.CPrintTile.UseTileImage = True
        '
        'CCloseTile
        '
        Me.CCloseTile.ActiveControl = Nothing
        Me.CCloseTile.AutoSize = True
        Me.CCloseTile.BackColor = System.Drawing.Color.White
        Me.CCloseTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CCloseTile.Location = New System.Drawing.Point(33, 44)
        Me.CCloseTile.Name = "CCloseTile"
        Me.CCloseTile.Size = New System.Drawing.Size(38, 35)
        Me.CCloseTile.Style = MetroFramework.MetroColorStyle.White
        Me.CCloseTile.TabIndex = 87
        Me.CCloseTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CCloseTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Shutdown32
        Me.CCloseTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CCloseTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.CCloseTile.UseSelectable = True
        Me.CCloseTile.UseTileImage = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.BTCountLabel)
        Me.GroupBox4.Controls.Add(Me.BTBatchLabel)
        Me.GroupBox4.Controls.Add(Me.BTPrintTile)
        Me.GroupBox4.Controls.Add(Me.BTCloseTile)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 359)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(166, 85)
        Me.GroupBox4.TabIndex = 148
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "B+T"
        '
        'BTCountLabel
        '
        Me.BTCountLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BTCountLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.BTCountLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.BTCountLabel.Location = New System.Drawing.Point(95, 18)
        Me.BTCountLabel.Name = "BTCountLabel"
        Me.BTCountLabel.Size = New System.Drawing.Size(60, 25)
        Me.BTCountLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.BTCountLabel.TabIndex = 85
        Me.BTCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BTBatchLabel
        '
        Me.BTBatchLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.BTBatchLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.BTBatchLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.BTBatchLabel.Location = New System.Drawing.Point(9, 18)
        Me.BTBatchLabel.Name = "BTBatchLabel"
        Me.BTBatchLabel.Size = New System.Drawing.Size(80, 25)
        Me.BTBatchLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.BTBatchLabel.TabIndex = 84
        Me.BTBatchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BTPrintTile
        '
        Me.BTPrintTile.ActiveControl = Nothing
        Me.BTPrintTile.AutoSize = True
        Me.BTPrintTile.BackColor = System.Drawing.Color.White
        Me.BTPrintTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTPrintTile.Location = New System.Drawing.Point(101, 46)
        Me.BTPrintTile.Name = "BTPrintTile"
        Me.BTPrintTile.Size = New System.Drawing.Size(38, 35)
        Me.BTPrintTile.Style = MetroFramework.MetroColorStyle.White
        Me.BTPrintTile.TabIndex = 88
        Me.BTPrintTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BTPrintTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Print32
        Me.BTPrintTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BTPrintTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BTPrintTile.UseSelectable = True
        Me.BTPrintTile.UseTileImage = True
        '
        'BTCloseTile
        '
        Me.BTCloseTile.ActiveControl = Nothing
        Me.BTCloseTile.AutoSize = True
        Me.BTCloseTile.BackColor = System.Drawing.Color.White
        Me.BTCloseTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTCloseTile.Location = New System.Drawing.Point(35, 46)
        Me.BTCloseTile.Name = "BTCloseTile"
        Me.BTCloseTile.Size = New System.Drawing.Size(38, 35)
        Me.BTCloseTile.Style = MetroFramework.MetroColorStyle.White
        Me.BTCloseTile.TabIndex = 87
        Me.BTCloseTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BTCloseTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Shutdown32
        Me.BTCloseTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BTCloseTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BTCloseTile.UseSelectable = True
        Me.BTCloseTile.UseTileImage = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.FarmerLabel)
        Me.GroupBox6.Controls.Add(Me.MetroLabel32)
        Me.GroupBox6.Controls.Add(Me.CountMoistureLabel)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(166, 77)
        Me.GroupBox6.TabIndex = 153
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Moisture tested"
        '
        'FarmerLabel
        '
        Me.FarmerLabel.AutoSize = True
        Me.FarmerLabel.Location = New System.Drawing.Point(6, 16)
        Me.FarmerLabel.Name = "FarmerLabel"
        Me.FarmerLabel.Size = New System.Drawing.Size(82, 19)
        Me.FarmerLabel.TabIndex = 83
        Me.FarmerLabel.Text = "FarmerLabel"
        '
        'MetroLabel32
        '
        Me.MetroLabel32.AutoSize = True
        Me.MetroLabel32.Location = New System.Drawing.Point(101, 46)
        Me.MetroLabel32.Name = "MetroLabel32"
        Me.MetroLabel32.Size = New System.Drawing.Size(28, 19)
        Me.MetroLabel32.TabIndex = 82
        Me.MetroLabel32.Text = "ครั้ง"
        Me.MetroLabel32.Visible = False
        '
        'CountMoistureLabel
        '
        Me.CountMoistureLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CountMoistureLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CountMoistureLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.CountMoistureLabel.ForeColor = System.Drawing.Color.Blue
        Me.CountMoistureLabel.Location = New System.Drawing.Point(6, 42)
        Me.CountMoistureLabel.Name = "CountMoistureLabel"
        Me.CountMoistureLabel.Size = New System.Drawing.Size(89, 25)
        Me.CountMoistureLabel.Style = MetroFramework.MetroColorStyle.Black
        Me.CountMoistureLabel.TabIndex = 68
        Me.CountMoistureLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CountMoistureLabel.UseCustomBackColor = True
        Me.CountMoistureLabel.UseStyleColors = True
        '
        'RemainingTruckReceivingLabel
        '
        Me.RemainingTruckReceivingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RemainingTruckReceivingLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.RemainingTruckReceivingLabel.Location = New System.Drawing.Point(77, 56)
        Me.RemainingTruckReceivingLabel.Name = "RemainingTruckReceivingLabel"
        Me.RemainingTruckReceivingLabel.Size = New System.Drawing.Size(80, 25)
        Me.RemainingTruckReceivingLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.RemainingTruckReceivingLabel.TabIndex = 78
        Me.RemainingTruckReceivingLabel.Text = "0"
        Me.RemainingTruckReceivingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel50
        '
        Me.MetroLabel50.AutoSize = True
        Me.MetroLabel50.Location = New System.Drawing.Point(6, 59)
        Me.MetroLabel50.Name = "MetroLabel50"
        Me.MetroLabel50.Size = New System.Drawing.Size(68, 19)
        Me.MetroLabel50.TabIndex = 77
        Me.MetroLabel50.Text = "remaining"
        '
        'TotalBaleOnTruckLabel
        '
        Me.TotalBaleOnTruckLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalBaleOnTruckLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.TotalBaleOnTruckLabel.Location = New System.Drawing.Point(77, 24)
        Me.TotalBaleOnTruckLabel.Name = "TotalBaleOnTruckLabel"
        Me.TotalBaleOnTruckLabel.Size = New System.Drawing.Size(80, 25)
        Me.TotalBaleOnTruckLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalBaleOnTruckLabel.TabIndex = 75
        Me.TotalBaleOnTruckLabel.Text = "0"
        Me.TotalBaleOnTruckLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel54
        '
        Me.MetroLabel54.AutoSize = True
        Me.MetroLabel54.Location = New System.Drawing.Point(6, 24)
        Me.MetroLabel54.Name = "MetroLabel54"
        Me.MetroLabel54.Size = New System.Drawing.Size(35, 19)
        Me.MetroLabel54.TabIndex = 70
        Me.MetroLabel54.Text = "total"
        '
        'BtnLTLStart
        '
        Me.BtnLTLStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnLTLStart.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLTLStart.Location = New System.Drawing.Point(8, 49)
        Me.BtnLTLStart.Name = "BtnLTLStart"
        Me.BtnLTLStart.Size = New System.Drawing.Size(110, 25)
        Me.BtnLTLStart.TabIndex = 157
        Me.BtnLTLStart.Text = "Start"
        Me.BtnLTLStart.UseVisualStyleBackColor = True
        '
        'BtnLTLComplete
        '
        Me.BtnLTLComplete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnLTLComplete.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLTLComplete.Location = New System.Drawing.Point(124, 49)
        Me.BtnLTLComplete.Name = "BtnLTLComplete"
        Me.BtnLTLComplete.Size = New System.Drawing.Size(110, 25)
        Me.BtnLTLComplete.TabIndex = 156
        Me.BtnLTLComplete.Text = "Complete"
        Me.BtnLTLComplete.UseVisualStyleBackColor = True
        '
        'MetroLabel52
        '
        Me.MetroLabel52.AutoSize = True
        Me.MetroLabel52.Location = New System.Drawing.Point(94, 20)
        Me.MetroLabel52.Name = "MetroLabel52"
        Me.MetroLabel52.Size = New System.Drawing.Size(44, 19)
        Me.MetroLabel52.TabIndex = 146
        Me.MetroLabel52.Text = "จำนวน"
        '
        'Save
        '
        Me.Save.ActiveControl = Nothing
        Me.Save.AutoSize = True
        Me.Save.BackColor = System.Drawing.Color.White
        Me.Save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Save.Location = New System.Drawing.Point(1114, 107)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(38, 35)
        Me.Save.Style = MetroFramework.MetroColorStyle.White
        Me.Save.TabIndex = 9
        Me.Save.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Save.TileImage = Global.ReceivingSystem.My.Resources.Resources.Save32
        Me.Save.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Save.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.Save.UseSelectable = True
        Me.Save.UseTileImage = True
        Me.Save.Visible = False
        '
        'ReceivingDetailGroupBox
        '
        Me.ReceivingDetailGroupBox.AutoSize = True
        Me.ReceivingDetailGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel2)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.BuyerComboBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel27)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel25)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.TypeLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.rcnoLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel22)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.CropLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel5)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel20)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.DigitalWeightCheckBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.ManualInputCheckBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.CompanyLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel21)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel3)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.InvoiceNoLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.ClassifierComboBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel49)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel19)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.RcLinesLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.BaleTypeComboBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel9)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.RcfromLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel13)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel48)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.PlaceLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.TransportationDocumentCodeLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.TrucknoLabel)
        Me.ReceivingDetailGroupBox.Location = New System.Drawing.Point(3, 3)
        Me.ReceivingDetailGroupBox.Name = "ReceivingDetailGroupBox"
        Me.ReceivingDetailGroupBox.Size = New System.Drawing.Size(236, 434)
        Me.ReceivingDetailGroupBox.TabIndex = 169
        Me.ReceivingDetailGroupBox.TabStop = False
        Me.ReceivingDetailGroupBox.Text = "Receiving Detail"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel2.Location = New System.Drawing.Point(6, 344)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(42, 19)
        Me.MetroLabel2.TabIndex = 173
        Me.MetroLabel2.Text = "buyer"
        Me.MetroLabel2.UseCustomBackColor = True
        Me.MetroLabel2.UseCustomForeColor = True
        '
        'BuyerComboBox
        '
        Me.BuyerComboBox.BackColor = System.Drawing.Color.White
        Me.BuyerComboBox.DisplayMember = "name"
        Me.BuyerComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.BuyerComboBox.FormattingEnabled = True
        Me.BuyerComboBox.ItemHeight = 19
        Me.BuyerComboBox.Location = New System.Drawing.Point(110, 346)
        Me.BuyerComboBox.Name = "BuyerComboBox"
        Me.BuyerComboBox.Size = New System.Drawing.Size(120, 25)
        Me.BuyerComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.BuyerComboBox.TabIndex = 172
        Me.BuyerComboBox.UseCustomBackColor = True
        Me.BuyerComboBox.UseCustomForeColor = True
        Me.BuyerComboBox.UseSelectable = True
        Me.BuyerComboBox.ValueMember = "name"
        '
        'MetroLabel27
        '
        Me.MetroLabel27.AutoSize = True
        Me.MetroLabel27.Location = New System.Drawing.Point(6, 16)
        Me.MetroLabel27.Name = "MetroLabel27"
        Me.MetroLabel27.Size = New System.Drawing.Size(35, 19)
        Me.MetroLabel27.TabIndex = 171
        Me.MetroLabel27.Text = "rcno"
        '
        'MetroLabel25
        '
        Me.MetroLabel25.AutoSize = True
        Me.MetroLabel25.Location = New System.Drawing.Point(6, 70)
        Me.MetroLabel25.Name = "MetroLabel25"
        Me.MetroLabel25.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel25.TabIndex = 169
        Me.MetroLabel25.Text = "type"
        '
        'TypeLabel
        '
        Me.TypeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TypeLabel.Location = New System.Drawing.Point(110, 70)
        Me.TypeLabel.Name = "TypeLabel"
        Me.TypeLabel.Size = New System.Drawing.Size(120, 23)
        Me.TypeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TypeLabel.TabIndex = 168
        Me.TypeLabel.Text = "type"
        Me.TypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rcnoLabel
        '
        Me.rcnoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.rcnoLabel.Location = New System.Drawing.Point(110, 16)
        Me.rcnoLabel.Name = "rcnoLabel"
        Me.rcnoLabel.Size = New System.Drawing.Size(120, 23)
        Me.rcnoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.rcnoLabel.TabIndex = 170
        Me.rcnoLabel.Text = "rcno"
        Me.rcnoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel22
        '
        Me.MetroLabel22.AutoSize = True
        Me.MetroLabel22.Location = New System.Drawing.Point(6, 43)
        Me.MetroLabel22.Name = "MetroLabel22"
        Me.MetroLabel22.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel22.TabIndex = 167
        Me.MetroLabel22.Text = "crop"
        '
        'CropLabel
        '
        Me.CropLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CropLabel.Location = New System.Drawing.Point(110, 43)
        Me.CropLabel.Name = "CropLabel"
        Me.CropLabel.Size = New System.Drawing.Size(120, 23)
        Me.CropLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropLabel.TabIndex = 166
        Me.CropLabel.Text = "crop"
        Me.CropLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(6, 232)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(59, 19)
        Me.MetroLabel5.TabIndex = 144
        Me.MetroLabel5.Text = "truck no."
        '
        'FromLTLCartonGroupBox
        '
        Me.FromLTLCartonGroupBox.Controls.Add(Me.ChkFromLTLCarton)
        Me.FromLTLCartonGroupBox.Controls.Add(Me.TxtLTLCartons)
        Me.FromLTLCartonGroupBox.Controls.Add(Me.BtnLTLComplete)
        Me.FromLTLCartonGroupBox.Controls.Add(Me.BtnLTLStart)
        Me.FromLTLCartonGroupBox.Controls.Add(Me.MetroLabel52)
        Me.FromLTLCartonGroupBox.Location = New System.Drawing.Point(3, 443)
        Me.FromLTLCartonGroupBox.Name = "FromLTLCartonGroupBox"
        Me.FromLTLCartonGroupBox.Size = New System.Drawing.Size(240, 90)
        Me.FromLTLCartonGroupBox.TabIndex = 170
        Me.FromLTLCartonGroupBox.TabStop = False
        Me.FromLTLCartonGroupBox.Text = "From LTL Cartons"
        '
        'SummaryReceivingGroupBox
        '
        Me.SummaryReceivingGroupBox.Controls.Add(Me.TotalBaleLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel16)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.TotalReceivedWeightLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel14)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.TotalBuyingWeightLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel12)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.DiffWeightLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel36)
        Me.SummaryReceivingGroupBox.Location = New System.Drawing.Point(3, 539)
        Me.SummaryReceivingGroupBox.Name = "SummaryReceivingGroupBox"
        Me.SummaryReceivingGroupBox.Size = New System.Drawing.Size(240, 137)
        Me.SummaryReceivingGroupBox.TabIndex = 171
        Me.SummaryReceivingGroupBox.TabStop = False
        Me.SummaryReceivingGroupBox.Text = "Summary Receiving"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.ReceivingDetailGroupBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.FromLTLCartonGroupBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.SummaryReceivingGroupBox)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(17, 87)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(246, 679)
        Me.FlowLayoutPanel1.TabIndex = 172
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox6)
        Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox3)
        Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox4)
        Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox5)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(1114, 143)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(172, 584)
        Me.FlowLayoutPanel2.TabIndex = 173
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.VieeTransportDetailButton)
        Me.GroupBox5.Controls.Add(Me.TotalBaleOnTruckLabel)
        Me.GroupBox5.Controls.Add(Me.MetroLabel54)
        Me.GroupBox5.Controls.Add(Me.RemainingTruckReceivingLabel)
        Me.GroupBox5.Controls.Add(Me.MetroLabel50)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 450)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(166, 131)
        Me.GroupBox5.TabIndex = 174
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Truck/invoice detail"
        '
        'VieeTransportDetailButton
        '
        Me.VieeTransportDetailButton.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VieeTransportDetailButton.Location = New System.Drawing.Point(76, 88)
        Me.VieeTransportDetailButton.Name = "VieeTransportDetailButton"
        Me.VieeTransportDetailButton.Size = New System.Drawing.Size(81, 25)
        Me.VieeTransportDetailButton.TabIndex = 156
        Me.VieeTransportDetailButton.Text = "view detail"
        Me.VieeTransportDetailButton.UseVisualStyleBackColor = True
        '
        'ClassifyGradeComboBox
        '
        Me.ClassifyGradeComboBox.Enabled = False
        Me.ClassifyGradeComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!)
        Me.ClassifyGradeComboBox.FormattingEnabled = True
        Me.ClassifyGradeComboBox.Location = New System.Drawing.Point(890, 108)
        Me.ClassifyGradeComboBox.Name = "ClassifyGradeComboBox"
        Me.ClassifyGradeComboBox.Size = New System.Drawing.Size(90, 28)
        Me.ClassifyGradeComboBox.TabIndex = 6
        '
        'GreenGradeComboBox
        '
        Me.GreenGradeComboBox.Enabled = False
        Me.GreenGradeComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!)
        Me.GreenGradeComboBox.FormattingEnabled = True
        Me.GreenGradeComboBox.Location = New System.Drawing.Point(795, 108)
        Me.GreenGradeComboBox.Name = "GreenGradeComboBox"
        Me.GreenGradeComboBox.Size = New System.Drawing.Size(90, 28)
        Me.GreenGradeComboBox.TabIndex = 5
        '
        'BaleBarcodeTextBox
        '
        Me.BaleBarcodeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BaleBarcodeTextBox.Location = New System.Drawing.Point(269, 109)
        Me.BaleBarcodeTextBox.Name = "BaleBarcodeTextBox"
        Me.BaleBarcodeTextBox.Size = New System.Drawing.Size(269, 27)
        Me.BaleBarcodeTextBox.TabIndex = 1
        '
        'BaleNumberTextBox
        '
        Me.BaleNumberTextBox.Enabled = False
        Me.BaleNumberTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BaleNumberTextBox.Location = New System.Drawing.Point(632, 109)
        Me.BaleNumberTextBox.Name = "BaleNumberTextBox"
        Me.BaleNumberTextBox.Size = New System.Drawing.Size(63, 27)
        Me.BaleNumberTextBox.TabIndex = 3
        '
        'SupplierComboBox
        '
        Me.SupplierComboBox.Enabled = False
        Me.SupplierComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!)
        Me.SupplierComboBox.FormattingEnabled = True
        Me.SupplierComboBox.Location = New System.Drawing.Point(700, 108)
        Me.SupplierComboBox.Name = "SupplierComboBox"
        Me.SupplierComboBox.Size = New System.Drawing.Size(90, 28)
        Me.SupplierComboBox.TabIndex = 4
        '
        'BuyingWeightTextBox
        '
        Me.BuyingWeightTextBox.Enabled = False
        Me.BuyingWeightTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BuyingWeightTextBox.Location = New System.Drawing.Point(985, 109)
        Me.BuyingWeightTextBox.Name = "BuyingWeightTextBox"
        Me.BuyingWeightTextBox.Size = New System.Drawing.Size(60, 27)
        Me.BuyingWeightTextBox.TabIndex = 7
        '
        'ReceivingWeightTextBox
        '
        Me.ReceivingWeightTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ReceivingWeightTextBox.Location = New System.Drawing.Point(1050, 109)
        Me.ReceivingWeightTextBox.Name = "ReceivingWeightTextBox"
        Me.ReceivingWeightTextBox.Size = New System.Drawing.Size(60, 27)
        Me.ReceivingWeightTextBox.TabIndex = 8
        '
        'NPIFarmerCodeTextBox
        '
        Me.NPIFarmerCodeTextBox.Enabled = False
        Me.NPIFarmerCodeTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.NPIFarmerCodeTextBox.Location = New System.Drawing.Point(544, 109)
        Me.NPIFarmerCodeTextBox.Name = "NPIFarmerCodeTextBox"
        Me.NPIFarmerCodeTextBox.Size = New System.Drawing.Size(82, 27)
        Me.NPIFarmerCodeTextBox.TabIndex = 2
        '
        'NPIFarmerCodeLabel
        '
        Me.NPIFarmerCodeLabel.AutoSize = True
        Me.NPIFarmerCodeLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.NPIFarmerCodeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.NPIFarmerCodeLabel.Location = New System.Drawing.Point(543, 90)
        Me.NPIFarmerCodeLabel.Name = "NPIFarmerCodeLabel"
        Me.NPIFarmerCodeLabel.Size = New System.Drawing.Size(63, 15)
        Me.NPIFarmerCodeLabel.TabIndex = 175
        Me.NPIFarmerCodeLabel.Text = "Farmer ID"
        '
        'SpReceivingSELBuyerClassifierResultBindingSource
        '
        Me.SpReceivingSELBuyerClassifierResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_BuyerClassifier_Result)
        '
        'IsCUTRAGCheckBox
        '
        Me.IsCUTRAGCheckBox.AutoSize = True
        Me.IsCUTRAGCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.IsCUTRAGCheckBox.ForeColor = System.Drawing.Color.Red
        Me.IsCUTRAGCheckBox.Location = New System.Drawing.Point(460, 90)
        Me.IsCUTRAGCheckBox.Name = "IsCUTRAGCheckBox"
        Me.IsCUTRAGCheckBox.Size = New System.Drawing.Size(77, 17)
        Me.IsCUTRAGCheckBox.TabIndex = 176
        Me.IsCUTRAGCheckBox.Text = "CUTRAG"
        Me.IsCUTRAGCheckBox.UseVisualStyleBackColor = True
        '
        'FrmReceivingWeight
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1299, 780)
        Me.Controls.Add(Me.IsCUTRAGCheckBox)
        Me.Controls.Add(Me.NPIFarmerCodeTextBox)
        Me.Controls.Add(Me.NPIFarmerCodeLabel)
        Me.Controls.Add(Me.ReceivingWeightTextBox)
        Me.Controls.Add(Me.BuyingWeightTextBox)
        Me.Controls.Add(Me.SupplierComboBox)
        Me.Controls.Add(Me.BaleNumberTextBox)
        Me.Controls.Add(Me.BaleBarcodeTextBox)
        Me.Controls.Add(Me.GreenGradeComboBox)
        Me.Controls.Add(Me.ClassifyGradeComboBox)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.DigitalWeightPanel)
        Me.Controls.Add(Me.MetroLabel17)
        Me.Controls.Add(Me.MetroLabel15)
        Me.Controls.Add(Me.MetroLabel11)
        Me.Controls.Add(Me.Save)
        Me.Controls.Add(Me.SaveMetroLabel)
        Me.Controls.Add(Me.ReceivingDataGrid)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.MetroLabel8)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.MetroLabel18)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmReceivingWeight"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.DigitalWeightPanel.ResumeLayout(False)
        Me.DigitalWeightPanel.PerformLayout()
        CType(Me.ReceivingDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpReceivingSELMatByRcNoResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ReceivingDetailGroupBox.ResumeLayout(False)
        Me.ReceivingDetailGroupBox.PerformLayout()
        Me.FromLTLCartonGroupBox.ResumeLayout(False)
        Me.FromLTLCartonGroupBox.PerformLayout()
        Me.SummaryReceivingGroupBox.ResumeLayout(False)
        Me.SummaryReceivingGroupBox.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.SpReceivingSELBuyerClassifierResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents UsernameLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ChangeUserButton As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents TotalReceivedWeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalBaleLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents DigitalWeightCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Save As MetroFramework.Controls.MetroTile
    Friend WithEvents PlaceLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TrucknoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents DigitalWeightPanel As MetroFramework.Controls.MetroPanel
    Friend WithEvents DigitalWeightBackTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile5 As MetroFramework.Controls.MetroTile
    Friend WithEvents WeightFromDigitalScaleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SaveFromDigitalTile As MetroFramework.Controls.MetroTile
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents TotalBuyingWeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivingDataGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel17 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SaveMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel18 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CompanyLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RcfromLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel21 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BaleTypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents LXPrintTile As MetroFramework.Controls.MetroTile
    Friend WithEvents LXCloseTile As MetroFramework.Controls.MetroTile
    Friend WithEvents DiffWeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel36 As MetroFramework.Controls.MetroLabel
    Friend WithEvents rightValue As MetroFramework.Controls.MetroLabel
    Friend WithEvents leftValue As MetroFramework.Controls.MetroLabel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents LXCountLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents LXBatchLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents XCountLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents XBatchLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents XPrintTile As MetroFramework.Controls.MetroTile
    Friend WithEvents XCloseTile As MetroFramework.Controls.MetroTile
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents CCountLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CBatchLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CPrintTile As MetroFramework.Controls.MetroTile
    Friend WithEvents CCloseTile As MetroFramework.Controls.MetroTile
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents BTCountLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents BTBatchLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents BTPrintTile As MetroFramework.Controls.MetroTile
    Friend WithEvents BTCloseTile As MetroFramework.Controls.MetroTile
    Friend WithEvents RcLinesLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents MetroLabel32 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CountMoistureLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents FarmerLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ClassifierComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents ReplaceBalesCheckbox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents RemainingTruckReceivingLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel50 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalBaleOnTruckLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel54 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TransportationDocumentCodeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents NTRMFromBuyingAlertLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvoiceNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel49 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel48 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ChkFromLTLCarton As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents TxtLTLCartons As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel52 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnLTLComplete As System.Windows.Forms.Button
    Friend WithEvents BtnLTLStart As System.Windows.Forms.Button
    Friend WithEvents ManualInputCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents SpReceivingSELBuyerClassifierResultBindingSource As BindingSource
    Friend WithEvents SpReceivingSELMatByRcNoResultBindingSource As BindingSource
    Friend WithEvents ReceivingDetailGroupBox As GroupBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel25 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TypeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel22 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel27 As MetroFramework.Controls.MetroLabel
    Friend WithEvents rcnoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents FromLTLCartonGroupBox As GroupBox
    Friend WithEvents SummaryReceivingGroupBox As GroupBox
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents VieeTransportDetailButton As Button
    Friend WithEvents DeleteButton As Button
    Friend WithEvents RefreshButton As Button
    Friend WithEvents SetupDigitalScaleButton As Button
    Friend WithEvents FinishButton As Button
    Friend WithEvents RejectButton As Button
    Friend WithEvents ChangeStaffUserButton As Button
    Friend WithEvents NTRMInspectionButton As Button
    Friend WithEvents ClassifyGradeComboBox As ComboBox
    Friend WithEvents GreenGradeComboBox As ComboBox
    Friend WithEvents BaleBarcodeTextBox As TextBox
    Friend WithEvents BaleNumberTextBox As TextBox
    Friend WithEvents SupplierComboBox As ComboBox
    Friend WithEvents BuyingWeightTextBox As TextBox
    Friend WithEvents ReceivingWeightTextBox As TextBox
    Friend WithEvents NPIFarmerCodeTextBox As TextBox
    Friend WithEvents NPIFarmerCodeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents bc As DataGridViewTextBoxColumn
    Friend WithEvents farmer_code As DataGridViewTextBoxColumn
    Friend WithEvents baleno As DataGridViewTextBoxColumn
    Friend WithEvents CompanyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents supplier As DataGridViewTextBoxColumn
    Friend WithEvents green As DataGridViewTextBoxColumn
    Friend WithEvents classify As DataGridViewTextBoxColumn
    Friend WithEvents weightbuy As DataGridViewTextBoxColumn
    Friend WithEvents weight As DataGridViewTextBoxColumn
    Friend WithEvents WeightDiffDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RemarkChecked As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents classifier As DataGridViewTextBoxColumn
    Friend WithEvents ReplaceBales As DataGridViewCheckBoxColumn
    Friend WithEvents LockedDataGridViewTextBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents LeaflockedDataGridViewTextBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents DocnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceiveduserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DtrecordDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BuyerComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents LTLRemarkResultLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents IsCUTRAGCheckBox As CheckBox
End Class
