﻿Public Class FrmAddReceivingNo
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Public _type As String
    Dim _buyerList As List(Of sp_Receiving_SEL_BuyerClassifier_Result)
    Dim _classifierList As List(Of sp_Receiving_SEL_BuyerClassifier_Result)

    Private Sub FrmAddReceivingNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            CompanyBindingSource.DataSource = _businessLayerService.CompanyBL().GetCompanies()
            TypeBindingSource.DataSource = _businessLayerService.TypeBL().GetTypes()
            SubTypeBindingSource.DataSource = _businessLayerService.TypeBL().GetSubTypes()
            SpReceivingSELMatWHResultBindingSource.DataSource = _businessLayerService.ReceivingDocumentBL().GetWarehouses()
            SpReceivingSELInvoiceDocNoResultBindingSource.DataSource = _businessLayerService.ReceivingDocumentBL().GetInvoices(_defaultCrop)

            _buyerList = _businessLayerService.ReceivingDocumentBL().GetClassifiers()
            _classifierList = _businessLayerService.ReceivingDocumentBL().GetClassifiers()
            BuyerComboBox.DataSource = _buyerList
            ClassifierComboBox.DataSource = _classifierList

            TypeComboBox.Text = _type

            If _type = "FC" Then
                SubtypeComboBox.Text = "F"
            ElseIf _type = "BU" Then
                SubtypeComboBox.Text = "BM"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub ClearForm()
        CompanyComboBox.Text = "STEC"
        TypeComboBox.Text = _type
        InvoiceCombobox.SelectedIndex = -1
        RcFromComboBox.SelectedIndex = -1
        PlaceComboBox.SelectedIndex = -1
        ClassifierComboBox.SelectedIndex = -1
        BuyerComboBox.SelectedIndex = -1
        ReplaceBalesCheckbox.Checked = False
        TransportDocNoTextBox.Text = ""
        InvoiceCombobox.SelectedIndex = -1
        TotalBalesTextBox.Text = ""
        TruckNoTextBox.Text = ""

        If TypeComboBox.Text = "BU" Then
            SubtypeComboBox.Text = "BM"
        ElseIf TypeComboBox.Text = "FC" Then
            SubtypeComboBox.Text = "F"
        Else
            SubtypeComboBox.SelectedIndex = -1
        End If
    End Sub
    Private Sub TransportDocNoTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles TransportDocNoTextBox.KeyDown
        Try
            TruckNoTextBox.Text = ""
            TotalBalesTextBox.Text = ""
            If e.KeyCode <> Keys.Enter Then
                Return
            End If

            Dim model = _businessLayerService.BurleyBuyingSystemBL().GetTransportationDocumentByID(TransportDocNoTextBox.Text)
            If model Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูลใบนำส่งหมายเลยดังกล่าวในระบบ Buying System โปรดตรวจสอบข้อมูลอีกครั้ง", "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            TruckNoTextBox.Text = model.TruckNumber
            TruckNoTextBox.ReadOnly = True
            TotalBalesTextBox.Text = model.TotalBale
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub CompanyComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CompanyComboBox.SelectedIndexChanged
        Try
            ShowRightForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TypeComboBox.SelectedIndexChanged
        Try
            ShowRightForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub InvoiceCombobox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles InvoiceCombobox.SelectedIndexChanged
        Try
            If InvoiceCombobox.SelectedIndex <= -1 Then
                Return
            End If

            TruckNoTextBox.Text = ""
            TotalBalesTextBox.Text = ""

            Dim balesFromInvoice As List(Of sp_Receiving_SEL_LoadBaleByInvoiceNo_Result)
            balesFromInvoice = _businessLayerService.ReceivingDocumentBL().GetBalesByInvoiceNo(InvoiceCombobox.SelectedValue)
            TotalBalesTextBox.Text = balesFromInvoice.Count.ToString("N0")
            TruckNoTextBox.Text = balesFromInvoice.FirstOrDefault().TruckNo
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub ReplaceBalesCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles ReplaceBalesCheckbox.CheckedChanged
        Try
            If ReplaceBalesCheckbox.Checked = True Then
                If CompanyComboBox.Text = "STEC" And TypeComboBox.Text = "BU" Then
                    TransportDocNoTextBox.Text = ""
                    TransportDocNoTextBox.Enabled = False
                    TransportDocNoTextBox.BackColor = Color.LightGray
                    TruckNoTextBox.Text = ""
                    TruckNoTextBox.Enabled = True
                    TruckNoTextBox.BackColor = Color.White
                    InvoiceCombobox.Enabled = False
                    InvoiceCombobox.BackColor = Color.LightGray
                ElseIf CompanyComboBox.Text = "LTL" Then
                    TransportDocNoTextBox.Text = ""
                    TransportDocNoTextBox.Enabled = False
                    TransportDocNoTextBox.BackColor = Color.LightGray
                    TruckNoTextBox.Text = ""
                    TruckNoTextBox.Enabled = True
                    TruckNoTextBox.BackColor = Color.White
                    InvoiceCombobox.Enabled = False
                    InvoiceCombobox.BackColor = Color.LightGray
                Else
                    TransportDocNoTextBox.Text = ""
                    TransportDocNoTextBox.Enabled = False
                    TransportDocNoTextBox.BackColor = Color.LightGray
                    TruckNoTextBox.Text = ""
                    TruckNoTextBox.Enabled = True
                    TruckNoTextBox.BackColor = Color.White
                    InvoiceCombobox.Enabled = False
                    InvoiceCombobox.BackColor = Color.LightGray
                End If
            Else
                If CompanyComboBox.Text = "STEC" And TypeComboBox.Text = "BU" Then
                    TransportDocNoTextBox.Text = ""
                    TransportDocNoTextBox.Enabled = True
                    TransportDocNoTextBox.BackColor = Color.White
                    TruckNoTextBox.Text = ""
                    TruckNoTextBox.Enabled = False
                    TruckNoTextBox.BackColor = Color.LightGray
                    InvoiceCombobox.Enabled = False
                    InvoiceCombobox.BackColor = Color.LightGray
                ElseIf CompanyComboBox.Text = "LTL" Then
                    TransportDocNoTextBox.Text = ""
                    TransportDocNoTextBox.Enabled = False
                    TransportDocNoTextBox.BackColor = Color.LightGray
                    TruckNoTextBox.Text = ""
                    TruckNoTextBox.Enabled = True
                    TruckNoTextBox.BackColor = Color.White
                    InvoiceCombobox.Enabled = True
                    InvoiceCombobox.BackColor = Color.White
                Else
                    TransportDocNoTextBox.Text = ""
                    TransportDocNoTextBox.Enabled = False
                    TransportDocNoTextBox.BackColor = Color.LightGray
                    TruckNoTextBox.Text = ""
                    TruckNoTextBox.Enabled = True
                    TruckNoTextBox.BackColor = Color.White
                    InvoiceCombobox.Enabled = False
                    InvoiceCombobox.BackColor = Color.LightGray
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If RcFromComboBox.SelectedIndex <= -1 Then
                MessageBox.Show("กรุณาเลือก Receiving form !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If PlaceComboBox.SelectedIndex <= -1 Then
                MessageBox.Show("กรุณาเลือก Place !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If ClassifierComboBox.SelectedIndex <= -1 Then
                MessageBox.Show("กรุณาเลือก Classifier !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If TruckNoTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ป้ายทะเบียนรถบรรทุก(Truck no.) !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If (TypeComboBox.Text = "FC" And SubtypeComboBox.Text <> "F") Or (TypeComboBox.Text = "BU" And SubtypeComboBox.Text = "F") Then
                MessageBox.Show("Subtype ไม่ถูกต้อง โปรดตรวจสอบ!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If ReplaceBalesCheckbox.Checked = False Then
                If TypeComboBox.Text = "BU" And CompanyComboBox.Text = "STEC" And TypeComboBox.Text = "Purchasing" Then
                    If TransportDocNoTextBox.Text = "" Then
                        MessageBox.Show("กรุณาคีย์ Transport Doc no. !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If
                    If TotalBalesTextBox.Text = "" Then
                        MessageBox.Show("Transport Doc no นี้ไม่ข้อมูลห่อยา กรุณาตรวจสอบข้อมูล !!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Return
                    End If
                End If
            End If

            If (CompanyComboBox.Text = "LTL") Then
                If ReplaceBalesCheckbox.Checked = True Then
                    _businessLayerService.ReceivingDocumentBL().AddMatRc(_defaultCrop, TypeComboBox.Text, SubtypeComboBox.Text, CompanyComboBox.Text, RcFromComboBox.Text, _username, PlaceComboBox.Text, TruckNoTextBox.Text, ClassifierComboBox.Text, BuyerComboBox.Text, "", "Receivinglines", "", "")
                    'db.sp_Receiving_INS_MatRC(rcno, _defaultCrop, TypeComboBox.Text, Now, PlaceComboBox.Text, TruckNoTextBox.Text, ClassifierComboBox.Text, ClassifierComboBox.Text, Now, RcFromComboBox.Text, "", "Receivinglines", _username, "", "")
                    MessageBox.Show("บันทึกข้อมูล Receiving no. เรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    _businessLayerService.ReceivingDocumentBL().AddMatRc(_defaultCrop, TypeComboBox.Text, SubtypeComboBox.Text, CompanyComboBox.Text, RcFromComboBox.Text, _username, PlaceComboBox.Text, TruckNoTextBox.Text, ClassifierComboBox.Text, BuyerComboBox.Text, "", "Receivinglines", "", InvoiceCombobox.Text)
                    'db.sp_Receiving_INS_MatRC(rcno, _defaultCrop, TypeComboBox.Text, Now, PlaceComboBox.Text, TruckNoTextBox.Text, ClassifierComboBox.Text, ClassifierComboBox.Text, Now, RcFromComboBox.Text, "", "Receivinglines", _username, "", InvoiceCombobox.Text)
                    MessageBox.Show("บันทึกข้อมูล Receiving no. เรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                _businessLayerService.ReceivingDocumentBL().AddMatRc(_defaultCrop, TypeComboBox.Text, SubtypeComboBox.Text, CompanyComboBox.Text, RcFromComboBox.Text, _username, PlaceComboBox.Text, TruckNoTextBox.Text, ClassifierComboBox.Text, BuyerComboBox.Text, "", "Receivinglines", TransportDocNoTextBox.Text, "")
                'db.sp_Receiving_INS_MatRC(rcno, _defaultCrop, TypeComboBox.Text, Now, PlaceComboBox.Text, TruckNoTextBox.Text, ClassifierComboBox.Text, ClassifierComboBox.Text, Now, RcFromComboBox.Text, "", "Receivinglines", _username, TransportDocNoTextBox.Text, "")
                MessageBox.Show("บันทึกข้อมูล Receiving no. เรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        ClearForm()
    End Sub

    Private Sub RcFromComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RcFromComboBox.SelectedIndexChanged
        Try
            ShowRightForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ShowRightForm()
        Try
            '********************************************************
            'ใช้เฉพาะการรับซื้อใบยาเพิ่มเติมโตยตรงจากคุณอุลย์และคุณเกียรติในปี 2022 เท่านั้น
            '********************************************************
            If DateTime.Now.Year = 2022 And DateTime.Now.Month = 8 And
                CompanyComboBox.Text = "STEC" And
                TypeComboBox.Text = "BU" And
                RcFromComboBox.Text = "Purchasing" Then
                TransportDocNoTextBox.Text = ""
                TransportDocNoTextBox.Visible = False
                InvoiceCombobox.Visible = False
                TotalBalesTextBox.Text = ""
                TotalBalesTextBox.Visible = False
                TruckNoTextBox.Text = ""
                TruckNoTextBox.Visible = True
                TruckNoTextBox.ReadOnly = False
                Return
            End If

            If CompanyComboBox.Text = "STEC" And
                TypeComboBox.Text = "BU" And
                RcFromComboBox.Text = "Purchasing" Then
                TransportDocNoTextBox.Text = ""
                TransportDocNoTextBox.Visible = True

                TotalBalesTextBox.Text = ""
                TotalBalesTextBox.Visible = True

                TruckNoTextBox.Text = ""
                TruckNoTextBox.Visible = True
                'TruckNoTextBox.ReadOnly = True

                InvoiceCombobox.Visible = False
            ElseIf CompanyComboBox.Text = "LTL" Then
                TransportDocNoTextBox.Text = ""
                TransportDocNoTextBox.Visible = False

                InvoiceCombobox.Visible = True

                TotalBalesTextBox.Text = ""
                TotalBalesTextBox.Visible = True

                TruckNoTextBox.Text = ""
                TruckNoTextBox.Visible = True
                TruckNoTextBox.ReadOnly = False
            Else
                TransportDocNoTextBox.Text = ""
                TransportDocNoTextBox.Visible = False

                InvoiceCombobox.Visible = False

                TotalBalesTextBox.Text = ""
                TotalBalesTextBox.Visible = False

                TruckNoTextBox.Text = ""
                TruckNoTextBox.Visible = True
                TruckNoTextBox.ReadOnly = False
            End If

            If TypeComboBox.Text = "BU" Then
                SubtypeComboBox.Text = "BM"
            ElseIf TypeComboBox.Text = "FC" Then
                SubtypeComboBox.Text = "F"
            Else
                SubtypeComboBox.SelectedIndex = -1
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class