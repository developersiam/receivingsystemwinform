﻿Public Class FrmLTLRemarkBrownTag
    Inherits MetroFramework.Forms.MetroForm

    Public _invoiceNo As String
    Public _remarkResult As String
    Public _LTLBarcode As String
    Public _resultType As Boolean

    Private Sub Save()
        Try
            'user จะต้องระบุเหตุผลของการใช้ป้ายสีน้ำตาล
            'หาก user เลือกเหตุผลข้อ 1-3 user จะต้องระบุหมายเลขบาร์โค้ตให้ตรงกับที่อยู่ใน invoice ปัจจุบัน หากระบบทำการค้นหาข้อมูลแล้วไม่พบ จะไม่อนุญาตให้ user เลือกเหุตผล 3 ข้อนี้ (บังคับให้เลือกข้ออื่น)
            'เมื่อ user เลือกเหตุผลข้อใดก็ตาม ให้ระบบทำเครื่องหมายดังนี้
            '[1] กลุ่มที่ต้องค้นหาข้อมูลใน invoice เพื่อนำไปแสดงบนฟอร์มบันทึกข้อมูล receiving โดยจะส่งค่า _LTLBarcodeReplace และ _remarkResult ไปยังฟอร์มหลัก
            '[2] กลุ่มที่ไม่ต้องค้นหาข้อมูลเพื่อนำไปแสดงผลบนฟอร์มบันทึกข้อมูล receiving โดยจะส่งค่า _remarkResult ไปยังฟอร์มหลัก

            If RdtLTLDiffWeight.Checked = True Then
                _remarkResult = "Diff. weight greater than or equal to 5 Kgs. "
                _resultType = True
            ElseIf RdtLTLCannotScan.Checked = True Then
                _remarkResult = "Can not scan"
                _resultType = True
            ElseIf RdtLTLNotinPackingList.Checked = True Then
                _remarkResult = "Not in the Packing List"
                _resultType = False
            ElseIf RdtLTLCreateNew.Checked = True Then
                _remarkResult = "Scraps"
                _resultType = False
            ElseIf RdtLTLDamageBarcode.Checked = True Then
                _remarkResult = "Damage barcode"
                _resultType = False
            ElseIf RdtLTLLostTicket.Checked = True Then
                _remarkResult = "Lost ticket"
                _resultType = False
            ElseIf RdtLTLLostTicketForBox.Checked = True Then
                _remarkResult = "Box"
                _resultType = False
            End If

            If _remarkResult Is Nothing Then
                MessageBox.Show("โปรดระบุเหตุผลของการใช้ป้ายสีน้ำตาล", "การแจ้งเตือน", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            Else
                If _resultType = False Then
                    _LTLBarcode = Nothing
                Else
                    If LTLBalebarcodeTextBox.Text = "" Then
                        MessageBox.Show("โปรดป้อนรหัสบาร์โค้ต", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        LTLBalebarcodeTextBox.Text = ""
                        LTLBalebarcodeTextBox.Focus()
                        Return
                    End If

                    'ถัาเลือกว่า can not scan จะต้องเช็คว่ามีใน packinglist หรือไม่ หากไม่มีให้ show message และไม่ให้ทำต่อ                  
                    Dim list = _businessLayerService.ReceivingBL().GetLTLLoadBaleNotReceiveByInvoiceNo(_invoiceNo)
                    Dim loadBale = list.SingleOrDefault(Function(x) x.BaleBarcode = LTLBalebarcodeTextBox.Text)

                    If loadBale Is Nothing Then
                        MessageBox.Show("ห่อยานี้ยังไม่ได้ Load ลงระบบฐานข้อมูลหรือถูกบันทึกรายการรับเข้ากรณีใข้ป้ายน้ำตาลแล้ว กรุณาตรวจสอบข้อมูลกับเจ้าหน้าที่ checker", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        LTLBalebarcodeTextBox.Text = ""
                        LTLBalebarcodeTextBox.Focus()
                        Return
                    End If
                    If loadBale.InvoiceNo <> _invoiceNo Then
                        MessageBox.Show("ห่อยานี้ Packlist Invoice no.ไม่ตรงกัน ไม่อนุญาตให้บันทึกข้อมูล หากต้องการบันทึกจะต้องใช้ป้ายบาร์โค้ดสีน้ำตาลและติดต่อ Checker ให้ทำการปรับปรุงข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If
                    If loadBale.Supplier Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูล suppplier
                        MessageBox.Show("ห่อยานี้ไม่มีข้อมูล Supplier", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If
                    If loadBale.WeightBuy Is Nothing Then 'ถ้ามีข้อมูลบาร์โค้ดแล้วแต่ไม่มีข้อมูลน้ำหนัก
                        MessageBox.Show("ห่อยานี้ไม่มีข้อมูลน้ำหนัก", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If
                    If loadBale.BaleBarcodeFromReplace <> "" Then
                        MessageBox.Show("ป้ายลาวที่คุณสแกนพบว่าถูกใช้บันทึกใน barcode " & loadBale.BaleBarcodeFromReplace & " แล้ว กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If

                    'รหัสบาร์โค้ตนี้ถูกตรวจสอบแล้วว่านำไปใช้งานต่อในหน้าหลักได้ เพื่อดึงข้อมูลห่อยาใน invoice มาโชว์ในฟอร์มกรอกข้อมูล
                    _LTLBarcode = loadBale.BaleBarcode
                End If
            End If

            DialogResult = DialogResult.OK
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Save()
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        LTLBalebarcodeTextBox.Text = ""
        RdtLTLDiffWeight.Checked = False
        RdtLTLCannotScan.Checked = False
        RdtLTLNotinPackingList.Checked = False
        RdtLTLDamageBarcode.Checked = False
        RdtLTLLostTicketForBox.Checked = False
    End Sub

    Private Sub FrmLTLRemarkBrownTag_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _LTLBarcode = Nothing
        _remarkResult = Nothing
        _resultType = True
    End Sub

    Private Sub FrmLTLRemarkBrownTag_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing

    End Sub

    Private Sub LTLBalebarcodeTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles LTLBalebarcodeTextBox.KeyUp
        If e.KeyCode = Keys.Enter And LTLBalebarcodeTextBox.Text.Length > 0 Then
            Save()
        End If
    End Sub

    Private Sub RdtLTLDiffWeight_CheckedChanged(sender As Object, e As EventArgs) Handles RdtLTLDiffWeight.CheckedChanged
        If RdtLTLDiffWeight.Checked = True Then
            LTLBalebarcodeTextBox.Focus()
        End If
    End Sub

    Private Sub RdtLTLCannotScan_CheckedChanged(sender As Object, e As EventArgs) Handles RdtLTLCannotScan.CheckedChanged
        If RdtLTLCannotScan.Checked = True Then
            LTLBalebarcodeTextBox.Focus()
        End If
    End Sub
End Class