﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSetDigitalScaleParameter
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel34 = New MetroFramework.Controls.MetroLabel()
        Me.ComPortComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel31 = New MetroFramework.Controls.MetroLabel()
        Me.BaudRateComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel27 = New MetroFramework.Controls.MetroLabel()
        Me.DatabitComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel30 = New MetroFramework.Controls.MetroLabel()
        Me.ParityComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel25 = New MetroFramework.Controls.MetroLabel()
        Me.StopBitsComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel34)
        Me.FlowLayoutPanel1.Controls.Add(Me.ComPortComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel31)
        Me.FlowLayoutPanel1.Controls.Add(Me.BaudRateComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel27)
        Me.FlowLayoutPanel1.Controls.Add(Me.DatabitComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel30)
        Me.FlowLayoutPanel1.Controls.Add(Me.ParityComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel25)
        Me.FlowLayoutPanel1.Controls.Add(Me.StopBitsComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(569, 416)
        Me.FlowLayoutPanel1.TabIndex = 109
        '
        'MetroLabel34
        '
        Me.MetroLabel34.AutoSize = True
        Me.MetroLabel34.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel34.Name = "MetroLabel34"
        Me.MetroLabel34.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel34.TabIndex = 100
        Me.MetroLabel34.Text = "Com port"
        '
        'ComPortComboBox
        '
        Me.ComPortComboBox.FormattingEnabled = True
        Me.ComPortComboBox.ItemHeight = 24
        Me.ComPortComboBox.Location = New System.Drawing.Point(3, 23)
        Me.ComPortComboBox.Name = "ComPortComboBox"
        Me.ComPortComboBox.Size = New System.Drawing.Size(146, 30)
        Me.ComPortComboBox.TabIndex = 98
        Me.ComPortComboBox.UseSelectable = True
        '
        'MetroLabel31
        '
        Me.MetroLabel31.AutoSize = True
        Me.MetroLabel31.Location = New System.Drawing.Point(3, 56)
        Me.MetroLabel31.Name = "MetroLabel31"
        Me.MetroLabel31.Size = New System.Drawing.Size(67, 20)
        Me.MetroLabel31.TabIndex = 103
        Me.MetroLabel31.Text = "BaudRate"
        '
        'BaudRateComboBox
        '
        Me.BaudRateComboBox.FormattingEnabled = True
        Me.BaudRateComboBox.ItemHeight = 24
        Me.BaudRateComboBox.Items.AddRange(New Object() {"1200", "2400", "4800", "9600", "14400", "19200"})
        Me.BaudRateComboBox.Location = New System.Drawing.Point(3, 79)
        Me.BaudRateComboBox.Name = "BaudRateComboBox"
        Me.BaudRateComboBox.Size = New System.Drawing.Size(146, 30)
        Me.BaudRateComboBox.TabIndex = 99
        Me.BaudRateComboBox.UseSelectable = True
        '
        'MetroLabel27
        '
        Me.MetroLabel27.AutoSize = True
        Me.MetroLabel27.Location = New System.Drawing.Point(3, 112)
        Me.MetroLabel27.Name = "MetroLabel27"
        Me.MetroLabel27.Size = New System.Drawing.Size(52, 20)
        Me.MetroLabel27.TabIndex = 106
        Me.MetroLabel27.Text = "Databit"
        '
        'DatabitComboBox
        '
        Me.DatabitComboBox.FormattingEnabled = True
        Me.DatabitComboBox.ItemHeight = 24
        Me.DatabitComboBox.Items.AddRange(New Object() {"8", "7", "6", "5", "4", "3", "2", "1"})
        Me.DatabitComboBox.Location = New System.Drawing.Point(3, 135)
        Me.DatabitComboBox.Name = "DatabitComboBox"
        Me.DatabitComboBox.Size = New System.Drawing.Size(146, 30)
        Me.DatabitComboBox.TabIndex = 101
        Me.DatabitComboBox.UseSelectable = True
        '
        'MetroLabel30
        '
        Me.MetroLabel30.AutoSize = True
        Me.MetroLabel30.Location = New System.Drawing.Point(3, 168)
        Me.MetroLabel30.Name = "MetroLabel30"
        Me.MetroLabel30.Size = New System.Drawing.Size(42, 20)
        Me.MetroLabel30.TabIndex = 105
        Me.MetroLabel30.Text = "Parity"
        '
        'ParityComboBox
        '
        Me.ParityComboBox.FormattingEnabled = True
        Me.ParityComboBox.ItemHeight = 24
        Me.ParityComboBox.Items.AddRange(New Object() {"None", "Odd", "Even", "Mark", "Space"})
        Me.ParityComboBox.Location = New System.Drawing.Point(3, 191)
        Me.ParityComboBox.Name = "ParityComboBox"
        Me.ParityComboBox.Size = New System.Drawing.Size(146, 30)
        Me.ParityComboBox.TabIndex = 102
        Me.ParityComboBox.UseSelectable = True
        '
        'MetroLabel25
        '
        Me.MetroLabel25.AutoSize = True
        Me.MetroLabel25.Location = New System.Drawing.Point(3, 224)
        Me.MetroLabel25.Name = "MetroLabel25"
        Me.MetroLabel25.Size = New System.Drawing.Size(57, 20)
        Me.MetroLabel25.TabIndex = 107
        Me.MetroLabel25.Text = "StopBits"
        '
        'StopBitsComboBox
        '
        Me.StopBitsComboBox.FormattingEnabled = True
        Me.StopBitsComboBox.ItemHeight = 24
        Me.StopBitsComboBox.Items.AddRange(New Object() {"None", "One", "Two", "OnePointFive"})
        Me.StopBitsComboBox.Location = New System.Drawing.Point(3, 247)
        Me.StopBitsComboBox.Name = "StopBitsComboBox"
        Me.StopBitsComboBox.Size = New System.Drawing.Size(146, 30)
        Me.StopBitsComboBox.TabIndex = 104
        Me.StopBitsComboBox.UseSelectable = True
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.SaveButton)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(4, 284)
        Me.FlowLayoutPanel6.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(63, 39)
        Me.FlowLayoutPanel6.TabIndex = 149
        '
        'SaveButton
        '
        Me.SaveButton.AutoSize = True
        Me.SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SaveButton.BackColor = System.Drawing.Color.DarkOrange
        Me.SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SaveButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveButton.FlatAppearance.BorderSize = 0
        Me.SaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SaveButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.ForeColor = System.Drawing.Color.Black
        Me.SaveButton.Location = New System.Drawing.Point(4, 4)
        Me.SaveButton.Margin = New System.Windows.Forms.Padding(4)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(55, 31)
        Me.SaveButton.TabIndex = 145
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'FrmSetDigitalScaleParameter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(609, 496)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "FrmSetDigitalScaleParameter"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Set Digital Scale Parameter"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MetroLabel34 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ComPortComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel31 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BaudRateComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel27 As MetroFramework.Controls.MetroLabel
    Friend WithEvents DatabitComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel30 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ParityComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel25 As MetroFramework.Controls.MetroLabel
    Friend WithEvents StopBitsComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents SaveButton As Button
End Class
