﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCallReceivingNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MatRcDataGrid = New MetroFramework.Controls.MetroGrid()
        Me.RcnoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlaceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RcfromDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TransportationDocumentCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvoiceNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrucknoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CountOfBaleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfWeightDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfWeightbuyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.finishtime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClassifierDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.userlocked = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CheckerlockedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CancleRcNoStatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.RemarkCancleRcNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpReceivingSELMatRCResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CropDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Place = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RcnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RcfromDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrucknoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClassifierDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CountOfBalesDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rcno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.crop = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CheckerLockedCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.CreateDocumentButton = New System.Windows.Forms.Button()
        Me.DeleteDocumentButton = New System.Windows.Forms.Button()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.ReceivingButton = New System.Windows.Forms.Button()
        Me.ReportButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.company = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rcfrom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.truckno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InvoiceNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CountOfBales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfBales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfWeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.MatRcDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpReceivingSELMatRCResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 3)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(35, 32)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.icons8_contacts_32
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(34, 12)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(70, 15)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MatRcDataGrid
        '
        Me.MatRcDataGrid.AllowUserToAddRows = False
        Me.MatRcDataGrid.AllowUserToDeleteRows = False
        Me.MatRcDataGrid.AllowUserToOrderColumns = True
        Me.MatRcDataGrid.AllowUserToResizeRows = False
        Me.MatRcDataGrid.AutoGenerateColumns = False
        Me.MatRcDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MatRcDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.MatRcDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRcDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MatRcDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MatRcDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRcDataGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.MatRcDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MatRcDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RcnoDataGridViewTextBoxColumn1, Me.TypeDataGridViewTextBoxColumn2, Me.CompanyDataGridViewTextBoxColumn, Me.PlaceDataGridViewTextBoxColumn, Me.RcfromDataGridViewTextBoxColumn1, Me.TransportationDocumentCodeDataGridViewTextBoxColumn, Me.InvoiceNoDataGridViewTextBoxColumn, Me.TrucknoDataGridViewTextBoxColumn1, Me.CountOfBaleDataGridViewTextBoxColumn, Me.SumOfWeightDataGridViewTextBoxColumn, Me.SumOfWeightbuyDataGridViewTextBoxColumn, Me.finishtime, Me.DateDataGridViewTextBoxColumn, Me.ClassifierDataGridViewTextBoxColumn1, Me.userlocked, Me.CheckerlockedDataGridViewTextBoxColumn, Me.CancleRcNoStatusDataGridViewTextBoxColumn, Me.RemarkCancleRcNoDataGridViewTextBoxColumn})
        Me.MatRcDataGrid.DataSource = Me.SpReceivingSELMatRCResultBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MatRcDataGrid.DefaultCellStyle = DataGridViewCellStyle6
        Me.MatRcDataGrid.EnableHeadersVisualStyles = False
        Me.MatRcDataGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRcDataGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRcDataGrid.Location = New System.Drawing.Point(2, 65)
        Me.MatRcDataGrid.Margin = New System.Windows.Forms.Padding(2)
        Me.MatRcDataGrid.Name = "MatRcDataGrid"
        Me.MatRcDataGrid.ReadOnly = True
        Me.MatRcDataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MatRcDataGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.MatRcDataGrid.RowHeadersVisible = False
        Me.MatRcDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MatRcDataGrid.RowTemplate.Height = 24
        Me.MatRcDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MatRcDataGrid.Size = New System.Drawing.Size(1145, 556)
        Me.MatRcDataGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.MatRcDataGrid.TabIndex = 118
        '
        'RcnoDataGridViewTextBoxColumn1
        '
        Me.RcnoDataGridViewTextBoxColumn1.DataPropertyName = "rcno"
        Me.RcnoDataGridViewTextBoxColumn1.HeaderText = "rcno"
        Me.RcnoDataGridViewTextBoxColumn1.Name = "RcnoDataGridViewTextBoxColumn1"
        Me.RcnoDataGridViewTextBoxColumn1.ReadOnly = True
        Me.RcnoDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.RcnoDataGridViewTextBoxColumn1.Width = 58
        '
        'TypeDataGridViewTextBoxColumn2
        '
        Me.TypeDataGridViewTextBoxColumn2.DataPropertyName = "type"
        Me.TypeDataGridViewTextBoxColumn2.HeaderText = "type"
        Me.TypeDataGridViewTextBoxColumn2.Name = "TypeDataGridViewTextBoxColumn2"
        Me.TypeDataGridViewTextBoxColumn2.ReadOnly = True
        Me.TypeDataGridViewTextBoxColumn2.Width = 59
        '
        'CompanyDataGridViewTextBoxColumn
        '
        Me.CompanyDataGridViewTextBoxColumn.DataPropertyName = "company"
        Me.CompanyDataGridViewTextBoxColumn.HeaderText = "company"
        Me.CompanyDataGridViewTextBoxColumn.Name = "CompanyDataGridViewTextBoxColumn"
        Me.CompanyDataGridViewTextBoxColumn.ReadOnly = True
        Me.CompanyDataGridViewTextBoxColumn.Width = 87
        '
        'PlaceDataGridViewTextBoxColumn
        '
        Me.PlaceDataGridViewTextBoxColumn.DataPropertyName = "place"
        Me.PlaceDataGridViewTextBoxColumn.HeaderText = "place"
        Me.PlaceDataGridViewTextBoxColumn.Name = "PlaceDataGridViewTextBoxColumn"
        Me.PlaceDataGridViewTextBoxColumn.ReadOnly = True
        Me.PlaceDataGridViewTextBoxColumn.Width = 64
        '
        'RcfromDataGridViewTextBoxColumn1
        '
        Me.RcfromDataGridViewTextBoxColumn1.DataPropertyName = "rcfrom"
        Me.RcfromDataGridViewTextBoxColumn1.HeaderText = "rcfrom"
        Me.RcfromDataGridViewTextBoxColumn1.Name = "RcfromDataGridViewTextBoxColumn1"
        Me.RcfromDataGridViewTextBoxColumn1.ReadOnly = True
        Me.RcfromDataGridViewTextBoxColumn1.Width = 72
        '
        'TransportationDocumentCodeDataGridViewTextBoxColumn
        '
        Me.TransportationDocumentCodeDataGridViewTextBoxColumn.DataPropertyName = "TransportationDocumentCode"
        Me.TransportationDocumentCodeDataGridViewTextBoxColumn.HeaderText = "transport code"
        Me.TransportationDocumentCodeDataGridViewTextBoxColumn.Name = "TransportationDocumentCodeDataGridViewTextBoxColumn"
        Me.TransportationDocumentCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.TransportationDocumentCodeDataGridViewTextBoxColumn.Width = 121
        '
        'InvoiceNoDataGridViewTextBoxColumn
        '
        Me.InvoiceNoDataGridViewTextBoxColumn.DataPropertyName = "InvoiceNo"
        Me.InvoiceNoDataGridViewTextBoxColumn.HeaderText = "invoice#"
        Me.InvoiceNoDataGridViewTextBoxColumn.Name = "InvoiceNoDataGridViewTextBoxColumn"
        Me.InvoiceNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.InvoiceNoDataGridViewTextBoxColumn.Width = 83
        '
        'TrucknoDataGridViewTextBoxColumn1
        '
        Me.TrucknoDataGridViewTextBoxColumn1.DataPropertyName = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn1.HeaderText = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn1.Name = "TrucknoDataGridViewTextBoxColumn1"
        Me.TrucknoDataGridViewTextBoxColumn1.ReadOnly = True
        Me.TrucknoDataGridViewTextBoxColumn1.Width = 78
        '
        'CountOfBaleDataGridViewTextBoxColumn
        '
        Me.CountOfBaleDataGridViewTextBoxColumn.DataPropertyName = "CountOfBale"
        DataGridViewCellStyle2.Format = "N0"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.CountOfBaleDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.CountOfBaleDataGridViewTextBoxColumn.HeaderText = "bales"
        Me.CountOfBaleDataGridViewTextBoxColumn.Name = "CountOfBaleDataGridViewTextBoxColumn"
        Me.CountOfBaleDataGridViewTextBoxColumn.ReadOnly = True
        Me.CountOfBaleDataGridViewTextBoxColumn.Width = 64
        '
        'SumOfWeightDataGridViewTextBoxColumn
        '
        Me.SumOfWeightDataGridViewTextBoxColumn.DataPropertyName = "SumOfWeight"
        DataGridViewCellStyle3.Format = "N1"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.SumOfWeightDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.SumOfWeightDataGridViewTextBoxColumn.HeaderText = "weight"
        Me.SumOfWeightDataGridViewTextBoxColumn.Name = "SumOfWeightDataGridViewTextBoxColumn"
        Me.SumOfWeightDataGridViewTextBoxColumn.ReadOnly = True
        Me.SumOfWeightDataGridViewTextBoxColumn.Width = 74
        '
        'SumOfWeightbuyDataGridViewTextBoxColumn
        '
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.DataPropertyName = "SumOfWeightbuy"
        DataGridViewCellStyle4.Format = "N1"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.HeaderText = "weight buy"
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.Name = "SumOfWeightbuyDataGridViewTextBoxColumn"
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.ReadOnly = True
        '
        'finishtime
        '
        Me.finishtime.DataPropertyName = "finishtime"
        Me.finishtime.HeaderText = "finishtime"
        Me.finishtime.Name = "finishtime"
        Me.finishtime.ReadOnly = True
        Me.finishtime.Width = 93
        '
        'DateDataGridViewTextBoxColumn
        '
        Me.DateDataGridViewTextBoxColumn.DataPropertyName = "date"
        DataGridViewCellStyle5.Format = "g"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DateDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.DateDataGridViewTextBoxColumn.HeaderText = "rcno date"
        Me.DateDataGridViewTextBoxColumn.Name = "DateDataGridViewTextBoxColumn"
        Me.DateDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DateDataGridViewTextBoxColumn.Width = 89
        '
        'ClassifierDataGridViewTextBoxColumn1
        '
        Me.ClassifierDataGridViewTextBoxColumn1.DataPropertyName = "classifier"
        Me.ClassifierDataGridViewTextBoxColumn1.HeaderText = "classifier"
        Me.ClassifierDataGridViewTextBoxColumn1.Name = "ClassifierDataGridViewTextBoxColumn1"
        Me.ClassifierDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ClassifierDataGridViewTextBoxColumn1.Width = 85
        '
        'userlocked
        '
        Me.userlocked.DataPropertyName = "userlocked"
        Me.userlocked.HeaderText = "userlocked"
        Me.userlocked.Name = "userlocked"
        Me.userlocked.ReadOnly = True
        Me.userlocked.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.userlocked.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.userlocked.Width = 99
        '
        'CheckerlockedDataGridViewTextBoxColumn
        '
        Me.CheckerlockedDataGridViewTextBoxColumn.DataPropertyName = "checkerlocked"
        Me.CheckerlockedDataGridViewTextBoxColumn.HeaderText = "checker locked"
        Me.CheckerlockedDataGridViewTextBoxColumn.Name = "CheckerlockedDataGridViewTextBoxColumn"
        Me.CheckerlockedDataGridViewTextBoxColumn.ReadOnly = True
        Me.CheckerlockedDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CheckerlockedDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CheckerlockedDataGridViewTextBoxColumn.Width = 123
        '
        'CancleRcNoStatusDataGridViewTextBoxColumn
        '
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.DataPropertyName = "CancleRcNoStatus"
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.HeaderText = "cancel status"
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.Name = "CancleRcNoStatusDataGridViewTextBoxColumn"
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.ReadOnly = True
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CancleRcNoStatusDataGridViewTextBoxColumn.Width = 110
        '
        'RemarkCancleRcNoDataGridViewTextBoxColumn
        '
        Me.RemarkCancleRcNoDataGridViewTextBoxColumn.DataPropertyName = "RemarkCancleRcNo"
        Me.RemarkCancleRcNoDataGridViewTextBoxColumn.HeaderText = "remark"
        Me.RemarkCancleRcNoDataGridViewTextBoxColumn.Name = "RemarkCancleRcNoDataGridViewTextBoxColumn"
        Me.RemarkCancleRcNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.RemarkCancleRcNoDataGridViewTextBoxColumn.Width = 75
        '
        'SpReceivingSELMatRCResultBindingSource
        '
        Me.SpReceivingSELMatRCResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_MatRC_Result)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(36, 19)
        Me.MetroLabel2.TabIndex = 119
        Me.MetroLabel2.Text = "Type"
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource
        Me.TypeComboBox.DisplayMember = "type1"
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 23
        Me.TypeComboBox.Location = New System.Drawing.Point(2, 21)
        Me.TypeComboBox.Margin = New System.Windows.Forms.Padding(2)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(100, 29)
        Me.TypeComboBox.TabIndex = 120
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type1"
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataSource = GetType(ReceivingSystem.type)
        '
        'CropDataGridViewTextBoxColumn
        '
        Me.CropDataGridViewTextBoxColumn.DataPropertyName = "crop"
        Me.CropDataGridViewTextBoxColumn.HeaderText = "Crop"
        Me.CropDataGridViewTextBoxColumn.Name = "CropDataGridViewTextBoxColumn"
        Me.CropDataGridViewTextBoxColumn.Width = 70
        '
        'Place
        '
        Me.Place.DataPropertyName = "place"
        Me.Place.HeaderText = "Place"
        Me.Place.Name = "Place"
        Me.Place.Width = 73
        '
        'TypeDataGridViewTextBoxColumn
        '
        Me.TypeDataGridViewTextBoxColumn.DataPropertyName = "type"
        Me.TypeDataGridViewTextBoxColumn.HeaderText = "Type"
        Me.TypeDataGridViewTextBoxColumn.Name = "TypeDataGridViewTextBoxColumn"
        Me.TypeDataGridViewTextBoxColumn.Width = 69
        '
        'RcnoDataGridViewTextBoxColumn
        '
        Me.RcnoDataGridViewTextBoxColumn.DataPropertyName = "rcno"
        Me.RcnoDataGridViewTextBoxColumn.HeaderText = "RC no."
        Me.RcnoDataGridViewTextBoxColumn.Name = "RcnoDataGridViewTextBoxColumn"
        Me.RcnoDataGridViewTextBoxColumn.Width = 82
        '
        'RcfromDataGridViewTextBoxColumn
        '
        Me.RcfromDataGridViewTextBoxColumn.DataPropertyName = "rcfrom"
        Me.RcfromDataGridViewTextBoxColumn.HeaderText = "From"
        Me.RcfromDataGridViewTextBoxColumn.Name = "RcfromDataGridViewTextBoxColumn"
        Me.RcfromDataGridViewTextBoxColumn.Width = 74
        '
        'TrucknoDataGridViewTextBoxColumn
        '
        Me.TrucknoDataGridViewTextBoxColumn.DataPropertyName = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn.HeaderText = "Truck no."
        Me.TrucknoDataGridViewTextBoxColumn.Name = "TrucknoDataGridViewTextBoxColumn"
        Me.TrucknoDataGridViewTextBoxColumn.Width = 104
        '
        'ClassifierDataGridViewTextBoxColumn
        '
        Me.ClassifierDataGridViewTextBoxColumn.DataPropertyName = "classifier"
        Me.ClassifierDataGridViewTextBoxColumn.HeaderText = "Classifier"
        Me.ClassifierDataGridViewTextBoxColumn.Name = "ClassifierDataGridViewTextBoxColumn"
        Me.ClassifierDataGridViewTextBoxColumn.Width = 102
        '
        'CountOfBalesDataGridViewTextBoxColumn
        '
        Me.CountOfBalesDataGridViewTextBoxColumn.DataPropertyName = "CountOfBales"
        Me.CountOfBalesDataGridViewTextBoxColumn.HeaderText = "CountOfBales"
        Me.CountOfBalesDataGridViewTextBoxColumn.Name = "CountOfBalesDataGridViewTextBoxColumn"
        Me.CountOfBalesDataGridViewTextBoxColumn.Width = 138
        '
        'rcno
        '
        Me.rcno.DataPropertyName = "rcno"
        Me.rcno.HeaderText = "rcno"
        Me.rcno.Name = "rcno"
        '
        'crop
        '
        Me.crop.DataPropertyName = "crop"
        Me.crop.HeaderText = "crop"
        Me.crop.Name = "crop"
        '
        'type
        '
        Me.type.DataPropertyName = "type"
        Me.type.HeaderText = "type"
        Me.type.Name = "type"
        '
        'CheckerLockedCheckBox
        '
        Me.CheckerLockedCheckBox.AutoSize = True
        Me.CheckerLockedCheckBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.CheckerLockedCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.CheckerLockedCheckBox.Location = New System.Drawing.Point(110, 36)
        Me.CheckerLockedCheckBox.Margin = New System.Windows.Forms.Padding(2)
        Me.CheckerLockedCheckBox.Name = "CheckerLockedCheckBox"
        Me.CheckerLockedCheckBox.Size = New System.Drawing.Size(115, 19)
        Me.CheckerLockedCheckBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CheckerLockedCheckBox.TabIndex = 121
        Me.CheckerLockedCheckBox.Text = "Cheker Locked"
        Me.CheckerLockedCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CheckerLockedCheckBox.UseSelectable = True
        '
        'CreateDocumentButton
        '
        Me.CreateDocumentButton.AutoSize = True
        Me.CreateDocumentButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CreateDocumentButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.CreateDocumentButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CreateDocumentButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CreateDocumentButton.FlatAppearance.BorderSize = 0
        Me.CreateDocumentButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.CreateDocumentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CreateDocumentButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreateDocumentButton.ForeColor = System.Drawing.Color.Black
        Me.CreateDocumentButton.Image = Global.ReceivingSystem.My.Resources.Resources.Plus_32
        Me.CreateDocumentButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.CreateDocumentButton.Location = New System.Drawing.Point(3, 3)
        Me.CreateDocumentButton.Name = "CreateDocumentButton"
        Me.CreateDocumentButton.Size = New System.Drawing.Size(70, 62)
        Me.CreateDocumentButton.TabIndex = 135
        Me.CreateDocumentButton.Text = "Create"
        Me.CreateDocumentButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.CreateDocumentButton.UseVisualStyleBackColor = True
        '
        'DeleteDocumentButton
        '
        Me.DeleteDocumentButton.AutoSize = True
        Me.DeleteDocumentButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DeleteDocumentButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.DeleteDocumentButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteDocumentButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DeleteDocumentButton.FlatAppearance.BorderSize = 0
        Me.DeleteDocumentButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.DeleteDocumentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.DeleteDocumentButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteDocumentButton.ForeColor = System.Drawing.Color.Black
        Me.DeleteDocumentButton.Image = Global.ReceivingSystem.My.Resources.Resources.Cancel32
        Me.DeleteDocumentButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DeleteDocumentButton.Location = New System.Drawing.Point(3, 139)
        Me.DeleteDocumentButton.Name = "DeleteDocumentButton"
        Me.DeleteDocumentButton.Size = New System.Drawing.Size(70, 62)
        Me.DeleteDocumentButton.TabIndex = 136
        Me.DeleteDocumentButton.Text = "Delete"
        Me.DeleteDocumentButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.DeleteDocumentButton.UseVisualStyleBackColor = True
        '
        'RefreshButton
        '
        Me.RefreshButton.AutoSize = True
        Me.RefreshButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RefreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.RefreshButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RefreshButton.FlatAppearance.BorderSize = 0
        Me.RefreshButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RefreshButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RefreshButton.ForeColor = System.Drawing.Color.Black
        Me.RefreshButton.Image = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.RefreshButton.Location = New System.Drawing.Point(3, 207)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(70, 62)
        Me.RefreshButton.TabIndex = 137
        Me.RefreshButton.Text = "Refresh"
        Me.RefreshButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.RefreshButton.UseVisualStyleBackColor = True
        '
        'ReceivingButton
        '
        Me.ReceivingButton.AutoSize = True
        Me.ReceivingButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ReceivingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ReceivingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ReceivingButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReceivingButton.FlatAppearance.BorderSize = 0
        Me.ReceivingButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ReceivingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ReceivingButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReceivingButton.ForeColor = System.Drawing.Color.Black
        Me.ReceivingButton.Image = Global.ReceivingSystem.My.Resources.Resources.Scale32
        Me.ReceivingButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ReceivingButton.Location = New System.Drawing.Point(3, 71)
        Me.ReceivingButton.Name = "ReceivingButton"
        Me.ReceivingButton.Size = New System.Drawing.Size(70, 62)
        Me.ReceivingButton.TabIndex = 138
        Me.ReceivingButton.Text = "Receive"
        Me.ReceivingButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ReceivingButton.UseVisualStyleBackColor = False
        '
        'ReportButton
        '
        Me.ReportButton.AutoSize = True
        Me.ReportButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ReportButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ReportButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ReportButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportButton.FlatAppearance.BorderSize = 0
        Me.ReportButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ReportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ReportButton.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReportButton.ForeColor = System.Drawing.Color.Black
        Me.ReportButton.Image = Global.ReceivingSystem.My.Resources.Resources.PurchaseOrder32
        Me.ReportButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ReportButton.Location = New System.Drawing.Point(3, 275)
        Me.ReportButton.Name = "ReportButton"
        Me.ReportButton.Size = New System.Drawing.Size(70, 62)
        Me.ReportButton.TabIndex = 139
        Me.ReportButton.Text = "Report"
        Me.ReportButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ReportButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.CreateDocumentButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.ReceivingButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.DeleteDocumentButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.RefreshButton)
        Me.FlowLayoutPanel1.Controls.Add(Me.ReportButton)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(76, 340)
        Me.FlowLayoutPanel1.TabIndex = 140
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.MetroLabel2)
        Me.FlowLayoutPanel2.Controls.Add(Me.TypeComboBox)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(2, 2)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(104, 53)
        Me.FlowLayoutPanel2.TabIndex = 141
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel5)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 47)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1233, 624)
        Me.FlowLayoutPanel3.TabIndex = 142
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.Controls.Add(Me.FlowLayoutPanel4)
        Me.FlowLayoutPanel5.Controls.Add(Me.MatRcDataGrid)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(84, 2)
        Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(1147, 620)
        Me.FlowLayoutPanel5.TabIndex = 144
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel4.Controls.Add(Me.CheckerLockedCheckBox)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(227, 57)
        Me.FlowLayoutPanel4.TabIndex = 143
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "rcno"
        Me.DataGridViewTextBoxColumn1.HeaderText = "rcno"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 58
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "crop"
        Me.DataGridViewTextBoxColumn2.HeaderText = "crop"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'TypeDataGridViewTextBoxColumn1
        '
        Me.TypeDataGridViewTextBoxColumn1.DataPropertyName = "type"
        Me.TypeDataGridViewTextBoxColumn1.HeaderText = "type"
        Me.TypeDataGridViewTextBoxColumn1.Name = "TypeDataGridViewTextBoxColumn1"
        Me.TypeDataGridViewTextBoxColumn1.Width = 59
        '
        'company
        '
        Me.company.DataPropertyName = "company"
        Me.company.HeaderText = "company"
        Me.company.Name = "company"
        Me.company.ReadOnly = True
        '
        'rcfrom
        '
        Me.rcfrom.DataPropertyName = "rcfrom"
        Me.rcfrom.HeaderText = "rcfrom"
        Me.rcfrom.Name = "rcfrom"
        Me.rcfrom.Width = 72
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "TransportationDocumentCode"
        Me.Column1.HeaderText = "transportcode"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 118
        '
        'truckno
        '
        Me.truckno.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.truckno.DataPropertyName = "truckno"
        Me.truckno.HeaderText = "truckno"
        Me.truckno.Name = "truckno"
        '
        'InvoiceNo
        '
        Me.InvoiceNo.DataPropertyName = "InvoiceNo"
        Me.InvoiceNo.HeaderText = "invoice#"
        Me.InvoiceNo.Name = "InvoiceNo"
        Me.InvoiceNo.Width = 83
        '
        'CountOfBales
        '
        Me.CountOfBales.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.CountOfBales.DataPropertyName = "CountOfBale"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Format = "N0"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.CountOfBales.DefaultCellStyle = DataGridViewCellStyle8
        Me.CountOfBales.HeaderText = "bales"
        Me.CountOfBales.Name = "CountOfBales"
        '
        'SumOfBales
        '
        Me.SumOfBales.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SumOfBales.DataPropertyName = "SumOfWeightbuy"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.SumOfBales.DefaultCellStyle = DataGridViewCellStyle9
        Me.SumOfBales.HeaderText = "weightbuy"
        Me.SumOfBales.Name = "SumOfBales"
        '
        'SumOfWeight
        '
        Me.SumOfWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SumOfWeight.DataPropertyName = "SumOfWeight"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.SumOfWeight.DefaultCellStyle = DataGridViewCellStyle10
        Me.SumOfWeight.HeaderText = "weight"
        Me.SumOfWeight.Name = "SumOfWeight"
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel6.Controls.Add(Me.FlowLayoutPanel3)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(1240, 674)
        Me.FlowLayoutPanel6.TabIndex = 143
        '
        'Panel1
        '
        Me.Panel1.AutoSize = True
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.Controls.Add(Me.MetroTile1)
        Me.Panel1.Controls.Add(Me.UsernameMetroLabel)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(107, 38)
        Me.Panel1.TabIndex = 144
        '
        'FrmCallReceivingNo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1280, 754)
        Me.Controls.Add(Me.FlowLayoutPanel6)
        Me.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmCallReceivingNo"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Receiving Document (matrc)"
        CType(Me.MatRcDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpReceivingSELMatRCResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel5.PerformLayout()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel4.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MatRcDataGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents CropDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Place As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RcnoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RcfromDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TrucknoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClassifierDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CountOfBalesDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rcno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents crop As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CheckerLockedCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents CreateDocumentButton As Button
    Friend WithEvents DeleteDocumentButton As Button
    Friend WithEvents RefreshButton As Button
    Friend WithEvents ReceivingButton As Button
    Friend WithEvents ReportButton As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents company As DataGridViewTextBoxColumn
    Friend WithEvents rcfrom As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents truckno As DataGridViewTextBoxColumn
    Friend WithEvents InvoiceNo As DataGridViewTextBoxColumn
    Friend WithEvents CountOfBales As DataGridViewTextBoxColumn
    Friend WithEvents SumOfBales As DataGridViewTextBoxColumn
    Friend WithEvents SumOfWeight As DataGridViewTextBoxColumn
    Friend WithEvents SpReceivingSELMatRCResultBindingSource As BindingSource
    Friend WithEvents TypeBindingSource As BindingSource
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents RcnoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents CompanyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PlaceDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RcfromDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents TransportationDocumentCodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InvoiceNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TrucknoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CountOfBaleDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SumOfWeightDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SumOfWeightbuyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents finishtime As DataGridViewTextBoxColumn
    Friend WithEvents DateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ClassifierDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents userlocked As DataGridViewCheckBoxColumn
    Friend WithEvents CheckerlockedDataGridViewTextBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents CancleRcNoStatusDataGridViewTextBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents RemarkCancleRcNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
