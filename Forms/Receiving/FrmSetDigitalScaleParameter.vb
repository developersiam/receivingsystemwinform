﻿Public Class FrmSetDigitalScaleParameter
    Inherits MetroFramework.Forms.MetroForm

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            My.Settings.ComPort = ComPortComboBox.Text
            My.Settings.BourdRate = BaudRateComboBox.Text
            My.Settings.Parity = ParityComboBox.Text
            My.Settings.Databit = DatabitComboBox.Text
            My.Settings.StopBits = StopBitsComboBox.Text
            My.Settings.Save()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CancelButton_Click(sender As Object, e As EventArgs) 
        Me.Close()
    End Sub

    Private Sub FrmSetDigitalScaleParameter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Dim allPorts As String() = System.IO.Ports.SerialPort.GetPortNames
            ComPortComboBox.Items.Clear()
            ComPortComboBox.MaxDropDownItems = allPorts.Length
            For Each port As String In allPorts
                ComPortComboBox.Items.Add(port)
            Next

            'Get user setting parameter from user machine.
            If My.Settings.ComPort <> "" Then
                'Get ComboBox Index from My.Setting.
                Dim comportComboBoxIndex As Integer = ComPortComboBox.FindStringExact(My.Settings.ComPort)
                Dim baudRateComboBoxIndex As Integer = BaudRateComboBox.FindStringExact(My.Settings.BourdRate)
                Dim databitComboBoxIndex As Integer = DatabitComboBox.FindStringExact(My.Settings.Databit)
                Dim parityComboBoxIndex As Integer = ParityComboBox.FindStringExact(My.Settings.Parity)
                Dim stopBitComboBoxIndex As Integer = StopBitsComboBox.FindStringExact(My.Settings.StopBits)

                'Set ComboBox Selected Index.
                ComPortComboBox.SelectedIndex = comportComboBoxIndex
                BaudRateComboBox.SelectedIndex = baudRateComboBoxIndex
                DatabitComboBox.SelectedIndex = databitComboBoxIndex
                ParityComboBox.SelectedIndex = parityComboBoxIndex
                StopBitsComboBox.SelectedIndex = stopBitComboBoxIndex
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class