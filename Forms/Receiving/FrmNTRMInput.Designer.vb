﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNTRMInput
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.NTRMRemarkTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel33 = New MetroFramework.Controls.MetroLabel()
        Me.BtnDelete = New System.Windows.Forms.Button()
        Me.NTRMQuantityTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel39 = New MetroFramework.Controls.MetroLabel()
        Me.NTRMInspectionDataGrid = New System.Windows.Forms.DataGridView()
        Me.BaleBarCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NTRMTypeCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NTRMTypeNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NTRMCategoryDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InspectionLocationDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InspectionDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InspectionUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpNTRMSELNTRMInspectionByBCResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.NTRMTypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.NTRMInspectionDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpNTRMSELNTRMInspectionByBCResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'NTRMRemarkTextBox
        '
        '
        '
        '
        Me.NTRMRemarkTextBox.CustomButton.Image = Nothing
        Me.NTRMRemarkTextBox.CustomButton.Location = New System.Drawing.Point(93, 2)
        Me.NTRMRemarkTextBox.CustomButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NTRMRemarkTextBox.CustomButton.Name = ""
        Me.NTRMRemarkTextBox.CustomButton.Size = New System.Drawing.Size(17, 19)
        Me.NTRMRemarkTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NTRMRemarkTextBox.CustomButton.TabIndex = 1
        Me.NTRMRemarkTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NTRMRemarkTextBox.CustomButton.UseSelectable = True
        Me.NTRMRemarkTextBox.CustomButton.Visible = False
        Me.NTRMRemarkTextBox.Lines = New String(-1) {}
        Me.NTRMRemarkTextBox.Location = New System.Drawing.Point(2, 122)
        Me.NTRMRemarkTextBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NTRMRemarkTextBox.MaxLength = 32767
        Me.NTRMRemarkTextBox.Name = "NTRMRemarkTextBox"
        Me.NTRMRemarkTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NTRMRemarkTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NTRMRemarkTextBox.SelectedText = ""
        Me.NTRMRemarkTextBox.SelectionLength = 0
        Me.NTRMRemarkTextBox.SelectionStart = 0
        Me.NTRMRemarkTextBox.ShortcutsEnabled = True
        Me.NTRMRemarkTextBox.Size = New System.Drawing.Size(150, 28)
        Me.NTRMRemarkTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.NTRMRemarkTextBox.TabIndex = 174
        Me.NTRMRemarkTextBox.UseSelectable = True
        Me.NTRMRemarkTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NTRMRemarkTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel33
        '
        Me.MetroLabel33.AutoSize = True
        Me.MetroLabel33.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel33.Location = New System.Drawing.Point(2, 101)
        Me.MetroLabel33.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel33.Name = "MetroLabel33"
        Me.MetroLabel33.Size = New System.Drawing.Size(54, 19)
        Me.MetroLabel33.TabIndex = 173
        Me.MetroLabel33.Text = "Remark"
        Me.MetroLabel33.UseCustomBackColor = True
        Me.MetroLabel33.UseCustomForeColor = True
        '
        'BtnDelete
        '
        Me.BtnDelete.Enabled = False
        Me.BtnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnDelete.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.BtnDelete.Location = New System.Drawing.Point(76, 2)
        Me.BtnDelete.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.BtnDelete.Name = "BtnDelete"
        Me.BtnDelete.Size = New System.Drawing.Size(70, 24)
        Me.BtnDelete.TabIndex = 172
        Me.BtnDelete.Text = "Delete"
        Me.BtnDelete.UseVisualStyleBackColor = True
        '
        'NTRMQuantityTextBox
        '
        '
        '
        '
        Me.NTRMQuantityTextBox.CustomButton.Image = Nothing
        Me.NTRMQuantityTextBox.CustomButton.Location = New System.Drawing.Point(93, 2)
        Me.NTRMQuantityTextBox.CustomButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NTRMQuantityTextBox.CustomButton.Name = ""
        Me.NTRMQuantityTextBox.CustomButton.Size = New System.Drawing.Size(17, 19)
        Me.NTRMQuantityTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.NTRMQuantityTextBox.CustomButton.TabIndex = 1
        Me.NTRMQuantityTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.NTRMQuantityTextBox.CustomButton.UseSelectable = True
        Me.NTRMQuantityTextBox.CustomButton.Visible = False
        Me.NTRMQuantityTextBox.Lines = New String(-1) {}
        Me.NTRMQuantityTextBox.Location = New System.Drawing.Point(2, 71)
        Me.NTRMQuantityTextBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NTRMQuantityTextBox.MaxLength = 32767
        Me.NTRMQuantityTextBox.Name = "NTRMQuantityTextBox"
        Me.NTRMQuantityTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.NTRMQuantityTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.NTRMQuantityTextBox.SelectedText = ""
        Me.NTRMQuantityTextBox.SelectionLength = 0
        Me.NTRMQuantityTextBox.SelectionStart = 0
        Me.NTRMQuantityTextBox.ShortcutsEnabled = True
        Me.NTRMQuantityTextBox.Size = New System.Drawing.Size(150, 28)
        Me.NTRMQuantityTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.NTRMQuantityTextBox.TabIndex = 171
        Me.NTRMQuantityTextBox.UseSelectable = True
        Me.NTRMQuantityTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.NTRMQuantityTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel39
        '
        Me.MetroLabel39.AutoSize = True
        Me.MetroLabel39.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel39.Location = New System.Drawing.Point(2, 50)
        Me.MetroLabel39.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel39.Name = "MetroLabel39"
        Me.MetroLabel39.Size = New System.Drawing.Size(58, 19)
        Me.MetroLabel39.TabIndex = 170
        Me.MetroLabel39.Text = "Quantity"
        Me.MetroLabel39.UseCustomBackColor = True
        Me.MetroLabel39.UseCustomForeColor = True
        '
        'NTRMInspectionDataGrid
        '
        Me.NTRMInspectionDataGrid.AllowUserToAddRows = False
        Me.NTRMInspectionDataGrid.AllowUserToDeleteRows = False
        Me.NTRMInspectionDataGrid.AllowUserToOrderColumns = True
        Me.NTRMInspectionDataGrid.AllowUserToResizeColumns = False
        Me.NTRMInspectionDataGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NTRMInspectionDataGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.NTRMInspectionDataGrid.AutoGenerateColumns = False
        Me.NTRMInspectionDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.NTRMInspectionDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.NTRMInspectionDataGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.NTRMInspectionDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.NTRMInspectionDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BaleBarCodeDataGridViewTextBoxColumn, Me.NTRMTypeCodeDataGridViewTextBoxColumn, Me.NTRMTypeNameDataGridViewTextBoxColumn, Me.NTRMCategoryDataGridViewTextBoxColumn, Me.InspectionLocationDataGridViewTextBoxColumn, Me.QuantityDataGridViewTextBoxColumn, Me.InspectionDateDataGridViewTextBoxColumn, Me.InspectionUserDataGridViewTextBoxColumn})
        Me.NTRMInspectionDataGrid.DataSource = Me.SpNTRMSELNTRMInspectionByBCResultBindingSource
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.NTRMInspectionDataGrid.DefaultCellStyle = DataGridViewCellStyle3
        Me.NTRMInspectionDataGrid.Dock = System.Windows.Forms.DockStyle.Top
        Me.NTRMInspectionDataGrid.Location = New System.Drawing.Point(161, 2)
        Me.NTRMInspectionDataGrid.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NTRMInspectionDataGrid.Name = "NTRMInspectionDataGrid"
        Me.NTRMInspectionDataGrid.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.NTRMInspectionDataGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.NTRMInspectionDataGrid.RowHeadersVisible = False
        Me.NTRMInspectionDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 10.2!)
        Me.NTRMInspectionDataGrid.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.NTRMInspectionDataGrid.RowTemplate.Height = 24
        Me.NTRMInspectionDataGrid.Size = New System.Drawing.Size(759, 439)
        Me.NTRMInspectionDataGrid.TabIndex = 168
        '
        'BaleBarCodeDataGridViewTextBoxColumn
        '
        Me.BaleBarCodeDataGridViewTextBoxColumn.DataPropertyName = "BaleBarCode"
        Me.BaleBarCodeDataGridViewTextBoxColumn.HeaderText = "Bale Barcode"
        Me.BaleBarCodeDataGridViewTextBoxColumn.Name = "BaleBarCodeDataGridViewTextBoxColumn"
        Me.BaleBarCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.BaleBarCodeDataGridViewTextBoxColumn.Width = 108
        '
        'NTRMTypeCodeDataGridViewTextBoxColumn
        '
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.DataPropertyName = "NTRMTypeCode"
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.HeaderText = "Type Code"
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.Name = "NTRMTypeCodeDataGridViewTextBoxColumn"
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.Width = 90
        '
        'NTRMTypeNameDataGridViewTextBoxColumn
        '
        Me.NTRMTypeNameDataGridViewTextBoxColumn.DataPropertyName = "NTRMTypeName"
        Me.NTRMTypeNameDataGridViewTextBoxColumn.HeaderText = "Type Name"
        Me.NTRMTypeNameDataGridViewTextBoxColumn.Name = "NTRMTypeNameDataGridViewTextBoxColumn"
        Me.NTRMTypeNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.NTRMTypeNameDataGridViewTextBoxColumn.Width = 96
        '
        'NTRMCategoryDataGridViewTextBoxColumn
        '
        Me.NTRMCategoryDataGridViewTextBoxColumn.DataPropertyName = "NTRMCategory"
        Me.NTRMCategoryDataGridViewTextBoxColumn.HeaderText = "Category"
        Me.NTRMCategoryDataGridViewTextBoxColumn.Name = "NTRMCategoryDataGridViewTextBoxColumn"
        Me.NTRMCategoryDataGridViewTextBoxColumn.ReadOnly = True
        Me.NTRMCategoryDataGridViewTextBoxColumn.Width = 84
        '
        'InspectionLocationDataGridViewTextBoxColumn
        '
        Me.InspectionLocationDataGridViewTextBoxColumn.DataPropertyName = "InspectionLocation"
        Me.InspectionLocationDataGridViewTextBoxColumn.HeaderText = "Location"
        Me.InspectionLocationDataGridViewTextBoxColumn.Name = "InspectionLocationDataGridViewTextBoxColumn"
        Me.InspectionLocationDataGridViewTextBoxColumn.ReadOnly = True
        Me.InspectionLocationDataGridViewTextBoxColumn.Width = 81
        '
        'QuantityDataGridViewTextBoxColumn
        '
        Me.QuantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity"
        Me.QuantityDataGridViewTextBoxColumn.HeaderText = "Quantity"
        Me.QuantityDataGridViewTextBoxColumn.Name = "QuantityDataGridViewTextBoxColumn"
        Me.QuantityDataGridViewTextBoxColumn.ReadOnly = True
        Me.QuantityDataGridViewTextBoxColumn.Width = 82
        '
        'InspectionDateDataGridViewTextBoxColumn
        '
        Me.InspectionDateDataGridViewTextBoxColumn.DataPropertyName = "InspectionDate"
        Me.InspectionDateDataGridViewTextBoxColumn.HeaderText = "InspectionDate"
        Me.InspectionDateDataGridViewTextBoxColumn.Name = "InspectionDateDataGridViewTextBoxColumn"
        Me.InspectionDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.InspectionDateDataGridViewTextBoxColumn.Width = 119
        '
        'InspectionUserDataGridViewTextBoxColumn
        '
        Me.InspectionUserDataGridViewTextBoxColumn.DataPropertyName = "InspectionUser"
        Me.InspectionUserDataGridViewTextBoxColumn.HeaderText = "InspectionUser"
        Me.InspectionUserDataGridViewTextBoxColumn.Name = "InspectionUserDataGridViewTextBoxColumn"
        Me.InspectionUserDataGridViewTextBoxColumn.ReadOnly = True
        Me.InspectionUserDataGridViewTextBoxColumn.Width = 117
        '
        'SpNTRMSELNTRMInspectionByBCResultBindingSource
        '
        Me.SpNTRMSELNTRMInspectionByBCResultBindingSource.DataSource = GetType(ReceivingSystem.sp_NTRM_SEL_NTRMInspectionByBC_Result)
        '
        'SaveButton
        '
        Me.SaveButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SaveButton.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Bold)
        Me.SaveButton.Location = New System.Drawing.Point(2, 2)
        Me.SaveButton.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(70, 24)
        Me.SaveButton.TabIndex = 166
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.NTRMTypeComboBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel39)
        Me.FlowLayoutPanel1.Controls.Add(Me.NTRMQuantityTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel33)
        Me.FlowLayoutPanel1.Controls.Add(Me.NTRMRemarkTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(2, 2)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(155, 184)
        Me.FlowLayoutPanel1.TabIndex = 176
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel1.Location = New System.Drawing.Point(2, 0)
        Me.MetroLabel1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(77, 19)
        Me.MetroLabel1.TabIndex = 176
        Me.MetroLabel1.Text = "NTRM Type"
        Me.MetroLabel1.UseCustomBackColor = True
        Me.MetroLabel1.UseCustomForeColor = True
        '
        'NTRMTypeComboBox
        '
        Me.NTRMTypeComboBox.BackColor = System.Drawing.Color.White
        Me.NTRMTypeComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.NTRMTypeComboBox.FormattingEnabled = True
        Me.NTRMTypeComboBox.ItemHeight = 19
        Me.NTRMTypeComboBox.Items.AddRange(New Object() {"Open bales", "Close bales"})
        Me.NTRMTypeComboBox.Location = New System.Drawing.Point(2, 21)
        Me.NTRMTypeComboBox.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.NTRMTypeComboBox.Name = "NTRMTypeComboBox"
        Me.NTRMTypeComboBox.Size = New System.Drawing.Size(151, 25)
        Me.NTRMTypeComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.NTRMTypeComboBox.TabIndex = 177
        Me.NTRMTypeComboBox.UseCustomBackColor = True
        Me.NTRMTypeComboBox.UseCustomForeColor = True
        Me.NTRMTypeComboBox.UseSelectable = True
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.SaveButton)
        Me.FlowLayoutPanel2.Controls.Add(Me.BtnDelete)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(2, 154)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(148, 28)
        Me.FlowLayoutPanel2.TabIndex = 177
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel3.Controls.Add(Me.NTRMInspectionDataGrid)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(15, 49)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(922, 444)
        Me.FlowLayoutPanel3.TabIndex = 177
        '
        'FrmNTRMInput
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 509)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "FrmNTRMInput"
        Me.Padding = New System.Windows.Forms.Padding(15, 49, 15, 16)
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "NTRM Inspection Input"
        CType(Me.NTRMInspectionDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpNTRMSELNTRMInspectionByBCResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NTRMRemarkTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel33 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnDelete As Button
    Friend WithEvents NTRMQuantityTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel39 As MetroFramework.Controls.MetroLabel
    Friend WithEvents NTRMInspectionDataGrid As DataGridView
    Friend WithEvents SaveButton As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents NTRMTypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents SpNTRMSELNTRMInspectionByBCResultBindingSource As BindingSource
    Friend WithEvents BaleBarCodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NTRMTypeCodeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NTRMTypeNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NTRMCategoryDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InspectionLocationDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents QuantityDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InspectionDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents InspectionUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
