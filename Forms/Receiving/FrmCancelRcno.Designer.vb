﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCancelRcno
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ClearButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.rcnoLabel = New MetroFramework.Controls.MetroLabel()
        Me.CancelRcnoRemarkTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.SaveButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.ClearButton)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(4, 68)
        Me.FlowLayoutPanel6.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(129, 39)
        Me.FlowLayoutPanel6.TabIndex = 148
        '
        'SaveButton
        '
        Me.SaveButton.AutoSize = True
        Me.SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SaveButton.BackColor = System.Drawing.Color.DarkOrange
        Me.SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SaveButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveButton.FlatAppearance.BorderSize = 0
        Me.SaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SaveButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.ForeColor = System.Drawing.Color.Black
        Me.SaveButton.Location = New System.Drawing.Point(4, 4)
        Me.SaveButton.Margin = New System.Windows.Forms.Padding(4)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(55, 31)
        Me.SaveButton.TabIndex = 145
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'ClearButton
        '
        Me.ClearButton.AutoSize = True
        Me.ClearButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClearButton.BackColor = System.Drawing.Color.Gainsboro
        Me.ClearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClearButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearButton.FlatAppearance.BorderSize = 0
        Me.ClearButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ClearButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClearButton.ForeColor = System.Drawing.Color.Black
        Me.ClearButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ClearButton.Location = New System.Drawing.Point(67, 4)
        Me.ClearButton.Margin = New System.Windows.Forms.Padding(4)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(58, 31)
        Me.ClearButton.TabIndex = 146
        Me.ClearButton.Text = "Clear"
        Me.ClearButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ClearButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.rcnoLabel)
        Me.FlowLayoutPanel1.Controls.Add(Me.CancelRcnoRemarkTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(27, 74)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(623, 323)
        Me.FlowLayoutPanel1.TabIndex = 149
        '
        'rcnoLabel
        '
        Me.rcnoLabel.AutoSize = True
        Me.rcnoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.rcnoLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.rcnoLabel.Location = New System.Drawing.Point(4, 0)
        Me.rcnoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.rcnoLabel.Name = "rcnoLabel"
        Me.rcnoLabel.Size = New System.Drawing.Size(53, 25)
        Me.rcnoLabel.TabIndex = 166
        Me.rcnoLabel.Text = "rcno"
        '
        'CancelRcnoRemarkTextBox
        '
        '
        '
        '
        Me.CancelRcnoRemarkTextBox.CustomButton.Image = Nothing
        Me.CancelRcnoRemarkTextBox.CustomButton.Location = New System.Drawing.Point(533, 1)
        Me.CancelRcnoRemarkTextBox.CustomButton.Margin = New System.Windows.Forms.Padding(4)
        Me.CancelRcnoRemarkTextBox.CustomButton.Name = ""
        Me.CancelRcnoRemarkTextBox.CustomButton.Size = New System.Drawing.Size(29, 29)
        Me.CancelRcnoRemarkTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CancelRcnoRemarkTextBox.CustomButton.TabIndex = 1
        Me.CancelRcnoRemarkTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CancelRcnoRemarkTextBox.CustomButton.UseSelectable = True
        Me.CancelRcnoRemarkTextBox.CustomButton.Visible = False
        Me.CancelRcnoRemarkTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.CancelRcnoRemarkTextBox.Lines = New String(-1) {}
        Me.CancelRcnoRemarkTextBox.Location = New System.Drawing.Point(4, 29)
        Me.CancelRcnoRemarkTextBox.Margin = New System.Windows.Forms.Padding(4)
        Me.CancelRcnoRemarkTextBox.MaxLength = 100
        Me.CancelRcnoRemarkTextBox.Name = "CancelRcnoRemarkTextBox"
        Me.CancelRcnoRemarkTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CancelRcnoRemarkTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CancelRcnoRemarkTextBox.SelectedText = ""
        Me.CancelRcnoRemarkTextBox.SelectionLength = 0
        Me.CancelRcnoRemarkTextBox.SelectionStart = 0
        Me.CancelRcnoRemarkTextBox.Size = New System.Drawing.Size(563, 31)
        Me.CancelRcnoRemarkTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CancelRcnoRemarkTextBox.TabIndex = 165
        Me.CancelRcnoRemarkTextBox.UseSelectable = True
        Me.CancelRcnoRemarkTextBox.WaterMark = "Remark (กรุณาระบุหมายเหตุกรณีต้องการยกเลิก Receiving no. นี้)"
        Me.CancelRcnoRemarkTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CancelRcnoRemarkTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'FrmCancelRcno
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 422)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "FrmCancelRcno"
        Me.Padding = New System.Windows.Forms.Padding(27, 74, 27, 25)
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Cancel rcno."
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents SaveButton As Button
    Friend WithEvents ClearButton As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents CancelRcnoRemarkTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents rcnoLabel As MetroFramework.Controls.MetroLabel
End Class
