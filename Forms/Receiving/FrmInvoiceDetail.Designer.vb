﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmInvoiceDetail
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.CationLabel = New MetroFramework.Controls.MetroLabel()
        Me.InvoiceDetailDataGridView = New System.Windows.Forms.DataGridView()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.InvoiceDetailDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.CationLabel)
        Me.FlowLayoutPanel1.Controls.Add(Me.InvoiceDetailDataGridView)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1022, 656)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'CationLabel
        '
        Me.CationLabel.AutoSize = True
        Me.CationLabel.Location = New System.Drawing.Point(3, 0)
        Me.CationLabel.Name = "CationLabel"
        Me.CationLabel.Size = New System.Drawing.Size(614, 20)
        Me.CationLabel.TabIndex = 84
        Me.CationLabel.Text = "Barcodes of this Packlist Invoice no. and waiting for receive (ป้ายบาร์โค้ดในInvo" &
    "ice นี้และรอการรับเข้า)"
        '
        'InvoiceDetailDataGridView
        '
        Me.InvoiceDetailDataGridView.AllowUserToAddRows = False
        Me.InvoiceDetailDataGridView.AllowUserToDeleteRows = False
        Me.InvoiceDetailDataGridView.AllowUserToResizeColumns = False
        Me.InvoiceDetailDataGridView.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InvoiceDetailDataGridView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.InvoiceDetailDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.InvoiceDetailDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.InvoiceDetailDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.InvoiceDetailDataGridView.DefaultCellStyle = DataGridViewCellStyle3
        Me.InvoiceDetailDataGridView.Location = New System.Drawing.Point(3, 23)
        Me.InvoiceDetailDataGridView.Name = "InvoiceDetailDataGridView"
        Me.InvoiceDetailDataGridView.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.InvoiceDetailDataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.InvoiceDetailDataGridView.RowHeadersVisible = False
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Calibri", 10.2!)
        Me.InvoiceDetailDataGridView.RowsDefaultCellStyle = DataGridViewCellStyle5
        Me.InvoiceDetailDataGridView.RowTemplate.Height = 24
        Me.InvoiceDetailDataGridView.Size = New System.Drawing.Size(1011, 624)
        Me.InvoiceDetailDataGridView.TabIndex = 155
        '
        'FrmInvoiceDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1062, 736)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "FrmInvoiceDetail"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Invoice Detail"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.InvoiceDetailDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents CationLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvoiceDetailDataGridView As DataGridView
End Class
