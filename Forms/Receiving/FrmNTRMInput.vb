﻿Imports FastMember

Public Class FrmNTRMInput
    Inherits MetroFramework.Forms.MetroForm

    Dim db As New ReceivingDataClassesDataContext

    Public _baleBarcode As String
    Dim _mat As sp_Receiving_SEL_MatByBC_Result
    Dim _ntrmInspectionList As List(Of sp_NTRM_SEL_NTRMInspectionByBC_Result)
    Dim _ntrmSelected As sp_NTRM_SEL_NTRMInspectionByBC_Result

    Private Sub FrmNTRMInput_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _mat = _businessLayerService.ReceivingBL().GetMatByBaleBarcode(_baleBarcode)

        NTRMTypeComboBox.DataSource = Nothing
        NTRMTypeComboBox.DataSource = _businessLayerService.ReceivingBL().GetNTRMTypes()

        NTRMTypeComboBox.DisplayMember = "NTRMTypeName"
        NTRMTypeComboBox.ValueMember = "NTRMTypeCode"

        NTRMInspectionDataGridBinding()
    End Sub

    Private Sub NTRMInspectionDataGridBinding()
        _ntrmInspectionList = _businessLayerService.ReceivingBL().GetNTRMInspectionByBaleBarcode(_baleBarcode)

        Dim dt As New DataTable()
        Dim reader = ObjectReader.Create(_ntrmInspectionList)
        dt.Load(reader)

        NTRMInspectionDataGrid.DataSource = Nothing
        NTRMInspectionDataGrid.DataSource = dt
    End Sub

    Private Sub ClearForm()
        NTRMQuantityTextBox.Text = ""
        NTRMRemarkTextBox.Text = ""
        NTRMInspectionDataGridBinding()
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If _baleBarcode = "" Then
                MessageBox.Show("กรุณาคีย์ barcode", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If NTRMTypeComboBox.SelectedIndex <= -1 Then
                MessageBox.Show("กรุณาเลือกประเภท NTRM", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If NTRMQuantityTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุจำนวน NTRM ที่พบ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                NTRMQuantityTextBox.Focus()
                Return
            End If
            If MessageBox.Show("ต้องการบันทึก NTRM ของBarcode tag นี้ใช่หรือไม่ ?", "Confirmation", MessageBoxButtons.YesNo) = DialogResult.No Then
                Return
            End If

            db.sp_Receiving_INS_NTRMInspectionByBc(_baleBarcode, NTRMTypeComboBox.SelectedValue, _username, Val(NTRMQuantityTextBox.Text), "Receiving", Now, _mat.type, _mat.company)
            MessageBox.Show("บันทึก NTRM เรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ClearForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnDelete_Click(sender As Object, e As EventArgs) Handles BtnDelete.Click
        Try
            If NTRMTypeComboBox.SelectedIndex <= -1 Then
                MessageBox.Show("กรุณาเลือกประเภทของ NTRM ที่พบ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                NTRMQuantityTextBox.Focus()
                Return
            End If

            If NTRMQuantityTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุจำนวน NTRM ที่พบ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                NTRMQuantityTextBox.Focus()
                Return
            End If

            If NTRMRemarkTextBox.Text = "" Then
                MessageBox.Show("กรุณาระบุหมายเหตุที่ต้องการลบจำนวน NTRM นี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                NTRMRemarkTextBox.Focus()
                Return
            End If

            If _ntrmSelected Is Nothing Then
                MessageBox.Show("ไม่พบข้อมูลรายการที่จะลบ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'ถ้ามีแล้ว ให้ show message box ว่าต้องการ update หรือไม่ ?
            If MessageBox.Show("ต้องการลบ NTRM นี้ของ Barcode tag ใช่หรือไม่ ?", "Confirmation", MessageBoxButtons.YesNo) = DialogResult.No Then
                Return
            End If
            db.sp_Receiving_DEL_NTRMInspectionByBc(_ntrmSelected.BaleBarCode, _ntrmSelected.NTRMTypeCode, _username, "Delete",
                                                   _ntrmSelected.Quantity, "", NTRMRemarkTextBox.Text, _ntrmSelected.InspectionDate,
                                                   _mat.type, _mat.company)
            MessageBox.Show("ลบ NTRM นี้เรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ClearForm()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub NTRMInspectionDataGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles NTRMInspectionDataGrid.CellMouseClick
        Try
            If e.RowIndex < 0 Then
                Return
            End If

            Dim row = NTRMInspectionDataGrid.Rows(e.RowIndex)
            Dim baleBarcode = row.Cells(0).Value
            Dim typeCode = row.Cells(1).Value
            Dim inspectionDate = row.Cells(6).Value

            _ntrmSelected = _ntrmInspectionList.SingleOrDefault(Function(x) x.BaleBarCode = baleBarcode And x.NTRMTypeCode = typeCode And x.InspectionDate = inspectionDate)

            BtnDelete.Enabled = True
            Me.NTRMTypeComboBox.Text = _ntrmSelected.NTRMTypeName
            Me.NTRMQuantityTextBox.Text = _ntrmSelected.Quantity
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class