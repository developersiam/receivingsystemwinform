﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLTLRemarkBrownTag
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.RdtLTLDiffWeight = New MetroFramework.Controls.MetroRadioButton()
        Me.RdtLTLCannotScan = New MetroFramework.Controls.MetroRadioButton()
        Me.RdtLTLNotinPackingList = New MetroFramework.Controls.MetroRadioButton()
        Me.RdtLTLCreateNew = New MetroFramework.Controls.MetroRadioButton()
        Me.RdtLTLDamageBarcode = New MetroFramework.Controls.MetroRadioButton()
        Me.RdtLTLLostTicket = New MetroFramework.Controls.MetroRadioButton()
        Me.RdtLTLLostTicketForBox = New MetroFramework.Controls.MetroRadioButton()
        Me.MetroLabel47 = New MetroFramework.Controls.MetroLabel()
        Me.LTLBalebarcodeTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel51 = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.ClearButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLDiffWeight)
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLCannotScan)
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLNotinPackingList)
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLCreateNew)
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLDamageBarcode)
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLLostTicket)
        Me.FlowLayoutPanel1.Controls.Add(Me.RdtLTLLostTicketForBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel47)
        Me.FlowLayoutPanel1.Controls.Add(Me.LTLBalebarcodeTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel51)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(592, 297)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'RdtLTLDiffWeight
        '
        Me.RdtLTLDiffWeight.AutoSize = True
        Me.RdtLTLDiffWeight.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLDiffWeight.Location = New System.Drawing.Point(3, 3)
        Me.RdtLTLDiffWeight.Name = "RdtLTLDiffWeight"
        Me.RdtLTLDiffWeight.Size = New System.Drawing.Size(446, 19)
        Me.RdtLTLDiffWeight.TabIndex = 147
        Me.RdtLTLDiffWeight.Text = "1.Diff. weight greater than or equal 5 kgs.(น้ำหนักต่างกันตั้งแต่5กิโลขึ้นไป)"
        Me.RdtLTLDiffWeight.UseCustomForeColor = True
        Me.RdtLTLDiffWeight.UseSelectable = True
        '
        'RdtLTLCannotScan
        '
        Me.RdtLTLCannotScan.AutoSize = True
        Me.RdtLTLCannotScan.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLCannotScan.Location = New System.Drawing.Point(3, 28)
        Me.RdtLTLCannotScan.Name = "RdtLTLCannotScan"
        Me.RdtLTLCannotScan.Size = New System.Drawing.Size(329, 19)
        Me.RdtLTLCannotScan.TabIndex = 148
        Me.RdtLTLCannotScan.Text = "2.Can not scan (ไม่สามารถสแกนบาร์โค้ดได้แต่ยังอ่านได้)"
        Me.RdtLTLCannotScan.UseCustomForeColor = True
        Me.RdtLTLCannotScan.UseSelectable = True
        '
        'RdtLTLNotinPackingList
        '
        Me.RdtLTLNotinPackingList.AutoSize = True
        Me.RdtLTLNotinPackingList.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLNotinPackingList.Location = New System.Drawing.Point(3, 53)
        Me.RdtLTLNotinPackingList.Name = "RdtLTLNotinPackingList"
        Me.RdtLTLNotinPackingList.Size = New System.Drawing.Size(412, 19)
        Me.RdtLTLNotinPackingList.TabIndex = 149
        Me.RdtLTLNotinPackingList.Text = "3.Barcode not in the packing list (บาร์โค้ดไม่ได้อยู่ใน packing list นี้)"
        Me.RdtLTLNotinPackingList.UseCustomForeColor = True
        Me.RdtLTLNotinPackingList.UseSelectable = True
        '
        'RdtLTLCreateNew
        '
        Me.RdtLTLCreateNew.AutoSize = True
        Me.RdtLTLCreateNew.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLCreateNew.Location = New System.Drawing.Point(3, 78)
        Me.RdtLTLCreateNew.Name = "RdtLTLCreateNew"
        Me.RdtLTLCreateNew.Size = New System.Drawing.Size(441, 19)
        Me.RdtLTLCreateNew.TabIndex = 150
        Me.RdtLTLCreateNew.Text = "4.Create new bale barcode from Scraps (สร้างบาร์โค้ดใหม่เพราะเป็นยาร่วง)"
        Me.RdtLTLCreateNew.UseCustomForeColor = True
        Me.RdtLTLCreateNew.UseSelectable = True
        '
        'RdtLTLDamageBarcode
        '
        Me.RdtLTLDamageBarcode.AutoSize = True
        Me.RdtLTLDamageBarcode.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLDamageBarcode.Location = New System.Drawing.Point(3, 103)
        Me.RdtLTLDamageBarcode.Name = "RdtLTLDamageBarcode"
        Me.RdtLTLDamageBarcode.Size = New System.Drawing.Size(306, 19)
        Me.RdtLTLDamageBarcode.TabIndex = 152
        Me.RdtLTLDamageBarcode.Text = "5.Damage barcode  (บาร์โค้ดสแกนไม่ได้ อ่านไม่ได้)"
        Me.RdtLTLDamageBarcode.UseCustomForeColor = True
        Me.RdtLTLDamageBarcode.UseSelectable = True
        '
        'RdtLTLLostTicket
        '
        Me.RdtLTLLostTicket.AutoSize = True
        Me.RdtLTLLostTicket.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLLostTicket.Location = New System.Drawing.Point(3, 128)
        Me.RdtLTLLostTicket.Name = "RdtLTLLostTicket"
        Me.RdtLTLLostTicket.Size = New System.Drawing.Size(287, 19)
        Me.RdtLTLLostTicket.TabIndex = 153
        Me.RdtLTLLostTicket.Text = "6.Lost ticket(ป้ายหาย ไม่พบป้ายที่ติดมากับห่อยา)"
        Me.RdtLTLLostTicket.UseCustomForeColor = True
        Me.RdtLTLLostTicket.UseSelectable = True
        '
        'RdtLTLLostTicketForBox
        '
        Me.RdtLTLLostTicketForBox.AutoSize = True
        Me.RdtLTLLostTicketForBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.RdtLTLLostTicketForBox.Location = New System.Drawing.Point(3, 153)
        Me.RdtLTLLostTicketForBox.Name = "RdtLTLLostTicketForBox"
        Me.RdtLTLLostTicketForBox.Size = New System.Drawing.Size(263, 19)
        Me.RdtLTLLostTicketForBox.TabIndex = 154
        Me.RdtLTLLostTicketForBox.Text = "7.Box (ป้ายหาย ไม่พบป้ายที่ติดมากับกล่องยา)"
        Me.RdtLTLLostTicketForBox.UseCustomForeColor = True
        Me.RdtLTLLostTicketForBox.UseSelectable = True
        '
        'MetroLabel47
        '
        Me.MetroLabel47.AutoSize = True
        Me.MetroLabel47.Location = New System.Drawing.Point(3, 175)
        Me.MetroLabel47.Name = "MetroLabel47"
        Me.MetroLabel47.Size = New System.Drawing.Size(0, 0)
        Me.MetroLabel47.TabIndex = 145
        '
        'LTLBalebarcodeTextBox
        '
        Me.LTLBalebarcodeTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.LTLBalebarcodeTextBox.CustomButton.Image = Nothing
        Me.LTLBalebarcodeTextBox.CustomButton.Location = New System.Drawing.Point(230, 1)
        Me.LTLBalebarcodeTextBox.CustomButton.Name = ""
        Me.LTLBalebarcodeTextBox.CustomButton.Size = New System.Drawing.Size(17, 19)
        Me.LTLBalebarcodeTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.LTLBalebarcodeTextBox.CustomButton.TabIndex = 1
        Me.LTLBalebarcodeTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.LTLBalebarcodeTextBox.CustomButton.UseSelectable = True
        Me.LTLBalebarcodeTextBox.CustomButton.Visible = False
        Me.LTLBalebarcodeTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.LTLBalebarcodeTextBox.Lines = New String(-1) {}
        Me.LTLBalebarcodeTextBox.Location = New System.Drawing.Point(3, 195)
        Me.LTLBalebarcodeTextBox.Margin = New System.Windows.Forms.Padding(3, 20, 3, 3)
        Me.LTLBalebarcodeTextBox.MaxLength = 17
        Me.LTLBalebarcodeTextBox.Name = "LTLBalebarcodeTextBox"
        Me.LTLBalebarcodeTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.LTLBalebarcodeTextBox.PromptText = "LTL barcode (บาร์โค้ดเดิมของ LTL)"
        Me.LTLBalebarcodeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.LTLBalebarcodeTextBox.SelectedText = ""
        Me.LTLBalebarcodeTextBox.SelectionLength = 0
        Me.LTLBalebarcodeTextBox.SelectionStart = 0
        Me.LTLBalebarcodeTextBox.ShortcutsEnabled = True
        Me.LTLBalebarcodeTextBox.Size = New System.Drawing.Size(330, 25)
        Me.LTLBalebarcodeTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.LTLBalebarcodeTextBox.TabIndex = 146
        Me.LTLBalebarcodeTextBox.UseCustomBackColor = True
        Me.LTLBalebarcodeTextBox.UseSelectable = True
        Me.LTLBalebarcodeTextBox.WaterMark = "LTL barcode (บาร์โค้ดเดิมของ LTL)"
        Me.LTLBalebarcodeTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.LTLBalebarcodeTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'MetroLabel51
        '
        Me.MetroLabel51.AutoSize = True
        Me.MetroLabel51.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel51.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MetroLabel51.Location = New System.Drawing.Point(3, 226)
        Me.MetroLabel51.Margin = New System.Windows.Forms.Padding(3, 3, 3, 0)
        Me.MetroLabel51.Name = "MetroLabel51"
        Me.MetroLabel51.Size = New System.Drawing.Size(552, 19)
        Me.MetroLabel51.TabIndex = 151
        Me.MetroLabel51.Text = "!!! กรุณาระบุบาร์โค้ดเดิมของยาลาวในทุกกรณี แต่กรณีเลือกข้อ 4 , 5 , 6 ให้ระบุบาร์โ" &
    "ค้ดเป็น -"
        Me.MetroLabel51.UseCustomForeColor = True
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.SaveButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.ClearButton)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(3, 248)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(106, 32)
        Me.FlowLayoutPanel6.TabIndex = 155
        '
        'SaveButton
        '
        Me.SaveButton.AutoSize = True
        Me.SaveButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.SaveButton.BackColor = System.Drawing.Color.DarkOrange
        Me.SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SaveButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveButton.FlatAppearance.BorderSize = 0
        Me.SaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SaveButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.ForeColor = System.Drawing.Color.Black
        Me.SaveButton.Location = New System.Drawing.Point(3, 3)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(46, 26)
        Me.SaveButton.TabIndex = 145
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'ClearButton
        '
        Me.ClearButton.AutoSize = True
        Me.ClearButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClearButton.BackColor = System.Drawing.Color.Gainsboro
        Me.ClearButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClearButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearButton.FlatAppearance.BorderSize = 0
        Me.ClearButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.ClearButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ClearButton.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClearButton.ForeColor = System.Drawing.Color.Black
        Me.ClearButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ClearButton.Location = New System.Drawing.Point(55, 3)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(48, 26)
        Me.ClearButton.TabIndex = 146
        Me.ClearButton.Text = "Clear"
        Me.ClearButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ClearButton.UseVisualStyleBackColor = False
        '
        'FrmLTLRemarkBrownTag
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(632, 377)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "FrmLTLRemarkBrownTag"
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "LTL input remark of STEC brown tag"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents RdtLTLDiffWeight As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents RdtLTLCannotScan As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents RdtLTLNotinPackingList As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents RdtLTLCreateNew As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents RdtLTLDamageBarcode As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents RdtLTLLostTicket As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents RdtLTLLostTicketForBox As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents MetroLabel51 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel47 As MetroFramework.Controls.MetroLabel
    Friend WithEvents LTLBalebarcodeTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents SaveButton As Button
    Friend WithEvents ClearButton As Button
End Class
