﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMenu
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_DecodePasswordBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_DecodePasswordTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.BtnLoadData = New System.Windows.Forms.Button()
        Me.BtnReceiving = New System.Windows.Forms.Button()
        Me.BtnAddDocNo = New System.Windows.Forms.Button()
        Me.BtnUnLockRcNo = New System.Windows.Forms.Button()
        Me.BtnApproveFromChecker = New System.Windows.Forms.Button()
        Me.BtnCPAResult = New System.Windows.Forms.Button()
        Me.BtnReport = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.UserButton = New System.Windows.Forms.Button()
        Me.SettingButton = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ReceivingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_DecodePasswordBindingSource
        '
        Me.Sp_Receiving_DecodePasswordBindingSource.DataMember = "sp_Receiving_DecodePassword"
        Me.Sp_Receiving_DecodePasswordBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_DecodePasswordTableAdapter
        '
        Me.Sp_Receiving_DecodePasswordTableAdapter.ClearBeforeFill = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnLoadData)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnReceiving)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnAddDocNo)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnUnLockRcNo)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnApproveFromChecker)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnCPAResult)
        Me.FlowLayoutPanel1.Controls.Add(Me.BtnReport)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(2, 79)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(882, 171)
        Me.FlowLayoutPanel1.TabIndex = 128
        '
        'BtnLoadData
        '
        Me.BtnLoadData.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnLoadData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnLoadData.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnLoadData.FlatAppearance.BorderSize = 0
        Me.BtnLoadData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnLoadData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnLoadData.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnLoadData.ForeColor = System.Drawing.Color.White
        Me.BtnLoadData.Image = Global.ReceivingSystem.My.Resources.Resources.Internal_64
        Me.BtnLoadData.Location = New System.Drawing.Point(2, 2)
        Me.BtnLoadData.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnLoadData.Name = "BtnLoadData"
        Me.BtnLoadData.Size = New System.Drawing.Size(122, 167)
        Me.BtnLoadData.TabIndex = 126
        Me.BtnLoadData.Text = "Load Data"
        Me.BtnLoadData.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnLoadData.UseVisualStyleBackColor = False
        '
        'BtnReceiving
        '
        Me.BtnReceiving.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnReceiving.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnReceiving.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnReceiving.FlatAppearance.BorderSize = 0
        Me.BtnReceiving.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnReceiving.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnReceiving.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnReceiving.ForeColor = System.Drawing.Color.White
        Me.BtnReceiving.Image = Global.ReceivingSystem.My.Resources.Resources.Add_ShoppingCart64
        Me.BtnReceiving.Location = New System.Drawing.Point(128, 2)
        Me.BtnReceiving.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnReceiving.Name = "BtnReceiving"
        Me.BtnReceiving.Size = New System.Drawing.Size(122, 167)
        Me.BtnReceiving.TabIndex = 121
        Me.BtnReceiving.Text = "Receiving on PC"
        Me.BtnReceiving.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnReceiving.UseVisualStyleBackColor = False
        '
        'BtnAddDocNo
        '
        Me.BtnAddDocNo.BackColor = System.Drawing.Color.YellowGreen
        Me.BtnAddDocNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnAddDocNo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAddDocNo.FlatAppearance.BorderSize = 0
        Me.BtnAddDocNo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnAddDocNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAddDocNo.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAddDocNo.ForeColor = System.Drawing.Color.White
        Me.BtnAddDocNo.Image = Global.ReceivingSystem.My.Resources.Resources.Plus64
        Me.BtnAddDocNo.Location = New System.Drawing.Point(254, 2)
        Me.BtnAddDocNo.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnAddDocNo.Name = "BtnAddDocNo"
        Me.BtnAddDocNo.Size = New System.Drawing.Size(122, 167)
        Me.BtnAddDocNo.TabIndex = 122
        Me.BtnAddDocNo.Text = "Checker"
        Me.BtnAddDocNo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnAddDocNo.UseVisualStyleBackColor = False
        '
        'BtnUnLockRcNo
        '
        Me.BtnUnLockRcNo.BackColor = System.Drawing.Color.Coral
        Me.BtnUnLockRcNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnUnLockRcNo.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnUnLockRcNo.FlatAppearance.BorderSize = 0
        Me.BtnUnLockRcNo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnUnLockRcNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnUnLockRcNo.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnUnLockRcNo.ForeColor = System.Drawing.Color.White
        Me.BtnUnLockRcNo.Image = Global.ReceivingSystem.My.Resources.Resources.Unlock64
        Me.BtnUnLockRcNo.Location = New System.Drawing.Point(380, 2)
        Me.BtnUnLockRcNo.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnUnLockRcNo.Name = "BtnUnLockRcNo"
        Me.BtnUnLockRcNo.Size = New System.Drawing.Size(122, 167)
        Me.BtnUnLockRcNo.TabIndex = 123
        Me.BtnUnLockRcNo.Text = "Unlock Rc no.(Sup)"
        Me.BtnUnLockRcNo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnUnLockRcNo.UseVisualStyleBackColor = False
        '
        'BtnApproveFromChecker
        '
        Me.BtnApproveFromChecker.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.BtnApproveFromChecker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnApproveFromChecker.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnApproveFromChecker.FlatAppearance.BorderSize = 0
        Me.BtnApproveFromChecker.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnApproveFromChecker.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnApproveFromChecker.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnApproveFromChecker.ForeColor = System.Drawing.Color.White
        Me.BtnApproveFromChecker.Image = Global.ReceivingSystem.My.Resources.Resources.CheckedCheckbox64
        Me.BtnApproveFromChecker.Location = New System.Drawing.Point(506, 2)
        Me.BtnApproveFromChecker.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnApproveFromChecker.Name = "BtnApproveFromChecker"
        Me.BtnApproveFromChecker.Size = New System.Drawing.Size(122, 167)
        Me.BtnApproveFromChecker.TabIndex = 124
        Me.BtnApproveFromChecker.Text = "Approve from checker(Sup)"
        Me.BtnApproveFromChecker.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnApproveFromChecker.UseVisualStyleBackColor = False
        '
        'BtnCPAResult
        '
        Me.BtnCPAResult.BackColor = System.Drawing.Color.SandyBrown
        Me.BtnCPAResult.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnCPAResult.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCPAResult.FlatAppearance.BorderSize = 0
        Me.BtnCPAResult.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnCPAResult.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCPAResult.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnCPAResult.ForeColor = System.Drawing.Color.White
        Me.BtnCPAResult.Image = Global.ReceivingSystem.My.Resources.Resources.Natural_Food642
        Me.BtnCPAResult.Location = New System.Drawing.Point(632, 2)
        Me.BtnCPAResult.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnCPAResult.Name = "BtnCPAResult"
        Me.BtnCPAResult.Size = New System.Drawing.Size(122, 167)
        Me.BtnCPAResult.TabIndex = 125
        Me.BtnCPAResult.Text = "CPA result"
        Me.BtnCPAResult.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnCPAResult.UseVisualStyleBackColor = False
        '
        'BtnReport
        '
        Me.BtnReport.BackColor = System.Drawing.Color.OliveDrab
        Me.BtnReport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnReport.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnReport.FlatAppearance.BorderSize = 0
        Me.BtnReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnReport.Font = New System.Drawing.Font("Tahoma", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnReport.ForeColor = System.Drawing.Color.White
        Me.BtnReport.Image = Global.ReceivingSystem.My.Resources.Resources.Purchase1
        Me.BtnReport.Location = New System.Drawing.Point(758, 2)
        Me.BtnReport.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnReport.Name = "BtnReport"
        Me.BtnReport.Size = New System.Drawing.Size(122, 167)
        Me.BtnReport.TabIndex = 127
        Me.BtnReport.Text = "Report"
        Me.BtnReport.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnReport.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.FlowLayoutPanel4)
        Me.FlowLayoutPanel2.Controls.Add(Me.FlowLayoutPanel6)
        Me.FlowLayoutPanel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(15, 60)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(892, 373)
        Me.FlowLayoutPanel2.TabIndex = 129
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.AutoSize = True
        Me.FlowLayoutPanel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(2, 2)
        Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(0, 0)
        Me.FlowLayoutPanel4.TabIndex = 130
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.AutoSize = True
        Me.FlowLayoutPanel6.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel6.Controls.Add(Me.UserButton)
        Me.FlowLayoutPanel6.Controls.Add(Me.SettingButton)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(2, 6)
        Me.FlowLayoutPanel6.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(882, 69)
        Me.FlowLayoutPanel6.TabIndex = 133
        '
        'UserButton
        '
        Me.UserButton.AutoSize = True
        Me.UserButton.BackColor = System.Drawing.Color.White
        Me.UserButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.UserButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.UserButton.Enabled = False
        Me.UserButton.FlatAppearance.BorderSize = 0
        Me.UserButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.UserButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.UserButton.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.UserButton.ForeColor = System.Drawing.Color.Black
        Me.UserButton.Image = Global.ReceivingSystem.My.Resources.Resources.icons8_user_male_48
        Me.UserButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.UserButton.Location = New System.Drawing.Point(2, 2)
        Me.UserButton.Margin = New System.Windows.Forms.Padding(2)
        Me.UserButton.Name = "UserButton"
        Me.UserButton.Size = New System.Drawing.Size(71, 65)
        Me.UserButton.TabIndex = 135
        Me.UserButton.Text = "Username"
        Me.UserButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.UserButton.UseVisualStyleBackColor = False
        '
        'SettingButton
        '
        Me.SettingButton.AutoSize = True
        Me.SettingButton.BackColor = System.Drawing.Color.White
        Me.SettingButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SettingButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SettingButton.FlatAppearance.BorderSize = 0
        Me.SettingButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SettingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SettingButton.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SettingButton.ForeColor = System.Drawing.Color.Black
        Me.SettingButton.Image = Global.ReceivingSystem.My.Resources.Resources.icons8_services_48
        Me.SettingButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.SettingButton.Location = New System.Drawing.Point(77, 2)
        Me.SettingButton.Margin = New System.Windows.Forms.Padding(2)
        Me.SettingButton.Name = "SettingButton"
        Me.SettingButton.Size = New System.Drawing.Size(57, 65)
        Me.SettingButton.TabIndex = 135
        Me.SettingButton.Text = "Setting"
        Me.SettingButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.SettingButton.UseVisualStyleBackColor = False
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.AutoSize = True
        Me.FlowLayoutPanel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(568, 99)
        Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(2)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(0, 0)
        Me.FlowLayoutPanel5.TabIndex = 131
        '
        'FrmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(922, 449)
        Me.Controls.Add(Me.FlowLayoutPanel5)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmMenu"
        Me.Padding = New System.Windows.Forms.Padding(15, 60, 15, 16)
        Me.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Main Menu"
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel6.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_DecodePasswordBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_DecodePasswordTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter
    Friend WithEvents BtnReport As Button
    Friend WithEvents BtnLoadData As Button
    Friend WithEvents BtnCPAResult As Button
    Friend WithEvents BtnApproveFromChecker As Button
    Friend WithEvents BtnUnLockRcNo As Button
    Friend WithEvents BtnAddDocNo As Button
    Friend WithEvents BtnReceiving As Button
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
    Friend WithEvents SettingButton As Button
    Friend WithEvents UserButton As Button
End Class
