﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUnlockRCBFinish
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ReceivingWeightMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.RcNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.UnlockedTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.CBaleBarcodeLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.Sp_Receiving_SEL_MatRCByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatRCByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCByRcNoTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.crop = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subtype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.company = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.supplier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.baleno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.green = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classify = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weightbuy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WeightDiff = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.docno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.ReceivingWeightMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatRCByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReceivingWeightMetroGrid
        '
        Me.ReceivingWeightMetroGrid.AllowUserToAddRows = False
        Me.ReceivingWeightMetroGrid.AllowUserToDeleteRows = False
        Me.ReceivingWeightMetroGrid.AllowUserToResizeRows = False
        Me.ReceivingWeightMetroGrid.AutoGenerateColumns = False
        Me.ReceivingWeightMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.ReceivingWeightMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ReceivingWeightMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ReceivingWeightMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.ReceivingWeightMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReceivingWeightMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ReceivingWeightMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ReceivingWeightMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.crop, Me.type, Me.subtype, Me.company, Me.bc, Me.supplier, Me.baleno, Me.green, Me.classify, Me.weightbuy, Me.weight, Me.WeightDiff, Me.docno})
        Me.ReceivingWeightMetroGrid.DataSource = Me.Sp_Receiving_SEL_MatByRcNoBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ReceivingWeightMetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.ReceivingWeightMetroGrid.EnableHeadersVisualStyles = False
        Me.ReceivingWeightMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.ReceivingWeightMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ReceivingWeightMetroGrid.Location = New System.Drawing.Point(23, 94)
        Me.ReceivingWeightMetroGrid.Name = "ReceivingWeightMetroGrid"
        Me.ReceivingWeightMetroGrid.ReadOnly = True
        Me.ReceivingWeightMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReceivingWeightMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.ReceivingWeightMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.ReceivingWeightMetroGrid.RowTemplate.Height = 24
        Me.ReceivingWeightMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ReceivingWeightMetroGrid.Size = New System.Drawing.Size(1303, 587)
        Me.ReceivingWeightMetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.ReceivingWeightMetroGrid.TabIndex = 125
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RcNoTextbox)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.UnlockedTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.CBaleBarcodeLabel)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel16)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(23, 34)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 54)
        Me.MetroPanel1.TabIndex = 124
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'RcNoTextbox
        '
        '
        '
        '
        Me.RcNoTextbox.CustomButton.Image = Nothing
        Me.RcNoTextbox.CustomButton.Location = New System.Drawing.Point(79, 1)
        Me.RcNoTextbox.CustomButton.Name = ""
        Me.RcNoTextbox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.RcNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.RcNoTextbox.CustomButton.TabIndex = 1
        Me.RcNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.RcNoTextbox.CustomButton.UseSelectable = True
        Me.RcNoTextbox.CustomButton.Visible = False
        Me.RcNoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.RcNoTextbox.Lines = New String(-1) {}
        Me.RcNoTextbox.Location = New System.Drawing.Point(430, 10)
        Me.RcNoTextbox.MaxLength = 17
        Me.RcNoTextbox.Name = "RcNoTextbox"
        Me.RcNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.RcNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.RcNoTextbox.SelectedText = ""
        Me.RcNoTextbox.SelectionLength = 0
        Me.RcNoTextbox.SelectionStart = 0
        Me.RcNoTextbox.Size = New System.Drawing.Size(113, 35)
        Me.RcNoTextbox.Style = MetroFramework.MetroColorStyle.Orange
        Me.RcNoTextbox.TabIndex = 125
        Me.RcNoTextbox.UseSelectable = True
        Me.RcNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.RcNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel5.Location = New System.Drawing.Point(363, 17)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(61, 25)
        Me.MetroLabel5.TabIndex = 124
        Me.MetroLabel5.Text = "Rc no."
        '
        'UnlockedTile
        '
        Me.UnlockedTile.ActiveControl = Nothing
        Me.UnlockedTile.AutoSize = True
        Me.UnlockedTile.BackColor = System.Drawing.Color.White
        Me.UnlockedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.UnlockedTile.Location = New System.Drawing.Point(811, 10)
        Me.UnlockedTile.Name = "UnlockedTile"
        Me.UnlockedTile.Size = New System.Drawing.Size(36, 35)
        Me.UnlockedTile.Style = MetroFramework.MetroColorStyle.White
        Me.UnlockedTile.TabIndex = 123
        Me.UnlockedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnlockedTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Unlock32
        Me.UnlockedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnlockedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.UnlockedTile.UseSelectable = True
        Me.UnlockedTile.UseTileImage = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(853, 17)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel4.TabIndex = 122
        Me.MetroLabel4.Text = "Unlocked"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(953, 10)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'CBaleBarcodeLabel
        '
        Me.CBaleBarcodeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CBaleBarcodeLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.CBaleBarcodeLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.CBaleBarcodeLabel.Location = New System.Drawing.Point(549, 10)
        Me.CBaleBarcodeLabel.Name = "CBaleBarcodeLabel"
        Me.CBaleBarcodeLabel.Size = New System.Drawing.Size(82, 35)
        Me.CBaleBarcodeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CBaleBarcodeLabel.TabIndex = 75
        Me.CBaleBarcodeLabel.Text = "CropMetroLabel"
        Me.CBaleBarcodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.Location = New System.Drawing.Point(637, 20)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(44, 20)
        Me.MetroLabel16.TabIndex = 70
        Me.MetroLabel16.Text = "bales."
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(995, 17)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1126, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Receiving_SEL_MatRCByRcNoBindingSource
        '
        Me.Sp_Receiving_SEL_MatRCByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatRCByRcNo"
        Me.Sp_Receiving_SEL_MatRCByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatRCByRcNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatRCByRcNoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_SEL_MatByRcNoBindingSource
        '
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo"
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatByRcNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.ClearBeforeFill = True
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "rcno"
        Me.Column1.HeaderText = "Rcno"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 71
        '
        'crop
        '
        Me.crop.DataPropertyName = "crop"
        Me.crop.HeaderText = "crop"
        Me.crop.Name = "crop"
        Me.crop.ReadOnly = True
        Me.crop.Width = 68
        '
        'type
        '
        Me.type.DataPropertyName = "type"
        Me.type.HeaderText = "type"
        Me.type.Name = "type"
        Me.type.ReadOnly = True
        Me.type.Width = 67
        '
        'subtype
        '
        Me.subtype.DataPropertyName = "subtype"
        Me.subtype.HeaderText = "subtype"
        Me.subtype.Name = "subtype"
        Me.subtype.ReadOnly = True
        Me.subtype.Width = 95
        '
        'company
        '
        Me.company.DataPropertyName = "company"
        Me.company.HeaderText = "company"
        Me.company.Name = "company"
        Me.company.ReadOnly = True
        Me.company.Width = 104
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "bc"
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 51
        '
        'supplier
        '
        Me.supplier.DataPropertyName = "supplier"
        Me.supplier.HeaderText = "supplier"
        Me.supplier.Name = "supplier"
        Me.supplier.ReadOnly = True
        Me.supplier.Width = 95
        '
        'baleno
        '
        Me.baleno.DataPropertyName = "baleno"
        Me.baleno.HeaderText = "baleno"
        Me.baleno.Name = "baleno"
        Me.baleno.ReadOnly = True
        Me.baleno.Width = 85
        '
        'green
        '
        Me.green.DataPropertyName = "green"
        Me.green.HeaderText = "green"
        Me.green.Name = "green"
        Me.green.ReadOnly = True
        Me.green.Width = 77
        '
        'classify
        '
        Me.classify.DataPropertyName = "classify"
        Me.classify.HeaderText = "classify"
        Me.classify.Name = "classify"
        Me.classify.ReadOnly = True
        Me.classify.Width = 89
        '
        'weightbuy
        '
        Me.weightbuy.DataPropertyName = "weightbuy"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.weightbuy.DefaultCellStyle = DataGridViewCellStyle2
        Me.weightbuy.HeaderText = "weightbuy"
        Me.weightbuy.Name = "weightbuy"
        Me.weightbuy.ReadOnly = True
        Me.weightbuy.Width = 114
        '
        'weight
        '
        Me.weight.DataPropertyName = "weight"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.weight.DefaultCellStyle = DataGridViewCellStyle3
        Me.weight.HeaderText = "weight"
        Me.weight.Name = "weight"
        Me.weight.ReadOnly = True
        Me.weight.Width = 85
        '
        'WeightDiff
        '
        Me.WeightDiff.DataPropertyName = "WeightDiff"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.WeightDiff.DefaultCellStyle = DataGridViewCellStyle4
        Me.WeightDiff.HeaderText = "WeightDiff"
        Me.WeightDiff.Name = "WeightDiff"
        Me.WeightDiff.ReadOnly = True
        Me.WeightDiff.Width = 114
        '
        'docno
        '
        Me.docno.DataPropertyName = "docno"
        Me.docno.HeaderText = "docno"
        Me.docno.Name = "docno"
        Me.docno.ReadOnly = True
        Me.docno.Width = 81
        '
        'FrmUnlockRCBFinish
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1377, 715)
        Me.Controls.Add(Me.ReceivingWeightMetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmUnlockRCBFinish"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        CType(Me.ReceivingWeightMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatRCByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReceivingWeightMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RcNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents UnlockedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents CBaleBarcodeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents Sp_Receiving_SEL_MatRCByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatRCByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCByRcNoTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents crop As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents subtype As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents company As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents supplier As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents baleno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents green As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents classify As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents weightbuy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents weight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WeightDiff As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents docno As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
