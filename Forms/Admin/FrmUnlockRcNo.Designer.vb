﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmUnlockRcNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.UnlockedTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.RequestApproveMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.CropDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RcNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateFromCheckerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CheckerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemarkFromCheckerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateFromApprovedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApproveStatusDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ApproveByDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter()
        Me.ApproveStatusComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.RequestApproveMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.UnlockedTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 33)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1381, 54)
        Me.MetroPanel1.TabIndex = 118
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(965, 13)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'UnlockedTile
        '
        Me.UnlockedTile.ActiveControl = Nothing
        Me.UnlockedTile.AutoSize = True
        Me.UnlockedTile.BackColor = System.Drawing.Color.White
        Me.UnlockedTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.UnlockedTile.Location = New System.Drawing.Point(832, 13)
        Me.UnlockedTile.Name = "UnlockedTile"
        Me.UnlockedTile.Size = New System.Drawing.Size(36, 35)
        Me.UnlockedTile.Style = MetroFramework.MetroColorStyle.White
        Me.UnlockedTile.TabIndex = 119
        Me.UnlockedTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnlockedTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Unlock32
        Me.UnlockedTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.UnlockedTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.UnlockedTile.UseSelectable = True
        Me.UnlockedTile.UseTileImage = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(874, 20)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel4.TabIndex = 118
        Me.MetroLabel4.Text = "Unlocked"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(1007, 20)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1114, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'RequestApproveMetroGrid
        '
        Me.RequestApproveMetroGrid.AllowUserToAddRows = False
        Me.RequestApproveMetroGrid.AllowUserToDeleteRows = False
        Me.RequestApproveMetroGrid.AllowUserToResizeRows = False
        Me.RequestApproveMetroGrid.AutoGenerateColumns = False
        Me.RequestApproveMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.RequestApproveMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.RequestApproveMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RequestApproveMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.RequestApproveMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RequestApproveMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.RequestApproveMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RequestApproveMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CropDataGridViewTextBoxColumn, Me.RequestNoDataGridViewTextBoxColumn, Me.RequestTypeDataGridViewTextBoxColumn, Me.RcNoDataGridViewTextBoxColumn, Me.BCDataGridViewTextBoxColumn, Me.DateFromCheckerDataGridViewTextBoxColumn, Me.CheckerDataGridViewTextBoxColumn, Me.RemarkFromCheckerDataGridViewTextBoxColumn, Me.DateFromApprovedDataGridViewTextBoxColumn, Me.ApproveStatusDataGridViewCheckBoxColumn, Me.ApproveByDataGridViewTextBoxColumn})
        Me.RequestApproveMetroGrid.DataSource = Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.RequestApproveMetroGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.RequestApproveMetroGrid.EnableHeadersVisualStyles = False
        Me.RequestApproveMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.RequestApproveMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.RequestApproveMetroGrid.Location = New System.Drawing.Point(3, 140)
        Me.RequestApproveMetroGrid.Name = "RequestApproveMetroGrid"
        Me.RequestApproveMetroGrid.ReadOnly = True
        Me.RequestApproveMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RequestApproveMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.RequestApproveMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.RequestApproveMetroGrid.RowTemplate.Height = 24
        Me.RequestApproveMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.RequestApproveMetroGrid.Size = New System.Drawing.Size(1334, 529)
        Me.RequestApproveMetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.RequestApproveMetroGrid.TabIndex = 127
        '
        'CropDataGridViewTextBoxColumn
        '
        Me.CropDataGridViewTextBoxColumn.DataPropertyName = "Crop"
        Me.CropDataGridViewTextBoxColumn.HeaderText = "Crop"
        Me.CropDataGridViewTextBoxColumn.Name = "CropDataGridViewTextBoxColumn"
        Me.CropDataGridViewTextBoxColumn.ReadOnly = True
        Me.CropDataGridViewTextBoxColumn.Width = 70
        '
        'RequestNoDataGridViewTextBoxColumn
        '
        Me.RequestNoDataGridViewTextBoxColumn.DataPropertyName = "RequestNo"
        Me.RequestNoDataGridViewTextBoxColumn.HeaderText = "RequestNo"
        Me.RequestNoDataGridViewTextBoxColumn.Name = "RequestNoDataGridViewTextBoxColumn"
        Me.RequestNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.RequestNoDataGridViewTextBoxColumn.Visible = False
        Me.RequestNoDataGridViewTextBoxColumn.Width = 117
        '
        'RequestTypeDataGridViewTextBoxColumn
        '
        Me.RequestTypeDataGridViewTextBoxColumn.DataPropertyName = "RequestType"
        Me.RequestTypeDataGridViewTextBoxColumn.HeaderText = "RequestType"
        Me.RequestTypeDataGridViewTextBoxColumn.Name = "RequestTypeDataGridViewTextBoxColumn"
        Me.RequestTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.RequestTypeDataGridViewTextBoxColumn.Width = 131
        '
        'RcNoDataGridViewTextBoxColumn
        '
        Me.RcNoDataGridViewTextBoxColumn.DataPropertyName = "RcNo"
        Me.RcNoDataGridViewTextBoxColumn.HeaderText = "RcNo"
        Me.RcNoDataGridViewTextBoxColumn.Name = "RcNoDataGridViewTextBoxColumn"
        Me.RcNoDataGridViewTextBoxColumn.ReadOnly = True
        Me.RcNoDataGridViewTextBoxColumn.Width = 73
        '
        'BCDataGridViewTextBoxColumn
        '
        Me.BCDataGridViewTextBoxColumn.DataPropertyName = "BC"
        Me.BCDataGridViewTextBoxColumn.HeaderText = "BC"
        Me.BCDataGridViewTextBoxColumn.Name = "BCDataGridViewTextBoxColumn"
        Me.BCDataGridViewTextBoxColumn.ReadOnly = True
        Me.BCDataGridViewTextBoxColumn.Width = 53
        '
        'DateFromCheckerDataGridViewTextBoxColumn
        '
        Me.DateFromCheckerDataGridViewTextBoxColumn.DataPropertyName = "DateFromChecker"
        Me.DateFromCheckerDataGridViewTextBoxColumn.HeaderText = "DateFromChecker"
        Me.DateFromCheckerDataGridViewTextBoxColumn.Name = "DateFromCheckerDataGridViewTextBoxColumn"
        Me.DateFromCheckerDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateFromCheckerDataGridViewTextBoxColumn.Width = 171
        '
        'CheckerDataGridViewTextBoxColumn
        '
        Me.CheckerDataGridViewTextBoxColumn.DataPropertyName = "Checker"
        Me.CheckerDataGridViewTextBoxColumn.HeaderText = "Checker"
        Me.CheckerDataGridViewTextBoxColumn.Name = "CheckerDataGridViewTextBoxColumn"
        Me.CheckerDataGridViewTextBoxColumn.ReadOnly = True
        Me.CheckerDataGridViewTextBoxColumn.Width = 94
        '
        'RemarkFromCheckerDataGridViewTextBoxColumn
        '
        Me.RemarkFromCheckerDataGridViewTextBoxColumn.DataPropertyName = "RemarkFromChecker"
        Me.RemarkFromCheckerDataGridViewTextBoxColumn.HeaderText = "RemarkFromChecker"
        Me.RemarkFromCheckerDataGridViewTextBoxColumn.Name = "RemarkFromCheckerDataGridViewTextBoxColumn"
        Me.RemarkFromCheckerDataGridViewTextBoxColumn.ReadOnly = True
        Me.RemarkFromCheckerDataGridViewTextBoxColumn.Width = 194
        '
        'DateFromApprovedDataGridViewTextBoxColumn
        '
        Me.DateFromApprovedDataGridViewTextBoxColumn.DataPropertyName = "DateFromApproved"
        Me.DateFromApprovedDataGridViewTextBoxColumn.HeaderText = "DateFromApproved"
        Me.DateFromApprovedDataGridViewTextBoxColumn.Name = "DateFromApprovedDataGridViewTextBoxColumn"
        Me.DateFromApprovedDataGridViewTextBoxColumn.ReadOnly = True
        Me.DateFromApprovedDataGridViewTextBoxColumn.Width = 186
        '
        'ApproveStatusDataGridViewCheckBoxColumn
        '
        Me.ApproveStatusDataGridViewCheckBoxColumn.DataPropertyName = "ApproveStatus"
        Me.ApproveStatusDataGridViewCheckBoxColumn.HeaderText = "ApproveStatus"
        Me.ApproveStatusDataGridViewCheckBoxColumn.Name = "ApproveStatusDataGridViewCheckBoxColumn"
        Me.ApproveStatusDataGridViewCheckBoxColumn.ReadOnly = True
        Me.ApproveStatusDataGridViewCheckBoxColumn.Width = 128
        '
        'ApproveByDataGridViewTextBoxColumn
        '
        Me.ApproveByDataGridViewTextBoxColumn.DataPropertyName = "ApproveBy"
        Me.ApproveByDataGridViewTextBoxColumn.HeaderText = "ApproveBy"
        Me.ApproveByDataGridViewTextBoxColumn.Name = "ApproveByDataGridViewTextBoxColumn"
        Me.ApproveByDataGridViewTextBoxColumn.ReadOnly = True
        Me.ApproveByDataGridViewTextBoxColumn.Width = 118
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource.DataMember = "sp_Receiving_SEL_ReceivingCheckerRequest"
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.ClearBeforeFill = True
        '
        'ApproveStatusComboBox
        '
        Me.ApproveStatusComboBox.FormattingEnabled = True
        Me.ApproveStatusComboBox.ItemHeight = 24
        Me.ApproveStatusComboBox.Items.AddRange(New Object() {"Not approve", "Approve"})
        Me.ApproveStatusComboBox.Location = New System.Drawing.Point(128, 103)
        Me.ApproveStatusComboBox.Name = "ApproveStatusComboBox"
        Me.ApproveStatusComboBox.Size = New System.Drawing.Size(172, 30)
        Me.ApproveStatusComboBox.TabIndex = 132
        Me.ApproveStatusComboBox.UseSelectable = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(21, 103)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(101, 20)
        Me.MetroLabel3.TabIndex = 131
        Me.MetroLabel3.Text = "Approve status"
        '
        'FrmUnlockRcNo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1360, 768)
        Me.Controls.Add(Me.ApproveStatusComboBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.RequestApproveMetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmUnlockRcNo"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.RequestApproveMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents UnlockedTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents RequestApproveMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter
    Friend WithEvents ApproveStatusComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestTypeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RcNoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateFromCheckerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CheckerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RemarkFromCheckerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateFromApprovedDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ApproveStatusDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ApproveByDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
