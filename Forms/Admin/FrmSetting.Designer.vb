﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSetting
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.BtnUnlocakLeafAcc = New System.Windows.Forms.Button()
        Me.BtnAddRemark = New System.Windows.Forms.Button()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.BtnUnLockRCBFinished = New System.Windows.Forms.Button()
        Me.MetroPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.BtnUnLockRCBFinished)
        Me.MetroPanel1.Controls.Add(Me.BtnUnlocakLeafAcc)
        Me.MetroPanel1.Controls.Add(Me.BtnAddRemark)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(120, 208)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1038, 275)
        Me.MetroPanel1.TabIndex = 113
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'BtnUnlocakLeafAcc
        '
        Me.BtnUnlocakLeafAcc.BackColor = System.Drawing.Color.YellowGreen
        Me.BtnUnlocakLeafAcc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnUnlocakLeafAcc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnUnlocakLeafAcc.FlatAppearance.BorderSize = 0
        Me.BtnUnlocakLeafAcc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnUnlocakLeafAcc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnUnlocakLeafAcc.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnUnlocakLeafAcc.ForeColor = System.Drawing.Color.White
        Me.BtnUnlocakLeafAcc.Image = Global.ReceivingSystem.My.Resources.Resources.Unlock64
        Me.BtnUnlocakLeafAcc.Location = New System.Drawing.Point(433, 14)
        Me.BtnUnlocakLeafAcc.Name = "BtnUnlocakLeafAcc"
        Me.BtnUnlocakLeafAcc.Size = New System.Drawing.Size(197, 240)
        Me.BtnUnlocakLeafAcc.TabIndex = 3
        Me.BtnUnlocakLeafAcc.Text = "Unlock Leaf Acc."
        Me.BtnUnlocakLeafAcc.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnUnlocakLeafAcc.UseVisualStyleBackColor = False
        '
        'BtnAddRemark
        '
        Me.BtnAddRemark.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnAddRemark.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnAddRemark.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnAddRemark.FlatAppearance.BorderSize = 0
        Me.BtnAddRemark.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnAddRemark.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAddRemark.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAddRemark.ForeColor = System.Drawing.Color.White
        Me.BtnAddRemark.Image = Global.ReceivingSystem.My.Resources.Resources.Plus64
        Me.BtnAddRemark.Location = New System.Drawing.Point(230, 14)
        Me.BtnAddRemark.Name = "BtnAddRemark"
        Me.BtnAddRemark.Size = New System.Drawing.Size(197, 240)
        Me.BtnAddRemark.TabIndex = 2
        Me.BtnAddRemark.Text = "Add Remark"
        Me.BtnAddRemark.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnAddRemark.UseVisualStyleBackColor = False
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1161, 29)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 119
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(10, 29)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 118
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(73, 38)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 117
        Me.UsernameMetroLabel.Text = "Username"
        '
        'BtnUnLockRCBFinished
        '
        Me.BtnUnLockRCBFinished.BackColor = System.Drawing.Color.Coral
        Me.BtnUnLockRCBFinished.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnUnLockRCBFinished.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnUnLockRCBFinished.FlatAppearance.BorderSize = 0
        Me.BtnUnLockRCBFinished.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnUnLockRCBFinished.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnUnLockRCBFinished.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnUnLockRCBFinished.ForeColor = System.Drawing.Color.White
        Me.BtnUnLockRCBFinished.Image = Global.ReceivingSystem.My.Resources.Resources.Unlock64
        Me.BtnUnLockRCBFinished.Location = New System.Drawing.Point(636, 14)
        Me.BtnUnLockRCBFinished.Name = "BtnUnLockRCBFinished"
        Me.BtnUnLockRCBFinished.Size = New System.Drawing.Size(197, 240)
        Me.BtnUnLockRCBFinished.TabIndex = 6
        Me.BtnUnLockRCBFinished.Text = "Unlock RCB Finished"
        Me.BtnUnLockRCBFinished.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnUnLockRCBFinished.UseVisualStyleBackColor = False
        '
        'FrmSetting
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1279, 691)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.MetroTile1)
        Me.Controls.Add(Me.UsernameMetroLabel)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmSetting"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents BtnUnlocakLeafAcc As System.Windows.Forms.Button
    Friend WithEvents BtnAddRemark As System.Windows.Forms.Button
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents BtnUnLockRCBFinished As System.Windows.Forms.Button
End Class
