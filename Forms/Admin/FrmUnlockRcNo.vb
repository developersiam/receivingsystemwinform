﻿Public Class FrmUnlockRcNo
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim XRcNo As String
    Dim XApproveStatus As Boolean
    Dim XRequestNo As Integer
    Dim Xtext As String = "Unlocked"   'กำหนดให้ใช้คำว่า Unlocked ในstore sp_Receiving_SEL_ReceivingCheckerRequest
    Private Sub FrmEditRcNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ReceivingDataSet.type' table. You can move, or remove it, as needed.
        'Me.TypeTableAdapter.Fill(Me.ReceivingDataSet.type)
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            ApproveStatusComboBox.SelectedIndex = 0
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If
            Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, Xtext, XApproveStatus)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    
    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            'XType = TypeComboBox.Text
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If
            Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, Xtext, XApproveStatus)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ApproveStatusComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ApproveStatusComboBox.SelectedIndexChanged
        Try
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If
            Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, Xtext, XApproveStatus)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub MatRCMetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles RequestApproveMetroGrid.CellMouseClick
        Try
            XRequestNo = RequestApproveMetroGrid.Item(1, RequestApproveMetroGrid.CurrentCell.RowIndex).Value
            XRcNo = RequestApproveMetroGrid.Item(3, RequestApproveMetroGrid.CurrentCell.RowIndex).Value
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub UnlockedTile_Click(sender As Object, e As EventArgs) Handles UnlockedTile.Click
        Try
            If XRcNo = "" Or Convert.ToString(XRequestNo) = "" Then
                MessageBox.Show("กรุณาเลือกRc no. ที่ต้องการUnlockจากตารางด้านล่าง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            Dim result As DialogResult
            result = MessageBox.Show("ต้องการUnlocked Rc no. " & XRcNo & " ใช่หรือไม่? ", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.Yes Then
                db.sp_Receiving_UPD_ManagerApprove(_defaultCrop, XRequestNo, XRcNo, True, _username)
                db.sp_Receiving_UPD_CheckerUnLocked(XRcNo)
                MessageBox.Show("ทำการUnlock Receiving no." & XRcNo & "เรียบร้อยแล้ว!! กรุณาแจ้งCheckerเพื่อดำเนินการต่อ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ApproveStatusComboBox.SelectedIndex = 0
                If ApproveStatusComboBox.SelectedIndex = 0 Then
                    XApproveStatus = False
                ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                    XApproveStatus = True
                End If
                Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, Xtext, XApproveStatus)
            ElseIf result = DialogResult.No Then
                Return
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            ApproveStatusComboBox.SelectedIndex = 0
            If ApproveStatusComboBox.SelectedIndex = 0 Then
                XApproveStatus = False
            ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
                XApproveStatus = True
            End If
            Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, Xtext, XApproveStatus)
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class