﻿Public Class FrmUnlockRCBFinish
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmUnlockRCBFinish_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1360, 768)
            UsernameMetroLabel.Text = _username
            RcNoTextbox.Text = ""
            CBaleBarcodeLabel.Text = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub RcNoTextbox_KeyDown(sender As Object, e As KeyEventArgs) Handles RcNoTextbox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                If RcNoTextbox.Text <> "" Then
                    Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, RcNoTextbox.Text)
                    CBaleBarcodeLabel.Text = Me.Sp_Receiving_SEL_MatByRcNoBindingSource.Count
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            If RcNoTextbox.Text <> "" Then
                Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, RcNoTextbox.Text)
                CBaleBarcodeLabel.Text = Me.Sp_Receiving_SEL_MatByRcNoBindingSource.Count
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub UnlockedTile_Click(sender As Object, e As EventArgs) Handles UnlockedTile.Click
        Try
            If RcNoTextbox.Text <> "" Then
                'If CBaleBarcodeLabel.Text = "" Or Val(CBaleBarcodeLabel.Text) <= 0 Then
                '    MessageBox.Show("Rc no. นี้ไม่มีข้อมูล Bale barcode กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    Return
                'End If

                db.sp_Receiving_UPD_RCBFinishedUnlocked(RcNoTextbox.Text)
                MessageBox.Show("Unlock RCB finished เรียบร้อยแล้ว", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, RcNoTextbox.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RcNoTextbox_Click(sender As Object, e As EventArgs) Handles RcNoTextbox.Click
        Try
            CBaleBarcodeLabel.Text = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RcNoTextbox_TextChanged(sender As Object, e As EventArgs) Handles RcNoTextbox.TextChanged
        Try
            CBaleBarcodeLabel.Text = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    
End Class