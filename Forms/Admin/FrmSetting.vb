﻿Public Class FrmSetting
    Inherits MetroFramework.Forms.MetroForm
    

    Private Sub BtnAddRemark_Click(sender As Object, e As EventArgs) Handles BtnAddRemark.Click
        Try
            FrmRemarkRequestFromChecker.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnUnlocakLeafAcc_Click(sender As Object, e As EventArgs) Handles BtnUnlocakLeafAcc.Click
        Try
            FrmUnlockLeafAcc.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmSetting_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
        
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub BtnUnLockRCBFinished_Click(sender As Object, e As EventArgs) Handles BtnUnLockRCBFinished.Click
        Try
            FrmUnlockRCBFinish.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class