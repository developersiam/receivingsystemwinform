﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCPAResult
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MatRCMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.CPABatchNoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.Txlab1 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab2 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab3 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab4 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab5 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab6 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab7 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab8 = New MetroFramework.Controls.MetroTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ClearButton = New System.Windows.Forms.Button()
        Me.View8Button = New System.Windows.Forms.Button()
        Me.View7Button = New System.Windows.Forms.Button()
        Me.View6Button = New System.Windows.Forms.Button()
        Me.View5Button = New System.Windows.Forms.Button()
        Me.View4Button = New System.Windows.Forms.Button()
        Me.View3Button = New System.Windows.Forms.Button()
        Me.View2Button = New System.Windows.Forms.Button()
        Me.View1Button = New System.Windows.Forms.Button()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TotalBaleLabel = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RowNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rcno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dtrecocrd = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.truckno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.supplier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RCLines = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LabName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Results = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ResultDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.MatRCMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MatRCMetroGrid
        '
        Me.MatRCMetroGrid.AllowUserToAddRows = False
        Me.MatRCMetroGrid.AllowUserToDeleteRows = False
        Me.MatRCMetroGrid.AllowUserToResizeRows = False
        Me.MatRCMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MatRCMetroGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.MatRCMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRCMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MatRCMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MatRCMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRCMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.MatRCMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MatRCMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RowNumber, Me.bc, Me.rcno, Me.dtrecocrd, Me.truckno, Me.supplier, Me.RCLines, Me.LabName, Me.Results, Me.ResultDate})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MatRCMetroGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.MatRCMetroGrid.EnableHeadersVisualStyles = False
        Me.MatRCMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRCMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRCMetroGrid.Location = New System.Drawing.Point(274, 86)
        Me.MatRCMetroGrid.Name = "MatRCMetroGrid"
        Me.MatRCMetroGrid.ReadOnly = True
        Me.MatRCMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRCMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.MatRCMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MatRCMetroGrid.RowTemplate.Height = 24
        Me.MatRCMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MatRCMetroGrid.Size = New System.Drawing.Size(990, 591)
        Me.MatRCMetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.MatRCMetroGrid.TabIndex = 120
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(10, 26)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1247, 54)
        Me.MetroPanel1.TabIndex = 119
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1194, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 4)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 26)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'CPABatchNoTextbox
        '
        Me.CPABatchNoTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CPABatchNoTextbox.CustomButton.Image = Nothing
        Me.CPABatchNoTextbox.CustomButton.Location = New System.Drawing.Point(147, 2)
        Me.CPABatchNoTextbox.CustomButton.Name = ""
        Me.CPABatchNoTextbox.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.CPABatchNoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CPABatchNoTextbox.CustomButton.TabIndex = 1
        Me.CPABatchNoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CPABatchNoTextbox.CustomButton.UseSelectable = True
        Me.CPABatchNoTextbox.CustomButton.Visible = False
        Me.CPABatchNoTextbox.Lines = New String(-1) {}
        Me.CPABatchNoTextbox.Location = New System.Drawing.Point(75, 20)
        Me.CPABatchNoTextbox.MaxLength = 32767
        Me.CPABatchNoTextbox.Name = "CPABatchNoTextbox"
        Me.CPABatchNoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CPABatchNoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CPABatchNoTextbox.SelectedText = ""
        Me.CPABatchNoTextbox.SelectionLength = 0
        Me.CPABatchNoTextbox.SelectionStart = 0
        Me.CPABatchNoTextbox.ShortcutsEnabled = True
        Me.CPABatchNoTextbox.Size = New System.Drawing.Size(165, 20)
        Me.CPABatchNoTextbox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CPABatchNoTextbox.TabIndex = 0
        Me.CPABatchNoTextbox.UseSelectable = True
        Me.CPABatchNoTextbox.UseStyleColors = True
        Me.CPABatchNoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CPABatchNoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Txlab1
        '
        '
        '
        '
        Me.Txlab1.CustomButton.Image = Nothing
        Me.Txlab1.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab1.CustomButton.Name = ""
        Me.Txlab1.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab1.CustomButton.TabIndex = 1
        Me.Txlab1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab1.CustomButton.UseSelectable = True
        Me.Txlab1.CustomButton.Visible = False
        Me.Txlab1.Lines = New String(-1) {}
        Me.Txlab1.Location = New System.Drawing.Point(133, 30)
        Me.Txlab1.MaxLength = 32767
        Me.Txlab1.Name = "Txlab1"
        Me.Txlab1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab1.SelectedText = ""
        Me.Txlab1.SelectionLength = 0
        Me.Txlab1.SelectionStart = 0
        Me.Txlab1.ShortcutsEnabled = True
        Me.Txlab1.Size = New System.Drawing.Size(59, 20)
        Me.Txlab1.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab1.TabIndex = 1
        Me.Txlab1.UseSelectable = True
        Me.Txlab1.UseStyleColors = True
        Me.Txlab1.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab1.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel7.Location = New System.Drawing.Point(8, 31)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(87, 15)
        Me.MetroLabel7.TabIndex = 135
        Me.MetroLabel7.Text = "[1] Cypermethrin"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel5.Location = New System.Drawing.Point(8, 57)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(59, 15)
        Me.MetroLabel5.TabIndex = 145
        Me.MetroLabel5.Text = "[2] Butralin"
        '
        'Txlab2
        '
        '
        '
        '
        Me.Txlab2.CustomButton.Image = Nothing
        Me.Txlab2.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab2.CustomButton.Name = ""
        Me.Txlab2.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab2.CustomButton.TabIndex = 1
        Me.Txlab2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab2.CustomButton.UseSelectable = True
        Me.Txlab2.CustomButton.Visible = False
        Me.Txlab2.Lines = New String(-1) {}
        Me.Txlab2.Location = New System.Drawing.Point(133, 56)
        Me.Txlab2.MaxLength = 32767
        Me.Txlab2.Name = "Txlab2"
        Me.Txlab2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab2.SelectedText = ""
        Me.Txlab2.SelectionLength = 0
        Me.Txlab2.SelectionStart = 0
        Me.Txlab2.ShortcutsEnabled = True
        Me.Txlab2.Size = New System.Drawing.Size(59, 20)
        Me.Txlab2.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab2.TabIndex = 143
        Me.Txlab2.UseSelectable = True
        Me.Txlab2.UseStyleColors = True
        Me.Txlab2.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab2.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel11.Location = New System.Drawing.Point(8, 83)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(108, 15)
        Me.MetroLabel11.TabIndex = 149
        Me.MetroLabel11.Text = "[3] Chlorantranilipole"
        '
        'Txlab3
        '
        '
        '
        '
        Me.Txlab3.CustomButton.Image = Nothing
        Me.Txlab3.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab3.CustomButton.Name = ""
        Me.Txlab3.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab3.CustomButton.TabIndex = 1
        Me.Txlab3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab3.CustomButton.UseSelectable = True
        Me.Txlab3.CustomButton.Visible = False
        Me.Txlab3.Lines = New String(-1) {}
        Me.Txlab3.Location = New System.Drawing.Point(133, 82)
        Me.Txlab3.MaxLength = 32767
        Me.Txlab3.Name = "Txlab3"
        Me.Txlab3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab3.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab3.SelectedText = ""
        Me.Txlab3.SelectionLength = 0
        Me.Txlab3.SelectionStart = 0
        Me.Txlab3.ShortcutsEnabled = True
        Me.Txlab3.Size = New System.Drawing.Size(59, 20)
        Me.Txlab3.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab3.TabIndex = 147
        Me.Txlab3.UseSelectable = True
        Me.Txlab3.UseStyleColors = True
        Me.Txlab3.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab3.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel3.Location = New System.Drawing.Point(8, 109)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(119, 15)
        Me.MetroLabel3.TabIndex = 151
        Me.MetroLabel3.Text = "[4] Carbendazim (sum)"
        '
        'Txlab4
        '
        '
        '
        '
        Me.Txlab4.CustomButton.Image = Nothing
        Me.Txlab4.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab4.CustomButton.Name = ""
        Me.Txlab4.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab4.CustomButton.TabIndex = 1
        Me.Txlab4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab4.CustomButton.UseSelectable = True
        Me.Txlab4.CustomButton.Visible = False
        Me.Txlab4.Lines = New String(-1) {}
        Me.Txlab4.Location = New System.Drawing.Point(133, 108)
        Me.Txlab4.MaxLength = 32767
        Me.Txlab4.Name = "Txlab4"
        Me.Txlab4.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab4.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab4.SelectedText = ""
        Me.Txlab4.SelectionLength = 0
        Me.Txlab4.SelectionStart = 0
        Me.Txlab4.ShortcutsEnabled = True
        Me.Txlab4.Size = New System.Drawing.Size(59, 20)
        Me.Txlab4.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab4.TabIndex = 150
        Me.Txlab4.UseSelectable = True
        Me.Txlab4.UseStyleColors = True
        Me.Txlab4.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab4.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel4.Location = New System.Drawing.Point(8, 135)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(90, 15)
        Me.MetroLabel4.TabIndex = 153
        Me.MetroLabel4.Text = "[5] Chlorothalonil"
        '
        'Txlab5
        '
        '
        '
        '
        Me.Txlab5.CustomButton.Image = Nothing
        Me.Txlab5.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab5.CustomButton.Name = ""
        Me.Txlab5.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab5.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab5.CustomButton.TabIndex = 1
        Me.Txlab5.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab5.CustomButton.UseSelectable = True
        Me.Txlab5.CustomButton.Visible = False
        Me.Txlab5.Lines = New String(-1) {}
        Me.Txlab5.Location = New System.Drawing.Point(133, 134)
        Me.Txlab5.MaxLength = 32767
        Me.Txlab5.Name = "Txlab5"
        Me.Txlab5.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab5.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab5.SelectedText = ""
        Me.Txlab5.SelectionLength = 0
        Me.Txlab5.SelectionStart = 0
        Me.Txlab5.ShortcutsEnabled = True
        Me.Txlab5.Size = New System.Drawing.Size(59, 20)
        Me.Txlab5.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab5.TabIndex = 152
        Me.Txlab5.UseSelectable = True
        Me.Txlab5.UseStyleColors = True
        Me.Txlab5.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab5.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel8.Location = New System.Drawing.Point(8, 161)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(71, 15)
        Me.MetroLabel8.TabIndex = 155
        Me.MetroLabel8.Text = "[6] Iprodione"
        '
        'Txlab6
        '
        '
        '
        '
        Me.Txlab6.CustomButton.Image = Nothing
        Me.Txlab6.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab6.CustomButton.Name = ""
        Me.Txlab6.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab6.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab6.CustomButton.TabIndex = 1
        Me.Txlab6.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab6.CustomButton.UseSelectable = True
        Me.Txlab6.CustomButton.Visible = False
        Me.Txlab6.Lines = New String(-1) {}
        Me.Txlab6.Location = New System.Drawing.Point(133, 160)
        Me.Txlab6.MaxLength = 32767
        Me.Txlab6.Name = "Txlab6"
        Me.Txlab6.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab6.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab6.SelectedText = ""
        Me.Txlab6.SelectionLength = 0
        Me.Txlab6.SelectionStart = 0
        Me.Txlab6.ShortcutsEnabled = True
        Me.Txlab6.Size = New System.Drawing.Size(59, 20)
        Me.Txlab6.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab6.TabIndex = 154
        Me.Txlab6.UseSelectable = True
        Me.Txlab6.UseStyleColors = True
        Me.Txlab6.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab6.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel9.Location = New System.Drawing.Point(8, 187)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(99, 15)
        Me.MetroLabel9.TabIndex = 157
        Me.MetroLabel9.Text = "[7] Methomyl(sum)"
        '
        'Txlab7
        '
        '
        '
        '
        Me.Txlab7.CustomButton.Image = Nothing
        Me.Txlab7.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab7.CustomButton.Name = ""
        Me.Txlab7.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab7.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab7.CustomButton.TabIndex = 1
        Me.Txlab7.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab7.CustomButton.UseSelectable = True
        Me.Txlab7.CustomButton.Visible = False
        Me.Txlab7.Lines = New String(-1) {}
        Me.Txlab7.Location = New System.Drawing.Point(133, 186)
        Me.Txlab7.MaxLength = 32767
        Me.Txlab7.Name = "Txlab7"
        Me.Txlab7.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab7.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab7.SelectedText = ""
        Me.Txlab7.SelectionLength = 0
        Me.Txlab7.SelectionStart = 0
        Me.Txlab7.ShortcutsEnabled = True
        Me.Txlab7.Size = New System.Drawing.Size(59, 20)
        Me.Txlab7.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab7.TabIndex = 156
        Me.Txlab7.UseSelectable = True
        Me.Txlab7.UseStyleColors = True
        Me.Txlab7.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab7.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel12.Location = New System.Drawing.Point(8, 213)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(91, 15)
        Me.MetroLabel12.TabIndex = 166
        Me.MetroLabel12.Text = "[8] Pendimethalin"
        '
        'Txlab8
        '
        '
        '
        '
        Me.Txlab8.CustomButton.Image = Nothing
        Me.Txlab8.CustomButton.Location = New System.Drawing.Point(41, 2)
        Me.Txlab8.CustomButton.Name = ""
        Me.Txlab8.CustomButton.Size = New System.Drawing.Size(15, 15)
        Me.Txlab8.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab8.CustomButton.TabIndex = 1
        Me.Txlab8.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab8.CustomButton.UseSelectable = True
        Me.Txlab8.CustomButton.Visible = False
        Me.Txlab8.Lines = New String(-1) {}
        Me.Txlab8.Location = New System.Drawing.Point(133, 212)
        Me.Txlab8.MaxLength = 32767
        Me.Txlab8.Name = "Txlab8"
        Me.Txlab8.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab8.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab8.SelectedText = ""
        Me.Txlab8.SelectionLength = 0
        Me.Txlab8.SelectionStart = 0
        Me.Txlab8.ShortcutsEnabled = True
        Me.Txlab8.Size = New System.Drawing.Size(59, 20)
        Me.Txlab8.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab8.TabIndex = 165
        Me.Txlab8.UseSelectable = True
        Me.Txlab8.UseStyleColors = True
        Me.Txlab8.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab8.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Controls.Add(Me.ClearButton)
        Me.GroupBox1.Controls.Add(Me.View8Button)
        Me.GroupBox1.Controls.Add(Me.View7Button)
        Me.GroupBox1.Controls.Add(Me.View6Button)
        Me.GroupBox1.Controls.Add(Me.View5Button)
        Me.GroupBox1.Controls.Add(Me.View4Button)
        Me.GroupBox1.Controls.Add(Me.View3Button)
        Me.GroupBox1.Controls.Add(Me.View2Button)
        Me.GroupBox1.Controls.Add(Me.View1Button)
        Me.GroupBox1.Controls.Add(Me.SaveButton)
        Me.GroupBox1.Controls.Add(Me.MetroLabel7)
        Me.GroupBox1.Controls.Add(Me.Txlab1)
        Me.GroupBox1.Controls.Add(Me.MetroLabel5)
        Me.GroupBox1.Controls.Add(Me.MetroLabel12)
        Me.GroupBox1.Controls.Add(Me.Txlab2)
        Me.GroupBox1.Controls.Add(Me.Txlab8)
        Me.GroupBox1.Controls.Add(Me.MetroLabel11)
        Me.GroupBox1.Controls.Add(Me.Txlab3)
        Me.GroupBox1.Controls.Add(Me.MetroLabel3)
        Me.GroupBox1.Controls.Add(Me.MetroLabel4)
        Me.GroupBox1.Controls.Add(Me.MetroLabel8)
        Me.GroupBox1.Controls.Add(Me.Txlab4)
        Me.GroupBox1.Controls.Add(Me.MetroLabel9)
        Me.GroupBox1.Controls.Add(Me.Txlab5)
        Me.GroupBox1.Controls.Add(Me.Txlab6)
        Me.GroupBox1.Controls.Add(Me.Txlab7)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 164)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(258, 273)
        Me.GroupBox1.TabIndex = 168
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "CPA result form"
        '
        'ClearButton
        '
        Me.ClearButton.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClearButton.Location = New System.Drawing.Point(198, 238)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(43, 23)
        Me.ClearButton.TabIndex = 174
        Me.ClearButton.Text = "Clear"
        Me.ClearButton.UseVisualStyleBackColor = False
        '
        'View8Button
        '
        Me.View8Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View8Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View8Button.Location = New System.Drawing.Point(198, 212)
        Me.View8Button.Name = "View8Button"
        Me.View8Button.Size = New System.Drawing.Size(43, 20)
        Me.View8Button.TabIndex = 173
        Me.View8Button.Text = "View"
        Me.View8Button.UseVisualStyleBackColor = False
        '
        'View7Button
        '
        Me.View7Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View7Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View7Button.Location = New System.Drawing.Point(198, 186)
        Me.View7Button.Name = "View7Button"
        Me.View7Button.Size = New System.Drawing.Size(43, 20)
        Me.View7Button.TabIndex = 170
        Me.View7Button.Text = "View"
        Me.View7Button.UseVisualStyleBackColor = False
        '
        'View6Button
        '
        Me.View6Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View6Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View6Button.Location = New System.Drawing.Point(198, 160)
        Me.View6Button.Name = "View6Button"
        Me.View6Button.Size = New System.Drawing.Size(43, 20)
        Me.View6Button.TabIndex = 172
        Me.View6Button.Text = "View"
        Me.View6Button.UseVisualStyleBackColor = False
        '
        'View5Button
        '
        Me.View5Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View5Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View5Button.Location = New System.Drawing.Point(198, 134)
        Me.View5Button.Name = "View5Button"
        Me.View5Button.Size = New System.Drawing.Size(43, 20)
        Me.View5Button.TabIndex = 170
        Me.View5Button.Text = "View"
        Me.View5Button.UseVisualStyleBackColor = False
        '
        'View4Button
        '
        Me.View4Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View4Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View4Button.Location = New System.Drawing.Point(198, 109)
        Me.View4Button.Name = "View4Button"
        Me.View4Button.Size = New System.Drawing.Size(43, 20)
        Me.View4Button.TabIndex = 171
        Me.View4Button.Text = "View"
        Me.View4Button.UseVisualStyleBackColor = False
        '
        'View3Button
        '
        Me.View3Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View3Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View3Button.Location = New System.Drawing.Point(198, 83)
        Me.View3Button.Name = "View3Button"
        Me.View3Button.Size = New System.Drawing.Size(43, 20)
        Me.View3Button.TabIndex = 170
        Me.View3Button.Text = "View"
        Me.View3Button.UseVisualStyleBackColor = False
        '
        'View2Button
        '
        Me.View2Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View2Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View2Button.Location = New System.Drawing.Point(198, 57)
        Me.View2Button.Name = "View2Button"
        Me.View2Button.Size = New System.Drawing.Size(43, 20)
        Me.View2Button.TabIndex = 169
        Me.View2Button.Text = "View"
        Me.View2Button.UseVisualStyleBackColor = False
        '
        'View1Button
        '
        Me.View1Button.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.View1Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.View1Button.Location = New System.Drawing.Point(198, 30)
        Me.View1Button.Name = "View1Button"
        Me.View1Button.Size = New System.Drawing.Size(43, 20)
        Me.View1Button.TabIndex = 168
        Me.View1Button.Text = "View"
        Me.View1Button.UseVisualStyleBackColor = False
        '
        'SaveButton
        '
        Me.SaveButton.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.SaveButton.Location = New System.Drawing.Point(133, 238)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(59, 23)
        Me.SaveButton.TabIndex = 167
        Me.SaveButton.Text = "Save"
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox2.Controls.Add(Me.MetroLabel14)
        Me.GroupBox2.Controls.Add(Me.CPABatchNoTextbox)
        Me.GroupBox2.Controls.Add(Me.MetroLabel13)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(10, 86)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(258, 72)
        Me.GroupBox2.TabIndex = 169
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Batch search"
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel14.Location = New System.Drawing.Point(73, 43)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(167, 15)
        Me.MetroLabel14.Style = MetroFramework.MetroColorStyle.Red
        Me.MetroLabel14.TabIndex = 124
        Me.MetroLabel14.Text = "*Input batch no and press enter."
        Me.MetroLabel14.Theme = MetroFramework.MetroThemeStyle.Light
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel13.Location = New System.Drawing.Point(9, 21)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(51, 15)
        Me.MetroLabel13.TabIndex = 123
        Me.MetroLabel13.Text = "Batch no"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(23, 460)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(68, 29)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Total"
        '
        'TotalBaleLabel
        '
        Me.TotalBaleLabel.AutoSize = True
        Me.TotalBaleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.TotalBaleLabel.Location = New System.Drawing.Point(120, 460)
        Me.TotalBaleLabel.Name = "TotalBaleLabel"
        Me.TotalBaleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TotalBaleLabel.Size = New System.Drawing.Size(26, 29)
        Me.TotalBaleLabel.TabIndex = 170
        Me.TotalBaleLabel.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(188, 460)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(62, 29)
        Me.Label3.TabIndex = 171
        Me.Label3.Text = "Bale"
        '
        'RowNumber
        '
        Me.RowNumber.DataPropertyName = "RowNumber"
        Me.RowNumber.HeaderText = "no."
        Me.RowNumber.Name = "RowNumber"
        Me.RowNumber.ReadOnly = True
        Me.RowNumber.Width = 51
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "bale barcode"
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 111
        '
        'rcno
        '
        Me.rcno.DataPropertyName = "rcno"
        Me.rcno.HeaderText = "rcno"
        Me.rcno.Name = "rcno"
        Me.rcno.ReadOnly = True
        Me.rcno.Width = 58
        '
        'dtrecocrd
        '
        Me.dtrecocrd.DataPropertyName = "dtrecord"
        Me.dtrecocrd.HeaderText = "receive date"
        Me.dtrecocrd.Name = "dtrecocrd"
        Me.dtrecocrd.ReadOnly = True
        Me.dtrecocrd.Width = 108
        '
        'truckno
        '
        Me.truckno.DataPropertyName = "truckno"
        Me.truckno.HeaderText = "tuck no"
        Me.truckno.Name = "truckno"
        Me.truckno.ReadOnly = True
        Me.truckno.Width = 76
        '
        'supplier
        '
        Me.supplier.DataPropertyName = "supplier"
        Me.supplier.HeaderText = "supplier"
        Me.supplier.Name = "supplier"
        Me.supplier.ReadOnly = True
        Me.supplier.Width = 82
        '
        'RCLines
        '
        Me.RCLines.DataPropertyName = "RCLines"
        Me.RCLines.HeaderText = "rc line"
        Me.RCLines.Name = "RCLines"
        Me.RCLines.ReadOnly = True
        Me.RCLines.Width = 69
        '
        'LabName
        '
        Me.LabName.DataPropertyName = "LabName"
        Me.LabName.HeaderText = "lab name"
        Me.LabName.Name = "LabName"
        Me.LabName.ReadOnly = True
        Me.LabName.Width = 88
        '
        'Results
        '
        Me.Results.DataPropertyName = "Results"
        Me.Results.HeaderText = "result"
        Me.Results.Name = "Results"
        Me.Results.ReadOnly = True
        Me.Results.Width = 67
        '
        'ResultDate
        '
        Me.ResultDate.DataPropertyName = "ResultDate"
        Me.ResultDate.HeaderText = "result date"
        Me.ResultDate.Name = "ResultDate"
        Me.ResultDate.ReadOnly = True
        Me.ResultDate.Width = 98
        '
        'FrmCPAResult
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1280, 700)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TotalBaleLabel)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MatRCMetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmCPAResult"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        CType(Me.MatRCMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MatRCMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CPABatchNoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Txlab1 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab2 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab3 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab4 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab5 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab6 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab7 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab8 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SaveButton As Button
    Friend WithEvents View1Button As Button
    Friend WithEvents View2Button As Button
    Friend WithEvents View3Button As Button
    Friend WithEvents View4Button As Button
    Friend WithEvents View5Button As Button
    Friend WithEvents View6Button As Button
    Friend WithEvents View7Button As Button
    Friend WithEvents View8Button As Button
    Friend WithEvents ClearButton As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents TotalBaleLabel As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents RowNumber As DataGridViewTextBoxColumn
    Friend WithEvents bc As DataGridViewTextBoxColumn
    Friend WithEvents rcno As DataGridViewTextBoxColumn
    Friend WithEvents dtrecocrd As DataGridViewTextBoxColumn
    Friend WithEvents truckno As DataGridViewTextBoxColumn
    Friend WithEvents supplier As DataGridViewTextBoxColumn
    Friend WithEvents RCLines As DataGridViewTextBoxColumn
    Friend WithEvents LabName As DataGridViewTextBoxColumn
    Friend WithEvents Results As DataGridViewTextBoxColumn
    Friend WithEvents ResultDate As DataGridViewTextBoxColumn
End Class
