﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCPAFcv
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Sp_QCLab_SEL_CPASetupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatByBatchNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.ClearTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel24 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.ReportTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.CPABatchnoTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.Curertxt = New MetroFramework.Controls.MetroTextBox()
        Me.BaleFromTxt = New MetroFramework.Controls.MetroTextBox()
        Me.BaleToTxt = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.FarmerTxt = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel17 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab1 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab2 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab3 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab4 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab5 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab6 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.Txlab7 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.FindLab1 = New MetroFramework.Controls.MetroTile()
        Me.FindLab2 = New MetroFramework.Controls.MetroTile()
        Me.FindLab3 = New MetroFramework.Controls.MetroTile()
        Me.FindLab4 = New MetroFramework.Controls.MetroTile()
        Me.FindLab5 = New MetroFramework.Controls.MetroTile()
        Me.FindLab6 = New MetroFramework.Controls.MetroTile()
        Me.FindLab7 = New MetroFramework.Controls.MetroTile()
        Me.Txtlab8 = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.FindLab8 = New MetroFramework.Controls.MetroTile()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New MetroFramework.Controls.MetroGrid()
        Me.SpQClabSelAllCPAFCVByCurerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.sp_QCLab_INS_CPAFCVResultsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_QCLab_UPD_CPAFCVResultsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter()
        Me.TypeTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.typeTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_QCLab_SEL_CPASetupTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QCLab_SEL_CPASetupTableAdapter()
        Me.Sp_Receiving_SEL_MatByBatchNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByBatchNoTableAdapter()
        Me.Sp_QClab_SEL_CPAFCVByCurerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_QClab_SEL_CPAFCVByCurerTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByCurerTableAdapter()
        Me.Sp_QClab_GETMAX_BatchnoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_QClab_GETMAX_BatchnoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_GETMAX_BatchnoTableAdapter()
        Me.Sp_QClab_SelAll_CPAFCVByCurerTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_SelAll_CPAFCVByCurerTableAdapter()
        Me.sp_QCLab_INS_CPAFCVResultsTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QCLab_INS_CPAFCVResultsTableAdapter()
        Me.sp_QCLab_UPD_CPAFCVResultsTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QCLab_UPD_CPAFCVResultsTableAdapter()
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter()
        Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter()
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter()
        Me.sp_QClab_GETMAX_LotnoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_QClab_GETMAX_LotnoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_QClab_GETMAX_LotnoTableAdapter()
        Me.txtLot = New MetroFramework.Controls.MetroTextBox()
        Me.BatchNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Farmer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CurerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BaleFromDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BaleToDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LabNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ResultsDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.Sp_QCLab_SEL_CPASetupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatByBatchNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpQClabSelAllCPAFCVByCurerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_QCLab_INS_CPAFCVResultsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_QCLab_UPD_CPAFCVResultsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_QClab_SEL_CPAFCVByCurerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_QClab_GETMAX_BatchnoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_QClab_GETMAX_LotnoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.ClearTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel24)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.AddMetroTile)
        Me.MetroPanel1.Controls.Add(Me.ReportTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel10)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(28, 23)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 54)
        Me.MetroPanel1.TabIndex = 167
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'ClearTile
        '
        Me.ClearTile.ActiveControl = Nothing
        Me.ClearTile.AutoSize = True
        Me.ClearTile.BackColor = System.Drawing.Color.White
        Me.ClearTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearTile.Location = New System.Drawing.Point(699, 10)
        Me.ClearTile.Name = "ClearTile"
        Me.ClearTile.Size = New System.Drawing.Size(36, 35)
        Me.ClearTile.Style = MetroFramework.MetroColorStyle.White
        Me.ClearTile.TabIndex = 127
        Me.ClearTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CancelFile32
        Me.ClearTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ClearTile.UseSelectable = True
        Me.ClearTile.UseTileImage = True
        '
        'MetroLabel24
        '
        Me.MetroLabel24.AutoSize = True
        Me.MetroLabel24.Location = New System.Drawing.Point(741, 20)
        Me.MetroLabel24.Name = "MetroLabel24"
        Me.MetroLabel24.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel24.TabIndex = 126
        Me.MetroLabel24.Text = "clear"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(630, 20)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(37, 20)
        Me.MetroLabel1.TabIndex = 125
        Me.MetroLabel1.Text = "save"
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(589, 7)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(40, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 124
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Save32
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'ReportTile
        '
        Me.ReportTile.ActiveControl = Nothing
        Me.ReportTile.AutoSize = True
        Me.ReportTile.BackColor = System.Drawing.Color.White
        Me.ReportTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ReportTile.Location = New System.Drawing.Point(945, 16)
        Me.ReportTile.Name = "ReportTile"
        Me.ReportTile.Size = New System.Drawing.Size(36, 35)
        Me.ReportTile.Style = MetroFramework.MetroColorStyle.White
        Me.ReportTile.TabIndex = 123
        Me.ReportTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReportTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.PurchaseOrder32
        Me.ReportTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReportTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ReportTile.UseSelectable = True
        Me.ReportTile.UseTileImage = True
        Me.ReportTile.Visible = False
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(987, 23)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(48, 20)
        Me.MetroLabel10.TabIndex = 122
        Me.MetroLabel10.Text = "report"
        Me.MetroLabel10.Visible = False
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(804, 10)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Cancel32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(846, 20)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(48, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "delete"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1126, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'CPABatchnoTextbox
        '
        Me.CPABatchnoTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.CPABatchnoTextbox.CustomButton.Image = Nothing
        Me.CPABatchnoTextbox.CustomButton.Location = New System.Drawing.Point(161, 2)
        Me.CPABatchnoTextbox.CustomButton.Name = ""
        Me.CPABatchnoTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.CPABatchnoTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.CPABatchnoTextbox.CustomButton.TabIndex = 1
        Me.CPABatchnoTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.CPABatchnoTextbox.CustomButton.UseSelectable = True
        Me.CPABatchnoTextbox.CustomButton.Visible = False
        Me.CPABatchnoTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.CPABatchnoTextbox.Lines = New String(-1) {}
        Me.CPABatchnoTextbox.Location = New System.Drawing.Point(1016, 91)
        Me.CPABatchnoTextbox.MaxLength = 32767
        Me.CPABatchnoTextbox.Name = "CPABatchnoTextbox"
        Me.CPABatchnoTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.CPABatchnoTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.CPABatchnoTextbox.SelectedText = ""
        Me.CPABatchnoTextbox.SelectionLength = 0
        Me.CPABatchnoTextbox.SelectionStart = 0
        Me.CPABatchnoTextbox.ShortcutsEnabled = True
        Me.CPABatchnoTextbox.Size = New System.Drawing.Size(189, 30)
        Me.CPABatchnoTextbox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CPABatchnoTextbox.TabIndex = 192
        Me.CPABatchnoTextbox.UseSelectable = True
        Me.CPABatchnoTextbox.UseStyleColors = True
        Me.CPABatchnoTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.CPABatchnoTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel13.Location = New System.Drawing.Point(889, 96)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(120, 25)
        Me.MetroLabel13.TabIndex = 193
        Me.MetroLabel13.Text = "Sample batch"
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel14.Location = New System.Drawing.Point(237, 96)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(56, 25)
        Me.MetroLabel14.TabIndex = 194
        Me.MetroLabel14.Text = "Curer"
        '
        'Curertxt
        '
        '
        '
        '
        Me.Curertxt.CustomButton.Image = Nothing
        Me.Curertxt.CustomButton.Location = New System.Drawing.Point(86, 2)
        Me.Curertxt.CustomButton.Name = ""
        Me.Curertxt.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Curertxt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Curertxt.CustomButton.TabIndex = 1
        Me.Curertxt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Curertxt.CustomButton.UseSelectable = True
        Me.Curertxt.CustomButton.Visible = False
        Me.Curertxt.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Curertxt.Lines = New String(-1) {}
        Me.Curertxt.Location = New System.Drawing.Point(295, 91)
        Me.Curertxt.MaxLength = 32767
        Me.Curertxt.Name = "Curertxt"
        Me.Curertxt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Curertxt.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Curertxt.SelectedText = ""
        Me.Curertxt.SelectionLength = 0
        Me.Curertxt.SelectionStart = 0
        Me.Curertxt.ShortcutsEnabled = True
        Me.Curertxt.Size = New System.Drawing.Size(114, 30)
        Me.Curertxt.Style = MetroFramework.MetroColorStyle.Orange
        Me.Curertxt.TabIndex = 195
        Me.Curertxt.UseSelectable = True
        Me.Curertxt.UseStyleColors = True
        Me.Curertxt.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Curertxt.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'BaleFromTxt
        '
        '
        '
        '
        Me.BaleFromTxt.CustomButton.Image = Nothing
        Me.BaleFromTxt.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.BaleFromTxt.CustomButton.Name = ""
        Me.BaleFromTxt.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.BaleFromTxt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BaleFromTxt.CustomButton.TabIndex = 1
        Me.BaleFromTxt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BaleFromTxt.CustomButton.UseSelectable = True
        Me.BaleFromTxt.CustomButton.Visible = False
        Me.BaleFromTxt.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BaleFromTxt.Lines = New String(-1) {}
        Me.BaleFromTxt.Location = New System.Drawing.Point(511, 91)
        Me.BaleFromTxt.MaxLength = 32767
        Me.BaleFromTxt.Name = "BaleFromTxt"
        Me.BaleFromTxt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BaleFromTxt.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BaleFromTxt.SelectedText = ""
        Me.BaleFromTxt.SelectionLength = 0
        Me.BaleFromTxt.SelectionStart = 0
        Me.BaleFromTxt.ShortcutsEnabled = True
        Me.BaleFromTxt.Size = New System.Drawing.Size(101, 30)
        Me.BaleFromTxt.Style = MetroFramework.MetroColorStyle.Orange
        Me.BaleFromTxt.TabIndex = 196
        Me.BaleFromTxt.UseSelectable = True
        Me.BaleFromTxt.UseStyleColors = True
        Me.BaleFromTxt.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BaleFromTxt.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'BaleToTxt
        '
        '
        '
        '
        Me.BaleToTxt.CustomButton.Image = Nothing
        Me.BaleToTxt.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.BaleToTxt.CustomButton.Name = ""
        Me.BaleToTxt.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.BaleToTxt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.BaleToTxt.CustomButton.TabIndex = 1
        Me.BaleToTxt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.BaleToTxt.CustomButton.UseSelectable = True
        Me.BaleToTxt.CustomButton.Visible = False
        Me.BaleToTxt.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.BaleToTxt.Lines = New String(-1) {}
        Me.BaleToTxt.Location = New System.Drawing.Point(654, 91)
        Me.BaleToTxt.MaxLength = 32767
        Me.BaleToTxt.Name = "BaleToTxt"
        Me.BaleToTxt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.BaleToTxt.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.BaleToTxt.SelectedText = ""
        Me.BaleToTxt.SelectionLength = 0
        Me.BaleToTxt.SelectionStart = 0
        Me.BaleToTxt.ShortcutsEnabled = True
        Me.BaleToTxt.Size = New System.Drawing.Size(101, 30)
        Me.BaleToTxt.Style = MetroFramework.MetroColorStyle.Orange
        Me.BaleToTxt.TabIndex = 197
        Me.BaleToTxt.UseSelectable = True
        Me.BaleToTxt.UseStyleColors = True
        Me.BaleToTxt.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.BaleToTxt.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel15.Location = New System.Drawing.Point(430, 96)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(78, 25)
        Me.MetroLabel15.TabIndex = 198
        Me.MetroLabel15.Text = "Bale No."
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel16.Location = New System.Drawing.Point(617, 96)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(30, 25)
        Me.MetroLabel16.TabIndex = 199
        Me.MetroLabel16.Text = "To"
        '
        'FarmerTxt
        '
        '
        '
        '
        Me.FarmerTxt.CustomButton.Image = Nothing
        Me.FarmerTxt.CustomButton.Location = New System.Drawing.Point(86, 2)
        Me.FarmerTxt.CustomButton.Name = ""
        Me.FarmerTxt.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.FarmerTxt.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.FarmerTxt.CustomButton.TabIndex = 1
        Me.FarmerTxt.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.FarmerTxt.CustomButton.UseSelectable = True
        Me.FarmerTxt.CustomButton.Visible = False
        Me.FarmerTxt.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.FarmerTxt.Lines = New String(-1) {}
        Me.FarmerTxt.Location = New System.Drawing.Point(114, 91)
        Me.FarmerTxt.MaxLength = 32767
        Me.FarmerTxt.Name = "FarmerTxt"
        Me.FarmerTxt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.FarmerTxt.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.FarmerTxt.SelectedText = ""
        Me.FarmerTxt.SelectionLength = 0
        Me.FarmerTxt.SelectionStart = 0
        Me.FarmerTxt.ShortcutsEnabled = True
        Me.FarmerTxt.Size = New System.Drawing.Size(114, 30)
        Me.FarmerTxt.Style = MetroFramework.MetroColorStyle.Orange
        Me.FarmerTxt.TabIndex = 201
        Me.FarmerTxt.UseSelectable = True
        Me.FarmerTxt.UseStyleColors = True
        Me.FarmerTxt.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FarmerTxt.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel17
        '
        Me.MetroLabel17.AutoSize = True
        Me.MetroLabel17.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel17.Location = New System.Drawing.Point(33, 96)
        Me.MetroLabel17.Name = "MetroLabel17"
        Me.MetroLabel17.Size = New System.Drawing.Size(67, 25)
        Me.MetroLabel17.TabIndex = 200
        Me.MetroLabel17.Text = "Farmer"
        '
        'Txlab1
        '
        '
        '
        '
        Me.Txlab1.CustomButton.Image = Nothing
        Me.Txlab1.CustomButton.Location = New System.Drawing.Point(86, 2)
        Me.Txlab1.CustomButton.Name = ""
        Me.Txlab1.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab1.CustomButton.TabIndex = 1
        Me.Txlab1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab1.CustomButton.UseSelectable = True
        Me.Txlab1.CustomButton.Visible = False
        Me.Txlab1.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab1.Lines = New String(-1) {}
        Me.Txlab1.Location = New System.Drawing.Point(171, 6)
        Me.Txlab1.MaxLength = 32767
        Me.Txlab1.Name = "Txlab1"
        Me.Txlab1.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab1.SelectedText = ""
        Me.Txlab1.SelectionLength = 0
        Me.Txlab1.SelectionStart = 0
        Me.Txlab1.ShortcutsEnabled = True
        Me.Txlab1.Size = New System.Drawing.Size(114, 30)
        Me.Txlab1.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab1.TabIndex = 166
        Me.Txlab1.UseSelectable = True
        Me.Txlab1.UseStyleColors = True
        Me.Txlab1.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab1.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel7.Location = New System.Drawing.Point(39, 6)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(122, 25)
        Me.MetroLabel7.TabIndex = 169
        Me.MetroLabel7.Text = "Cypermethrin"
        '
        'Txlab2
        '
        '
        '
        '
        Me.Txlab2.CustomButton.Image = Nothing
        Me.Txlab2.CustomButton.Location = New System.Drawing.Point(86, 2)
        Me.Txlab2.CustomButton.Name = ""
        Me.Txlab2.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab2.CustomButton.TabIndex = 1
        Me.Txlab2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab2.CustomButton.UseSelectable = True
        Me.Txlab2.CustomButton.Visible = False
        Me.Txlab2.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab2.Lines = New String(-1) {}
        Me.Txlab2.Location = New System.Drawing.Point(171, 42)
        Me.Txlab2.MaxLength = 32767
        Me.Txlab2.Name = "Txlab2"
        Me.Txlab2.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab2.SelectedText = ""
        Me.Txlab2.SelectionLength = 0
        Me.Txlab2.SelectionStart = 0
        Me.Txlab2.ShortcutsEnabled = True
        Me.Txlab2.Size = New System.Drawing.Size(114, 30)
        Me.Txlab2.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab2.TabIndex = 170
        Me.Txlab2.UseSelectable = True
        Me.Txlab2.UseStyleColors = True
        Me.Txlab2.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab2.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel5.Location = New System.Drawing.Point(81, 47)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(71, 25)
        Me.MetroLabel5.TabIndex = 171
        Me.MetroLabel5.Text = "Butralin"
        '
        'Txlab3
        '
        '
        '
        '
        Me.Txlab3.CustomButton.Image = Nothing
        Me.Txlab3.CustomButton.Location = New System.Drawing.Point(86, 2)
        Me.Txlab3.CustomButton.Name = ""
        Me.Txlab3.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab3.CustomButton.TabIndex = 1
        Me.Txlab3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab3.CustomButton.UseSelectable = True
        Me.Txlab3.CustomButton.Visible = False
        Me.Txlab3.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab3.Lines = New String(-1) {}
        Me.Txlab3.Location = New System.Drawing.Point(171, 78)
        Me.Txlab3.MaxLength = 32767
        Me.Txlab3.Name = "Txlab3"
        Me.Txlab3.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab3.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab3.SelectedText = ""
        Me.Txlab3.SelectionLength = 0
        Me.Txlab3.SelectionStart = 0
        Me.Txlab3.ShortcutsEnabled = True
        Me.Txlab3.Size = New System.Drawing.Size(114, 30)
        Me.Txlab3.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab3.TabIndex = 172
        Me.Txlab3.UseSelectable = True
        Me.Txlab3.UseStyleColors = True
        Me.Txlab3.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab3.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel11.Location = New System.Drawing.Point(4, 81)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(153, 25)
        Me.MetroLabel11.TabIndex = 173
        Me.MetroLabel11.Text = "Chlorantranilipole"
        '
        'Txlab4
        '
        '
        '
        '
        Me.Txlab4.CustomButton.Image = Nothing
        Me.Txlab4.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.Txlab4.CustomButton.Name = ""
        Me.Txlab4.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab4.CustomButton.TabIndex = 1
        Me.Txlab4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab4.CustomButton.UseSelectable = True
        Me.Txlab4.CustomButton.Visible = False
        Me.Txlab4.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab4.Lines = New String(-1) {}
        Me.Txlab4.Location = New System.Drawing.Point(522, 6)
        Me.Txlab4.MaxLength = 32767
        Me.Txlab4.Name = "Txlab4"
        Me.Txlab4.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab4.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab4.SelectedText = ""
        Me.Txlab4.SelectionLength = 0
        Me.Txlab4.SelectionStart = 0
        Me.Txlab4.ShortcutsEnabled = True
        Me.Txlab4.Size = New System.Drawing.Size(101, 30)
        Me.Txlab4.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab4.TabIndex = 174
        Me.Txlab4.UseSelectable = True
        Me.Txlab4.UseStyleColors = True
        Me.Txlab4.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab4.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(347, 6)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(164, 25)
        Me.MetroLabel3.TabIndex = 175
        Me.MetroLabel3.Text = "Carbendazim(Sum)"
        '
        'Txlab5
        '
        '
        '
        '
        Me.Txlab5.CustomButton.Image = Nothing
        Me.Txlab5.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.Txlab5.CustomButton.Name = ""
        Me.Txlab5.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab5.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab5.CustomButton.TabIndex = 1
        Me.Txlab5.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab5.CustomButton.UseSelectable = True
        Me.Txlab5.CustomButton.Visible = False
        Me.Txlab5.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab5.Lines = New String(-1) {}
        Me.Txlab5.Location = New System.Drawing.Point(522, 44)
        Me.Txlab5.MaxLength = 32767
        Me.Txlab5.Name = "Txlab5"
        Me.Txlab5.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab5.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab5.SelectedText = ""
        Me.Txlab5.SelectionLength = 0
        Me.Txlab5.SelectionStart = 0
        Me.Txlab5.ShortcutsEnabled = True
        Me.Txlab5.Size = New System.Drawing.Size(101, 30)
        Me.Txlab5.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab5.TabIndex = 176
        Me.Txlab5.UseSelectable = True
        Me.Txlab5.UseStyleColors = True
        Me.Txlab5.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab5.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel4.Location = New System.Drawing.Point(385, 41)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(124, 25)
        Me.MetroLabel4.TabIndex = 177
        Me.MetroLabel4.Text = "Chlorothalonil"
        '
        'Txlab6
        '
        '
        '
        '
        Me.Txlab6.CustomButton.Image = Nothing
        Me.Txlab6.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.Txlab6.CustomButton.Name = ""
        Me.Txlab6.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab6.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab6.CustomButton.TabIndex = 1
        Me.Txlab6.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab6.CustomButton.UseSelectable = True
        Me.Txlab6.CustomButton.Visible = False
        Me.Txlab6.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab6.Lines = New String(-1) {}
        Me.Txlab6.Location = New System.Drawing.Point(522, 78)
        Me.Txlab6.MaxLength = 32767
        Me.Txlab6.Name = "Txlab6"
        Me.Txlab6.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab6.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab6.SelectedText = ""
        Me.Txlab6.SelectionLength = 0
        Me.Txlab6.SelectionStart = 0
        Me.Txlab6.ShortcutsEnabled = True
        Me.Txlab6.Size = New System.Drawing.Size(101, 30)
        Me.Txlab6.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab6.TabIndex = 178
        Me.Txlab6.UseSelectable = True
        Me.Txlab6.UseStyleColors = True
        Me.Txlab6.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab6.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel8.Location = New System.Drawing.Point(419, 73)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(90, 25)
        Me.MetroLabel8.TabIndex = 179
        Me.MetroLabel8.Text = "Iprodione"
        '
        'Txlab7
        '
        '
        '
        '
        Me.Txlab7.CustomButton.Image = Nothing
        Me.Txlab7.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.Txlab7.CustomButton.Name = ""
        Me.Txlab7.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txlab7.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txlab7.CustomButton.TabIndex = 1
        Me.Txlab7.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txlab7.CustomButton.UseSelectable = True
        Me.Txlab7.CustomButton.Visible = False
        Me.Txlab7.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txlab7.Lines = New String(-1) {}
        Me.Txlab7.Location = New System.Drawing.Point(820, 6)
        Me.Txlab7.MaxLength = 32767
        Me.Txlab7.Name = "Txlab7"
        Me.Txlab7.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txlab7.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txlab7.SelectedText = ""
        Me.Txlab7.SelectionLength = 0
        Me.Txlab7.SelectionStart = 0
        Me.Txlab7.ShortcutsEnabled = True
        Me.Txlab7.Size = New System.Drawing.Size(101, 30)
        Me.Txlab7.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txlab7.TabIndex = 180
        Me.Txlab7.UseSelectable = True
        Me.Txlab7.UseStyleColors = True
        Me.Txlab7.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txlab7.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel9.Location = New System.Drawing.Point(673, 6)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(137, 25)
        Me.MetroLabel9.TabIndex = 181
        Me.MetroLabel9.Text = "Methomyl(sum)"
        '
        'FindLab1
        '
        Me.FindLab1.ActiveControl = Nothing
        Me.FindLab1.AutoSize = True
        Me.FindLab1.BackColor = System.Drawing.Color.White
        Me.FindLab1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab1.Location = New System.Drawing.Point(291, 5)
        Me.FindLab1.Name = "FindLab1"
        Me.FindLab1.Size = New System.Drawing.Size(34, 31)
        Me.FindLab1.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab1.TabIndex = 189
        Me.FindLab1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab1.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab1.UseSelectable = True
        Me.FindLab1.UseTileImage = True
        '
        'FindLab2
        '
        Me.FindLab2.ActiveControl = Nothing
        Me.FindLab2.AutoSize = True
        Me.FindLab2.BackColor = System.Drawing.Color.White
        Me.FindLab2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab2.Location = New System.Drawing.Point(291, 44)
        Me.FindLab2.Name = "FindLab2"
        Me.FindLab2.Size = New System.Drawing.Size(34, 31)
        Me.FindLab2.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab2.TabIndex = 183
        Me.FindLab2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab2.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab2.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab2.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab2.UseSelectable = True
        Me.FindLab2.UseTileImage = True
        '
        'FindLab3
        '
        Me.FindLab3.ActiveControl = Nothing
        Me.FindLab3.AutoSize = True
        Me.FindLab3.BackColor = System.Drawing.Color.White
        Me.FindLab3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab3.Location = New System.Drawing.Point(291, 78)
        Me.FindLab3.Name = "FindLab3"
        Me.FindLab3.Size = New System.Drawing.Size(34, 31)
        Me.FindLab3.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab3.TabIndex = 184
        Me.FindLab3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab3.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab3.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab3.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab3.UseSelectable = True
        Me.FindLab3.UseTileImage = True
        '
        'FindLab4
        '
        Me.FindLab4.ActiveControl = Nothing
        Me.FindLab4.AutoSize = True
        Me.FindLab4.BackColor = System.Drawing.Color.White
        Me.FindLab4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab4.Location = New System.Drawing.Point(629, 6)
        Me.FindLab4.Name = "FindLab4"
        Me.FindLab4.Size = New System.Drawing.Size(34, 31)
        Me.FindLab4.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab4.TabIndex = 185
        Me.FindLab4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab4.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab4.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab4.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab4.UseSelectable = True
        Me.FindLab4.UseTileImage = True
        '
        'FindLab5
        '
        Me.FindLab5.ActiveControl = Nothing
        Me.FindLab5.AutoSize = True
        Me.FindLab5.BackColor = System.Drawing.Color.White
        Me.FindLab5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab5.Location = New System.Drawing.Point(629, 47)
        Me.FindLab5.Name = "FindLab5"
        Me.FindLab5.Size = New System.Drawing.Size(34, 31)
        Me.FindLab5.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab5.TabIndex = 186
        Me.FindLab5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab5.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab5.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab5.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab5.UseSelectable = True
        Me.FindLab5.UseTileImage = True
        '
        'FindLab6
        '
        Me.FindLab6.ActiveControl = Nothing
        Me.FindLab6.AutoSize = True
        Me.FindLab6.BackColor = System.Drawing.Color.White
        Me.FindLab6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab6.Location = New System.Drawing.Point(632, 78)
        Me.FindLab6.Name = "FindLab6"
        Me.FindLab6.Size = New System.Drawing.Size(34, 31)
        Me.FindLab6.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab6.TabIndex = 187
        Me.FindLab6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab6.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab6.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab6.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab6.UseSelectable = True
        Me.FindLab6.UseTileImage = True
        '
        'FindLab7
        '
        Me.FindLab7.ActiveControl = Nothing
        Me.FindLab7.AutoSize = True
        Me.FindLab7.BackColor = System.Drawing.Color.White
        Me.FindLab7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab7.Location = New System.Drawing.Point(927, 6)
        Me.FindLab7.Name = "FindLab7"
        Me.FindLab7.Size = New System.Drawing.Size(34, 31)
        Me.FindLab7.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab7.TabIndex = 188
        Me.FindLab7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab7.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab7.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab7.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab7.UseSelectable = True
        Me.FindLab7.UseTileImage = True
        '
        'Txtlab8
        '
        '
        '
        '
        Me.Txtlab8.CustomButton.Image = Nothing
        Me.Txtlab8.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.Txtlab8.CustomButton.Name = ""
        Me.Txtlab8.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.Txtlab8.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.Txtlab8.CustomButton.TabIndex = 1
        Me.Txtlab8.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.Txtlab8.CustomButton.UseSelectable = True
        Me.Txtlab8.CustomButton.Visible = False
        Me.Txtlab8.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.Txtlab8.Lines = New String(-1) {}
        Me.Txtlab8.Location = New System.Drawing.Point(820, 41)
        Me.Txtlab8.MaxLength = 32767
        Me.Txtlab8.Name = "Txtlab8"
        Me.Txtlab8.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.Txtlab8.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.Txtlab8.SelectedText = ""
        Me.Txtlab8.SelectionLength = 0
        Me.Txtlab8.SelectionStart = 0
        Me.Txtlab8.ShortcutsEnabled = True
        Me.Txtlab8.Size = New System.Drawing.Size(101, 30)
        Me.Txtlab8.Style = MetroFramework.MetroColorStyle.Orange
        Me.Txtlab8.TabIndex = 182
        Me.Txtlab8.UseSelectable = True
        Me.Txtlab8.UseStyleColors = True
        Me.Txtlab8.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.Txtlab8.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel12.Location = New System.Drawing.Point(673, 41)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(125, 25)
        Me.MetroLabel12.TabIndex = 190
        Me.MetroLabel12.Text = "Pendimethalin"
        '
        'FindLab8
        '
        Me.FindLab8.ActiveControl = Nothing
        Me.FindLab8.AutoSize = True
        Me.FindLab8.BackColor = System.Drawing.Color.White
        Me.FindLab8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FindLab8.Location = New System.Drawing.Point(927, 41)
        Me.FindLab8.Name = "FindLab8"
        Me.FindLab8.Size = New System.Drawing.Size(34, 31)
        Me.FindLab8.Style = MetroFramework.MetroColorStyle.White
        Me.FindLab8.TabIndex = 191
        Me.FindLab8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.FindLab8.TileImage = Global.ReceivingSystem.My.Resources.Resources.Search32
        Me.FindLab8.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.FindLab8.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.FindLab8.UseSelectable = True
        Me.FindLab8.UseTileImage = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.FindLab8)
        Me.Panel1.Controls.Add(Me.MetroLabel12)
        Me.Panel1.Controls.Add(Me.Txtlab8)
        Me.Panel1.Controls.Add(Me.FindLab7)
        Me.Panel1.Controls.Add(Me.FindLab6)
        Me.Panel1.Controls.Add(Me.FindLab5)
        Me.Panel1.Controls.Add(Me.FindLab4)
        Me.Panel1.Controls.Add(Me.FindLab3)
        Me.Panel1.Controls.Add(Me.FindLab2)
        Me.Panel1.Controls.Add(Me.FindLab1)
        Me.Panel1.Controls.Add(Me.MetroLabel9)
        Me.Panel1.Controls.Add(Me.Txlab7)
        Me.Panel1.Controls.Add(Me.MetroLabel8)
        Me.Panel1.Controls.Add(Me.Txlab6)
        Me.Panel1.Controls.Add(Me.MetroLabel4)
        Me.Panel1.Controls.Add(Me.Txlab5)
        Me.Panel1.Controls.Add(Me.MetroLabel3)
        Me.Panel1.Controls.Add(Me.Txlab4)
        Me.Panel1.Controls.Add(Me.MetroLabel11)
        Me.Panel1.Controls.Add(Me.Txlab3)
        Me.Panel1.Controls.Add(Me.MetroLabel5)
        Me.Panel1.Controls.Add(Me.Txlab2)
        Me.Panel1.Controls.Add(Me.MetroLabel7)
        Me.Panel1.Controls.Add(Me.Txlab1)
        Me.Panel1.Location = New System.Drawing.Point(28, 139)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1176, 124)
        Me.Panel1.TabIndex = 202
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.DataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BatchNoDataGridViewTextBoxColumn, Me.Farmer, Me.CurerDataGridViewTextBoxColumn, Me.BaleFromDataGridViewTextBoxColumn, Me.BaleToDataGridViewTextBoxColumn, Me.LabNameDataGridViewTextBoxColumn, Me.ResultsDataGridViewTextBoxColumn, Me.Lot})
        Me.DataGridView1.DataSource = Me.SpQClabSelAllCPAFCVByCurerBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView1.EnableHeadersVisualStyles = False
        Me.DataGridView1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.DataGridView1.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.DataGridView1.Location = New System.Drawing.Point(31, 290)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(1068, 387)
        Me.DataGridView1.Style = MetroFramework.MetroColorStyle.Orange
        Me.DataGridView1.TabIndex = 203
        '
        'SpQClabSelAllCPAFCVByCurerBindingSource
        '
        Me.SpQClabSelAllCPAFCVByCurerBindingSource.DataMember = "sp_QClab_SelAll_CPAFCVByCurer"
        Me.SpQClabSelAllCPAFCVByCurerBindingSource.DataSource = Me.ReceivingDataSetBindingSource
        '
        'ReceivingDataSetBindingSource
        '
        Me.ReceivingDataSetBindingSource.DataSource = Me.ReceivingDataSet
        Me.ReceivingDataSetBindingSource.Position = 0
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'sp_QCLab_INS_CPAFCVResultsBindingSource
        '
        Me.sp_QCLab_INS_CPAFCVResultsBindingSource.DataMember = "sp_QCLab_INS_CPAFCVResults"
        Me.sp_QCLab_INS_CPAFCVResultsBindingSource.DataSource = Me.ReceivingDataSetBindingSource
        '
        'sp_QCLab_UPD_CPAFCVResultsBindingSource
        '
        Me.sp_QCLab_UPD_CPAFCVResultsBindingSource.DataMember = "sp_QCLab_UPD_CPAFCVResults"
        Me.sp_QCLab_UPD_CPAFCVResultsBindingSource.DataSource = Me.ReceivingDataSetBindingSource
        '
        'Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource
        '
        Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.DataMember = "sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo"
        Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter
        '
        Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.ClearBeforeFill = True
        '
        'TypeTableAdapter
        '
        Me.TypeTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_QCLab_SEL_CPASetupTableAdapter
        '
        Me.Sp_QCLab_SEL_CPASetupTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_MatByBatchNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatByBatchNoTableAdapter.ClearBeforeFill = True
        '
        'Sp_QClab_SEL_CPAFCVByCurerBindingSource
        '
        Me.Sp_QClab_SEL_CPAFCVByCurerBindingSource.DataMember = "sp_QClab_SEL_CPAFCVByCurer"
        Me.Sp_QClab_SEL_CPAFCVByCurerBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_QClab_SEL_CPAFCVByCurerTableAdapter
        '
        Me.Sp_QClab_SEL_CPAFCVByCurerTableAdapter.ClearBeforeFill = True
        '
        'Sp_QClab_GETMAX_BatchnoBindingSource
        '
        Me.Sp_QClab_GETMAX_BatchnoBindingSource.DataMember = "sp_QClab_GETMAX_Batchno"
        Me.Sp_QClab_GETMAX_BatchnoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_QClab_GETMAX_BatchnoTableAdapter
        '
        Me.Sp_QClab_GETMAX_BatchnoTableAdapter.ClearBeforeFill = True
        '
        'Sp_QClab_SelAll_CPAFCVByCurerTableAdapter
        '
        Me.Sp_QClab_SelAll_CPAFCVByCurerTableAdapter.ClearBeforeFill = True
        '
        'sp_QCLab_INS_CPAFCVResultsTableAdapter
        '
        Me.sp_QCLab_INS_CPAFCVResultsTableAdapter.ClearBeforeFill = True
        '
        'sp_QCLab_UPD_CPAFCVResultsTableAdapter
        '
        Me.sp_QCLab_UPD_CPAFCVResultsTableAdapter.ClearBeforeFill = True
        '
        'sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource
        '
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.DataMember = "sp_QClab_SEL_CPAFCVByLabCodeANDCurer"
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter
        '
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.ClearBeforeFill = True
        '
        'sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource
        '
        Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource.DataMember = "sp_QClab_SEL_CPAFCVByCurerANDFarmer"
        Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter
        '
        Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter.ClearBeforeFill = True
        '
        'sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource
        '
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.DataMember = "sp_QClab_SEL_CPAFCVByLabCodeANDFarmer"
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter
        '
        Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.ClearBeforeFill = True
        '
        'sp_QClab_GETMAX_LotnoBindingSource
        '
        Me.sp_QClab_GETMAX_LotnoBindingSource.DataMember = "sp_QClab_GETMAX_Lotno"
        Me.sp_QClab_GETMAX_LotnoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_QClab_GETMAX_LotnoTableAdapter
        '
        Me.sp_QClab_GETMAX_LotnoTableAdapter.ClearBeforeFill = True
        '
        'txtLot
        '
        '
        '
        '
        Me.txtLot.CustomButton.Image = Nothing
        Me.txtLot.CustomButton.Location = New System.Drawing.Point(73, 2)
        Me.txtLot.CustomButton.Name = ""
        Me.txtLot.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.txtLot.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtLot.CustomButton.TabIndex = 1
        Me.txtLot.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtLot.CustomButton.UseSelectable = True
        Me.txtLot.CustomButton.Visible = False
        Me.txtLot.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.txtLot.Lines = New String() {"รอบที่"}
        Me.txtLot.Location = New System.Drawing.Point(782, 91)
        Me.txtLot.MaxLength = 32767
        Me.txtLot.Name = "txtLot"
        Me.txtLot.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtLot.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtLot.SelectedText = ""
        Me.txtLot.SelectionLength = 0
        Me.txtLot.SelectionStart = 0
        Me.txtLot.ShortcutsEnabled = True
        Me.txtLot.Size = New System.Drawing.Size(101, 30)
        Me.txtLot.Style = MetroFramework.MetroColorStyle.Orange
        Me.txtLot.TabIndex = 204
        Me.txtLot.Text = "รอบที่"
        Me.txtLot.UseSelectable = True
        Me.txtLot.UseStyleColors = True
        Me.txtLot.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtLot.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'BatchNoDataGridViewTextBoxColumn
        '
        Me.BatchNoDataGridViewTextBoxColumn.DataPropertyName = "BatchNo"
        Me.BatchNoDataGridViewTextBoxColumn.HeaderText = "BatchNo"
        Me.BatchNoDataGridViewTextBoxColumn.Name = "BatchNoDataGridViewTextBoxColumn"
        Me.BatchNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Farmer
        '
        Me.Farmer.DataPropertyName = "Farmer"
        Me.Farmer.HeaderText = "Farmer"
        Me.Farmer.Name = "Farmer"
        Me.Farmer.ReadOnly = True
        '
        'CurerDataGridViewTextBoxColumn
        '
        Me.CurerDataGridViewTextBoxColumn.DataPropertyName = "Curer"
        Me.CurerDataGridViewTextBoxColumn.HeaderText = "Curer"
        Me.CurerDataGridViewTextBoxColumn.Name = "CurerDataGridViewTextBoxColumn"
        Me.CurerDataGridViewTextBoxColumn.ReadOnly = True
        '
        'BaleFromDataGridViewTextBoxColumn
        '
        Me.BaleFromDataGridViewTextBoxColumn.DataPropertyName = "BaleFrom"
        Me.BaleFromDataGridViewTextBoxColumn.HeaderText = "Bale From"
        Me.BaleFromDataGridViewTextBoxColumn.Name = "BaleFromDataGridViewTextBoxColumn"
        Me.BaleFromDataGridViewTextBoxColumn.ReadOnly = True
        Me.BaleFromDataGridViewTextBoxColumn.Width = 150
        '
        'BaleToDataGridViewTextBoxColumn
        '
        Me.BaleToDataGridViewTextBoxColumn.DataPropertyName = "BaleTo"
        Me.BaleToDataGridViewTextBoxColumn.HeaderText = "Bale To"
        Me.BaleToDataGridViewTextBoxColumn.Name = "BaleToDataGridViewTextBoxColumn"
        Me.BaleToDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LabNameDataGridViewTextBoxColumn
        '
        Me.LabNameDataGridViewTextBoxColumn.DataPropertyName = "LabName"
        Me.LabNameDataGridViewTextBoxColumn.HeaderText = "Lab Name"
        Me.LabNameDataGridViewTextBoxColumn.Name = "LabNameDataGridViewTextBoxColumn"
        Me.LabNameDataGridViewTextBoxColumn.ReadOnly = True
        Me.LabNameDataGridViewTextBoxColumn.Width = 250
        '
        'ResultsDataGridViewTextBoxColumn
        '
        Me.ResultsDataGridViewTextBoxColumn.DataPropertyName = "Results"
        Me.ResultsDataGridViewTextBoxColumn.HeaderText = "Results"
        Me.ResultsDataGridViewTextBoxColumn.Name = "ResultsDataGridViewTextBoxColumn"
        Me.ResultsDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Lot
        '
        Me.Lot.DataPropertyName = "Lot"
        Me.Lot.HeaderText = "Lot"
        Me.Lot.Name = "Lot"
        Me.Lot.ReadOnly = True
        '
        'FrmCPAFcv
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1280, 700)
        Me.Controls.Add(Me.txtLot)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.FarmerTxt)
        Me.Controls.Add(Me.MetroLabel17)
        Me.Controls.Add(Me.MetroLabel16)
        Me.Controls.Add(Me.MetroLabel15)
        Me.Controls.Add(Me.BaleToTxt)
        Me.Controls.Add(Me.BaleFromTxt)
        Me.Controls.Add(Me.Curertxt)
        Me.Controls.Add(Me.MetroLabel14)
        Me.Controls.Add(Me.CPABatchnoTextbox)
        Me.Controls.Add(Me.MetroLabel13)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmCPAFcv"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        CType(Me.Sp_QCLab_SEL_CPASetupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatByBatchNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpQClabSelAllCPAFCVByCurerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_QCLab_INS_CPAFCVResultsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_QCLab_UPD_CPAFCVResultsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_QClab_SEL_CPAFCVByCurerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_QClab_GETMAX_BatchnoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_QClab_GETMAX_LotnoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As MetroFramework.Controls.MetroGrid
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents ClearTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel24 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents ReportTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents CPABatchnoTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel

    Friend WithEvents ReceivingDataSet As ReceivingDataSet
    Friend WithEvents TypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TypeTableAdapter As ReceivingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_QCLab_SEL_CPASetupBindingSource As BindingSource
    Friend WithEvents Sp_QCLab_SEL_CPASetupTableAdapter As ReceivingDataSetTableAdapters.sp_QCLab_SEL_CPASetupTableAdapter

    Friend WithEvents Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource As BindingSource
    Friend WithEvents Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter

    Friend WithEvents Sp_Receiving_SEL_MatByBatchNoBindingSource As BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatByBatchNoTableAdapter As ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByBatchNoTableAdapter

    Friend WithEvents Sp_QClab_SEL_CPAFCVByCurerBindingSource As BindingSource
    Friend WithEvents Sp_QClab_SEL_CPAFCVByCurerTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByCurerTableAdapter
    Friend WithEvents Sp_QClab_GETMAX_BatchnoBindingSource As BindingSource
    Friend WithEvents Sp_QClab_GETMAX_BatchnoTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_GETMAX_BatchnoTableAdapter
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Curertxt As MetroFramework.Controls.MetroTextBox
    Friend WithEvents BaleFromTxt As MetroFramework.Controls.MetroTextBox
    Friend WithEvents BaleToTxt As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FarmerTxt As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel17 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab1 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab2 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab3 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab4 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab5 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab6 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Txlab7 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FindLab1 As MetroFramework.Controls.MetroTile
    Friend WithEvents FindLab2 As MetroFramework.Controls.MetroTile
    Friend WithEvents FindLab3 As MetroFramework.Controls.MetroTile
    Friend WithEvents FindLab4 As MetroFramework.Controls.MetroTile
    Friend WithEvents FindLab5 As MetroFramework.Controls.MetroTile
    Friend WithEvents FindLab6 As MetroFramework.Controls.MetroTile
    Friend WithEvents FindLab7 As MetroFramework.Controls.MetroTile
    Friend WithEvents Txtlab8 As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents FindLab8 As MetroFramework.Controls.MetroTile
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ReceivingDataSetBindingSource As BindingSource
    Friend WithEvents SpQClabSelAllCPAFCVByCurerBindingSource As BindingSource
    Friend WithEvents Sp_QClab_SelAll_CPAFCVByCurerTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_SelAll_CPAFCVByCurerTableAdapter
    Friend WithEvents sp_QCLab_INS_CPAFCVResultsBindingSource As BindingSource
    Friend WithEvents sp_QCLab_INS_CPAFCVResultsTableAdapter As ReceivingDataSetTableAdapters.sp_QCLab_INS_CPAFCVResultsTableAdapter
    Friend WithEvents sp_QCLab_UPD_CPAFCVResultsBindingSource As BindingSource
    Friend WithEvents sp_QCLab_UPD_CPAFCVResultsTableAdapter As ReceivingDataSetTableAdapters.sp_QCLab_UPD_CPAFCVResultsTableAdapter
    Friend WithEvents sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource As BindingSource
    Friend WithEvents sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter
    Friend WithEvents sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource As BindingSource
    Friend WithEvents sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter
    Friend WithEvents sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource As BindingSource
    Friend WithEvents sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter
    Friend WithEvents sp_QClab_GETMAX_LotnoBindingSource As BindingSource
    Friend WithEvents sp_QClab_GETMAX_LotnoTableAdapter As ReceivingDataSetTableAdapters.sp_QClab_GETMAX_LotnoTableAdapter
    Friend WithEvents txtLot As MetroFramework.Controls.MetroTextBox
    Friend WithEvents BatchNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Farmer As DataGridViewTextBoxColumn
    Friend WithEvents CurerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BaleFromDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BaleToDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents LabNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ResultsDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Lot As DataGridViewTextBoxColumn
End Class
