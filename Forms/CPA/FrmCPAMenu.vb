﻿Public Class FrmCPAMenu
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext

    Private Sub FrmCPAMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Try
            FrmLogIn.Show()
            Me.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


    Private Sub BtnBUCPA_Click(sender As Object, e As EventArgs) Handles BtnBUCPA.Click
        Dim window As New FrmCPAResult()
        window.ShowDialog()
    End Sub

    Private Sub BtnFCVCPA_Click(sender As Object, e As EventArgs) Handles BtnFCVCPA.Click
        Dim window As New FrmCPAFcv()
        window.ShowDialog()
    End Sub

End Class