﻿Public Class FrmCPAFcv
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim batchNo As String
    Dim FarmerC As String
    Dim MaxLotNo As Int32
    Private Sub FrmCPAFcv_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 700)
            UsernameMetroLabel.Text = _username
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub CPABatchnoTextbox_KeyDown(sender As Object, e As KeyEventArgs) Handles CPABatchnoTextbox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then

                ClearAllText()

                If CPABatchnoTextbox.Text = "" Then
                    MessageBox.Show("กรุณาคีย์หรือสแกนBatch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                ReceivingDataSet.sp_Receiving_SEL_MatByBatchNo.Clear()
                ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Clear()

                Sp_Receiving_SEL_MatByBatchNoTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatByBatchNo, CPABatchnoTextbox.Text)


                If Sp_Receiving_SEL_MatByBatchNoBindingSource.Count <= 0 Then
                    MessageBox.Show("ไม่มีข้อมูลBarcodeของBatchนี้ หรือแผนกReceiving ยังไม่ได้ทำการFinished Batch นี้ กรุณาตรวจสอบข้อมูลกับแผนก Receivingอีกครั้ง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    CPABatchnoTextbox.Text = ""
                    CPABatchnoTextbox.Focus()
                    Return
                End If


                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 1, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab1.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If

                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 2, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab2.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If

                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 3, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab3.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If

                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 4, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab4.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If

                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 5, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab5.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If

                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 6, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab6.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If

                Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo, 7, CPABatchnoTextbox.Text)
                If Sp_QClab_SEL_CPAResultsByLabCodeANDBatchNoBindingSource.Count > 0 Then
                    Txlab7.Text = ReceivingDataSet.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo.Rows(0).Item("Results").ToString()
                End If


                Txlab1.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearTile_Click(sender As Object, e As EventArgs) Handles ClearTile.Click
        CPABatchnoTextbox.Text = ""
        Curertxt.Text = ""
        FarmerTxt.Text = ""
        BaleToTxt.Text = ""
        BaleFromTxt.Text = ""

        ClearAllText()

        ReceivingDataSet.sp_QClab_GETMAX_Batchno.Clear()
        ReceivingDataSet.sp_QClab_SelAll_CPAFCVByCurer.Clear()
        ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Clear()

        ReceivingDataSet.sp_Receiving_SEL_MatByBatchNo.Clear()
        Curertxt.Focus()

    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            If (CPABatchnoTextbox.Text = "") Then
                MessageBox.Show("กรุณาระบุข้อมูลที่มีอยู่ในระบบค่ะ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'Delete by BatchNo
            db.sp_QCLab_DEL_CPAFCVResults(_defaultCrop, CPABatchnoTextbox.Text)
            MessageBox.Show("ลบข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            Dim count As Integer = 0
            If Curertxt.Text = "" Then
                MessageBox.Show("กรุณาระบุ Curer's Code/ Farmer Code", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If BaleFromTxt.Text = "" Then
                MessageBox.Show("กรุณาระบุ Bale เริ่มต้น", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If BaleToTxt.Text = "" Then
                MessageBox.Show("กรุณาระบุ Bale สุดท้าย", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If txtLot.Text = "" Or txtLot.Text = "รอบที่" Then
                MessageBox.Show("กรุณาระบุ รอบของการบันทึก", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'NPI Case
            If FarmerTxt.Text <> "" Then
                'Check existed by Farmer and get Max
                FarmerC = FarmerTxt.Text
                sp_QClab_SEL_CPAFCVByCurerANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurerANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByCurerANDFarmerBindingSource.Count = 0 Then
                    AddNewCPA()
                Else
                    UpdateExistedCPAFarmer()
                End If
            Else
                ' Check existed by Curer Code
                FarmerC = ""
                Sp_QClab_SEL_CPAFCVByCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text)
                If Sp_QClab_SEL_CPAFCVByCurerBindingSource.Count = 0 Then
                    '----------------------------- Add New CPA -------------------------------
                    AddNewCPA()
                Else
                    '----------------------------- Update existed CPA -------------------------------
                    UpdateExistedCPACurrer()
                End If
            End If



            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub GenerateBatchno()
        Sp_QClab_GETMAX_BatchnoTableAdapter.Fill(ReceivingDataSet.sp_QClab_GETMAX_Batchno, _defaultCrop, Curertxt.Text, _username, FarmerTxt.Text)
        batchNo = ReceivingDataSet.sp_QClab_GETMAX_Batchno.Rows(0).Item("BatchNo").ToString

    End Sub
    Private Sub GetMaxLotNo()
        sp_QClab_GETMAX_LotnoTableAdapter.Fill(ReceivingDataSet.sp_QClab_GETMAX_Lotno, Curertxt.Text, FarmerTxt.Text, BaleToTxt.Text, BaleFromTxt.Text, _defaultCrop)
        'MaxLotNo = ReceivingDataSet.sp_QClab_GETMAX_Lotno.Rows(0).Item("MaxLot").ToString
        MaxLotNo = txtLot.Text
    End Sub

    Private Sub ClearAllText()
        Try
            Txlab1.Text = ""
            Txlab2.Text = ""
            Txlab3.Text = ""
            Txlab4.Text = ""
            Txlab5.Text = ""
            Txlab6.Text = ""
            Txlab7.Text = ""
            Txtlab8.Text = ""
            CPABatchnoTextbox.Text = ""
            txtLot.Text = ""

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BaleToTxt_KeyDown(sender As Object, e As KeyEventArgs) Handles BaleToTxt.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then


                If Curertxt.Text = "" Then
                    MessageBox.Show("กรุณาคีย์ Curer Code / Farmer Code ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If BaleFromTxt.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Bale เริ่มต้น", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                If BaleToTxt.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Bale สุดท้าย", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If

                'Check existed data from CPAResults table by bc
                Sp_QClab_SEL_CPAFCVByCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text)
                Dim count As Integer = 0
                If Sp_QClab_SEL_CPAFCVByCurerBindingSource.Count > 0 Then
                    'Show all Bale To and Bale from in Table
                    ClearAllText()

                    CPABatchnoTextbox.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(0).Item("BatchNo").ToString()

                    txtLot.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(0).Item("Lot").ToString()
                    'for farmerCode
                    'If String.IsNullOrEmpty(ReceivingDataSet.sp_QClab_SelAll_CPAFCVByCurer.Rows(0).Item("Farmer").ToString()) Then
                    '    'FarmerTxt.Text = ""
                    'Else
                    '    FarmerTxt.Text = ReceivingDataSet.sp_QClab_SelAll_CPAFCVByCurer.Rows(0).Item("Farmer").ToString()
                    'End If

                    Do While (count < Sp_QClab_SEL_CPAFCVByCurerBindingSource.Count)
                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "1" Then
                            Txlab1.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "2" Then
                            Txlab2.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "3" Then
                            Txlab3.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "4" Then
                            Txlab4.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "5" Then
                            Txlab5.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "6" Then
                            Txlab6.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "7" Then
                            Txlab7.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        If ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("LabCode").ToString() = "8" Then
                            Txtlab8.Text = ReceivingDataSet.sp_QClab_SEL_CPAFCVByCurer.Rows(count).Item("Results").ToString()
                        End If

                        count = count + 1
                    Loop
                Else
                    ClearAllText()
                End If

                Txlab1.Focus()

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub FarmerTxt_KeyPress(sender As Object, e As KeyPressEventArgs) Handles FarmerTxt.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub
    Private Sub FarmerTxt_KeyDown(sender As Object, e As KeyEventArgs) Handles FarmerTxt.KeyDown
        If e.KeyCode <> Keys.Enter Then
            Return
        End If
        Curertxt.Focus()
        AutoReplaceSupplierByFarmerID(FarmerTxt.Text)
    End Sub
    Private Sub AutoReplaceSupplierByFarmerID(farmerID As String)
        If farmerID.Contains("MS") Then
            Curertxt.Text = "NPIMS"
        ElseIf farmerID.Contains("PK") Then
            Curertxt.Text = "NPIPK"
        ElseIf farmerID.Contains("MJ") Then
            Curertxt.Text = "NPIMJ"
        ElseIf farmerID.Contains("PR") Then
            Curertxt.Text = "NPIPR"
        End If
    End Sub

    Private Sub Curertxt_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Curertxt.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.KeyChar = Char.ToUpper(e.KeyChar)
        End If
    End Sub

    Private Sub AddNewCPA()
        Try
            '----------------------------- Add New CPA -------------------------------
            'Generate Batch No (using year,Curer's Code and runing no)
            '1. Generate at CPABatch
            GenerateBatchno()

            GetMaxLotNo()

            '2. Insert New CPA data
            If Txlab1.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(1, batchNo, Val(Txlab1.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(1, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txlab2.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(2, batchNo, Val(Txlab2.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(2, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txlab3.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(3, batchNo, Val(Txlab3.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(3, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txlab4.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(4, batchNo, Val(Txlab4.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(4, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txlab5.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(5, batchNo, Val(Txlab5.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(5, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txlab6.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(6, batchNo, Val(Txlab6.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(6, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txlab7.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(7, batchNo, Val(Txlab7.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(7, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
            If Txtlab8.Text <> "" Then
                db.sp_QCLab_INS_CPAFCVResults(8, batchNo, Val(Txtlab8.Text), _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            Else
                db.sp_QCLab_INS_CPAFCVResults(8, batchNo, 0, _username, Curertxt.Text, FarmerC, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub UpdateExistedCPACurrer()
        Try
            '----------------------------- Update existed CPA -------------------------------
            If Txlab1.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 1)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(1, CPABatchnoTextbox.Text, Val(Txlab1.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(1, CPABatchnoTextbox.Text, Val(Txlab1.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If

            If Txlab2.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 2)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(2, CPABatchnoTextbox.Text, Val(Txlab2.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(2, CPABatchnoTextbox.Text, Val(Txlab2.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab3.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 3)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(3, CPABatchnoTextbox.Text, Val(Txlab3.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(3, CPABatchnoTextbox.Text, Val(Txlab3.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If

            If Txlab4.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 4)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(4, CPABatchnoTextbox.Text, Val(Txlab4.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(4, CPABatchnoTextbox.Text, Val(Txlab4.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab5.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 5)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(5, CPABatchnoTextbox.Text, Val(Txlab5.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(5, CPABatchnoTextbox.Text, Val(Txlab5.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab6.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 6)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(6, CPABatchnoTextbox.Text, Val(Txlab6.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(6, CPABatchnoTextbox.Text, Val(Txlab6.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab7.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 7)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(7, CPABatchnoTextbox.Text, Val(Txlab7.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(7, CPABatchnoTextbox.Text, Val(Txlab7.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txtlab8.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDCurerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDCurer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 8)
                If sp_QClab_SEL_CPAFCVByLabCodeANDCurerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(8, CPABatchnoTextbox.Text, Val(Txtlab8.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(8, CPABatchnoTextbox.Text, Val(Txtlab8.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Private Sub UpdateExistedCPAFarmer()
        Try
            '----------------------------- Update existed CPA -------------------------------
            If Txlab1.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 1, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(1, CPABatchnoTextbox.Text, Val(Txlab1.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(1, CPABatchnoTextbox.Text, Val(Txlab1.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If

            If Txlab2.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 2, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(2, CPABatchnoTextbox.Text, Val(Txlab2.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(2, CPABatchnoTextbox.Text, Val(Txlab2.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab3.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 3, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(3, CPABatchnoTextbox.Text, Val(Txlab3.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(3, CPABatchnoTextbox.Text, Val(Txlab3.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If

            If Txlab4.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 4, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(4, CPABatchnoTextbox.Text, Val(Txlab4.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(4, CPABatchnoTextbox.Text, Val(Txlab4.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab5.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 5, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(5, CPABatchnoTextbox.Text, Val(Txlab5.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(5, CPABatchnoTextbox.Text, Val(Txlab5.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab6.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 6, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(6, CPABatchnoTextbox.Text, Val(Txlab6.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(6, CPABatchnoTextbox.Text, Val(Txlab6.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txlab7.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 7, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(7, CPABatchnoTextbox.Text, Val(Txlab7.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(7, CPABatchnoTextbox.Text, Val(Txlab7.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
            If Txtlab8.Text <> "" Then
                sp_QClab_SEL_CPAFCVByLabCodeANDFarmerTableAdapter.Fill(ReceivingDataSet.sp_QClab_SEL_CPAFCVByLabCodeANDFarmer, Curertxt.Text, BaleToTxt.Text, BaleFromTxt.Text, 8, FarmerTxt.Text)
                If sp_QClab_SEL_CPAFCVByLabCodeANDFarmerBindingSource.Count <= 0 Then
                    db.sp_QCLab_INS_CPAFCVResults(8, CPABatchnoTextbox.Text, Val(Txtlab8.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop, MaxLotNo)
                Else
                    db.sp_QCLab_UPD_CPAFCVResults(8, CPABatchnoTextbox.Text, Val(Txtlab8.Text), _username, Curertxt.Text, FarmerTxt.Text, Convert.ToInt32(BaleToTxt.Text), Convert.ToInt32(BaleFromTxt.Text), _defaultCrop)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Curertxt_KeyDown(sender As Object, e As KeyEventArgs) Handles Curertxt.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.TypeTableAdapter.Fill(Me.ReceivingDataSet.type)
            'If FarmerTxt.Text = "" Then
            Me.Sp_QClab_SelAll_CPAFCVByCurerTableAdapter.Fill(Me.ReceivingDataSet.sp_QClab_SelAll_CPAFCVByCurer, Curertxt.Text, _defaultCrop, FarmerTxt.Text)
            'Else
            'Me.sp_QClab_SelAll_CPAFCVByFarmerTableAdapter.Fill(Me.ReceivingDataSet.sp_QClab_SelAll_CPAFCVByFarmer, Curertxt.Text, _defaultCrop, FarmerTxt.Text)
            'End If
        End If
    End Sub
End Class