﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCPAMenu
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.BtnFCVCPA = New System.Windows.Forms.Button()
        Me.BtnBUCPA = New System.Windows.Forms.Button()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_DecodePasswordBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_DecodePasswordTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.BackMetroTile.Location = New System.Drawing.Point(1148, 28)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 127
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        Me.BackMetroTile.UseWaitCursor = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.MetroTile1.Location = New System.Drawing.Point(54, 42)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 126
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        Me.MetroTile1.UseWaitCursor = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(117, 51)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 125
        Me.UsernameMetroLabel.Text = "Username"
        Me.UsernameMetroLabel.UseWaitCursor = True
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.BtnFCVCPA)
        Me.MetroPanel1.Controls.Add(Me.BtnBUCPA)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(54, 237)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1230, 275)
        Me.MetroPanel1.TabIndex = 128
        Me.MetroPanel1.UseWaitCursor = True
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'BtnFCVCPA
        '
        Me.BtnFCVCPA.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.BtnFCVCPA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnFCVCPA.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.BtnFCVCPA.FlatAppearance.BorderSize = 0
        Me.BtnFCVCPA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnFCVCPA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnFCVCPA.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnFCVCPA.ForeColor = System.Drawing.Color.White
        Me.BtnFCVCPA.Image = Global.ReceivingSystem.My.Resources.Resources.Purchase1
        Me.BtnFCVCPA.Location = New System.Drawing.Point(341, 25)
        Me.BtnFCVCPA.Name = "BtnFCVCPA"
        Me.BtnFCVCPA.Size = New System.Drawing.Size(182, 206)
        Me.BtnFCVCPA.TabIndex = 8
        Me.BtnFCVCPA.Text = "FCV"
        Me.BtnFCVCPA.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnFCVCPA.UseVisualStyleBackColor = False
        Me.BtnFCVCPA.UseWaitCursor = True
        '
        'BtnBUCPA
        '
        Me.BtnBUCPA.BackColor = System.Drawing.Color.Goldenrod
        Me.BtnBUCPA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnBUCPA.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.BtnBUCPA.FlatAppearance.BorderSize = 0
        Me.BtnBUCPA.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnBUCPA.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBUCPA.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBUCPA.ForeColor = System.Drawing.Color.White
        Me.BtnBUCPA.Image = Global.ReceivingSystem.My.Resources.Resources.Natural_Food641
        Me.BtnBUCPA.Location = New System.Drawing.Point(529, 25)
        Me.BtnBUCPA.Name = "BtnBUCPA"
        Me.BtnBUCPA.Size = New System.Drawing.Size(182, 206)
        Me.BtnBUCPA.TabIndex = 2
        Me.BtnBUCPA.Text = "BU"
        Me.BtnBUCPA.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnBUCPA.UseVisualStyleBackColor = False
        Me.BtnBUCPA.UseWaitCursor = True
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ReceivingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_DecodePasswordBindingSource
        '
        Me.Sp_Receiving_DecodePasswordBindingSource.DataMember = "sp_Receiving_DecodePassword"
        Me.Sp_Receiving_DecodePasswordBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_DecodePasswordTableAdapter
        '
        Me.Sp_Receiving_DecodePasswordTableAdapter.ClearBeforeFill = True
        '
        'FrmCPAMenu
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1331, 737)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.MetroTile1)
        Me.Controls.Add(Me.UsernameMetroLabel)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Name = "FrmCPAMenu"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.UseWaitCursor = True
        Me.MetroPanel1.ResumeLayout(False)
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents BtnFCVCPA As Button
    Friend WithEvents BtnBUCPA As Button
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_DecodePasswordBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_DecodePasswordTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter
End Class
