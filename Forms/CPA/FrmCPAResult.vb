﻿Imports FastMember

Public Class FrmCPAResult
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext

    Dim _list1 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list2 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list3 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list4 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list5 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list6 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list7 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Dim _list8 As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)

    Private Sub DataGridBinding(list As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result))
        Dim resultList As New List(Of m_CPAResult)
        Dim rowNumber = 1
        For Each item In list
            resultList.Add(New m_CPAResult() With {
                           .bc = item.bc,
                           .crop = item.crop,
                           .type = item.type,
                           .truckno = item.truckno,
                           .TransportationDocumentCode = item.TransportationDocumentCode,
                           .dtrecord = item.dtrecord,
                           .BatchNo = item.BatchNo,
                           .LabName = item.LabName,
                           .CPAResult = item.CPAResult,
                           .CPAResultDate = item.CPAResultDate,
                           .CPAResultUser = item.CPAResultUser,
                           .Results = item.Results,
                           .ResultDate = item.ResultDate,
                           .ByUsers = item.ByUsers,
                           .rcno = item.rcno,
                           .Finished = item.Finished,
                           .RCLines = item.RCLines,
                           .supplier = item.supplier,
                           .RowNumber = rowNumber
            })
            rowNumber = rowNumber + 1
        Next

        Dim dt As New DataTable()
        Dim reader = ObjectReader.Create(resultList)
        dt.Load(reader)

        MatRCMetroGrid.AutoGenerateColumns = False
        MatRCMetroGrid.DataSource = dt

        TotalBaleLabel.Text = dt.Rows.Count()
    End Sub

    Private Sub ReloadCPALabResultList()
        Try
            If CPABatchNoTextbox.Text = Nothing Then
                Return
            End If

            _list1 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(1, CPABatchNoTextbox.Text)
            _list2 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(2, CPABatchNoTextbox.Text)
            _list3 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(3, CPABatchNoTextbox.Text)
            _list4 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(4, CPABatchNoTextbox.Text)
            _list5 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(5, CPABatchNoTextbox.Text)
            _list6 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(6, CPABatchNoTextbox.Text)
            _list7 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(7, CPABatchNoTextbox.Text)
            _list8 = _businessLayerService.QCLabBL().GetCPAResultByLabCode(8, CPABatchNoTextbox.Text)

            Txlab1.Text = If(_list1.Count > 0, _list1.FirstOrDefault().Results, "")
            Txlab2.Text = If(_list2.Count > 0, _list2.FirstOrDefault().Results, "")
            Txlab3.Text = If(_list3.Count > 0, _list3.FirstOrDefault().Results, "")
            Txlab4.Text = If(_list4.Count > 0, _list4.FirstOrDefault().Results, "")
            Txlab5.Text = If(_list5.Count > 0, _list5.FirstOrDefault().Results, "")
            Txlab6.Text = If(_list6.Count > 0, _list6.FirstOrDefault().Results, "")
            Txlab7.Text = If(_list7.Count > 0, _list7.FirstOrDefault().Results, "")
            Txlab8.Text = If(_list8.Count > 0, _list8.FirstOrDefault().Results, "")

            DataGridBinding(_list1)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmCPAResult_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            CPABatchNoTextbox.Focus()
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub CPABatchnoTextbox_KeyDown(sender As Object, e As KeyEventArgs) Handles CPABatchNoTextbox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                If CPABatchNoTextbox.Text = Nothing Then
                    MessageBox.Show("กรุณาคีย์หรือสแกน Batch No ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    CPABatchNoTextbox.Focus()
                    Return
                End If

                ReloadCPALabResultList()
                DataGridBinding(_list1)
            End If
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub CPAResultTextbox_KeyPress(sender As Object, e As KeyPressEventArgs)
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab1.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab2_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab2.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab3_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab3.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab4_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab4.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab5.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab6_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab6.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab7_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab7.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab8_KeyPress(sender As Object, e As KeyPressEventArgs) Handles Txlab8.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub View1Button_Click(sender As Object, e As EventArgs) Handles View1Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(1, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View2Button_Click(sender As Object, e As EventArgs) Handles View2Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(2, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View3Button_Click(sender As Object, e As EventArgs) Handles View3Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(3, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View4Button_Click(sender As Object, e As EventArgs) Handles View4Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(4, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View5Button_Click(sender As Object, e As EventArgs) Handles View5Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(5, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View6Button_Click(sender As Object, e As EventArgs) Handles View6Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(6, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View7Button_Click(sender As Object, e As EventArgs) Handles View7Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(7, CPABatchNoTextbox.Text))
    End Sub

    Private Sub View8Button_Click(sender As Object, e As EventArgs) Handles View8Button.Click
        If (CPABatchNoTextbox.Text = "") Then
            MessageBox.Show("กรุณาคีย์หรือสแกน Batch No.ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If
        DataGridBinding(_businessLayerService.QCLabBL().GetCPAResultByLabCode(8, CPABatchNoTextbox.Text))
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        Try
            Txlab1.Clear()
            Txlab2.Clear()
            Txlab3.Clear()
            Txlab4.Clear()
            Txlab5.Clear()
            Txlab6.Clear()
            Txlab7.Clear()
            Txlab8.Clear()

            _list1 = Nothing
            _list2 = Nothing
            _list3 = Nothing
            _list4 = Nothing
            _list5 = Nothing
            _list6 = Nothing
            _list7 = Nothing
            _list8 = Nothing

            MatRCMetroGrid.DataSource = Nothing

            CPABatchNoTextbox.Clear()
            CPABatchNoTextbox.Focus()
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If CPABatchNoTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์หรือสแกน Batch No ที่ต้องการ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                CPABatchNoTextbox.Focus()
                Return
            End If

            If _businessLayerService.ReceivingBL.GetMatByBatchNo(CPABatchNoTextbox.Text).Count <= 0 Then
                MessageBox.Show("ไม่มีข้อมูล Barcode ใน batch no นี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If Txlab1.Text <> Nothing Then
                If _list1.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(1, CPABatchNoTextbox.Text, Val(Txlab1.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(1, CPABatchNoTextbox.Text, Val(Txlab1.Text), _username, True)
                End If
            End If

            If Txlab2.Text <> Nothing Then
                If _list2.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(2, CPABatchNoTextbox.Text, Val(Txlab2.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(2, CPABatchNoTextbox.Text, Val(Txlab2.Text), _username, True)
                End If
            End If


            If Txlab3.Text <> Nothing Then
                If _list3.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(3, CPABatchNoTextbox.Text, Val(Txlab3.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(3, CPABatchNoTextbox.Text, Val(Txlab3.Text), _username, True)
                End If
            End If

            If Txlab4.Text <> Nothing Then
                If _list4.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(4, CPABatchNoTextbox.Text, Val(Txlab4.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(4, CPABatchNoTextbox.Text, Val(Txlab4.Text), _username, True)
                End If
            End If

            If Txlab5.Text <> Nothing Then
                If _list5.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(5, CPABatchNoTextbox.Text, Val(Txlab5.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(5, CPABatchNoTextbox.Text, Val(Txlab5.Text), _username, True)
                End If
            End If

            If Txlab6.Text <> Nothing Then
                If _list6.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(6, CPABatchNoTextbox.Text, Val(Txlab6.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(6, CPABatchNoTextbox.Text, Val(Txlab6.Text), _username, True)
                End If
            End If

            If Txlab7.Text <> Nothing Then
                If _list7.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(7, CPABatchNoTextbox.Text, Val(Txlab7.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(7, CPABatchNoTextbox.Text, Val(Txlab7.Text), _username, True)
                End If
            End If

            If Txlab8.Text <> Nothing Then
                If _list8.FirstOrDefault().Results Is Nothing Then
                    db.sp_QCLab_INS_CPAResults(8, CPABatchNoTextbox.Text, Val(Txlab8.Text), _username, True)
                Else
                    db.sp_QCLab_UPD_CPAResults(8, CPABatchNoTextbox.Text, Val(Txlab8.Text), _username, True)
                End If
            End If

            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

            ReloadCPALabResultList()
            CPABatchNoTextbox.Focus()
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message, "warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Txlab1_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab1.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab2.Focus()
        End If
    End Sub

    Private Sub Txlab2_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab2.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab3.Focus()
        End If
    End Sub

    Private Sub Txlab3_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab3.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab4.Focus()
        End If
    End Sub

    Private Sub Txlab4_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab4.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab5.Focus()
        End If
    End Sub

    Private Sub Txlab5_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab5.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab6.Focus()
        End If
    End Sub

    Private Sub Txlab6_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab6.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab7.Focus()
        End If
    End Sub

    Private Sub Txlab7_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab7.KeyUp
        If e.KeyCode = Keys.Enter Then
            Txlab8.Focus()
        End If
    End Sub

    Private Sub Txlab8_KeyUp(sender As Object, e As KeyEventArgs) Handles Txlab8.KeyUp
        If e.KeyCode = Keys.Enter Then
            SaveButton.Focus()
        End If
    End Sub
End Class