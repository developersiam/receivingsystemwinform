﻿Imports FastMember

Public Class FrmRcnoDetail
    Inherits MetroFramework.Forms.MetroForm
    Public _matrc As matrc
    Dim _matSubTypeCompany As sp_Receiving_SEL_MatSubtypeCompany_Result
    Dim _company As sp_Receiving_SEL_Company_Result
    Dim _matList As List(Of sp_Receiving_SEL_MatByRcNo_Result)
    Dim _buyerList As List(Of sp_Receiving_SEL_BuyerClassifier_Result)
    Dim _classifierList As List(Of sp_Receiving_SEL_BuyerClassifier_Result)

    Sub RefreshReceivingDocument()
        Try
            UsernameLabel.Text = _username
            ReceivingDataGrid.AutoGenerateColumns = False

            If _matrc.InvoiceNo <> "" Then
                'กรณียา LTL ให้แสดงข้อมูลจำนวนห่อยาทั้งหมดใน LTL Invoice
                TotalBaleOnTruckLabel.Text = _businessLayerService.ReceivingDocumentBL().GetBalesByInvoiceNo(_matrc.InvoiceNo).Count()
            ElseIf _matrc.TransportationDocumentCode <> "" Then
                'กรณี STEC BU ให้แสดงจำนวนห่อยาทั้งหมดในการขนส่งครั้งนั้น (TransportationDocument)
                TotalBaleOnTruckLabel.Text = _businessLayerService.BurleyBuyingSystemBL().GetTransportationDocumentByID(_matrc.TransportationDocumentCode).TotalBale
            Else
                TransportationDocumentCodeLabel.Text = ""
            End If

            _matSubTypeCompany = _businessLayerService.ReceivingDocumentBL().GetMatSupTypeCompanyByRcno(_matrc.rcno)
            _company = _businessLayerService.CompanyBL().GetCompanies().SingleOrDefault(Function(x) x.name = _matSubTypeCompany.Company)

            rcnoLabel.Text = _matrc.rcno
            CropLabel.Text = _matrc.crop
            TypeLabel.Text = _matrc.type
            CompanyLabel.Text = _company.name
            RcfromLabel.Text = _matrc.rcfrom
            PlaceLabel.Text = _matrc.place
            ClassifierComboBox.Text = _matrc.classifier
            BuyerComboBox.Text = _matrc.buyer
            TransportationDocumentCodeLabel.Text = _matrc.TransportationDocumentCode
            InvoiceNoLabel.Text = _matrc.InvoiceNo
            TrucknoLabel.Text = _matrc.truckno

            _matList = _businessLayerService.ReceivingBL().GetMatByRcno(_matrc.rcno)
            TotalBaleLabel.Text = _matList.Count

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_matList)
            dt.Load(reader)

            ReceivingDataGrid.DataSource = dt

            Dim totalBuyingWeight As Decimal = _matList.Sum(Function(x) x.weightbuy)
            Dim totalReceivingWeight As Decimal = _matList.Sum(Function(x) x.weight)
            TotalBuyingWeightLabel.Text = totalBuyingWeight.ToString("N2")
            TotalReceivedWeightLabel.Text = totalReceivingWeight.ToString("N2")
            DiffWeightLabel.Text = (totalReceivingWeight - totalBuyingWeight).ToString("N2")

            If _matrc.type = "BU" And _company.name = "STEC" Then
                RemainingTruckReceivingLabel.Text = _businessLayerService.ReceivingBL() _
                    .GetRemainingBurleyReceivingBaleFromTransportationCode(_matrc.TransportationDocumentCode, _matrc.crop, _matrc.rcno).Count.ToString("N2")
            ElseIf _company.name = "LTL" And _matrc.InvoiceNo <> "" Then
                RemainingTruckReceivingLabel.Text = _businessLayerService.ReceivingBL().GetRemainingLTLReceivingBaleFromInvoiceNo(_matrc.InvoiceNo).ToString("N2")
            Else
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub

    Private Sub FrmRcnoDetail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RefreshReceivingDocument()
    End Sub

    Private Sub RefreshButton_Click(sender As Object, e As EventArgs) Handles RefreshButton.Click
        RefreshReceivingDocument()
    End Sub
End Class