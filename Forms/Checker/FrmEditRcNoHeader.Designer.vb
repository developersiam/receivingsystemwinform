﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEditRcNoHeader
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.SaveTile = New MetroFramework.Controls.MetroTile()
        Me.SaveMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.SubtypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Receiving_SEL_SubtypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.CompanyComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Receiving_SEL_CompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.RcFromComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.ClassifierComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Receiving_SEL_ClassifierBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_BuyerClassifierBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BuyerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.PlaceComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Receiving_SEL_MatWHBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TruckNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.RcNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Receiving_SEL_MatRCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatRCTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.TypeTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.typeTableAdapter()
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter()
        Me.Sp_Receiving_SEL_CompanyTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_CompanyTableAdapter()
        Me.Sp_Receiving_SEL_BuyerClassifierTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_BuyerClassifierTableAdapter()
        Me.Sp_Receiving_SEL_MatWHTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatWHTableAdapter()
        Me.Sp_Receiving_SEL_SubtypeTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_SubtypeTableAdapter()
        Me.Sp_Receiving_SEL_ClassifierTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ClassifierTableAdapter()
        Me.InvoiceNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.TransportDocNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.Sp_Receiving_SEL_SubtypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_CompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_ClassifierBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_BuyerClassifierBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatWHBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatRCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.SaveTile)
        Me.MetroPanel1.Controls.Add(Me.SaveMetroLabel)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 33)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1176, 54)
        Me.MetroPanel1.TabIndex = 119
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'SaveTile
        '
        Me.SaveTile.ActiveControl = Nothing
        Me.SaveTile.AutoSize = True
        Me.SaveTile.BackColor = System.Drawing.Color.White
        Me.SaveTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveTile.Location = New System.Drawing.Point(862, 10)
        Me.SaveTile.Name = "SaveTile"
        Me.SaveTile.Size = New System.Drawing.Size(38, 35)
        Me.SaveTile.Style = MetroFramework.MetroColorStyle.White
        Me.SaveTile.TabIndex = 135
        Me.SaveTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Save32
        Me.SaveTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SaveTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SaveTile.UseSelectable = True
        Me.SaveTile.UseTileImage = True
        '
        'SaveMetroLabel
        '
        Me.SaveMetroLabel.AutoSize = True
        Me.SaveMetroLabel.Location = New System.Drawing.Point(906, 20)
        Me.SaveMetroLabel.Name = "SaveMetroLabel"
        Me.SaveMetroLabel.Size = New System.Drawing.Size(37, 20)
        Me.SaveMetroLabel.TabIndex = 134
        Me.SaveMetroLabel.Text = "save"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1116, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'SubtypeComboBox
        '
        Me.SubtypeComboBox.DataSource = Me.Sp_Receiving_SEL_SubtypeBindingSource
        Me.SubtypeComboBox.DisplayMember = "subtype"
        Me.SubtypeComboBox.FormattingEnabled = True
        Me.SubtypeComboBox.ItemHeight = 24
        Me.SubtypeComboBox.Location = New System.Drawing.Point(491, 259)
        Me.SubtypeComboBox.Name = "SubtypeComboBox"
        Me.SubtypeComboBox.Size = New System.Drawing.Size(243, 30)
        Me.SubtypeComboBox.TabIndex = 147
        Me.SubtypeComboBox.UseSelectable = True
        Me.SubtypeComboBox.ValueMember = "subtype"
        '
        'Sp_Receiving_SEL_SubtypeBindingSource
        '
        Me.Sp_Receiving_SEL_SubtypeBindingSource.DataMember = "sp_Receiving_SEL_Subtype"
        Me.Sp_Receiving_SEL_SubtypeBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(426, 269)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(59, 20)
        Me.MetroLabel8.TabIndex = 146
        Me.MetroLabel8.Text = "Subtype"
        '
        'CompanyComboBox
        '
        Me.CompanyComboBox.DataSource = Me.Sp_Receiving_SEL_CompanyBindingSource
        Me.CompanyComboBox.DisplayMember = "name"
        Me.CompanyComboBox.FormattingEnabled = True
        Me.CompanyComboBox.ItemHeight = 24
        Me.CompanyComboBox.Location = New System.Drawing.Point(491, 223)
        Me.CompanyComboBox.Name = "CompanyComboBox"
        Me.CompanyComboBox.Size = New System.Drawing.Size(243, 30)
        Me.CompanyComboBox.TabIndex = 145
        Me.CompanyComboBox.UseSelectable = True
        Me.CompanyComboBox.ValueMember = "code"
        '
        'Sp_Receiving_SEL_CompanyBindingSource
        '
        Me.Sp_Receiving_SEL_CompanyBindingSource.DataMember = "sp_Receiving_SEL_Company"
        Me.Sp_Receiving_SEL_CompanyBindingSource.DataSource = Me.ReceivingDataSet
        '
        'TypeBindingSource
        '
        Me.TypeBindingSource.DataMember = "type"
        Me.TypeBindingSource.DataSource = Me.ReceivingDataSet
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(417, 233)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel7.TabIndex = 144
        Me.MetroLabel7.Text = "Company"
        '
        'RcFromComboBox
        '
        Me.RcFromComboBox.FormattingEnabled = True
        Me.RcFromComboBox.ItemHeight = 24
        Me.RcFromComboBox.Items.AddRange(New Object() {"Purchasing", "Free"})
        Me.RcFromComboBox.Location = New System.Drawing.Point(491, 331)
        Me.RcFromComboBox.Name = "RcFromComboBox"
        Me.RcFromComboBox.Size = New System.Drawing.Size(243, 30)
        Me.RcFromComboBox.TabIndex = 143
        Me.RcFromComboBox.UseSelectable = True
        '
        'TypeComboBox
        '
        Me.TypeComboBox.DataSource = Me.TypeBindingSource
        Me.TypeComboBox.DisplayMember = "type"
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 24
        Me.TypeComboBox.Location = New System.Drawing.Point(491, 295)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(243, 30)
        Me.TypeComboBox.TabIndex = 142
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type"
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(446, 305)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(39, 20)
        Me.MetroLabel6.TabIndex = 141
        Me.MetroLabel6.Text = "Type"
        '
        'ClassifierComboBox
        '
        Me.ClassifierComboBox.DataSource = Me.Sp_Receiving_SEL_ClassifierBindingSource
        Me.ClassifierComboBox.DisplayMember = "name"
        Me.ClassifierComboBox.FormattingEnabled = True
        Me.ClassifierComboBox.ItemHeight = 24
        Me.ClassifierComboBox.Location = New System.Drawing.Point(491, 439)
        Me.ClassifierComboBox.Name = "ClassifierComboBox"
        Me.ClassifierComboBox.Size = New System.Drawing.Size(243, 30)
        Me.ClassifierComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ClassifierComboBox.TabIndex = 140
        Me.ClassifierComboBox.UseSelectable = True
        Me.ClassifierComboBox.ValueMember = "name"
        '
        'Sp_Receiving_SEL_ClassifierBindingSource
        '
        Me.Sp_Receiving_SEL_ClassifierBindingSource.DataMember = "sp_Receiving_SEL_Classifier"
        Me.Sp_Receiving_SEL_ClassifierBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_BuyerClassifierBindingSource
        '
        Me.Sp_Receiving_SEL_BuyerClassifierBindingSource.DataMember = "sp_Receiving_SEL_BuyerClassifier"
        Me.Sp_Receiving_SEL_BuyerClassifierBindingSource.DataSource = Me.ReceivingDataSet
        '
        'BuyerComboBox
        '
        Me.BuyerComboBox.DataSource = Me.Sp_Receiving_SEL_BuyerClassifierBindingSource
        Me.BuyerComboBox.DisplayMember = "name"
        Me.BuyerComboBox.FormattingEnabled = True
        Me.BuyerComboBox.ItemHeight = 24
        Me.BuyerComboBox.Location = New System.Drawing.Point(491, 403)
        Me.BuyerComboBox.Name = "BuyerComboBox"
        Me.BuyerComboBox.Size = New System.Drawing.Size(243, 30)
        Me.BuyerComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.BuyerComboBox.TabIndex = 139
        Me.BuyerComboBox.UseSelectable = True
        Me.BuyerComboBox.ValueMember = "name"
        '
        'PlaceComboBox
        '
        Me.PlaceComboBox.DataSource = Me.Sp_Receiving_SEL_MatWHBindingSource
        Me.PlaceComboBox.DisplayMember = "wh"
        Me.PlaceComboBox.FormattingEnabled = True
        Me.PlaceComboBox.ItemHeight = 24
        Me.PlaceComboBox.Location = New System.Drawing.Point(491, 367)
        Me.PlaceComboBox.Name = "PlaceComboBox"
        Me.PlaceComboBox.Size = New System.Drawing.Size(243, 30)
        Me.PlaceComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.PlaceComboBox.TabIndex = 138
        Me.PlaceComboBox.UseSelectable = True
        Me.PlaceComboBox.ValueMember = "wh"
        '
        'Sp_Receiving_SEL_MatWHBindingSource
        '
        Me.Sp_Receiving_SEL_MatWHBindingSource.DataMember = "sp_Receiving_SEL_MatWH"
        Me.Sp_Receiving_SEL_MatWHBindingSource.DataSource = Me.ReceivingDataSet
        '
        'TruckNoTextBox
        '
        '
        '
        '
        Me.TruckNoTextBox.CustomButton.Image = Nothing
        Me.TruckNoTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.TruckNoTextBox.CustomButton.Name = ""
        Me.TruckNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextBox.CustomButton.TabIndex = 1
        Me.TruckNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextBox.CustomButton.UseSelectable = True
        Me.TruckNoTextBox.CustomButton.Visible = False
        Me.TruckNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.TruckNoTextBox.Lines = New String(-1) {}
        Me.TruckNoTextBox.Location = New System.Drawing.Point(491, 477)
        Me.TruckNoTextBox.MaxLength = 32767
        Me.TruckNoTextBox.Name = "TruckNoTextBox"
        Me.TruckNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextBox.SelectedText = ""
        Me.TruckNoTextBox.SelectionLength = 0
        Me.TruckNoTextBox.SelectionStart = 0
        Me.TruckNoTextBox.Size = New System.Drawing.Size(243, 30)
        Me.TruckNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.TruckNoTextBox.TabIndex = 137
        Me.TruckNoTextBox.UseSelectable = True
        Me.TruckNoTextBox.UseStyleColors = True
        Me.TruckNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(383, 341)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(102, 20)
        Me.MetroLabel5.TabIndex = 136
        Me.MetroLabel5.Text = "Receiving from"
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(422, 449)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(63, 20)
        Me.MetroLabel4.TabIndex = 135
        Me.MetroLabel4.Text = "Classifier"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(440, 413)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(45, 20)
        Me.MetroLabel3.TabIndex = 134
        Me.MetroLabel3.Text = "Buyer"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(420, 484)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(65, 20)
        Me.MetroLabel1.TabIndex = 133
        Me.MetroLabel1.Text = "Truck no."
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(443, 377)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(42, 20)
        Me.MetroLabel2.TabIndex = 132
        Me.MetroLabel2.Text = "Place"
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(433, 181)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(52, 20)
        Me.MetroLabel9.TabIndex = 163
        Me.MetroLabel9.Text = "RC No."
        '
        'RcNoLabel
        '
        Me.RcNoLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.RcNoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.RcNoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.RcNoLabel.Location = New System.Drawing.Point(491, 172)
        Me.RcNoLabel.Name = "RcNoLabel"
        Me.RcNoLabel.Size = New System.Drawing.Size(243, 39)
        Me.RcNoLabel.Style = MetroFramework.MetroColorStyle.Black
        Me.RcNoLabel.TabIndex = 164
        Me.RcNoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RcNoLabel.UseCustomBackColor = True
        Me.RcNoLabel.UseStyleColors = True
        '
        'Sp_Receiving_SEL_MatRCBindingSource
        '
        Me.Sp_Receiving_SEL_MatRCBindingSource.DataMember = "sp_Receiving_SEL_MatRC"
        Me.Sp_Receiving_SEL_MatRCBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatRCTableAdapter
        '
        Me.Sp_Receiving_SEL_MatRCTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Me.TypeTableAdapter
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'TypeTableAdapter
        '
        Me.TypeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_MatByRcNoBindingSource
        '
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo"
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatByRcNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_CompanyTableAdapter
        '
        Me.Sp_Receiving_SEL_CompanyTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_BuyerClassifierTableAdapter
        '
        Me.Sp_Receiving_SEL_BuyerClassifierTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_MatWHTableAdapter
        '
        Me.Sp_Receiving_SEL_MatWHTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_SubtypeTableAdapter
        '
        Me.Sp_Receiving_SEL_SubtypeTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_ClassifierTableAdapter
        '
        Me.Sp_Receiving_SEL_ClassifierTableAdapter.ClearBeforeFill = True
        '
        'InvoiceNoTextBox
        '
        Me.InvoiceNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.InvoiceNoTextBox.CustomButton.Image = Nothing
        Me.InvoiceNoTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.InvoiceNoTextBox.CustomButton.Name = ""
        Me.InvoiceNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.InvoiceNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.InvoiceNoTextBox.CustomButton.TabIndex = 1
        Me.InvoiceNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.InvoiceNoTextBox.CustomButton.UseSelectable = True
        Me.InvoiceNoTextBox.CustomButton.Visible = False
        Me.InvoiceNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.InvoiceNoTextBox.Lines = New String(-1) {}
        Me.InvoiceNoTextBox.Location = New System.Drawing.Point(491, 549)
        Me.InvoiceNoTextBox.MaxLength = 32767
        Me.InvoiceNoTextBox.Name = "InvoiceNoTextBox"
        Me.InvoiceNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InvoiceNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.InvoiceNoTextBox.SelectedText = ""
        Me.InvoiceNoTextBox.SelectionLength = 0
        Me.InvoiceNoTextBox.SelectionStart = 0
        Me.InvoiceNoTextBox.Size = New System.Drawing.Size(243, 30)
        Me.InvoiceNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.InvoiceNoTextBox.TabIndex = 168
        Me.InvoiceNoTextBox.UseCustomBackColor = True
        Me.InvoiceNoTextBox.UseSelectable = True
        Me.InvoiceNoTextBox.UseStyleColors = True
        Me.InvoiceNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.InvoiceNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.Location = New System.Drawing.Point(361, 554)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(124, 20)
        Me.MetroLabel11.TabIndex = 167
        Me.MetroLabel11.Text = "Packlist Invoice no."
        '
        'TransportDocNoTextBox
        '
        Me.TransportDocNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TransportDocNoTextBox.CustomButton.Image = Nothing
        Me.TransportDocNoTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.TransportDocNoTextBox.CustomButton.Name = ""
        Me.TransportDocNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TransportDocNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TransportDocNoTextBox.CustomButton.TabIndex = 1
        Me.TransportDocNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TransportDocNoTextBox.CustomButton.UseSelectable = True
        Me.TransportDocNoTextBox.CustomButton.Visible = False
        Me.TransportDocNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TransportDocNoTextBox.Lines = New String(-1) {}
        Me.TransportDocNoTextBox.Location = New System.Drawing.Point(491, 513)
        Me.TransportDocNoTextBox.MaxLength = 32767
        Me.TransportDocNoTextBox.Name = "TransportDocNoTextBox"
        Me.TransportDocNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TransportDocNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TransportDocNoTextBox.SelectedText = ""
        Me.TransportDocNoTextBox.SelectionLength = 0
        Me.TransportDocNoTextBox.SelectionStart = 0
        Me.TransportDocNoTextBox.Size = New System.Drawing.Size(243, 30)
        Me.TransportDocNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.TransportDocNoTextBox.TabIndex = 166
        Me.TransportDocNoTextBox.UseCustomBackColor = True
        Me.TransportDocNoTextBox.UseSelectable = True
        Me.TransportDocNoTextBox.UseStyleColors = True
        Me.TransportDocNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TransportDocNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(297, 518)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(187, 20)
        Me.MetroLabel10.TabIndex = 165
        Me.MetroLabel10.Text = "Transportation document no."
        '
        'FrmEditRcNoHeader
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1489, 704)
        Me.Controls.Add(Me.InvoiceNoTextBox)
        Me.Controls.Add(Me.MetroLabel11)
        Me.Controls.Add(Me.TransportDocNoTextBox)
        Me.Controls.Add(Me.MetroLabel10)
        Me.Controls.Add(Me.MetroLabel9)
        Me.Controls.Add(Me.RcNoLabel)
        Me.Controls.Add(Me.SubtypeComboBox)
        Me.Controls.Add(Me.MetroLabel8)
        Me.Controls.Add(Me.CompanyComboBox)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.RcFromComboBox)
        Me.Controls.Add(Me.TypeComboBox)
        Me.Controls.Add(Me.MetroLabel6)
        Me.Controls.Add(Me.ClassifierComboBox)
        Me.Controls.Add(Me.BuyerComboBox)
        Me.Controls.Add(Me.PlaceComboBox)
        Me.Controls.Add(Me.TruckNoTextBox)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmEditRcNoHeader"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.Sp_Receiving_SEL_SubtypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_CompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_ClassifierBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_BuyerClassifierBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatWHBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatRCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents SaveTile As MetroFramework.Controls.MetroTile
    Friend WithEvents SaveMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SubtypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CompanyComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RcFromComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ClassifierComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents BuyerComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents PlaceComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TruckNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RcNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents Sp_Receiving_SEL_MatRCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatRCTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter
    Friend WithEvents Sp_Receiving_SEL_CompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_CompanyTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_CompanyTableAdapter
    Friend WithEvents TypeTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.typeTableAdapter
    Friend WithEvents TypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_BuyerClassifierBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_BuyerClassifierTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_BuyerClassifierTableAdapter
    Friend WithEvents Sp_Receiving_SEL_MatWHBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatWHTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatWHTableAdapter
    Friend WithEvents Sp_Receiving_SEL_SubtypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_SubtypeTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_SubtypeTableAdapter
    Friend WithEvents Sp_Receiving_SEL_ClassifierBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_ClassifierTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ClassifierTableAdapter
    Friend WithEvents InvoiceNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TransportDocNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
End Class
