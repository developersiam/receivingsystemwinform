﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCallReceivingNoByChecker
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.AddDocnoMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.ReportTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.CheckerLockedMetroCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.matrcDg = New MetroFramework.Controls.MetroGrid()
        Me.RcnoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CropDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DATEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlaceDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TrucknoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CheckerlockedDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.COUNTOFBALESDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfWeightDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SumOfWeightbuyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RcfromDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StarttimeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinishtimeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BuyerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClassifierDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SpReceivingSELMatRCByReceivingFinishResultBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.matrcDg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpReceivingSELMatRCByReceivingFinishResultBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TypeComboBox
        '
        Me.TypeComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TypeComboBox.DisplayMember = "type1"
        Me.TypeComboBox.FormattingEnabled = True
        Me.TypeComboBox.ItemHeight = 23
        Me.TypeComboBox.Location = New System.Drawing.Point(45, 3)
        Me.TypeComboBox.Name = "TypeComboBox"
        Me.TypeComboBox.Size = New System.Drawing.Size(132, 29)
        Me.TypeComboBox.TabIndex = 124
        Me.TypeComboBox.Tag = ""
        Me.TypeComboBox.UseSelectable = True
        Me.TypeComboBox.ValueMember = "type1"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(36, 35)
        Me.MetroLabel2.TabIndex = 123
        Me.MetroLabel2.Text = "Type"
        Me.MetroLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MetroPanel1
        '
        Me.MetroPanel1.AutoSize = True
        Me.MetroPanel1.Controls.Add(Me.AddDocnoMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.ReportTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel10)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 3)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1105, 54)
        Me.MetroPanel1.TabIndex = 121
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'AddDocnoMetroTile
        '
        Me.AddDocnoMetroTile.ActiveControl = Nothing
        Me.AddDocnoMetroTile.AutoSize = True
        Me.AddDocnoMetroTile.BackColor = System.Drawing.Color.White
        Me.AddDocnoMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddDocnoMetroTile.Location = New System.Drawing.Point(684, 16)
        Me.AddDocnoMetroTile.Name = "AddDocnoMetroTile"
        Me.AddDocnoMetroTile.Size = New System.Drawing.Size(36, 35)
        Me.AddDocnoMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddDocnoMetroTile.TabIndex = 125
        Me.AddDocnoMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddDocnoMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Download32
        Me.AddDocnoMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddDocnoMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddDocnoMetroTile.UseSelectable = True
        Me.AddDocnoMetroTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(726, 23)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(80, 19)
        Me.MetroLabel1.TabIndex = 124
        Me.MetroLabel1.Text = "add doc no."
        '
        'ReportTile
        '
        Me.ReportTile.ActiveControl = Nothing
        Me.ReportTile.AutoSize = True
        Me.ReportTile.BackColor = System.Drawing.Color.White
        Me.ReportTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ReportTile.Location = New System.Drawing.Point(945, 16)
        Me.ReportTile.Name = "ReportTile"
        Me.ReportTile.Size = New System.Drawing.Size(36, 35)
        Me.ReportTile.Style = MetroFramework.MetroColorStyle.White
        Me.ReportTile.TabIndex = 123
        Me.ReportTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReportTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.PurchaseOrder32
        Me.ReportTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReportTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ReportTile.UseSelectable = True
        Me.ReportTile.UseTileImage = True
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.Location = New System.Drawing.Point(987, 23)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(47, 19)
        Me.MetroLabel10.TabIndex = 122
        Me.MetroLabel10.Text = "report"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(828, 16)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(870, 23)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1052, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'CheckerLockedMetroCheckBox
        '
        Me.CheckerLockedMetroCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.CheckerLockedMetroCheckBox.AutoSize = True
        Me.CheckerLockedMetroCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.CheckerLockedMetroCheckBox.Location = New System.Drawing.Point(183, 13)
        Me.CheckerLockedMetroCheckBox.Name = "CheckerLockedMetroCheckBox"
        Me.CheckerLockedMetroCheckBox.Size = New System.Drawing.Size(115, 19)
        Me.CheckerLockedMetroCheckBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.CheckerLockedMetroCheckBox.TabIndex = 131
        Me.CheckerLockedMetroCheckBox.Text = "Cheker Locked"
        Me.CheckerLockedMetroCheckBox.UseSelectable = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroPanel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.matrcDg)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(20, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1320, 688)
        Me.FlowLayoutPanel1.TabIndex = 132
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.Controls.Add(Me.MetroLabel2)
        Me.FlowLayoutPanel2.Controls.Add(Me.TypeComboBox)
        Me.FlowLayoutPanel2.Controls.Add(Me.CheckerLockedMetroCheckBox)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 63)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1105, 35)
        Me.FlowLayoutPanel2.TabIndex = 122
        '
        'matrcDg
        '
        Me.matrcDg.AllowUserToAddRows = False
        Me.matrcDg.AllowUserToDeleteRows = False
        Me.matrcDg.AllowUserToOrderColumns = True
        Me.matrcDg.AllowUserToResizeRows = False
        Me.matrcDg.AutoGenerateColumns = False
        Me.matrcDg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.matrcDg.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.matrcDg.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.matrcDg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.matrcDg.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.matrcDg.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.matrcDg.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.matrcDg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.matrcDg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RcnoDataGridViewTextBoxColumn, Me.CropDataGridViewTextBoxColumn, Me.TypeDataGridViewTextBoxColumn, Me.DATEDataGridViewTextBoxColumn, Me.PlaceDataGridViewTextBoxColumn, Me.TrucknoDataGridViewTextBoxColumn, Me.CheckerlockedDataGridViewTextBoxColumn, Me.COUNTOFBALESDataGridViewTextBoxColumn, Me.SumOfWeightDataGridViewTextBoxColumn, Me.SumOfWeightbuyDataGridViewTextBoxColumn, Me.RcfromDataGridViewTextBoxColumn, Me.StarttimeDataGridViewTextBoxColumn, Me.FinishtimeDataGridViewTextBoxColumn, Me.BuyerDataGridViewTextBoxColumn, Me.ClassifierDataGridViewTextBoxColumn})
        Me.matrcDg.DataSource = Me.SpReceivingSELMatRCByReceivingFinishResultBindingSource
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.matrcDg.DefaultCellStyle = DataGridViewCellStyle6
        Me.matrcDg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.matrcDg.EnableHeadersVisualStyles = False
        Me.matrcDg.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.matrcDg.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.matrcDg.Location = New System.Drawing.Point(3, 104)
        Me.matrcDg.Name = "matrcDg"
        Me.matrcDg.ReadOnly = True
        Me.matrcDg.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.matrcDg.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.matrcDg.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.Padding = New System.Windows.Forms.Padding(2)
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.matrcDg.RowsDefaultCellStyle = DataGridViewCellStyle8
        Me.matrcDg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.matrcDg.Size = New System.Drawing.Size(1105, 580)
        Me.matrcDg.Style = MetroFramework.MetroColorStyle.Orange
        Me.matrcDg.TabIndex = 123
        '
        'RcnoDataGridViewTextBoxColumn
        '
        Me.RcnoDataGridViewTextBoxColumn.DataPropertyName = "rcno"
        Me.RcnoDataGridViewTextBoxColumn.HeaderText = "rcno"
        Me.RcnoDataGridViewTextBoxColumn.Name = "RcnoDataGridViewTextBoxColumn"
        Me.RcnoDataGridViewTextBoxColumn.ReadOnly = True
        Me.RcnoDataGridViewTextBoxColumn.Width = 57
        '
        'CropDataGridViewTextBoxColumn
        '
        Me.CropDataGridViewTextBoxColumn.DataPropertyName = "crop"
        Me.CropDataGridViewTextBoxColumn.HeaderText = "crop"
        Me.CropDataGridViewTextBoxColumn.Name = "CropDataGridViewTextBoxColumn"
        Me.CropDataGridViewTextBoxColumn.ReadOnly = True
        Me.CropDataGridViewTextBoxColumn.Width = 58
        '
        'TypeDataGridViewTextBoxColumn
        '
        Me.TypeDataGridViewTextBoxColumn.DataPropertyName = "type"
        Me.TypeDataGridViewTextBoxColumn.HeaderText = "type"
        Me.TypeDataGridViewTextBoxColumn.Name = "TypeDataGridViewTextBoxColumn"
        Me.TypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.TypeDataGridViewTextBoxColumn.Width = 56
        '
        'DATEDataGridViewTextBoxColumn
        '
        Me.DATEDataGridViewTextBoxColumn.DataPropertyName = "DATE"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.DATEDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.DATEDataGridViewTextBoxColumn.HeaderText = "date"
        Me.DATEDataGridViewTextBoxColumn.Name = "DATEDataGridViewTextBoxColumn"
        Me.DATEDataGridViewTextBoxColumn.ReadOnly = True
        Me.DATEDataGridViewTextBoxColumn.Width = 57
        '
        'PlaceDataGridViewTextBoxColumn
        '
        Me.PlaceDataGridViewTextBoxColumn.DataPropertyName = "place"
        Me.PlaceDataGridViewTextBoxColumn.HeaderText = "place"
        Me.PlaceDataGridViewTextBoxColumn.Name = "PlaceDataGridViewTextBoxColumn"
        Me.PlaceDataGridViewTextBoxColumn.ReadOnly = True
        Me.PlaceDataGridViewTextBoxColumn.Width = 62
        '
        'TrucknoDataGridViewTextBoxColumn
        '
        Me.TrucknoDataGridViewTextBoxColumn.DataPropertyName = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn.HeaderText = "truckno"
        Me.TrucknoDataGridViewTextBoxColumn.Name = "TrucknoDataGridViewTextBoxColumn"
        Me.TrucknoDataGridViewTextBoxColumn.ReadOnly = True
        Me.TrucknoDataGridViewTextBoxColumn.Width = 74
        '
        'CheckerlockedDataGridViewTextBoxColumn
        '
        Me.CheckerlockedDataGridViewTextBoxColumn.DataPropertyName = "checkerlocked"
        Me.CheckerlockedDataGridViewTextBoxColumn.HeaderText = "checkerlocked"
        Me.CheckerlockedDataGridViewTextBoxColumn.Name = "CheckerlockedDataGridViewTextBoxColumn"
        Me.CheckerlockedDataGridViewTextBoxColumn.ReadOnly = True
        Me.CheckerlockedDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CheckerlockedDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CheckerlockedDataGridViewTextBoxColumn.Width = 113
        '
        'COUNTOFBALESDataGridViewTextBoxColumn
        '
        Me.COUNTOFBALESDataGridViewTextBoxColumn.DataPropertyName = "COUNTOFBALES"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N0"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.COUNTOFBALESDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.COUNTOFBALESDataGridViewTextBoxColumn.HeaderText = "bales"
        Me.COUNTOFBALESDataGridViewTextBoxColumn.Name = "COUNTOFBALESDataGridViewTextBoxColumn"
        Me.COUNTOFBALESDataGridViewTextBoxColumn.ReadOnly = True
        Me.COUNTOFBALESDataGridViewTextBoxColumn.Width = 62
        '
        'SumOfWeightDataGridViewTextBoxColumn
        '
        Me.SumOfWeightDataGridViewTextBoxColumn.DataPropertyName = "SumOfWeight"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.SumOfWeightDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.SumOfWeightDataGridViewTextBoxColumn.HeaderText = "weight"
        Me.SumOfWeightDataGridViewTextBoxColumn.Name = "SumOfWeightDataGridViewTextBoxColumn"
        Me.SumOfWeightDataGridViewTextBoxColumn.ReadOnly = True
        Me.SumOfWeightDataGridViewTextBoxColumn.Width = 69
        '
        'SumOfWeightbuyDataGridViewTextBoxColumn
        '
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.DataPropertyName = "SumOfWeightbuy"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.HeaderText = "weightbuy"
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.Name = "SumOfWeightbuyDataGridViewTextBoxColumn"
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.ReadOnly = True
        Me.SumOfWeightbuyDataGridViewTextBoxColumn.Width = 90
        '
        'RcfromDataGridViewTextBoxColumn
        '
        Me.RcfromDataGridViewTextBoxColumn.DataPropertyName = "rcfrom"
        Me.RcfromDataGridViewTextBoxColumn.HeaderText = "rcfrom"
        Me.RcfromDataGridViewTextBoxColumn.Name = "RcfromDataGridViewTextBoxColumn"
        Me.RcfromDataGridViewTextBoxColumn.ReadOnly = True
        Me.RcfromDataGridViewTextBoxColumn.Width = 70
        '
        'StarttimeDataGridViewTextBoxColumn
        '
        Me.StarttimeDataGridViewTextBoxColumn.DataPropertyName = "starttime"
        Me.StarttimeDataGridViewTextBoxColumn.HeaderText = "starttime"
        Me.StarttimeDataGridViewTextBoxColumn.Name = "StarttimeDataGridViewTextBoxColumn"
        Me.StarttimeDataGridViewTextBoxColumn.ReadOnly = True
        Me.StarttimeDataGridViewTextBoxColumn.Visible = False
        Me.StarttimeDataGridViewTextBoxColumn.Width = 82
        '
        'FinishtimeDataGridViewTextBoxColumn
        '
        Me.FinishtimeDataGridViewTextBoxColumn.DataPropertyName = "finishtime"
        Me.FinishtimeDataGridViewTextBoxColumn.HeaderText = "finishtime"
        Me.FinishtimeDataGridViewTextBoxColumn.Name = "FinishtimeDataGridViewTextBoxColumn"
        Me.FinishtimeDataGridViewTextBoxColumn.ReadOnly = True
        Me.FinishtimeDataGridViewTextBoxColumn.Visible = False
        Me.FinishtimeDataGridViewTextBoxColumn.Width = 86
        '
        'BuyerDataGridViewTextBoxColumn
        '
        Me.BuyerDataGridViewTextBoxColumn.DataPropertyName = "buyer"
        Me.BuyerDataGridViewTextBoxColumn.HeaderText = "buyer"
        Me.BuyerDataGridViewTextBoxColumn.Name = "BuyerDataGridViewTextBoxColumn"
        Me.BuyerDataGridViewTextBoxColumn.ReadOnly = True
        Me.BuyerDataGridViewTextBoxColumn.Visible = False
        Me.BuyerDataGridViewTextBoxColumn.Width = 64
        '
        'ClassifierDataGridViewTextBoxColumn
        '
        Me.ClassifierDataGridViewTextBoxColumn.DataPropertyName = "classifier"
        Me.ClassifierDataGridViewTextBoxColumn.HeaderText = "classifier"
        Me.ClassifierDataGridViewTextBoxColumn.Name = "ClassifierDataGridViewTextBoxColumn"
        Me.ClassifierDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClassifierDataGridViewTextBoxColumn.Visible = False
        Me.ClassifierDataGridViewTextBoxColumn.Width = 81
        '
        'SpReceivingSELMatRCByReceivingFinishResultBindingSource
        '
        Me.SpReceivingSELMatRCByReceivingFinishResultBindingSource.DataSource = GetType(ReceivingSystem.sp_Receiving_SEL_MatRCByReceivingFinish_Result)
        '
        'FrmCallReceivingNoByChecker
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1360, 768)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "FrmCallReceivingNoByChecker"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        CType(Me.matrcDg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpReceivingSELMatRCByReceivingFinishResultBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents AddDocnoMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReportTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    'Friend WithEvents rcno As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents crop As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents type As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents rcfrom As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents truckno As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents place As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents buyer As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents classifier As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents starttime As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents finishtime As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents checkerlocked As System.Windows.Forms.DataGridViewCheckBoxColumn
    'Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    'Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    'Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CheckerLockedMetroCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents RcnoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CropDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DATEDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PlaceDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TrucknoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CheckerlockedDataGridViewTextBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents COUNTOFBALESDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SumOfWeightDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SumOfWeightbuyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RcfromDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StarttimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FinishtimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents BuyerDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ClassifierDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SpReceivingSELMatRCByReceivingFinishResultBindingSource As BindingSource
    Friend WithEvents matrcDg As MetroFramework.Controls.MetroGrid
End Class
