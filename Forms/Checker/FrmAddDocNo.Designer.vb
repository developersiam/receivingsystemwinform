﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAddDocNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.EditRcNoHeaderTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.SendRequestTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.LockedLabel = New MetroFramework.Controls.MetroLabel()
        Me.ReceivingNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.LockTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.AddDocno = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.MatRcMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.CropDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.supplier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.subtype = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.company = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.docno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.baleno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.green = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classify = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weightbuy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WeightDiff = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fromclassify = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemarkChecked = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReplaceBales = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter()
        Me.Sp_Receiving_SEL_MatRCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatRCTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCTableAdapter()
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.lblRcFrom = New MetroFramework.Controls.MetroLabel()
        Me.RemarkFromCheckerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.Sp_Receiving_SEL_RemarkCheckerRequestBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RequestTypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter()
        Me.MetroPanel3 = New MetroFramework.Controls.MetroPanel()
        Me.DiffWeightLabel = New System.Windows.Forms.Label()
        Me.SumReceivedWeightLabel = New System.Windows.Forms.Label()
        Me.SumBuyingWeightLabel = New System.Windows.Forms.Label()
        Me.TotalBaleLabel = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_RemarkCheckerRequestTableAdapter()
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter()
        Me.MetroGrid1 = New MetroFramework.Controls.MetroGrid()
        Me.BtnNTRM = New System.Windows.Forms.Button()
        Me.Sp_NTRM_SEL_NTRMInspectionByBCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_NTRM_SEL_NTRMInspectionByBCTableAdapter()
        Me.NTRMPanel = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel40 = New MetroFramework.Controls.MetroLabel()
        Me.txtNTRMRemak = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel33 = New MetroFramework.Controls.MetroLabel()
        Me.TxtNTRMQuantity = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel39 = New MetroFramework.Controls.MetroLabel()
        Me.lblNTRMDetail = New MetroFramework.Controls.MetroLabel()
        Me.DataGridNTRMInspection = New System.Windows.Forms.DataGridView()
        Me.BaleBarcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NTRMTypeCodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NTRMTYPENAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InspectionDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QuantityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InspectionUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InspectionLocationDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BtnNTRMExit = New System.Windows.Forms.Button()
        Me.MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.NTRMatBuying = New MetroFramework.Controls.MetroLabel()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bales = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.buyingWeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.receiveWeight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.summaryDataGridCompany = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MatRcMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatRCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel2.SuspendLayout()
        CType(Me.Sp_Receiving_SEL_RemarkCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel3.SuspendLayout()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MetroGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_NTRM_SEL_NTRMInspectionByBCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.NTRMPanel.SuspendLayout()
        CType(Me.DataGridNTRMInspection, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.EditRcNoHeaderTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel1.Controls.Add(Me.SendRequestTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel1.Controls.Add(Me.LockedLabel)
        Me.MetroPanel1.Controls.Add(Me.ReceivingNoLabel)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel1.Controls.Add(Me.LockTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.AddDocno)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 33)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(998, 54)
        Me.MetroPanel1.TabIndex = 117
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'EditRcNoHeaderTile
        '
        Me.EditRcNoHeaderTile.ActiveControl = Nothing
        Me.EditRcNoHeaderTile.AutoSize = True
        Me.EditRcNoHeaderTile.BackColor = System.Drawing.Color.White
        Me.EditRcNoHeaderTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditRcNoHeaderTile.Location = New System.Drawing.Point(371, 13)
        Me.EditRcNoHeaderTile.Name = "EditRcNoHeaderTile"
        Me.EditRcNoHeaderTile.Size = New System.Drawing.Size(38, 35)
        Me.EditRcNoHeaderTile.Style = MetroFramework.MetroColorStyle.White
        Me.EditRcNoHeaderTile.TabIndex = 138
        Me.EditRcNoHeaderTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EditRcNoHeaderTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.InTransit32
        Me.EditRcNoHeaderTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EditRcNoHeaderTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.EditRcNoHeaderTile.UseSelectable = True
        Me.EditRcNoHeaderTile.UseTileImage = True
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.Location = New System.Drawing.Point(411, 21)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(74, 19)
        Me.MetroLabel8.TabIndex = 137
        Me.MetroLabel8.Text = "edit Rc No."
        '
        'SendRequestTile
        '
        Me.SendRequestTile.ActiveControl = Nothing
        Me.SendRequestTile.AutoSize = True
        Me.SendRequestTile.BackColor = System.Drawing.Color.White
        Me.SendRequestTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SendRequestTile.Location = New System.Drawing.Point(698, 11)
        Me.SendRequestTile.Name = "SendRequestTile"
        Me.SendRequestTile.Size = New System.Drawing.Size(36, 35)
        Me.SendRequestTile.Style = MetroFramework.MetroColorStyle.White
        Me.SendRequestTile.TabIndex = 136
        Me.SendRequestTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SendRequestTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledUserMaleFilled32
        Me.SendRequestTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SendRequestTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.SendRequestTile.UseSelectable = True
        Me.SendRequestTile.UseTileImage = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(740, 18)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(83, 19)
        Me.MetroLabel3.TabIndex = 135
        Me.MetroLabel3.Text = "send request"
        '
        'LockedLabel
        '
        Me.LockedLabel.AutoSize = True
        Me.LockedLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LockedLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.LockedLabel.ForeColor = System.Drawing.Color.White
        Me.LockedLabel.Location = New System.Drawing.Point(300, 20)
        Me.LockedLabel.Name = "LockedLabel"
        Me.LockedLabel.Size = New System.Drawing.Size(65, 25)
        Me.LockedLabel.TabIndex = 134
        Me.LockedLabel.Text = "Locked"
        Me.LockedLabel.UseCustomBackColor = True
        Me.LockedLabel.UseCustomForeColor = True
        Me.LockedLabel.Visible = False
        '
        'ReceivingNoLabel
        '
        Me.ReceivingNoLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ReceivingNoLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ReceivingNoLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.ReceivingNoLabel.Location = New System.Drawing.Point(200, 10)
        Me.ReceivingNoLabel.Name = "ReceivingNoLabel"
        Me.ReceivingNoLabel.Size = New System.Drawing.Size(77, 39)
        Me.ReceivingNoLabel.Style = MetroFramework.MetroColorStyle.Black
        Me.ReceivingNoLabel.TabIndex = 133
        Me.ReceivingNoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ReceivingNoLabel.UseCustomBackColor = True
        Me.ReceivingNoLabel.UseStyleColors = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel5.Location = New System.Drawing.Point(142, 20)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(59, 25)
        Me.MetroLabel5.TabIndex = 132
        Me.MetroLabel5.Text = "Rc no."
        '
        'LockTile
        '
        Me.LockTile.ActiveControl = Nothing
        Me.LockTile.AutoSize = True
        Me.LockTile.BackColor = System.Drawing.Color.White
        Me.LockTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LockTile.Location = New System.Drawing.Point(615, 11)
        Me.LockTile.Name = "LockTile"
        Me.LockTile.Size = New System.Drawing.Size(36, 35)
        Me.LockTile.Style = MetroFramework.MetroColorStyle.White
        Me.LockTile.TabIndex = 131
        Me.LockTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LockTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Lock32
        Me.LockTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LockTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.LockTile.UseSelectable = True
        Me.LockTile.UseTileImage = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(648, 18)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(32, 19)
        Me.MetroLabel1.TabIndex = 130
        Me.MetroLabel1.Text = "lock"
        '
        'AddDocno
        '
        Me.AddDocno.ActiveControl = Nothing
        Me.AddDocno.AutoSize = True
        Me.AddDocno.BackColor = System.Drawing.Color.White
        Me.AddDocno.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddDocno.Location = New System.Drawing.Point(491, 10)
        Me.AddDocno.Name = "AddDocno"
        Me.AddDocno.Size = New System.Drawing.Size(38, 35)
        Me.AddDocno.Style = MetroFramework.MetroColorStyle.White
        Me.AddDocno.TabIndex = 129
        Me.AddDocno.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddDocno.TileImage = Global.ReceivingSystem.My.Resources.Resources.Download32
        Me.AddDocno.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddDocno.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddDocno.UseSelectable = True
        Me.AddDocno.UseTileImage = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(531, 18)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(76, 19)
        Me.MetroLabel2.TabIndex = 128
        Me.MetroLabel2.Text = "add docno."
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(836, 11)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(878, 18)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(933, 0)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(55, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Sp_Receiving_SEL_MatByRcNoBindingSource
        '
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo"
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatByRcNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'MatRcMetroGrid
        '
        Me.MatRcMetroGrid.AllowUserToAddRows = False
        Me.MatRcMetroGrid.AllowUserToDeleteRows = False
        Me.MatRcMetroGrid.AllowUserToResizeRows = False
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRcMetroGrid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.MatRcMetroGrid.AutoGenerateColumns = False
        Me.MatRcMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MatRcMetroGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.MatRcMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRcMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MatRcMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MatRcMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRcMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.MatRcMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MatRcMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CropDataGridViewTextBoxColumn, Me.supplier, Me.subtype, Me.company, Me.docno, Me.bc, Me.baleno, Me.green, Me.classify, Me.weightbuy, Me.weight, Me.WeightDiff, Me.fromclassify, Me.RemarkChecked, Me.ReplaceBales})
        Me.MatRcMetroGrid.DataSource = Me.Sp_Receiving_SEL_MatByRcNoBindingSource
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MatRcMetroGrid.DefaultCellStyle = DataGridViewCellStyle7
        Me.MatRcMetroGrid.EnableHeadersVisualStyles = False
        Me.MatRcMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRcMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MatRcMetroGrid.Location = New System.Drawing.Point(3, 151)
        Me.MatRcMetroGrid.Name = "MatRcMetroGrid"
        Me.MatRcMetroGrid.ReadOnly = True
        Me.MatRcMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MatRcMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.MatRcMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MatRcMetroGrid.RowsDefaultCellStyle = DataGridViewCellStyle9
        Me.MatRcMetroGrid.RowTemplate.Height = 24
        Me.MatRcMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MatRcMetroGrid.Size = New System.Drawing.Size(998, 594)
        Me.MatRcMetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.MatRcMetroGrid.TabIndex = 123
        '
        'CropDataGridViewTextBoxColumn
        '
        Me.CropDataGridViewTextBoxColumn.DataPropertyName = "bz"
        Me.CropDataGridViewTextBoxColumn.HeaderText = "OpenBale"
        Me.CropDataGridViewTextBoxColumn.Name = "CropDataGridViewTextBoxColumn"
        Me.CropDataGridViewTextBoxColumn.ReadOnly = True
        Me.CropDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CropDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CropDataGridViewTextBoxColumn.Width = 80
        '
        'supplier
        '
        Me.supplier.DataPropertyName = "supplier"
        Me.supplier.HeaderText = "Supp."
        Me.supplier.Name = "supplier"
        Me.supplier.ReadOnly = True
        Me.supplier.Width = 60
        '
        'subtype
        '
        Me.subtype.DataPropertyName = "subtype"
        Me.subtype.HeaderText = "Subtype"
        Me.subtype.Name = "subtype"
        Me.subtype.ReadOnly = True
        Me.subtype.Width = 72
        '
        'company
        '
        Me.company.DataPropertyName = "company"
        Me.company.HeaderText = "Comp."
        Me.company.Name = "company"
        Me.company.ReadOnly = True
        Me.company.Width = 63
        '
        'docno
        '
        Me.docno.DataPropertyName = "docno"
        Me.docno.HeaderText = "Doc no."
        Me.docno.Name = "docno"
        Me.docno.ReadOnly = True
        Me.docno.Width = 70
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "BC"
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 43
        '
        'baleno
        '
        Me.baleno.DataPropertyName = "baleno"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.baleno.DefaultCellStyle = DataGridViewCellStyle3
        Me.baleno.HeaderText = "Baleno"
        Me.baleno.Name = "baleno"
        Me.baleno.ReadOnly = True
        Me.baleno.Width = 65
        '
        'green
        '
        Me.green.DataPropertyName = "green"
        Me.green.HeaderText = "Green"
        Me.green.Name = "green"
        Me.green.ReadOnly = True
        Me.green.Width = 61
        '
        'classify
        '
        Me.classify.DataPropertyName = "classify"
        Me.classify.HeaderText = "Classify"
        Me.classify.Name = "classify"
        Me.classify.ReadOnly = True
        Me.classify.Width = 68
        '
        'weightbuy
        '
        Me.weightbuy.DataPropertyName = "weightbuy"
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.weightbuy.DefaultCellStyle = DataGridViewCellStyle4
        Me.weightbuy.HeaderText = "B.weight"
        Me.weightbuy.Name = "weightbuy"
        Me.weightbuy.ReadOnly = True
        Me.weightbuy.Width = 75
        '
        'weight
        '
        Me.weight.DataPropertyName = "weight"
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.weight.DefaultCellStyle = DataGridViewCellStyle5
        Me.weight.HeaderText = "R.weight"
        Me.weight.Name = "weight"
        Me.weight.ReadOnly = True
        Me.weight.Width = 76
        '
        'WeightDiff
        '
        Me.WeightDiff.DataPropertyName = "WeightDiff"
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.WeightDiff.DefaultCellStyle = DataGridViewCellStyle6
        Me.WeightDiff.HeaderText = "Diff"
        Me.WeightDiff.Name = "WeightDiff"
        Me.WeightDiff.ReadOnly = True
        Me.WeightDiff.Width = 49
        '
        'fromclassify
        '
        Me.fromclassify.DataPropertyName = "fromclass"
        Me.fromclassify.HeaderText = "fromclassify"
        Me.fromclassify.Name = "fromclassify"
        Me.fromclassify.ReadOnly = True
        Me.fromclassify.Width = 90
        '
        'RemarkChecked
        '
        Me.RemarkChecked.DataPropertyName = "RemarkChecked"
        Me.RemarkChecked.HeaderText = "NTRM inspec."
        Me.RemarkChecked.Name = "RemarkChecked"
        Me.RemarkChecked.ReadOnly = True
        '
        'ReplaceBales
        '
        Me.ReplaceBales.DataPropertyName = "ReplaceBales"
        Me.ReplaceBales.HeaderText = "ReplaceBales"
        Me.ReplaceBales.Name = "ReplaceBales"
        Me.ReplaceBales.ReadOnly = True
        Me.ReplaceBales.Width = 77
        '
        'Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource
        '
        Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource.DataMember = "sp_Receiving_SEL_MatbyRcNoGroupByField"
        Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter
        '
        Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_MatRCBindingSource
        '
        Me.Sp_Receiving_SEL_MatRCBindingSource.DataMember = "sp_Receiving_SEL_MatRC"
        Me.Sp_Receiving_SEL_MatRCBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatRCTableAdapter
        '
        Me.Sp_Receiving_SEL_MatRCTableAdapter.ClearBeforeFill = True
        '
        'MetroPanel2
        '
        Me.MetroPanel2.Controls.Add(Me.lblRcFrom)
        Me.MetroPanel2.Controls.Add(Me.RemarkFromCheckerComboBox)
        Me.MetroPanel2.Controls.Add(Me.RequestTypeComboBox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel7)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(3, 93)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(998, 52)
        Me.MetroPanel2.TabIndex = 124
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'lblRcFrom
        '
        Me.lblRcFrom.AutoSize = True
        Me.lblRcFrom.BackColor = System.Drawing.Color.Green
        Me.lblRcFrom.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.lblRcFrom.ForeColor = System.Drawing.Color.White
        Me.lblRcFrom.Location = New System.Drawing.Point(38, 12)
        Me.lblRcFrom.Name = "lblRcFrom"
        Me.lblRcFrom.Size = New System.Drawing.Size(65, 25)
        Me.lblRcFrom.TabIndex = 136
        Me.lblRcFrom.Text = "Locked"
        Me.lblRcFrom.UseCustomBackColor = True
        Me.lblRcFrom.UseCustomForeColor = True
        '
        'RemarkFromCheckerComboBox
        '
        Me.RemarkFromCheckerComboBox.DataSource = Me.Sp_Receiving_SEL_RemarkCheckerRequestBindingSource
        Me.RemarkFromCheckerComboBox.DisplayMember = "RemarkName"
        Me.RemarkFromCheckerComboBox.FormattingEnabled = True
        Me.RemarkFromCheckerComboBox.ItemHeight = 23
        Me.RemarkFromCheckerComboBox.Location = New System.Drawing.Point(551, 10)
        Me.RemarkFromCheckerComboBox.Name = "RemarkFromCheckerComboBox"
        Me.RemarkFromCheckerComboBox.Size = New System.Drawing.Size(432, 29)
        Me.RemarkFromCheckerComboBox.TabIndex = 135
        Me.RemarkFromCheckerComboBox.UseSelectable = True
        Me.RemarkFromCheckerComboBox.ValueMember = "RemarkNo"
        '
        'Sp_Receiving_SEL_RemarkCheckerRequestBindingSource
        '
        Me.Sp_Receiving_SEL_RemarkCheckerRequestBindingSource.DataMember = "sp_Receiving_SEL_RemarkCheckerRequest"
        Me.Sp_Receiving_SEL_RemarkCheckerRequestBindingSource.DataSource = Me.ReceivingDataSet
        '
        'RequestTypeComboBox
        '
        Me.RequestTypeComboBox.FormattingEnabled = True
        Me.RequestTypeComboBox.ItemHeight = 23
        Me.RequestTypeComboBox.Items.AddRange(New Object() {"Edit", "Delete", "Unlocked"})
        Me.RequestTypeComboBox.Location = New System.Drawing.Point(296, 11)
        Me.RequestTypeComboBox.Name = "RequestTypeComboBox"
        Me.RequestTypeComboBox.Size = New System.Drawing.Size(158, 29)
        Me.RequestTypeComboBox.TabIndex = 134
        Me.RequestTypeComboBox.UseSelectable = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(200, 12)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(84, 19)
        Me.MetroLabel7.TabIndex = 133
        Me.MetroLabel7.Text = "Request type"
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.Location = New System.Drawing.Point(475, 11)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(54, 19)
        Me.MetroLabel4.TabIndex = 18
        Me.MetroLabel4.Text = "Remark"
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource.DataMember = "sp_Receiving_SEL_ReceivingCheckerRequest"
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.ClearBeforeFill = True
        '
        'MetroPanel3
        '
        Me.MetroPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel3.Controls.Add(Me.DiffWeightLabel)
        Me.MetroPanel3.Controls.Add(Me.SumReceivedWeightLabel)
        Me.MetroPanel3.Controls.Add(Me.SumBuyingWeightLabel)
        Me.MetroPanel3.Controls.Add(Me.TotalBaleLabel)
        Me.MetroPanel3.Controls.Add(Me.Label5)
        Me.MetroPanel3.Controls.Add(Me.Label4)
        Me.MetroPanel3.Controls.Add(Me.Label3)
        Me.MetroPanel3.Controls.Add(Me.Label2)
        Me.MetroPanel3.Controls.Add(Me.Label1)
        Me.MetroPanel3.HorizontalScrollbarBarColor = True
        Me.MetroPanel3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.HorizontalScrollbarSize = 10
        Me.MetroPanel3.Location = New System.Drawing.Point(1007, 153)
        Me.MetroPanel3.Name = "MetroPanel3"
        Me.MetroPanel3.Size = New System.Drawing.Size(242, 156)
        Me.MetroPanel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel3.TabIndex = 125
        Me.MetroPanel3.VerticalScrollbarBarColor = True
        Me.MetroPanel3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.VerticalScrollbarSize = 10
        '
        'DiffWeightLabel
        '
        Me.DiffWeightLabel.AutoSize = True
        Me.DiffWeightLabel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiffWeightLabel.Location = New System.Drawing.Point(121, 119)
        Me.DiffWeightLabel.Name = "DiffWeightLabel"
        Me.DiffWeightLabel.Size = New System.Drawing.Size(66, 15)
        Me.DiffWeightLabel.TabIndex = 168
        Me.DiffWeightLabel.Text = "DiffWeight"
        Me.DiffWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SumReceivedWeightLabel
        '
        Me.SumReceivedWeightLabel.AutoSize = True
        Me.SumReceivedWeightLabel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SumReceivedWeightLabel.Location = New System.Drawing.Point(121, 91)
        Me.SumReceivedWeightLabel.Name = "SumReceivedWeightLabel"
        Me.SumReceivedWeightLabel.Size = New System.Drawing.Size(90, 15)
        Me.SumReceivedWeightLabel.TabIndex = 167
        Me.SumReceivedWeightLabel.Text = "ReceivedWeigh"
        Me.SumReceivedWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SumBuyingWeightLabel
        '
        Me.SumBuyingWeightLabel.AutoSize = True
        Me.SumBuyingWeightLabel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SumBuyingWeightLabel.Location = New System.Drawing.Point(121, 60)
        Me.SumBuyingWeightLabel.Name = "SumBuyingWeightLabel"
        Me.SumBuyingWeightLabel.Size = New System.Drawing.Size(83, 15)
        Me.SumBuyingWeightLabel.TabIndex = 166
        Me.SumBuyingWeightLabel.Text = "BuyingWeight"
        Me.SumBuyingWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TotalBaleLabel
        '
        Me.TotalBaleLabel.AutoSize = True
        Me.TotalBaleLabel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalBaleLabel.Location = New System.Drawing.Point(121, 34)
        Me.TotalBaleLabel.Name = "TotalBaleLabel"
        Me.TotalBaleLabel.Size = New System.Drawing.Size(37, 15)
        Me.TotalBaleLabel.TabIndex = 165
        Me.TotalBaleLabel.Text = "Bales"
        Me.TotalBaleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 119)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 15)
        Me.Label5.TabIndex = 164
        Me.Label5.Text = "Diff"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 91)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(105, 15)
        Me.Label4.TabIndex = 163
        Me.Label4.Text = "Weight (Received)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 15)
        Me.Label3.TabIndex = 162
        Me.Label3.Text = "Weight (Buy)"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 15)
        Me.Label2.TabIndex = 161
        Me.Label2.Text = "Bales"
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(240, 20)
        Me.Label1.TabIndex = 160
        Me.Label1.Text = "Total Summary"
        '
        'Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter
        '
        Me.Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource.DataMember = "sp_Receiving_SEL_ReceivingCheckerRequestByEditCase"
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter.ClearBeforeFill = True
        '
        'MetroGrid1
        '
        Me.MetroGrid1.AllowUserToAddRows = False
        Me.MetroGrid1.AllowUserToDeleteRows = False
        Me.MetroGrid1.AllowUserToResizeRows = False
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MetroGrid1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.MetroGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.MetroGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.MetroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.MetroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.MetroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Calibri", 9.75!)
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.MetroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.MetroGrid1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Bales, Me.buyingWeight, Me.receiveWeight, Me.summaryDataGridCompany})
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Arial Narrow", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MetroGrid1.DefaultCellStyle = DataGridViewCellStyle15
        Me.MetroGrid1.EnableHeadersVisualStyles = False
        Me.MetroGrid1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MetroGrid1.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MetroGrid1.Location = New System.Drawing.Point(1006, 315)
        Me.MetroGrid1.Name = "MetroGrid1"
        Me.MetroGrid1.ReadOnly = True
        Me.MetroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Calibri", 9.75!)
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MetroGrid1.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.MetroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.MetroGrid1.RowTemplate.Height = 24
        Me.MetroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MetroGrid1.Size = New System.Drawing.Size(242, 379)
        Me.MetroGrid1.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroGrid1.TabIndex = 126
        '
        'BtnNTRM
        '
        Me.BtnNTRM.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnNTRM.Location = New System.Drawing.Point(1007, 87)
        Me.BtnNTRM.Name = "BtnNTRM"
        Me.BtnNTRM.Size = New System.Drawing.Size(136, 60)
        Me.BtnNTRM.TabIndex = 156
        Me.BtnNTRM.Text = "NTRM"
        Me.BtnNTRM.UseVisualStyleBackColor = True
        '
        'Sp_NTRM_SEL_NTRMInspectionByBCBindingSource
        '
        Me.Sp_NTRM_SEL_NTRMInspectionByBCBindingSource.DataMember = "sp_NTRM_SEL_NTRMInspectionByBC"
        Me.Sp_NTRM_SEL_NTRMInspectionByBCBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter
        '
        Me.Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter.ClearBeforeFill = True
        '
        'NTRMPanel
        '
        Me.NTRMPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NTRMPanel.Controls.Add(Me.MetroLabel40)
        Me.NTRMPanel.Controls.Add(Me.txtNTRMRemak)
        Me.NTRMPanel.Controls.Add(Me.MetroLabel33)
        Me.NTRMPanel.Controls.Add(Me.TxtNTRMQuantity)
        Me.NTRMPanel.Controls.Add(Me.MetroLabel39)
        Me.NTRMPanel.Controls.Add(Me.lblNTRMDetail)
        Me.NTRMPanel.Controls.Add(Me.DataGridNTRMInspection)
        Me.NTRMPanel.Controls.Add(Me.BtnNTRMExit)
        Me.NTRMPanel.Controls.Add(Me.MetroTile2)
        Me.NTRMPanel.HorizontalScrollbarBarColor = True
        Me.NTRMPanel.HorizontalScrollbarHighlightOnWheel = False
        Me.NTRMPanel.HorizontalScrollbarSize = 10
        Me.NTRMPanel.Location = New System.Drawing.Point(37, 198)
        Me.NTRMPanel.Name = "NTRMPanel"
        Me.NTRMPanel.Size = New System.Drawing.Size(950, 729)
        Me.NTRMPanel.Style = MetroFramework.MetroColorStyle.Lime
        Me.NTRMPanel.TabIndex = 157
        Me.NTRMPanel.VerticalScrollbarBarColor = True
        Me.NTRMPanel.VerticalScrollbarHighlightOnWheel = False
        Me.NTRMPanel.VerticalScrollbarSize = 10
        Me.NTRMPanel.Visible = False
        '
        'MetroLabel40
        '
        Me.MetroLabel40.AutoSize = True
        Me.MetroLabel40.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel40.Location = New System.Drawing.Point(11, 563)
        Me.MetroLabel40.Name = "MetroLabel40"
        Me.MetroLabel40.Size = New System.Drawing.Size(46, 19)
        Me.MetroLabel40.TabIndex = 164
        Me.MetroLabel40.Text = "NTRM"
        Me.MetroLabel40.UseCustomBackColor = True
        Me.MetroLabel40.UseCustomForeColor = True
        '
        'txtNTRMRemak
        '
        '
        '
        '
        Me.txtNTRMRemak.CustomButton.Image = Nothing
        Me.txtNTRMRemak.CustomButton.Location = New System.Drawing.Point(244, 1)
        Me.txtNTRMRemak.CustomButton.Name = ""
        Me.txtNTRMRemak.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.txtNTRMRemak.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtNTRMRemak.CustomButton.TabIndex = 1
        Me.txtNTRMRemak.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtNTRMRemak.CustomButton.UseSelectable = True
        Me.txtNTRMRemak.CustomButton.Visible = False
        Me.txtNTRMRemak.Enabled = False
        Me.txtNTRMRemak.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.txtNTRMRemak.Lines = New String(-1) {}
        Me.txtNTRMRemak.Location = New System.Drawing.Point(65, 646)
        Me.txtNTRMRemak.MaxLength = 32767
        Me.txtNTRMRemak.Name = "txtNTRMRemak"
        Me.txtNTRMRemak.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtNTRMRemak.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtNTRMRemak.SelectedText = ""
        Me.txtNTRMRemak.SelectionLength = 0
        Me.txtNTRMRemak.SelectionStart = 0
        Me.txtNTRMRemak.ShortcutsEnabled = True
        Me.txtNTRMRemak.Size = New System.Drawing.Size(278, 35)
        Me.txtNTRMRemak.Style = MetroFramework.MetroColorStyle.Orange
        Me.txtNTRMRemak.TabIndex = 163
        Me.txtNTRMRemak.UseSelectable = True
        Me.txtNTRMRemak.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtNTRMRemak.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel33
        '
        Me.MetroLabel33.AutoSize = True
        Me.MetroLabel33.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel33.Location = New System.Drawing.Point(3, 658)
        Me.MetroLabel33.Name = "MetroLabel33"
        Me.MetroLabel33.Size = New System.Drawing.Size(54, 19)
        Me.MetroLabel33.TabIndex = 162
        Me.MetroLabel33.Text = "Remark"
        Me.MetroLabel33.UseCustomBackColor = True
        Me.MetroLabel33.UseCustomForeColor = True
        '
        'TxtNTRMQuantity
        '
        '
        '
        '
        Me.TxtNTRMQuantity.CustomButton.Image = Nothing
        Me.TxtNTRMQuantity.CustomButton.Location = New System.Drawing.Point(244, 1)
        Me.TxtNTRMQuantity.CustomButton.Name = ""
        Me.TxtNTRMQuantity.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.TxtNTRMQuantity.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TxtNTRMQuantity.CustomButton.TabIndex = 1
        Me.TxtNTRMQuantity.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TxtNTRMQuantity.CustomButton.UseSelectable = True
        Me.TxtNTRMQuantity.CustomButton.Visible = False
        Me.TxtNTRMQuantity.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TxtNTRMQuantity.Lines = New String(-1) {}
        Me.TxtNTRMQuantity.Location = New System.Drawing.Point(65, 594)
        Me.TxtNTRMQuantity.MaxLength = 32767
        Me.TxtNTRMQuantity.Name = "TxtNTRMQuantity"
        Me.TxtNTRMQuantity.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TxtNTRMQuantity.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TxtNTRMQuantity.SelectedText = ""
        Me.TxtNTRMQuantity.SelectionLength = 0
        Me.TxtNTRMQuantity.SelectionStart = 0
        Me.TxtNTRMQuantity.ShortcutsEnabled = True
        Me.TxtNTRMQuantity.Size = New System.Drawing.Size(278, 35)
        Me.TxtNTRMQuantity.Style = MetroFramework.MetroColorStyle.Orange
        Me.TxtNTRMQuantity.TabIndex = 160
        Me.TxtNTRMQuantity.UseSelectable = True
        Me.TxtNTRMQuantity.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TxtNTRMQuantity.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel39
        '
        Me.MetroLabel39.AutoSize = True
        Me.MetroLabel39.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel39.Location = New System.Drawing.Point(26, 600)
        Me.MetroLabel39.Name = "MetroLabel39"
        Me.MetroLabel39.Size = New System.Drawing.Size(32, 19)
        Me.MetroLabel39.TabIndex = 159
        Me.MetroLabel39.Text = "Qty."
        Me.MetroLabel39.UseCustomBackColor = True
        Me.MetroLabel39.UseCustomForeColor = True
        '
        'lblNTRMDetail
        '
        Me.lblNTRMDetail.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblNTRMDetail.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNTRMDetail.Location = New System.Drawing.Point(65, 560)
        Me.lblNTRMDetail.Name = "lblNTRMDetail"
        Me.lblNTRMDetail.Size = New System.Drawing.Size(278, 23)
        Me.lblNTRMDetail.Style = MetroFramework.MetroColorStyle.Lime
        Me.lblNTRMDetail.TabIndex = 158
        '
        'DataGridNTRMInspection
        '
        Me.DataGridNTRMInspection.AllowUserToAddRows = False
        Me.DataGridNTRMInspection.AllowUserToDeleteRows = False
        Me.DataGridNTRMInspection.AllowUserToResizeColumns = False
        Me.DataGridNTRMInspection.AllowUserToResizeRows = False
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridNTRMInspection.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle17
        Me.DataGridNTRMInspection.AutoGenerateColumns = False
        Me.DataGridNTRMInspection.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridNTRMInspection.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridNTRMInspection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridNTRMInspection.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BaleBarcodeDataGridViewTextBoxColumn, Me.NTRMTypeCodeDataGridViewTextBoxColumn, Me.NTRMTYPENAMEDataGridViewTextBoxColumn, Me.InspectionDateDataGridViewTextBoxColumn, Me.QuantityDataGridViewTextBoxColumn, Me.InspectionUserDataGridViewTextBoxColumn, Me.InspectionLocationDataGridViewTextBoxColumn})
        Me.DataGridNTRMInspection.DataSource = Me.Sp_NTRM_SEL_NTRMInspectionByBCBindingSource
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Calibri", 10.2!)
        DataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridNTRMInspection.DefaultCellStyle = DataGridViewCellStyle19
        Me.DataGridNTRMInspection.Location = New System.Drawing.Point(0, 36)
        Me.DataGridNTRMInspection.Name = "DataGridNTRMInspection"
        Me.DataGridNTRMInspection.ReadOnly = True
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Calibri", 9.75!)
        DataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridNTRMInspection.RowHeadersDefaultCellStyle = DataGridViewCellStyle20
        Me.DataGridNTRMInspection.RowHeadersVisible = False
        Me.DataGridNTRMInspection.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.DataGridNTRMInspection.RowsDefaultCellStyle = DataGridViewCellStyle21
        Me.DataGridNTRMInspection.RowTemplate.Height = 24
        Me.DataGridNTRMInspection.Size = New System.Drawing.Size(840, 491)
        Me.DataGridNTRMInspection.TabIndex = 157
        '
        'BaleBarcodeDataGridViewTextBoxColumn
        '
        Me.BaleBarcodeDataGridViewTextBoxColumn.DataPropertyName = "BaleBarcode"
        Me.BaleBarcodeDataGridViewTextBoxColumn.HeaderText = "BaleBarcode"
        Me.BaleBarcodeDataGridViewTextBoxColumn.Name = "BaleBarcodeDataGridViewTextBoxColumn"
        Me.BaleBarcodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.BaleBarcodeDataGridViewTextBoxColumn.Width = 101
        '
        'NTRMTypeCodeDataGridViewTextBoxColumn
        '
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.DataPropertyName = "NTRMTypeCode"
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.HeaderText = "NTRMTypeCode"
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.Name = "NTRMTypeCodeDataGridViewTextBoxColumn"
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.NTRMTypeCodeDataGridViewTextBoxColumn.Width = 115
        '
        'NTRMTYPENAMEDataGridViewTextBoxColumn
        '
        Me.NTRMTYPENAMEDataGridViewTextBoxColumn.DataPropertyName = "NTRMTYPENAME"
        Me.NTRMTYPENAMEDataGridViewTextBoxColumn.HeaderText = "NTRMTYPENAME"
        Me.NTRMTYPENAMEDataGridViewTextBoxColumn.Name = "NTRMTYPENAMEDataGridViewTextBoxColumn"
        Me.NTRMTYPENAMEDataGridViewTextBoxColumn.ReadOnly = True
        Me.NTRMTYPENAMEDataGridViewTextBoxColumn.Width = 121
        '
        'InspectionDateDataGridViewTextBoxColumn
        '
        Me.InspectionDateDataGridViewTextBoxColumn.DataPropertyName = "InspectionDate"
        Me.InspectionDateDataGridViewTextBoxColumn.HeaderText = "InspectionDate"
        Me.InspectionDateDataGridViewTextBoxColumn.Name = "InspectionDateDataGridViewTextBoxColumn"
        Me.InspectionDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.InspectionDateDataGridViewTextBoxColumn.Width = 114
        '
        'QuantityDataGridViewTextBoxColumn
        '
        Me.QuantityDataGridViewTextBoxColumn.DataPropertyName = "Quantity"
        Me.QuantityDataGridViewTextBoxColumn.HeaderText = "Quantity"
        Me.QuantityDataGridViewTextBoxColumn.Name = "QuantityDataGridViewTextBoxColumn"
        Me.QuantityDataGridViewTextBoxColumn.ReadOnly = True
        Me.QuantityDataGridViewTextBoxColumn.Width = 79
        '
        'InspectionUserDataGridViewTextBoxColumn
        '
        Me.InspectionUserDataGridViewTextBoxColumn.DataPropertyName = "InspectionUser"
        Me.InspectionUserDataGridViewTextBoxColumn.HeaderText = "InspectionUser"
        Me.InspectionUserDataGridViewTextBoxColumn.Name = "InspectionUserDataGridViewTextBoxColumn"
        Me.InspectionUserDataGridViewTextBoxColumn.ReadOnly = True
        Me.InspectionUserDataGridViewTextBoxColumn.Width = 114
        '
        'InspectionLocationDataGridViewTextBoxColumn
        '
        Me.InspectionLocationDataGridViewTextBoxColumn.DataPropertyName = "InspectionLocation"
        Me.InspectionLocationDataGridViewTextBoxColumn.HeaderText = "InspectionLocation"
        Me.InspectionLocationDataGridViewTextBoxColumn.Name = "InspectionLocationDataGridViewTextBoxColumn"
        Me.InspectionLocationDataGridViewTextBoxColumn.ReadOnly = True
        Me.InspectionLocationDataGridViewTextBoxColumn.Width = 135
        '
        'BtnNTRMExit
        '
        Me.BtnNTRMExit.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnNTRMExit.Location = New System.Drawing.Point(846, 36)
        Me.BtnNTRMExit.Name = "BtnNTRMExit"
        Me.BtnNTRMExit.Size = New System.Drawing.Size(94, 53)
        Me.BtnNTRMExit.TabIndex = 156
        Me.BtnNTRMExit.Text = "Exit"
        Me.BtnNTRMExit.UseVisualStyleBackColor = True
        '
        'MetroTile2
        '
        Me.MetroTile2.ActiveControl = Nothing
        Me.MetroTile2.AutoSize = True
        Me.MetroTile2.Dock = System.Windows.Forms.DockStyle.Top
        Me.MetroTile2.Location = New System.Drawing.Point(0, 0)
        Me.MetroTile2.Name = "MetroTile2"
        Me.MetroTile2.Size = New System.Drawing.Size(948, 30)
        Me.MetroTile2.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroTile2.TabIndex = 69
        Me.MetroTile2.Text = "NTRM"
        Me.MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile2.UseSelectable = True
        '
        'NTRMatBuying
        '
        Me.NTRMatBuying.AutoSize = True
        Me.NTRMatBuying.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.NTRMatBuying.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.NTRMatBuying.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.NTRMatBuying.ForeColor = System.Drawing.Color.White
        Me.NTRMatBuying.Location = New System.Drawing.Point(1008, 51)
        Me.NTRMatBuying.Name = "NTRMatBuying"
        Me.NTRMatBuying.Size = New System.Drawing.Size(160, 25)
        Me.NTRMatBuying.TabIndex = 159
        Me.NTRMatBuying.Text = "พบข้อมูล NTRM !!"
        Me.NTRMatBuying.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.NTRMatBuying.UseCustomBackColor = True
        Me.NTRMatBuying.UseCustomForeColor = True
        Me.NTRMatBuying.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "supplier"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Supp."
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 60
        '
        'Bales
        '
        Me.Bales.DataPropertyName = "totalBale"
        DataGridViewCellStyle12.Format = "N0"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.Bales.DefaultCellStyle = DataGridViewCellStyle12
        Me.Bales.HeaderText = "Bales"
        Me.Bales.Name = "Bales"
        Me.Bales.ReadOnly = True
        Me.Bales.Width = 60
        '
        'buyingWeight
        '
        Me.buyingWeight.DataPropertyName = "buyingWeight"
        DataGridViewCellStyle13.Format = "N1"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.buyingWeight.DefaultCellStyle = DataGridViewCellStyle13
        Me.buyingWeight.HeaderText = "B.Weight"
        Me.buyingWeight.Name = "buyingWeight"
        Me.buyingWeight.ReadOnly = True
        Me.buyingWeight.Width = 78
        '
        'receiveWeight
        '
        Me.receiveWeight.DataPropertyName = "receiveWeight"
        DataGridViewCellStyle14.Format = "N1"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.receiveWeight.DefaultCellStyle = DataGridViewCellStyle14
        Me.receiveWeight.HeaderText = "R.Weight"
        Me.receiveWeight.Name = "receiveWeight"
        Me.receiveWeight.ReadOnly = True
        Me.receiveWeight.Width = 78
        '
        'summaryDataGridCompany
        '
        Me.summaryDataGridCompany.DataPropertyName = "company"
        Me.summaryDataGridCompany.HeaderText = "company"
        Me.summaryDataGridCompany.Name = "summaryDataGridCompany"
        Me.summaryDataGridCompany.ReadOnly = True
        Me.summaryDataGridCompany.Width = 80
        '
        'FrmAddDocNo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1257, 768)
        Me.Controls.Add(Me.NTRMPanel)
        Me.Controls.Add(Me.BtnNTRM)
        Me.Controls.Add(Me.MetroGrid1)
        Me.Controls.Add(Me.MetroPanel3)
        Me.Controls.Add(Me.MetroPanel2)
        Me.Controls.Add(Me.MatRcMetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Controls.Add(Me.NTRMatBuying)
        Me.Name = "FrmAddDocNo"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MatRcMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatRCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel2.ResumeLayout(False)
        Me.MetroPanel2.PerformLayout()
        CType(Me.Sp_Receiving_SEL_RemarkCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel3.ResumeLayout(False)
        Me.MetroPanel3.PerformLayout()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MetroGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_NTRM_SEL_NTRMInspectionByBCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.NTRMPanel.ResumeLayout(False)
        Me.NTRMPanel.PerformLayout()
        CType(Me.DataGridNTRMInspection, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents MatRcMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents AddDocno As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Receiving_SEL_MatbyRcNoGroupByFieldBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter
    Friend WithEvents LockTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivingNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Receiving_SEL_MatRCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatRCTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCTableAdapter
    Friend WithEvents LockedLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SendRequestTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RequestTypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter
    Friend WithEvents RemarkFromCheckerComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroPanel3 As MetroFramework.Controls.MetroPanel
    Friend WithEvents Sp_Receiving_SEL_RemarkCheckerRequestBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_RemarkCheckerRequestTableAdapter
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter
    Friend WithEvents EditRcNoHeaderTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblRcFrom As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropDataGridViewTextBoxColumn As DataGridViewCheckBoxColumn
    Friend WithEvents supplier As DataGridViewTextBoxColumn
    Friend WithEvents subtype As DataGridViewTextBoxColumn
    Friend WithEvents company As DataGridViewTextBoxColumn
    Friend WithEvents docno As DataGridViewTextBoxColumn
    Friend WithEvents bc As DataGridViewTextBoxColumn
    Friend WithEvents baleno As DataGridViewTextBoxColumn
    Friend WithEvents green As DataGridViewTextBoxColumn
    Friend WithEvents classify As DataGridViewTextBoxColumn
    Friend WithEvents weightbuy As DataGridViewTextBoxColumn
    Friend WithEvents weight As DataGridViewTextBoxColumn
    Friend WithEvents WeightDiff As DataGridViewTextBoxColumn
    Friend WithEvents fromclassify As DataGridViewTextBoxColumn
    Friend WithEvents RemarkChecked As DataGridViewTextBoxColumn
    Friend WithEvents ReplaceBales As DataGridViewCheckBoxColumn
    Friend WithEvents BtnNTRM As System.Windows.Forms.Button
    Friend WithEvents Sp_NTRM_SEL_NTRMInspectionByBCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_NTRM_SEL_NTRMInspectionByBCTableAdapter
    Friend WithEvents NTRMPanel As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel40 As MetroFramework.Controls.MetroLabel
    Friend WithEvents txtNTRMRemak As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel33 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TxtNTRMQuantity As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel39 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lblNTRMDetail As MetroFramework.Controls.MetroLabel
    Friend WithEvents DataGridNTRMInspection As System.Windows.Forms.DataGridView
    Friend WithEvents BtnNTRMExit As System.Windows.Forms.Button
    Friend WithEvents MetroTile2 As MetroFramework.Controls.MetroTile
    Friend WithEvents BaleBarcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NTRMTypeCodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NTRMTYPENAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InspectionDateDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QuantityDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InspectionUserDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InspectionLocationDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NTRMatBuying As MetroFramework.Controls.MetroLabel
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DiffWeightLabel As Label
    Friend WithEvents SumReceivedWeightLabel As Label
    Friend WithEvents SumBuyingWeightLabel As Label
    Friend WithEvents TotalBaleLabel As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents Bales As DataGridViewTextBoxColumn
    Friend WithEvents buyingWeight As DataGridViewTextBoxColumn
    Friend WithEvents receiveWeight As DataGridViewTextBoxColumn
    Friend WithEvents summaryDataGridCompany As DataGridViewTextBoxColumn
    Friend WithEvents MetroGrid1 As MetroFramework.Controls.MetroGrid
End Class
