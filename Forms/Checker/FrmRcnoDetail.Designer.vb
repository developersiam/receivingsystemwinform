﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmRcnoDetail
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.RefreshButton = New System.Windows.Forms.Button()
        Me.ChangeUserButton = New MetroFramework.Controls.MetroTile()
        Me.UsernameLabel = New MetroFramework.Controls.MetroLabel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReceivingDetailGroupBox = New System.Windows.Forms.GroupBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.BuyerComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel27 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel25 = New MetroFramework.Controls.MetroLabel()
        Me.TypeLabel = New MetroFramework.Controls.MetroLabel()
        Me.rcnoLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel22 = New MetroFramework.Controls.MetroLabel()
        Me.CropLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.CompanyLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel21 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.InvoiceNoLabel = New MetroFramework.Controls.MetroLabel()
        Me.ClassifierComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel49 = New MetroFramework.Controls.MetroLabel()
        Me.RcLinesLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.RcfromLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel48 = New MetroFramework.Controls.MetroLabel()
        Me.PlaceLabel = New MetroFramework.Controls.MetroLabel()
        Me.TransportationDocumentCodeLabel = New MetroFramework.Controls.MetroLabel()
        Me.TrucknoLabel = New MetroFramework.Controls.MetroLabel()
        Me.SummaryReceivingGroupBox = New System.Windows.Forms.GroupBox()
        Me.TotalBaleLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.TotalReceivedWeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.TotalBuyingWeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.DiffWeightLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel36 = New MetroFramework.Controls.MetroLabel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.VieeTransportDetailButton = New System.Windows.Forms.Button()
        Me.TotalBaleOnTruckLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel54 = New MetroFramework.Controls.MetroLabel()
        Me.RemainingTruckReceivingLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel50 = New MetroFramework.Controls.MetroLabel()
        Me.ReceivingDataGrid = New MetroFramework.Controls.MetroGrid()
        Me.bc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.farmer_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.baleno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.supplier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.green = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classify = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weightbuy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemarkChecked = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.classifier = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReplaceBales = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MetroPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.ReceivingDetailGroupBox.SuspendLayout()
        Me.SummaryReceivingGroupBox.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.ReceivingDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.RefreshButton)
        Me.MetroPanel1.Controls.Add(Me.ChangeUserButton)
        Me.MetroPanel1.Controls.Add(Me.UsernameLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 12
        Me.MetroPanel1.Location = New System.Drawing.Point(31, 16)
        Me.MetroPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1612, 71)
        Me.MetroPanel1.TabIndex = 117
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 13
        '
        'RefreshButton
        '
        Me.RefreshButton.AutoSize = True
        Me.RefreshButton.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RefreshButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.RefreshButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshButton.FlatAppearance.BorderSize = 0
        Me.RefreshButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.RefreshButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.RefreshButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RefreshButton.ForeColor = System.Drawing.Color.Black
        Me.RefreshButton.Image = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.RefreshButton.Location = New System.Drawing.Point(336, 4)
        Me.RefreshButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.RefreshButton.Name = "RefreshButton"
        Me.RefreshButton.Size = New System.Drawing.Size(73, 63)
        Me.RefreshButton.TabIndex = 175
        Me.RefreshButton.Text = "refresh"
        Me.RefreshButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.RefreshButton.UseVisualStyleBackColor = True
        '
        'ChangeUserButton
        '
        Me.ChangeUserButton.ActiveControl = Nothing
        Me.ChangeUserButton.AutoSize = True
        Me.ChangeUserButton.BackColor = System.Drawing.Color.White
        Me.ChangeUserButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ChangeUserButton.Location = New System.Drawing.Point(12, 16)
        Me.ChangeUserButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ChangeUserButton.Name = "ChangeUserButton"
        Me.ChangeUserButton.Size = New System.Drawing.Size(37, 34)
        Me.ChangeUserButton.Style = MetroFramework.MetroColorStyle.White
        Me.ChangeUserButton.TabIndex = 111
        Me.ChangeUserButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ChangeUserButton.TileImage = Global.ReceivingSystem.My.Resources.Resources.icons8_contacts_32
        Me.ChangeUserButton.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ChangeUserButton.UseSelectable = True
        Me.ChangeUserButton.UseTileImage = True
        '
        'UsernameLabel
        '
        Me.UsernameLabel.AutoSize = True
        Me.UsernameLabel.Location = New System.Drawing.Point(57, 21)
        Me.UsernameLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.UsernameLabel.Name = "UsernameLabel"
        Me.UsernameLabel.Size = New System.Drawing.Size(73, 20)
        Me.UsernameLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameLabel.TabIndex = 112
        Me.UsernameLabel.Text = "Username"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.ReceivingDetailGroupBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.SummaryReceivingGroupBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox5)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(31, 95)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(328, 793)
        Me.FlowLayoutPanel1.TabIndex = 173
        '
        'ReceivingDetailGroupBox
        '
        Me.ReceivingDetailGroupBox.AutoSize = True
        Me.ReceivingDetailGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel2)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.BuyerComboBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel27)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel25)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.TypeLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.rcnoLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel22)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.CropLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel5)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel20)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.CompanyLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel21)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel3)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.InvoiceNoLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.ClassifierComboBox)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel49)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.RcLinesLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel9)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.RcfromLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel13)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.MetroLabel48)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.PlaceLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.TransportationDocumentCodeLabel)
        Me.ReceivingDetailGroupBox.Controls.Add(Me.TrucknoLabel)
        Me.ReceivingDetailGroupBox.Location = New System.Drawing.Point(4, 4)
        Me.ReceivingDetailGroupBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ReceivingDetailGroupBox.Name = "ReceivingDetailGroupBox"
        Me.ReceivingDetailGroupBox.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ReceivingDetailGroupBox.Size = New System.Drawing.Size(315, 439)
        Me.ReceivingDetailGroupBox.TabIndex = 169
        Me.ReceivingDetailGroupBox.TabStop = False
        Me.ReceivingDetailGroupBox.Text = "Receiving Detail"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel2.Location = New System.Drawing.Point(8, 386)
        Me.MetroLabel2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(45, 20)
        Me.MetroLabel2.TabIndex = 173
        Me.MetroLabel2.Text = "buyer"
        Me.MetroLabel2.UseCustomBackColor = True
        Me.MetroLabel2.UseCustomForeColor = True
        '
        'BuyerComboBox
        '
        Me.BuyerComboBox.BackColor = System.Drawing.Color.White
        Me.BuyerComboBox.DisplayMember = "name"
        Me.BuyerComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.BuyerComboBox.FormattingEnabled = True
        Me.BuyerComboBox.ItemHeight = 21
        Me.BuyerComboBox.Location = New System.Drawing.Point(147, 389)
        Me.BuyerComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BuyerComboBox.Name = "BuyerComboBox"
        Me.BuyerComboBox.Size = New System.Drawing.Size(159, 27)
        Me.BuyerComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.BuyerComboBox.TabIndex = 172
        Me.BuyerComboBox.UseCustomBackColor = True
        Me.BuyerComboBox.UseCustomForeColor = True
        Me.BuyerComboBox.UseSelectable = True
        Me.BuyerComboBox.ValueMember = "name"
        '
        'MetroLabel27
        '
        Me.MetroLabel27.AutoSize = True
        Me.MetroLabel27.Location = New System.Drawing.Point(8, 20)
        Me.MetroLabel27.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel27.Name = "MetroLabel27"
        Me.MetroLabel27.Size = New System.Drawing.Size(37, 20)
        Me.MetroLabel27.TabIndex = 171
        Me.MetroLabel27.Text = "rcno"
        '
        'MetroLabel25
        '
        Me.MetroLabel25.AutoSize = True
        Me.MetroLabel25.Location = New System.Drawing.Point(8, 86)
        Me.MetroLabel25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel25.Name = "MetroLabel25"
        Me.MetroLabel25.Size = New System.Drawing.Size(36, 20)
        Me.MetroLabel25.TabIndex = 169
        Me.MetroLabel25.Text = "type"
        '
        'TypeLabel
        '
        Me.TypeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TypeLabel.Location = New System.Drawing.Point(147, 86)
        Me.TypeLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TypeLabel.Name = "TypeLabel"
        Me.TypeLabel.Size = New System.Drawing.Size(160, 28)
        Me.TypeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TypeLabel.TabIndex = 168
        Me.TypeLabel.Text = "type"
        Me.TypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rcnoLabel
        '
        Me.rcnoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.rcnoLabel.Location = New System.Drawing.Point(147, 20)
        Me.rcnoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.rcnoLabel.Name = "rcnoLabel"
        Me.rcnoLabel.Size = New System.Drawing.Size(160, 28)
        Me.rcnoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.rcnoLabel.TabIndex = 170
        Me.rcnoLabel.Text = "rcno"
        Me.rcnoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel22
        '
        Me.MetroLabel22.AutoSize = True
        Me.MetroLabel22.Location = New System.Drawing.Point(8, 53)
        Me.MetroLabel22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel22.Name = "MetroLabel22"
        Me.MetroLabel22.Size = New System.Drawing.Size(37, 20)
        Me.MetroLabel22.TabIndex = 167
        Me.MetroLabel22.Text = "crop"
        '
        'CropLabel
        '
        Me.CropLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CropLabel.Location = New System.Drawing.Point(147, 53)
        Me.CropLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CropLabel.Name = "CropLabel"
        Me.CropLabel.Size = New System.Drawing.Size(160, 28)
        Me.CropLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CropLabel.TabIndex = 166
        Me.CropLabel.Text = "crop"
        Me.CropLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.Location = New System.Drawing.Point(8, 286)
        Me.MetroLabel5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(63, 20)
        Me.MetroLabel5.TabIndex = 144
        Me.MetroLabel5.Text = "truck no."
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.Location = New System.Drawing.Point(8, 119)
        Me.MetroLabel20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(66, 20)
        Me.MetroLabel20.TabIndex = 76
        Me.MetroLabel20.Text = "company"
        '
        'CompanyLabel
        '
        Me.CompanyLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.CompanyLabel.Location = New System.Drawing.Point(147, 119)
        Me.CompanyLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CompanyLabel.Name = "CompanyLabel"
        Me.CompanyLabel.Size = New System.Drawing.Size(160, 28)
        Me.CompanyLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.CompanyLabel.TabIndex = 77
        Me.CompanyLabel.Text = "company"
        Me.CompanyLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel21
        '
        Me.MetroLabel21.AutoSize = True
        Me.MetroLabel21.Location = New System.Drawing.Point(8, 153)
        Me.MetroLabel21.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel21.Name = "MetroLabel21"
        Me.MetroLabel21.Size = New System.Drawing.Size(50, 20)
        Me.MetroLabel21.TabIndex = 80
        Me.MetroLabel21.Text = "rcfrom"
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.ForeColor = System.Drawing.Color.Black
        Me.MetroLabel3.Location = New System.Drawing.Point(8, 348)
        Me.MetroLabel3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(61, 20)
        Me.MetroLabel3.TabIndex = 140
        Me.MetroLabel3.Text = "classifier"
        Me.MetroLabel3.UseCustomBackColor = True
        Me.MetroLabel3.UseCustomForeColor = True
        '
        'InvoiceNoLabel
        '
        Me.InvoiceNoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.InvoiceNoLabel.Location = New System.Drawing.Point(147, 252)
        Me.InvoiceNoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.InvoiceNoLabel.Name = "InvoiceNoLabel"
        Me.InvoiceNoLabel.Size = New System.Drawing.Size(160, 28)
        Me.InvoiceNoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.InvoiceNoLabel.TabIndex = 142
        Me.InvoiceNoLabel.Text = "invoice#"
        Me.InvoiceNoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ClassifierComboBox
        '
        Me.ClassifierComboBox.BackColor = System.Drawing.Color.White
        Me.ClassifierComboBox.DisplayMember = "name"
        Me.ClassifierComboBox.FontSize = MetroFramework.MetroComboBoxSize.Small
        Me.ClassifierComboBox.FormattingEnabled = True
        Me.ClassifierComboBox.ItemHeight = 21
        Me.ClassifierComboBox.Location = New System.Drawing.Point(147, 351)
        Me.ClassifierComboBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ClassifierComboBox.Name = "ClassifierComboBox"
        Me.ClassifierComboBox.Size = New System.Drawing.Size(159, 27)
        Me.ClassifierComboBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.ClassifierComboBox.TabIndex = 139
        Me.ClassifierComboBox.UseCustomBackColor = True
        Me.ClassifierComboBox.UseCustomForeColor = True
        Me.ClassifierComboBox.UseSelectable = True
        Me.ClassifierComboBox.ValueMember = "name"
        '
        'MetroLabel49
        '
        Me.MetroLabel49.AutoSize = True
        Me.MetroLabel49.Location = New System.Drawing.Point(8, 252)
        Me.MetroLabel49.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel49.Name = "MetroLabel49"
        Me.MetroLabel49.Size = New System.Drawing.Size(76, 20)
        Me.MetroLabel49.TabIndex = 144
        Me.MetroLabel49.Text = "invoice no."
        '
        'RcLinesLabel
        '
        Me.RcLinesLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RcLinesLabel.Location = New System.Drawing.Point(147, 319)
        Me.RcLinesLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RcLinesLabel.Name = "RcLinesLabel"
        Me.RcLinesLabel.Size = New System.Drawing.Size(160, 28)
        Me.RcLinesLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.RcLinesLabel.TabIndex = 138
        Me.RcLinesLabel.Text = "rc lines"
        Me.RcLinesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.Location = New System.Drawing.Point(8, 319)
        Me.MetroLabel9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(37, 20)
        Me.MetroLabel9.TabIndex = 137
        Me.MetroLabel9.Text = "lines"
        '
        'RcfromLabel
        '
        Me.RcfromLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RcfromLabel.Location = New System.Drawing.Point(147, 153)
        Me.RcfromLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RcfromLabel.Name = "RcfromLabel"
        Me.RcfromLabel.Size = New System.Drawing.Size(160, 28)
        Me.RcfromLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.RcfromLabel.TabIndex = 81
        Me.RcfromLabel.Text = "rcfrom"
        Me.RcfromLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.Location = New System.Drawing.Point(8, 186)
        Me.MetroLabel13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(42, 20)
        Me.MetroLabel13.TabIndex = 72
        Me.MetroLabel13.Text = "place"
        '
        'MetroLabel48
        '
        Me.MetroLabel48.AutoSize = True
        Me.MetroLabel48.Location = New System.Drawing.Point(8, 219)
        Me.MetroLabel48.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel48.Name = "MetroLabel48"
        Me.MetroLabel48.Size = New System.Drawing.Size(100, 20)
        Me.MetroLabel48.TabIndex = 143
        Me.MetroLabel48.Text = "transport code"
        '
        'PlaceLabel
        '
        Me.PlaceLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PlaceLabel.Location = New System.Drawing.Point(147, 186)
        Me.PlaceLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.PlaceLabel.Name = "PlaceLabel"
        Me.PlaceLabel.Size = New System.Drawing.Size(160, 28)
        Me.PlaceLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.PlaceLabel.TabIndex = 73
        Me.PlaceLabel.Text = "place"
        Me.PlaceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TransportationDocumentCodeLabel
        '
        Me.TransportationDocumentCodeLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TransportationDocumentCodeLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.TransportationDocumentCodeLabel.Location = New System.Drawing.Point(147, 219)
        Me.TransportationDocumentCodeLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TransportationDocumentCodeLabel.Name = "TransportationDocumentCodeLabel"
        Me.TransportationDocumentCodeLabel.Size = New System.Drawing.Size(160, 28)
        Me.TransportationDocumentCodeLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TransportationDocumentCodeLabel.TabIndex = 141
        Me.TransportationDocumentCodeLabel.Text = "transportCode"
        Me.TransportationDocumentCodeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TrucknoLabel
        '
        Me.TrucknoLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TrucknoLabel.FontSize = MetroFramework.MetroLabelSize.Small
        Me.TrucknoLabel.Location = New System.Drawing.Point(147, 286)
        Me.TrucknoLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TrucknoLabel.Name = "TrucknoLabel"
        Me.TrucknoLabel.Size = New System.Drawing.Size(160, 28)
        Me.TrucknoLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TrucknoLabel.TabIndex = 71
        Me.TrucknoLabel.Text = "truckNo"
        Me.TrucknoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'SummaryReceivingGroupBox
        '
        Me.SummaryReceivingGroupBox.Controls.Add(Me.TotalBaleLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel16)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.TotalReceivedWeightLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel14)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.TotalBuyingWeightLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel12)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.DiffWeightLabel)
        Me.SummaryReceivingGroupBox.Controls.Add(Me.MetroLabel36)
        Me.SummaryReceivingGroupBox.Location = New System.Drawing.Point(4, 451)
        Me.SummaryReceivingGroupBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SummaryReceivingGroupBox.Name = "SummaryReceivingGroupBox"
        Me.SummaryReceivingGroupBox.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.SummaryReceivingGroupBox.Size = New System.Drawing.Size(320, 169)
        Me.SummaryReceivingGroupBox.TabIndex = 171
        Me.SummaryReceivingGroupBox.TabStop = False
        Me.SummaryReceivingGroupBox.Text = "Summary Receiving"
        '
        'TotalBaleLabel
        '
        Me.TotalBaleLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalBaleLabel.Location = New System.Drawing.Point(152, 20)
        Me.TotalBaleLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TotalBaleLabel.Name = "TotalBaleLabel"
        Me.TotalBaleLabel.Size = New System.Drawing.Size(160, 28)
        Me.TotalBaleLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalBaleLabel.TabIndex = 75
        Me.TotalBaleLabel.Text = "total bales"
        Me.TotalBaleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.Location = New System.Drawing.Point(8, 25)
        Me.MetroLabel16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(41, 20)
        Me.MetroLabel16.TabIndex = 70
        Me.MetroLabel16.Text = "bales"
        '
        'TotalReceivedWeightLabel
        '
        Me.TotalReceivedWeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalReceivedWeightLabel.Location = New System.Drawing.Point(152, 123)
        Me.TotalReceivedWeightLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TotalReceivedWeightLabel.Name = "TotalReceivedWeightLabel"
        Me.TotalReceivedWeightLabel.Size = New System.Drawing.Size(160, 28)
        Me.TotalReceivedWeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalReceivedWeightLabel.TabIndex = 76
        Me.TotalReceivedWeightLabel.Text = "total received weight"
        Me.TotalReceivedWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.Location = New System.Drawing.Point(8, 121)
        Me.MetroLabel14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(111, 20)
        Me.MetroLabel14.TabIndex = 74
        Me.MetroLabel14.Text = "receiving weight"
        '
        'TotalBuyingWeightLabel
        '
        Me.TotalBuyingWeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalBuyingWeightLabel.Location = New System.Drawing.Point(152, 89)
        Me.TotalBuyingWeightLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TotalBuyingWeightLabel.Name = "TotalBuyingWeightLabel"
        Me.TotalBuyingWeightLabel.Size = New System.Drawing.Size(160, 28)
        Me.TotalBuyingWeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalBuyingWeightLabel.TabIndex = 78
        Me.TotalBuyingWeightLabel.Text = "total buying weight"
        Me.TotalBuyingWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.Location = New System.Drawing.Point(8, 89)
        Me.MetroLabel12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(96, 20)
        Me.MetroLabel12.TabIndex = 77
        Me.MetroLabel12.Text = "buying weight"
        '
        'DiffWeightLabel
        '
        Me.DiffWeightLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DiffWeightLabel.Location = New System.Drawing.Point(152, 54)
        Me.DiffWeightLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.DiffWeightLabel.Name = "DiffWeightLabel"
        Me.DiffWeightLabel.Size = New System.Drawing.Size(160, 28)
        Me.DiffWeightLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.DiffWeightLabel.TabIndex = 80
        Me.DiffWeightLabel.Text = "diff weight"
        Me.DiffWeightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel36
        '
        Me.MetroLabel36.AutoSize = True
        Me.MetroLabel36.Location = New System.Drawing.Point(8, 57)
        Me.MetroLabel36.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel36.Name = "MetroLabel36"
        Me.MetroLabel36.Size = New System.Drawing.Size(73, 20)
        Me.MetroLabel36.TabIndex = 79
        Me.MetroLabel36.Text = "diff weight"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.VieeTransportDetailButton)
        Me.GroupBox5.Controls.Add(Me.TotalBaleOnTruckLabel)
        Me.GroupBox5.Controls.Add(Me.MetroLabel54)
        Me.GroupBox5.Controls.Add(Me.RemainingTruckReceivingLabel)
        Me.GroupBox5.Controls.Add(Me.MetroLabel50)
        Me.GroupBox5.Location = New System.Drawing.Point(4, 628)
        Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox5.Size = New System.Drawing.Size(320, 161)
        Me.GroupBox5.TabIndex = 175
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Truck/invoice detail"
        '
        'VieeTransportDetailButton
        '
        Me.VieeTransportDetailButton.Font = New System.Drawing.Font("Calibri", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VieeTransportDetailButton.Location = New System.Drawing.Point(101, 108)
        Me.VieeTransportDetailButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.VieeTransportDetailButton.Name = "VieeTransportDetailButton"
        Me.VieeTransportDetailButton.Size = New System.Drawing.Size(108, 31)
        Me.VieeTransportDetailButton.TabIndex = 156
        Me.VieeTransportDetailButton.Text = "view detail"
        Me.VieeTransportDetailButton.UseVisualStyleBackColor = True
        '
        'TotalBaleOnTruckLabel
        '
        Me.TotalBaleOnTruckLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TotalBaleOnTruckLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.TotalBaleOnTruckLabel.Location = New System.Drawing.Point(103, 30)
        Me.TotalBaleOnTruckLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.TotalBaleOnTruckLabel.Name = "TotalBaleOnTruckLabel"
        Me.TotalBaleOnTruckLabel.Size = New System.Drawing.Size(107, 31)
        Me.TotalBaleOnTruckLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.TotalBaleOnTruckLabel.TabIndex = 75
        Me.TotalBaleOnTruckLabel.Text = "0"
        Me.TotalBaleOnTruckLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel54
        '
        Me.MetroLabel54.AutoSize = True
        Me.MetroLabel54.Location = New System.Drawing.Point(8, 30)
        Me.MetroLabel54.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel54.Name = "MetroLabel54"
        Me.MetroLabel54.Size = New System.Drawing.Size(35, 20)
        Me.MetroLabel54.TabIndex = 70
        Me.MetroLabel54.Text = "total"
        '
        'RemainingTruckReceivingLabel
        '
        Me.RemainingTruckReceivingLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RemainingTruckReceivingLabel.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.RemainingTruckReceivingLabel.Location = New System.Drawing.Point(103, 69)
        Me.RemainingTruckReceivingLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.RemainingTruckReceivingLabel.Name = "RemainingTruckReceivingLabel"
        Me.RemainingTruckReceivingLabel.Size = New System.Drawing.Size(107, 31)
        Me.RemainingTruckReceivingLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.RemainingTruckReceivingLabel.TabIndex = 78
        Me.RemainingTruckReceivingLabel.Text = "0"
        Me.RemainingTruckReceivingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'MetroLabel50
        '
        Me.MetroLabel50.AutoSize = True
        Me.MetroLabel50.Location = New System.Drawing.Point(8, 73)
        Me.MetroLabel50.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.MetroLabel50.Name = "MetroLabel50"
        Me.MetroLabel50.Size = New System.Drawing.Size(71, 20)
        Me.MetroLabel50.TabIndex = 77
        Me.MetroLabel50.Text = "remaining"
        '
        'ReceivingDataGrid
        '
        Me.ReceivingDataGrid.AllowUserToAddRows = False
        Me.ReceivingDataGrid.AllowUserToDeleteRows = False
        Me.ReceivingDataGrid.AllowUserToResizeRows = False
        Me.ReceivingDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.ReceivingDataGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.ReceivingDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ReceivingDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ReceivingDataGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.ReceivingDataGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReceivingDataGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.ReceivingDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ReceivingDataGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bc, Me.farmer_code, Me.baleno, Me.supplier, Me.green, Me.classify, Me.weightbuy, Me.weight, Me.RemarkChecked, Me.DataGridViewTextBoxColumn1, Me.classifier, Me.ReplaceBales})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.ReceivingDataGrid.DefaultCellStyle = DataGridViewCellStyle4
        Me.ReceivingDataGrid.EnableHeadersVisualStyles = False
        Me.ReceivingDataGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.ReceivingDataGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ReceivingDataGrid.Location = New System.Drawing.Point(367, 95)
        Me.ReceivingDataGrid.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ReceivingDataGrid.Name = "ReceivingDataGrid"
        Me.ReceivingDataGrid.ReadOnly = True
        Me.ReceivingDataGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReceivingDataGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.ReceivingDataGrid.RowHeadersVisible = False
        Me.ReceivingDataGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.ReceivingDataGrid.RowTemplate.Height = 24
        Me.ReceivingDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ReceivingDataGrid.Size = New System.Drawing.Size(1276, 795)
        Me.ReceivingDataGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.ReceivingDataGrid.TabIndex = 174
        '
        'bc
        '
        Me.bc.DataPropertyName = "bc"
        Me.bc.HeaderText = "bale barcode"
        Me.bc.Name = "bc"
        Me.bc.ReadOnly = True
        Me.bc.Width = 136
        '
        'farmer_code
        '
        Me.farmer_code.DataPropertyName = "farmer_code"
        Me.farmer_code.HeaderText = "farmer id"
        Me.farmer_code.Name = "farmer_code"
        Me.farmer_code.ReadOnly = True
        Me.farmer_code.Width = 108
        '
        'baleno
        '
        Me.baleno.DataPropertyName = "baleno"
        Me.baleno.HeaderText = "baleno"
        Me.baleno.Name = "baleno"
        Me.baleno.ReadOnly = True
        Me.baleno.Width = 89
        '
        'supplier
        '
        Me.supplier.DataPropertyName = "supplier"
        Me.supplier.HeaderText = "supplier"
        Me.supplier.Name = "supplier"
        Me.supplier.ReadOnly = True
        Me.supplier.Width = 99
        '
        'green
        '
        Me.green.DataPropertyName = "green"
        Me.green.HeaderText = "green"
        Me.green.Name = "green"
        Me.green.ReadOnly = True
        Me.green.Width = 81
        '
        'classify
        '
        Me.classify.DataPropertyName = "classify"
        Me.classify.HeaderText = "classify"
        Me.classify.Name = "classify"
        Me.classify.ReadOnly = True
        Me.classify.Width = 93
        '
        'weightbuy
        '
        Me.weightbuy.DataPropertyName = "weightbuy"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.weightbuy.DefaultCellStyle = DataGridViewCellStyle2
        Me.weightbuy.HeaderText = "b kg."
        Me.weightbuy.Name = "weightbuy"
        Me.weightbuy.ReadOnly = True
        Me.weightbuy.Width = 74
        '
        'weight
        '
        Me.weight.DataPropertyName = "weight"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.weight.DefaultCellStyle = DataGridViewCellStyle3
        Me.weight.HeaderText = "r kg."
        Me.weight.Name = "weight"
        Me.weight.ReadOnly = True
        Me.weight.Width = 71
        '
        'RemarkChecked
        '
        Me.RemarkChecked.DataPropertyName = "RemarkChecked"
        Me.RemarkChecked.HeaderText = "NTRM"
        Me.RemarkChecked.Name = "RemarkChecked"
        Me.RemarkChecked.ReadOnly = True
        Me.RemarkChecked.Visible = False
        Me.RemarkChecked.Width = 68
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "RemarkChecked"
        Me.DataGridViewTextBoxColumn1.HeaderText = "remark"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Visible = False
        Me.DataGridViewTextBoxColumn1.Width = 75
        '
        'classifier
        '
        Me.classifier.DataPropertyName = "classifier"
        Me.classifier.HeaderText = "classifier"
        Me.classifier.Name = "classifier"
        Me.classifier.ReadOnly = True
        Me.classifier.Width = 104
        '
        'ReplaceBales
        '
        Me.ReplaceBales.DataPropertyName = "ReplaceBales"
        Me.ReplaceBales.HeaderText = "replace bale"
        Me.ReplaceBales.Name = "ReplaceBales"
        Me.ReplaceBales.ReadOnly = True
        Me.ReplaceBales.Width = 106
        '
        'FrmRcnoDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1748, 911)
        Me.Controls.Add(Me.ReceivingDataGrid)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "FrmRcnoDetail"
        Me.Padding = New System.Windows.Forms.Padding(27, 74, 27, 25)
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ReceivingDetailGroupBox.ResumeLayout(False)
        Me.ReceivingDetailGroupBox.PerformLayout()
        Me.SummaryReceivingGroupBox.ResumeLayout(False)
        Me.SummaryReceivingGroupBox.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.ReceivingDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshButton As Button
    Friend WithEvents ChangeUserButton As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents ReceivingDetailGroupBox As GroupBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BuyerComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel27 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel25 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TypeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents rcnoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel22 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CropLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CompanyLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel21 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvoiceNoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents ClassifierComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel49 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RcLinesLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RcfromLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel48 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PlaceLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents TransportationDocumentCodeLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents TrucknoLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents SummaryReceivingGroupBox As GroupBox
    Friend WithEvents TotalBaleLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalReceivedWeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents TotalBuyingWeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents DiffWeightLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel36 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ReceivingDataGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents bc As DataGridViewTextBoxColumn
    Friend WithEvents farmer_code As DataGridViewTextBoxColumn
    Friend WithEvents baleno As DataGridViewTextBoxColumn
    Friend WithEvents supplier As DataGridViewTextBoxColumn
    Friend WithEvents green As DataGridViewTextBoxColumn
    Friend WithEvents classify As DataGridViewTextBoxColumn
    Friend WithEvents weightbuy As DataGridViewTextBoxColumn
    Friend WithEvents weight As DataGridViewTextBoxColumn
    Friend WithEvents RemarkChecked As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents classifier As DataGridViewTextBoxColumn
    Friend WithEvents ReplaceBales As DataGridViewCheckBoxColumn
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents VieeTransportDetailButton As Button
    Friend WithEvents TotalBaleOnTruckLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel54 As MetroFramework.Controls.MetroLabel
    Friend WithEvents RemainingTruckReceivingLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel50 As MetroFramework.Controls.MetroLabel
End Class
