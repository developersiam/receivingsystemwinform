﻿Imports FastMember

Public Class FrmAddDocNo
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim XRequestType As String 'เก็บค่า requesttype ที่userเลือก
    Dim _matList As List(Of sp_Receiving_SEL_MatByRcNo_Result)
    Dim _matrc As matrc
    Public _rcno As String

    Private Sub ReceivingDetailsBinding() ' Sub แสดงข้อมูล countOFbaleBarcode, SumOfBuyingweight,SumOfReceivingweight
        Try
            _matList = _businessLayerService.ReceivingBL().GetMatByRcno(_rcno)

            Dim totalWeight = _matList.Sum(Function(x) x.weight)
            Dim totalWeightBuy = _matList.Sum(Function(x) x.weightbuy)

            TotalBaleLabel.Text = _matList.Count.ToString("N0")
            SumReceivedWeightLabel.Text = Convert.ToDecimal(totalWeight).ToString("N1")
            SumBuyingWeightLabel.Text = Convert.ToDecimal(totalWeightBuy).ToString("N1")
            DiffWeightLabel.Text = Convert.ToDecimal(totalWeight - totalWeightBuy).ToString("N1")

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_matList)
            dt.Load(reader)
            MatRcMetroGrid.DataSource = dt

            Dim _matGroup = _matList.GroupBy(Function(x) New With {
                                                 Key x.rcno,
                                                 Key x.subtype,
                                                 Key x.company,
                                                 Key x.supplier}).
                                                 Select(Function(x) New With {
                                                 .rcno = x.Key.rcno,
                                                 .subtype = x.Key.subtype,
                                                 .company = x.Key.company,
                                                 .supplier = x.Key.supplier,
                                                 .totalBale = x.Count(),
                                                 .buyingWeight = x.Sum(Function(z) z.weightbuy),
                                                 .receiveWeight = x.Sum(Function(z) z.weight)
                                                            }).ToList()
            Dim dt1 As New DataTable()
            Dim reader1 = ObjectReader.Create(_matGroup)
            dt1.Load(reader1)
            MetroGrid1.DataSource = dt1

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ReceivingHeaderBinding()
        Try
            _matrc = _businessLayerService.ReceivingDocumentBL().GetMatRcByRcno(_rcno)
            If _matrc Is Nothing Then
                Throw New ArgumentException("ไม่พบข้อมูล rcno นี้ในระบบ โปรดตรวจสอบกับทางไอทีอีกครั้ง")
            End If

            'ถ้าเป็นการ purchasing หรือ  Free ให้ทำการ send request , add docno , eidt header detail ได้ 
            LockedLabel.Visible = _matrc.checkerlocked
            ReceivingNoLabel.Text = _matrc.rcno

            If _matrc.rcfrom = "Purchasing" Or _matrc.rcfrom = "Free" Or _matrc.rcfrom = "Blending" Then
                AddDocno.Visible = True
                MetroLabel2.Visible = True
            Else
                AddDocno.Visible = False
                MetroLabel2.Visible = False
            End If
            'Call ShowAllSummary()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub FrmChecking_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            UsernameMetroLabel.Text = _username
            MetroGrid1.AutoGenerateColumns = False

            ReceivingHeaderBinding()
            ReceivingDetailsBinding()

            Me.Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_RemarkCheckerRequest)

            ' Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
            'Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatbyRcNoGroupByField, _rcno)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    'Private Sub ShowAllSummary() ' Sub แสดงข้อมูล countOFbaleBarcode, SumOfBuyingweight,SumOfReceivingweight
    '    Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
    '    TotalBaleLabel.Text = Me.Sp_Receiving_SEL_MatByRcNoBindingSource.Count

    '    'show sum of buyingweight
    '    Dim XSumOfBuyingWeight As Double
    '    XSumOfBuyingWeight = IIf(IsDBNull(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.Compute("sum(Weightbuy)", "RcNo = '" & _rcno & "' ")), 0, Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.Compute("sum(Weightbuy)", "RcNo = '" & _rcno & "' "))
    '    SumBuyingWeightLabel.Text = XSumOfBuyingWeight.ToString("N2")

    '    'show sum of receivingweight
    '    Dim XSumOfReceivingWeight As Double
    '    XSumOfReceivingWeight = IIf(IsDBNull(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.Compute("sum(Weight)", "RcNo = '" & _rcno & "' ")), 0, Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.Compute("sum(Weight)", "RcNo = '" & _rcno & "' "))
    '    SumReceivedWeightLabel.Text = XSumOfReceivingWeight.ToString("N2")

    '    'show sum of diff (between buyingweight and receivingweight)
    '    Dim XSumOfDiff As Double
    '    XSumOfDiff = (XSumOfReceivingWeight - XSumOfBuyingWeight).ToString("N2")
    '    DiffWeightLabel.Text = XSumOfDiff.ToString("N2")

    'End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Close()
    End Sub

    Private Sub LockTile_Click(sender As Object, e As EventArgs) Handles LockTile.Click
        Try

            'ถ้าเคยlockข้อมูลแล้วจะlockอีกไม่ได้แล้ว
            If _matrc.checkerlocked Then
                Throw New ArgumentException("คุณทำการ Lock เลข rcno นี้แล้ว ไม่สามารถ Lock ได้อีก")
            End If

            Dim list = _businessLayerService.ReceivingBL().GetMatByRcno(_rcno)
            If (list.Where(Function(x) x.docno Is Nothing).Count() > 0) Then
                Throw New ArgumentException("มีห่อยาจำนวน " + list.Where(Function(x) x.docno = "").Count() + " ที่ยังไม่มีเลข docno")
            End If

            Dim result As DialogResult
            result = MessageBox.Show("ต้องการ lock rcno นี้ใช่หรือไม่?", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If result = DialogResult.No Then
                Return
            End If

            Dim rowcount = Me.MatRcMetroGrid.RowCount
            Dim colcount = Me.MatRcMetroGrid.ColumnCount
            db.sp_Receiving_UPD_CheckerLocked(_rcno, _username, True, True)
            MessageBox.Show("ทำการ Locked เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ReceivingHeaderBinding()

            'LockedLabel.Visible = True
            'Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub



    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            ReceivingHeaderBinding()
            ReceivingDetailsBinding()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AddDocno_Click(sender As Object, e As EventArgs) Handles AddDocno.Click
        Try
            If _matList.Where(Function(x) x.docno IsNot Nothing).Count() > 0 Then
                Throw New ArgumentException("ไม่สามารถ Add Docno ได้เนื่องจากมีการระบุ docno ในข้อมูลชุดนี้แล้ว")
            End If

            If _matrc.checkerlocked = True Then
                Throw New ArgumentException("ไม่สามารถ Add Docno ได้เนื่องจาก Checker lock ข้อมูลแล้ว")
            End If

            'Dim matGroup = _matList.GroupBy(Function(x) New With
            '                                    {Key x.subtype,
            '                                    x.company,
            '                                    x.supplier,
            '                                    x.rcno}).Select(Function(y) New With
            '                                    {.subtype = y.Key.subtype,
            '                                    .company = y.Key.company,
            '                                    .supplier = y.Key.supplier,
            '                                    .rcno = y.Key.rcno}).ToList()
            '
            ' ตรงนี้แม่ง ทำไม group by มาแล้วได้ record เดียววะ จริงๆ ควรมี 3
            Dim _matGroup = _matList.GroupBy(Function(x) New With {
                                                 Key x.rcno,
                                                 Key x.subtype,
                                                 Key x.company,
                                                 Key x.supplier}).
                                                 Select(Function(x) New With {
                                                 .rcno = x.Key.rcno,
                                                 .subtype = x.Key.subtype,
                                                 .company = x.Key.company,
                                                 .supplier = x.Key.supplier
                                                            }).
                                                            ToList()
            For Each item In _matGroup.
                OrderBy(Function(x) x.supplier).
                OrderBy(Function(x) x.company)
                db.sp_Receiving_UPD_DocNo(_defaultCrop, item.rcno, item.supplier, item.subtype, item.company)
            Next

            ReceivingHeaderBinding()
            ReceivingDetailsBinding()

            MessageBox.Show("ทำการ Add Docno เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'Dim XChkDocno As Boolean = False
            'Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
            'For Each dr As DataRow In Sp_Receiving_SEL_MatByRcNoTableAdapter.GetData(_rcno)
            '    If IsDBNull(dr.Item("docno")) = True Then
            '        XChkDocno = True
            '    End If
            'Next

            'If XChkDocno = False Then
            '    MessageBox.Show("ไม่สามารถAdd Docno ได้เนื่องจากมีการระบุ docno ในข้อมูลชุดนี้แล้ว", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Return
            'End If

            'Dim bRow As ReceivingDataSet.sp_Receiving_SEL_MatRCRow
            'Me.Sp_Receiving_SEL_MatRCTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatRC, _defaultCrop, _matrc.type, True)
            'bRow = Me.ReceivingDataSet.sp_Receiving_SEL_MatRC.FindByrcno(_rcno)
            'If bRow Is Nothing Then
            '    Me.Sp_Receiving_SEL_MatbyRcNoGroupByFieldTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatbyRcNoGroupByField, _rcno)
            '    For Each row As DataRow In Me.ReceivingDataSet.sp_Receiving_SEL_MatbyRcNoGroupByField.Rows
            '        db.sp_Receiving_UPD_DocNo(_defaultCrop, _rcno, row.Item("supplier"), row.Item("subtype"), row.Item("company"))
            '    Next

            '    MessageBox.Show("ทำการAdd Docno เรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
            'ElseIf Not bRow Is Nothing And bRow.checkerlocked = True Then
            '    MessageBox.Show("ไม่สามารถAdd Docno ได้เนื่องจาก Checker locked ข้อมูลแล้ว", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Return
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RequestTypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RequestTypeComboBox.SelectedIndexChanged
        Try
            XRequestType = RequestTypeComboBox.Text
            RemarkFromCheckerComboBox.Text = ""
            RemarkFromCheckerComboBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SendRequestTile_Click(sender As Object, e As EventArgs) Handles SendRequestTile.Click
        Try
            Dim XCheckedResult As Boolean = True

            'เช็คว่า Leaflocked DocNo ใน RC No. นี้หรือยัง ถ้า =1 แสดงว่าlocked แล้วไม่อนุญาตให้แก้ไขข้อมูล  ลบ หรือขอ ปลดล๊อก
            Dim XLeafLockedResult As Boolean = False
            Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
            For Each row As DataRow In Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.Rows
                If row.Item("Leaflocked") = True Then
                    XLeafLockedResult = True
                End If
            Next

            If XLeafLockedResult = True Then
                MessageBox.Show("LeafAccount ได้ทำการล๊อกข้อมูล DocNo ที่อยู๋ใน RC No.นี้แล้วคุณไม่สามารถทำการขอแก้ไขหรือส่งคำร้องใดๆได้อีก" + Environment.NewLine +
                                "หากต้องการแก้ไขต้องแจ้งแผนก LeafAccount เพื่อทำการลบข้อมูลใน Pricing ก่อนทำการแก้ไขเลข Receiving no.นี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'ให้เลือกประเภท
            If RequestTypeComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือกประเภท Request type", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'เช็คการคีย์หมายเหตุ โดยบังคับให้คีย์ทุกกรณี
            If RemarkFromCheckerComboBox.Text = "" Then
                MessageBox.Show("กรุณาระบุหมายเหตุเพื่อประกอบการพิจารณาที่ต้องการส่งคำร้องไปที่ Supervisor/Manager ของคุณ", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If


            If XRequestType = "Edit" Then  'หากเป็นการขอ Edit ต้องส่ง Barcode ไปด้วย
                'ให้เช็คว่าเคยส่งไปขอEditหรือยัง ถ้าเคยส่งไปแล้วและเป็นหัวข้อเดียวกันแต่ยังไม่ได้ approve ไม่ต้องส่งซ้ำอีก
                Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, "Edit", False)
                For Each dr As DataRow In Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.GetData(_defaultCrop, "Edit", False)
                    Dim dbBaleBarcode As String = dr.Item("BC").ToString  'dbBaleBarcode เก็บค่า Balebarcodeที่ได้จาก tableadapter
                    If XBaleBarcode = dbBaleBarcode Then 'ถ้าbarcode มีใน ReceivingCheckerRequest ให้ตรวจสอบว่าได้รับการ Approve หรือยัง
                        If dr.Item("ApproveStatus").ToString = False Then  'ถ้ายังไม่ได้รับการ approve
                            XCheckedResult = False
                        End If
                    End If
                Next

                If XCheckedResult = False Then
                    MessageBox.Show("คุณได้ทำการส่งคำร้องขอ Edit Barcodeนี้ไปแล้ว ไม่สามารถบันทึกซ้ำได้อีก กรุณาตรวจสอบกับSupervisor/Manager ")
                    Return
                End If


                Dim result As DialogResult
                result = MessageBox.Show("ต้องการขอแก้ไขข้อมูลใช่หรือไม่? หากใช่ คำร้องจะส่งไปที่Suppervisor/Managerของคุณ กรุณารอผล", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    If RemarkFromCheckerComboBox.SelectedValue <> "RM02" Then 'ถ้าเป็นการเลือกแก้ไขกับ barcode
                        db.sp_Receiving_INS_ReceivingCheckerRequest(_defaultCrop, XRequestType, _rcno, XBaleBarcode, _username, RemarkFromCheckerComboBox.SelectedValue)
                    Else 'ถ้าเป็นการแก้ไขหัว header rcno.
                        db.sp_Receiving_INS_ReceivingCheckerRequest(_defaultCrop, XRequestType, _rcno, "", _username, RemarkFromCheckerComboBox.SelectedValue)
                    End If

                    MessageBox.Show("ทำการส่งคำร้องไปเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
                ElseIf result = DialogResult.No Then
                    Return
                End If
            ElseIf XRequestType = "Delete" Then  'หากเป็นการขอ Delete ต้องส่ง Barcode ไปด้วย
                If XBaleBarcode = "" Then
                    MessageBox.Show("กรุณาเลือก Barcode ที่ต้องการลบจากตารางด้านล่าง", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Return
                End If

                'ให้เช็คว่าเคยส่งไปขอDeleteหรือยัง ถ้าเคยส่งไปแล้วและเป็นหัวข้อเดียวกันแต่ยังไม่ได้ approve ไม่ต้องส่งซ้ำอีก
                Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, "Delete", False)
                For Each dr As DataRow In Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.GetData(_defaultCrop, "Delete", False)
                    Dim dbBaleBarcode As String = dr.Item("BC").ToString  'dbBaleBarcode เก็บค่า Balebarcodeที่ได้จาก tableadapter
                    If XBaleBarcode = dbBaleBarcode Then 'ถ้าbarcode มีใน ReceivingCheckerRequest ให้ตรวจสอบว่าได้รับการ Approve หรือยัง
                        If dr.Item("ApproveStatus").ToString = False Then  'ถ้ายังไม่ได้รับการ approve
                            XCheckedResult = False
                        End If
                    End If
                Next

                If XCheckedResult = False Then
                    MessageBox.Show("คุณได้ทำการส่งคำร้องขอ Delete Barcodeนี้ไปแล้ว ไม่สามารถบันทึกซ้ำได้อีก กรุณาตรวจสอบกับSupervisor/Manager ")
                    Return
                End If

                Dim result As DialogResult
                result = MessageBox.Show("ต้องการขอDelete bale barcode " & XBaleBarcode & " ใช่หรือไม่? หากใช่คำร้องจะถูกส่งและSupervisor/ManagerจะทำการDeleteข้อมูลให้", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    db.sp_Receiving_INS_ReceivingCheckerRequest(_defaultCrop, XRequestType, _rcno, XBaleBarcode, _username, RemarkFromCheckerComboBox.SelectedValue)
                    MessageBox.Show("ทำการส่งคำร้องไปเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
                ElseIf result = DialogResult.No Then
                    Return
                End If
            ElseIf XRequestType = "Unlocked" Then  'หากเป็นการขอ Unlocked ไม่ต้องส่ง Barcode ไปเพราะเป็นการขอUnlocked Rc no.
                'ให้เช็คว่าเคยส่งไปขอ Unlocked หรือยัง ถ้าเคยส่งไปแล้วและเป็นหัวข้อเดียวกันแต่ยังไม่ได้ approve ไม่ต้องส่งซ้ำอีก
                Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, _defaultCrop, "Unlocked", False)
                For Each dr As DataRow In Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.GetData(_defaultCrop, "Unlocked", False)
                    Dim dbRcNo As String = dr.Item("RcNo").ToString  'dbBaleBarcode เก็บค่า RcNo ที่ได้จาก tableadapter
                    If _rcno = dbRcNo Then 'ถ้าRcNoมี
                        If dr.Item("ApproveStatus").ToString = False Then  'ถ้ายังไม่ได้รับการ approve
                            XCheckedResult = False
                        End If
                    End If
                Next

                If XCheckedResult = False Then
                    MessageBox.Show("คุณได้ทำการส่งคำร้องขอ Unlocked RC No.นี้ไปแล้ว ไม่สามารถบันทึกซ้ำได้อีก กรุณาตรวจสอบการปลดล๊อกกับSupervisor/Manager ")
                    Return
                End If


                Dim result As DialogResult
                result = MessageBox.Show("ต้องการขอปลดล๊อกเลข RC no. นี้ใช่หรือไม่? หากใช่ คำร้องจะส่งไปที่ Suppervisor/Manager ของคุณ กรุณารอผล", "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If result = DialogResult.Yes Then
                    db.sp_Receiving_INS_ReceivingCheckerRequest(_defaultCrop, XRequestType, _rcno, "", _username, RemarkFromCheckerComboBox.SelectedValue)
                    MessageBox.Show("ทำการส่งคำร้องไปเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    LockedLabel.Visible = True
                    Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
                ElseIf result = DialogResult.No Then
                    Return
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub MatRcMetroGrid_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles MatRcMetroGrid.CellMouseClick
        Try
            Dim bc As String = MatRcMetroGrid.Item(5, MatRcMetroGrid.CurrentCell.RowIndex).Value
            If String.IsNullOrEmpty(bc) Then
                Return
            End If

            Dim model = _businessLayerService.NTRMInspectionBL.GetByBaleBarcode(bc)
            If model IsNot Nothing Then
                NTRMatBuying.Visible = True
            Else
                NTRMatBuying.Visible = False
            End If

            'If (XBaleBarcode <> "") Then
            '    Me.Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter.Fill(Me.ReceivingDataSet.sp_NTRM_SEL_NTRMInspectionByBC, XBaleBarcode)
            '    If Sp_NTRM_SEL_NTRMInspectionByBCBindingSource.Count > 0 Then
            '        NTRMatBuying.Visible = True
            '    Else
            '        NTRMatBuying.Visible = False
            '    End If
            'End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub EditRcNoHeaderTile_Click(sender As Object, e As EventArgs) Handles EditRcNoHeaderTile.Click
        Dim XFindBaleBarcodeResult As Boolean = False
        Dim XCheckedResult As Boolean = True
        Dim XRemarkName As String = "" 'เก็บ Remark ที่ทำเรื่องขอแก้ไขข้อมูลจากการขอ Approve
        Try
            If _rcno = "" Then
                MessageBox.Show("ไม่มีเลข Rc No. กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'ให้เช็คว่ามีการ Unlocked RcNo. นี้หรือยัง  ถ้ายัง ไม่อนุญาตให้แก้ไขข้อมูลได้
            If LockedLabel.Visible = True Then
                MessageBox.Show("RC No.นี้ยังไม่ได้รับการปลดล๊อกจึงไม่สามารถแก้ไขข้อมูลได้ ให้ติดต่อSupervisor/Manager เพื่อตรวจสอบการปลดล๊อก", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'ให้เช็คก่อนว่า Approve from manager หรือยัง ถ้ายังไม่ได้ approve จะไม่สามารถแก้ไขข้อมูลได้  มีขั้นตอนดังนี้
            '1.เช็คใน table ReceivingCheckerRequest ว่ามี Rc No และเป็นการขอแก้ไข Header นี้หรือไม่
            '2.หากไม่มี  XFindBaleBarcodeResult = False และแจ้งให้ส่งคำร้องไป
            '3.หากมี เช็คต่อว่าได้รับการ Approve หรือยัง
            '4.หากยังไม่ได้รับการ Approve XCheckedResult = false
            '5.หากได้รับการ Approve แล้ว XCheckedResult = true
            Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequestByEditCase, _defaultCrop)
            For Each dr As DataRow In Sp_Receiving_SEL_ReceivingCheckerRequestByEditCaseTableAdapter.GetData(_defaultCrop)
                Dim dbRcNo As String = dr.Item("Rcno").ToString  'dbRcNo เก็บค่า RcNo ที่ได้จาก tableadapter
                If (_rcno = dbRcNo) And (IsDBNull(dr.Item("BC")) = True Or dr.Item("BC").ToString = "") Then 'ถ้าเป็นการขอแก้ไข RCNo header ให้ตรวจสอบว่าได้รับการ Approve หรือยัง
                    If dr.Item("ApproveStatus").ToString = False Then  'ถ้ายังไม่ได้รับการ approve
                        XCheckedResult = False
                        XFindBaleBarcodeResult = True 'แสดงว่ามีในคำร้องแล้ว
                        XRemarkName = dr.Item("RemarkName").ToString 'เก็บหมายเหตุที่ทำเรื่องขอแก้ไข
                    ElseIf dr.Item("ApproveStatus").ToString = True Then 'ถ้าได้รับการ Approve
                        XFindBaleBarcodeResult = True 'แสดงว่ามีในคำร้องแล้ว
                        XRemarkName = dr.Item("RemarkName").ToString 'เก็บหมายเหตุที่ทำเรื่องขอแก้ไข
                    End If
                End If
            Next

            'ถ้ายังไม่ได้ส่งคำร้องไป
            If XFindBaleBarcodeResult = False Then
                MessageBox.Show("เลข Rc no. นี้ยังไม่ได้ส่งคำร้องไปยังSupervisor/Manager จะไม่สามารถแก้ไขข้อมูลได้ หากต้องการแก้ไขให้ส่งคำร้องไป", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'ถ้ายังไม่ได้รับการ Approve
            If XCheckedResult = False Then
                MessageBox.Show("เลข Rc no. นี้ยังไม่ได้รับการApproveให้แก้ไขข้อมูล Header จากSupervisor/Manager กรุณาตรวจสอบข้อมูลกับSupervisor/Manager", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'ไปที่ frmEditRcNoHeader
            FrmEditRcNoHeader.RcNoLabel.Text = _rcno
            Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
            For Each dr As DataRow In Sp_Receiving_SEL_MatByRcNoTableAdapter.GetData(_rcno)
                If IsDBNull(dr.Item("BC")) = False Then
                    FrmEditRcNoHeader.XBaleBarcodeForEditRCNoHeader = dr.Item("BC").ToString
                End If
            Next
            FrmEditRcNoHeader.XTypeHeader = _matrc.type
            FrmEditRcNoHeader.XRemarkName = XRemarkName
            FrmEditRcNoHeader.ShowDialog()
            FrmEditRcNoHeader.Dispose()
            Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BtnNTRM_Click(sender As Object, e As EventArgs) Handles BtnNTRM.Click
        Try
            If (XBaleBarcode = "") Then
                MessageBox.Show("กรุณาคีย์บาร์โค้ดที่ต้องการดูข้อมูลหรือบันทึก NTRM", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            lblNTRMDetail.Text = ""
            TxtNTRMQuantity.Text = ""
            txtNTRMRemak.Text = ""

            NTRMPanel.Visible = True
            NTRMPanel.Size = New Size(1376, 786)
            NTRMPanel.Location = New Point(0, 0)
            If (XBaleBarcode <> "") Then
                Me.Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter.Fill(Me.ReceivingDataSet.sp_NTRM_SEL_NTRMInspectionByBC, XBaleBarcode)
            End If
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BtnNTRMExit_Click(sender As Object, e As EventArgs) Handles BtnNTRMExit.Click
        Try
            'เพิ่มเช็ค ถ้ามีข้อมูล NTRM ที่ buying หรือ stecdbms ให้โชว์ตัวหนังสือสีแดง user สามารถตรวจสอบได้จากการกดปุ่ม NTRM
            If (XBaleBarcode <> "") Then
                Me.Sp_NTRM_SEL_NTRMInspectionByBCTableAdapter.Fill(Me.ReceivingDataSet.sp_NTRM_SEL_NTRMInspectionByBC, XBaleBarcode)
                If Sp_NTRM_SEL_NTRMInspectionByBCBindingSource.Count > 0 Then
                    NTRMatBuying.Visible = True
                Else
                    NTRMatBuying.Visible = False
                End If
            End If

            NTRMPanel.Visible = False
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class