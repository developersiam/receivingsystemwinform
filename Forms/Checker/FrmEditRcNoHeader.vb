﻿Public Class FrmEditRcNoHeader
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim XCompanyCode As Integer
    Dim CompanyRow As ReceivingDataSet.sp_Receiving_SEL_CompanyRow  'Get CompanyRow 
    Public XBaleBarcodeForEditRCNoHeader As String 'เก็บค่า barcode ที่อยู๋ใน RC No. นี้ โดยเลือกมาแค่ 1 ตัวเพื่อจะได้ Get subtype , rcfrom , company เท่านั้น
    Public XRemarkName As String 'เก็บ Remark ที่ทำเรื่องขอแก้ไขข้อมูลจากการขอ Approve
    Public XTypeHeader As String
    Private Sub FrmEditRcNoHeader_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username

            Me.Sp_Receiving_SEL_CompanyTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Company)
            Me.Sp_Receiving_SEL_SubtypeTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Subtype)
            Me.TypeTableAdapter.Fill(Me.ReceivingDataSet.type)
            Me.Sp_Receiving_SEL_MatWHTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatWH)
            Me.Sp_Receiving_SEL_BuyerClassifierTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_BuyerClassifier) 'ใชกับ Buyer เท่านั้น
            Me.Sp_Receiving_SEL_ClassifierTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Classifier) 'ใช้กับ Classifier เท่านั้น

            TypeComboBox.Text = XTypeHeader

            Dim matRc = _businessLayerService.ReceivingDocumentBL().GetMatRcByRcno(RcNoLabel.Text)
            If (matRc Is Nothing) Then
                MessageBox.Show("ไม่พบข้อมูล rcno นี้", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If (matRc.checkerapproved = True) Then
                MessageBox.Show("rcno นี้ถูก approve โดย checker แล้วไม่สามารถดำเนินการใดๆ ได้ (checkerapprove = 1)", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim matSubtypeCompany = _businessLayerService.ReceivingDocumentBL().GetMatSupTypeCompanyByRcno(matRc.rcno)
            CompanyComboBox.Text = matSubtypeCompany.Company
            SubtypeComboBox.Text = matSubtypeCompany.Subtype
            RcFromComboBox.Text = matSubtypeCompany.rcfrom

            TypeComboBox.Text = matRc.type
            PlaceComboBox.Text = matRc.place
            BuyerComboBox.Text = matRc.buyer
            ClassifierComboBox.Text = matRc.classifier
            TruckNoTextBox.Text = matRc.truckno
            TransportDocNoTextBox.Text = matRc.TransportationDocumentCode
            InvoiceNoTextBox.Text = matRc.InvoiceNo

            Return


            ' **************************************************************************
            ' แก้ไขเมื่อวันที่ 27/03/2019 โดยเอกลักษณ์ แก้วมาเรือน
            ' เปลี่ยนจาการดึงข้อมูลผ่าน TableAdaptor มาใช้การดึงข้อมูลจาก class ที่ implement ใหม่
            ' Source code ด้านล่างนี้จะทำงาน



            ' Get data from MatRC
            Dim bRcNoRow As ReceivingDataSet.sp_Receiving_SEL_MatRCRow
            Me.Sp_Receiving_SEL_MatRCTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatRC, _defaultCrop, TypeComboBox.Text, True)
            bRcNoRow = Me.ReceivingDataSet.sp_Receiving_SEL_MatRC.FindByrcno(RcNoLabel.Text)
            If bRcNoRow Is Nothing Then
                MessageBox.Show("ไม่มีเลข RC No.นี้หรือ RC No.นี้ถูกcheckerล๊อกข้อมูลแล้ว กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            ElseIf Not bRcNoRow Is Nothing Then
                TypeComboBox.Text = bRcNoRow.type
                PlaceComboBox.Text = bRcNoRow.place
                BuyerComboBox.Text = bRcNoRow.buyer
                ClassifierComboBox.Text = bRcNoRow.classifier
                TruckNoTextBox.Text = bRcNoRow.truckno
                If (Not bRcNoRow.IsTransportationDocumentCodeNull) Then
                    TransportDocNoTextBox.Text = bRcNoRow.TransportationDocumentCode
                End If
                If (Not bRcNoRow.IsInvoiceNoNull) Then
                    InvoiceNoTextBox.Text = bRcNoRow.InvoiceNo
                End If
            End If

            'Get subtype,rcfrom,company from Mat   
            Dim bRow As ReceivingDataSet.sp_Receiving_SEL_MatByRcNoRow
            Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, RcNoLabel.Text)
            bRow = Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.FindBybc(XBaleBarcodeForEditRCNoHeader)
            If bRow Is Nothing Then
                MessageBox.Show("เลข barcode นี้ไม่มีในเลข Rc No.นี้ กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            ElseIf Not bRow Is Nothing Then
                SubtypeComboBox.Text = bRow.subtype
                RcFromComboBox.Text = bRow.rcfrom
                XCompanyCode = bRow.company
                'Get company 
                Me.Sp_Receiving_SEL_CompanyTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Company)
                CompanyRow = Me.ReceivingDataSet.sp_Receiving_SEL_Company.FindBycode(XCompanyCode)
                If CompanyRow Is Nothing Then
                    MessageBox.Show("Company นี้ไม่มีในฐานข้อมูล  กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                Else
                    CompanyComboBox.Text = CompanyRow.name
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    
    Private Sub SaveTile_Click(sender As Object, e As EventArgs) Handles SaveTile.Click
        Try
            If CompanyComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Company !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If SubtypeComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก subtype !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If TypeComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก type !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If (TypeComboBox.Text = "FC" And SubtypeComboBox.Text <> "F") Or (TypeComboBox.Text = "BU" And SubtypeComboBox.Text = "F") Then
                MessageBox.Show("Subtype ไม่ถูกต้อง โปรดตรวจสอบ!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If RcFromComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Receiving form !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If PlaceComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Place !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If BuyerComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Buyer !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If ClassifierComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Classifier !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            If TruckNoTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ป้ายทะเบียนรถบรรทุก !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            'Update MatRC where RcNo
            db.sp_Receiving_UPD_MatRC(RcNoLabel.Text, TypeComboBox.Text, PlaceComboBox.Text, TruckNoTextBox.Text, BuyerComboBox.Text, ClassifierComboBox.Text, RcFromComboBox.Text, XRemarkName, "Receiving lines", _username, SubtypeComboBox.SelectedValue, CompanyComboBox.SelectedValue, PlaceComboBox.Text, InvoiceNoTextBox.Text, TransportDocNoTextBox.Text)
            MessageBox.Show("แก้ไขข้อมูล Receiving no. เรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class