﻿Imports FastMember

Public Class FrmCallReceivingNoByChecker
    Inherits MetroFramework.Forms.MetroForm

    Private matList As List(Of sp_Receiving_SEL_MatRCByReceivingFinish_Result)
    Public Property _matList() As List(Of sp_Receiving_SEL_MatRCByReceivingFinish_Result)
        Get
            Return matList
        End Get
        Set(ByVal value As List(Of sp_Receiving_SEL_MatRCByReceivingFinish_Result))
            matList = value
        End Set
    End Property

    Private matrc As sp_Receiving_SEL_MatRCByReceivingFinish_Result
    Public Property _matrc() As sp_Receiving_SEL_MatRCByReceivingFinish_Result
        Get
            Return matrc
        End Get
        Set(ByVal value As sp_Receiving_SEL_MatRCByReceivingFinish_Result)
            matrc = value
        End Set
    End Property

    Private Sub FrmCallReceivingNoByChecker_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            TypeComboBox.DataSource = Nothing
            TypeComboBox.DataSource = _businessLayerService.TypeBL().GetTypes()
            TypeComboBox.SelectedIndex = 0
            matrcBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub matrcBinding()
        If TypeComboBox.SelectedIndex = -1 Then
            Return
        End If

        _matList = _businessLayerService.ReceivingDocumentBL().
            GetByChecker(_defaultCrop,
                                  TypeComboBox.Text,
                                  CheckerLockedMetroCheckBox.Checked).ToList()

        Dim dt As New DataTable()
        Dim reader = ObjectReader.Create(_matList)
        dt.Load(reader)

        matrcDg.DataSource = dt
    End Sub

    Private Sub TypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TypeComboBox.SelectedIndexChanged
        Try
            matrcBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub AddDocnoMetroTile_Click(sender As Object, e As EventArgs) Handles AddDocnoMetroTile.Click
        Try
            If String.IsNullOrEmpty(_matrc.rcno) Then
                Throw New ArgumentException("กรุณาเลือก rcno ที่ต้องการจากตารางด้านล่างใหม่อีกครั้ง")
            End If

            'ถ้ายังไม่ได้รับเข้า
            If _matrc.COUNTOFBALES <= 0 Then
                MessageBox.Show("เลข rcno นี้ไม่มีห่อยา จึงไม่สามารถ add docno ได้")
            End If

            'ถ้ามีการรับเข้าแล้วแต่ receiving user ยังไม่ได้ finish จะทำการ add docno ไม่ได้
            If String.IsNullOrEmpty(_matrc.finishtime) Then
                Throw New ArgumentException("เลข rcno นี้ staff ยังไม่ได้ finish จึงไม่สามารถ add docno ได้")
            End If

            FrmAddDocNo._rcno = _matrc.rcno
            FrmAddDocNo.ShowDialog()
            FrmAddDocNo.Dispose()

            matrcBinding()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub matrcDg_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles matrcDg.CellMouseClick
        Try
            Dim rcno = matrcDg.Item(0, matrcDg.CurrentCell.RowIndex).Value

            If String.IsNullOrEmpty(rcno) Then
                Throw New ArgumentException("ไม่พบ rcno ดังกล่าว โปรดลองคลิกเลือกที่แถวในตารางใหม่อีกครั้ง")
            End If

            _matrc = _matList.SingleOrDefault(Function(x) x.rcno = rcno)
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            matrcBinding()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CheckerLockedMetroCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles CheckerLockedMetroCheckBox.CheckedChanged
        Try
            matrcBinding()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub ReportTile_Click(sender As Object, e As EventArgs) Handles ReportTile.Click
        Try
            If String.IsNullOrEmpty(_matrc.rcno) Then
                Throw New ArgumentException("ไม่พบ rcno ดังกล่าว โปรดลองคลิกเลือกที่แถวในตารางใหม่อีกครั้ง")
            End If

            Dim form As New FrmRcnoDetail()
            form._matrc = _businessLayerService.ReceivingDocumentBL().GetMatRcByRcno(_matrc.rcno)
            form.ShowDialog()
            'FrmRPT_FCBY001.formParameter(XCrop, XStationCode, XCurerCode, XDocumentNumber, XDocumentNo)
            'FrmRPT_FCBY001.ShowDialog()
            'FrmRPT_FCBY001.Dispose()
            'FrmCheckingReport.XCheckingcheckerLocked = XCheckerLocked
            'FrmCheckingReport.XCheckingRcNo = XRcNo
            'FrmCheckingReport.XCheckingType = TypeComboBox.Text
            'FrmCheckingReport.ShowDialog()
            'FrmCheckingReport.Dispose()
            'Me.Sp_Receiving_SEL_MatRCTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatRC, XCrop, TypeComboBox.Text, XCheckerLocked)
            'Me.Sp_Receiving_SEL_MatRCByReceivingFinishTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatRCByReceivingFinish, ReceivingModule._defaultCrop, TypeComboBox.Text, XCheckerLocked)
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class