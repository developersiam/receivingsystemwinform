﻿Public Class FrmRPT_RCB005
    Inherits MetroFramework.Forms.MetroForm
    Public _type As String
    Public _rcno As String
    Private Sub FrmRPT_RCB005_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ReceivingDataSet.sp_Receiving_SEL_MatRCByRcNo' table. You can move, or remove it, as needed.


        Try
            Me.Size = New Size(1280, 768)
            'TODO: This line of code loads data into the 'ReceivingDataSet.sp_Receiving_SEL_MatRC' table. You can move, or remove it, as needed.
            Me.sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _rcno)
            Me.sp_Receiving_SEL_MatRCByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatRCByRcNo, _defaultCrop, _type, _rcno)
            Me.ReportViewer1.RefreshReport()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub
End Class