﻿Public Class FrmRPT_RCB006
    Inherits MetroFramework.Forms.MetroForm
    Public XRptType As String
    Public XRptRcNo As String
    Private Sub FrmRPT_RCB006_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            'TODO: This line of code loads data into the 'ReceivingDataSet.sp_Receiving_SEL_MatRC' table. You can move, or remove it, as needed.
            Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassify, XRptRcNo)
            Me.sp_Receiving_SEL_MatRCByRcNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_MatRCByRcNo, _defaultCrop, XRptType, XRptRcNo)
            Me.ReportViewer1.RefreshReport()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub
End Class