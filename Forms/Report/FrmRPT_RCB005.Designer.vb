﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRPT_RCB005
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.sp_Receiving_SEL_MatByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.sp_Receiving_SEL_MatRCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.sp_Receiving_SEL_MatByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter()
        Me.sp_Receiving_SEL_MatRCTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCTableAdapter()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.sp_Receiving_SEL_MatRCByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_Receiving_SEL_MatRCByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCByRcNoTableAdapter()
        CType(Me.sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_Receiving_SEL_MatRCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_Receiving_SEL_MatRCByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sp_Receiving_SEL_MatByRcNoBindingSource
        '
        Me.sp_Receiving_SEL_MatByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo"
        Me.sp_Receiving_SEL_MatByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'sp_Receiving_SEL_MatRCBindingSource
        '
        Me.sp_Receiving_SEL_MatRCBindingSource.DataMember = "sp_Receiving_SEL_MatRC"
        Me.sp_Receiving_SEL_MatRCBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "ReceivingMayByRcNoDataSet"
        ReportDataSource1.Value = Me.sp_Receiving_SEL_MatByRcNoBindingSource
        ReportDataSource2.Name = "ReceivingMatRcByRcNoDataSet"
        ReportDataSource2.Value = Me.sp_Receiving_SEL_MatRCByRcNoBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReceivingSystem.RPT_RCB005.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(6, 63)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(1234, 669)
        Me.ReportViewer1.TabIndex = 2
        '
        'sp_Receiving_SEL_MatByRcNoTableAdapter
        '
        Me.sp_Receiving_SEL_MatByRcNoTableAdapter.ClearBeforeFill = True
        '
        'sp_Receiving_SEL_MatRCTableAdapter
        '
        Me.sp_Receiving_SEL_MatRCTableAdapter.ClearBeforeFill = True
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1100, 9)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 117
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'sp_Receiving_SEL_MatRCByRcNoBindingSource
        '
        Me.sp_Receiving_SEL_MatRCByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatRCByRcNo"
        Me.sp_Receiving_SEL_MatRCByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_Receiving_SEL_MatRCByRcNoTableAdapter
        '
        Me.sp_Receiving_SEL_MatRCByRcNoTableAdapter.ClearBeforeFill = True
        '
        'FrmRPT_RCB005
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1280, 768)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmRPT_RCB005"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Receiving detail"
        CType(Me.sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_Receiving_SEL_MatRCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_Receiving_SEL_MatRCByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents sp_Receiving_SEL_MatByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents sp_Receiving_SEL_MatRCBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents sp_Receiving_SEL_MatByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter
    Friend WithEvents sp_Receiving_SEL_MatRCTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCTableAdapter
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents sp_Receiving_SEL_MatRCByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents sp_Receiving_SEL_MatRCByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCByRcNoTableAdapter
End Class
