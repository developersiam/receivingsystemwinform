﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRECLAO004
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.BtnViewReport = New System.Windows.Forms.Button()
        Me.InvoiceNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter()
        Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter()
        CType(Me.Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource
        '
        Me.Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource.DataMember = "sp_Receiving_SEL_LaoReportByInvoiceNo"
        Me.Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1228, 25)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 117
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "LaoReportByInvoiceNoDataSet"
        ReportDataSource1.Value = Me.Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource
        ReportDataSource2.Name = "DataSet1"
        ReportDataSource2.Value = Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReceivingSystem.RECLAO004.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(23, 87)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(1255, 661)
        Me.ReportViewer1.TabIndex = 116
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(24, 45)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(96, 19)
        Me.MetroLabel1.TabIndex = 120
        Me.MetroLabel1.Text = "LTL Invoice No."
        '
        'BtnViewReport
        '
        Me.BtnViewReport.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnViewReport.Location = New System.Drawing.Point(429, 25)
        Me.BtnViewReport.Name = "BtnViewReport"
        Me.BtnViewReport.Size = New System.Drawing.Size(112, 60)
        Me.BtnViewReport.TabIndex = 156
        Me.BtnViewReport.Text = "View report"
        Me.BtnViewReport.UseVisualStyleBackColor = True
        '
        'InvoiceNoTextBox
        '
        Me.InvoiceNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.InvoiceNoTextBox.CustomButton.Image = Nothing
        Me.InvoiceNoTextBox.CustomButton.Location = New System.Drawing.Point(263, 1)
        Me.InvoiceNoTextBox.CustomButton.Name = ""
        Me.InvoiceNoTextBox.CustomButton.Size = New System.Drawing.Size(33, 33)
        Me.InvoiceNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.InvoiceNoTextBox.CustomButton.TabIndex = 1
        Me.InvoiceNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.InvoiceNoTextBox.CustomButton.UseSelectable = True
        Me.InvoiceNoTextBox.CustomButton.Visible = False
        Me.InvoiceNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.InvoiceNoTextBox.Lines = New String(-1) {}
        Me.InvoiceNoTextBox.Location = New System.Drawing.Point(126, 38)
        Me.InvoiceNoTextBox.MaxLength = 50
        Me.InvoiceNoTextBox.Name = "InvoiceNoTextBox"
        Me.InvoiceNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InvoiceNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.InvoiceNoTextBox.SelectedText = ""
        Me.InvoiceNoTextBox.SelectionLength = 0
        Me.InvoiceNoTextBox.SelectionStart = 0
        Me.InvoiceNoTextBox.Size = New System.Drawing.Size(297, 35)
        Me.InvoiceNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.InvoiceNoTextBox.TabIndex = 157
        Me.InvoiceNoTextBox.UseCustomBackColor = True
        Me.InvoiceNoTextBox.UseSelectable = True
        Me.InvoiceNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.InvoiceNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter
        '
        Me.Sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource
        '
        Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource.DataMember = "sp_Receiving_SEL_InvoiceDocNoByInvoiceNo"
        Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter
        '
        Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter.ClearBeforeFill = True
        '
        'FrmRECLAO004
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1439, 768)
        Me.Controls.Add(Me.InvoiceNoTextBox)
        Me.Controls.Add(Me.BtnViewReport)
        Me.Controls.Add(Me.MetroLabel1)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmRECLAO004"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        CType(Me.Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Receiving_SEL_LaoReportByInvoiceNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter
    Friend WithEvents BtnViewReport As System.Windows.Forms.Button
    Friend WithEvents InvoiceNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter
End Class
