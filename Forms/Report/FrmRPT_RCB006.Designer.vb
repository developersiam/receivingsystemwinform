﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRPT_RCB006
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.sp_Receiving_SEL_MatRCByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_Receiving_SEL_MatRCByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCByRcNoTableAdapter()
        Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_Receiving_SEL_MatRCByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1100, 9)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 118
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "ReceivingMatRcByRcNoDataSet"
        ReportDataSource1.Value = Me.sp_Receiving_SEL_MatRCByRcNoBindingSource
        ReportDataSource2.Name = "ReceivingMatByRcNoBySupplierClassifyDataSet"
        ReportDataSource2.Value = Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "ReceivingSystem.RPT_RCB006.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(8, 63)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(1234, 669)
        Me.ReportViewer1.TabIndex = 119
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'sp_Receiving_SEL_MatRCByRcNoBindingSource
        '
        Me.sp_Receiving_SEL_MatRCByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatRCByRcNo"
        Me.sp_Receiving_SEL_MatRCByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_Receiving_SEL_MatRCByRcNoTableAdapter
        '
        Me.sp_Receiving_SEL_MatRCByRcNoTableAdapter.ClearBeforeFill = True
        '
        'sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource
        '
        Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassify"
        Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource.DataSource = Me.ReceivingDataSet
        '
        'sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter
        '
        Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter.ClearBeforeFill = True
        '
        'FrmRPT_RCB006
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1275, 756)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Controls.Add(Me.BackMetroTile)
        Me.Name = "FrmRPT_RCB006"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Receiving summary by classify grade"
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_Receiving_SEL_MatRCByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents sp_Receiving_SEL_MatRCByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents sp_Receiving_SEL_MatRCByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatRCByRcNoTableAdapter
    Friend WithEvents sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNo_GroupbySupplierClassifyTableAdapter
End Class
