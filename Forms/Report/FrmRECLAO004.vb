﻿Public Class FrmRECLAO004
    Inherits MetroFramework.Forms.MetroForm
    Private Sub FrmLTLReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            'Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNo, XCrop)
            'Me.ReportViewer1.RefreshReport()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub BtnViewReport_Click(sender As Object, e As EventArgs) Handles BtnViewReport.Click
        Try
            'If InvoiceNoComboBo.Text <> "" Then
            '    Dim XInvoiceNo As String
            '    XInvoiceNo = InvoiceNoComboBo.Text

            '    Me.Sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_LaoReportByInvoiceNo, XInvoiceNo)

            '    Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNo, XCrop)
            '    Dim set3 As DataSet = New DataSet("InvoiceDocNoDataSet")
            '    set3.Tables.Add(Me.Sp_Receiving_SEL_InvoiceDocNoBindingSource.Filter = "InvoiceNo = " & "'" & XInvoiceNo & "'")

            '    Me.ReportViewer1.RefreshReport()
            'End If

            If InvoiceNoTextBox.Text <> "" Then
                Me.Sp_Receiving_SEL_LaoReportByInvoiceNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_LaoReportByInvoiceNo, InvoiceNoTextBox.Text)
                Me.Sp_Receiving_SEL_InvoiceDocNoByInvoiceNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNoByInvoiceNo, InvoiceNoTextBox.Text)
              
                'Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNo, XCrop)
                'Dim set3 As DataSet = New DataSet("InvoiceDocNoDataSet")
                'set3.Tables.Add(Me.Sp_Receiving_SEL_InvoiceDocNoBindingSource.Filter = "InvoiceNo = " & "'" & InvoiceNoTextBox.Text & "'")

                Me.ReportViewer1.RefreshReport()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    
End Class