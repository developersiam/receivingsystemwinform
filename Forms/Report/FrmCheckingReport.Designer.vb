﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCheckingReport
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.BtnDetail = New System.Windows.Forms.Button()
        Me.BtnSummaryByClassify = New System.Windows.Forms.Button()
        Me.BtnDetailByGreen = New System.Windows.Forms.Button()
        Me.BtnSummaryByGreen = New System.Windows.Forms.Button()
        Me.MetroPanel1.SuspendLayout()
        Me.MetroPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 33)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1106, 54)
        Me.MetroPanel1.TabIndex = 122
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1052, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroPanel2
        '
        Me.MetroPanel2.Controls.Add(Me.BtnDetail)
        Me.MetroPanel2.Controls.Add(Me.BtnSummaryByClassify)
        Me.MetroPanel2.Controls.Add(Me.BtnDetailByGreen)
        Me.MetroPanel2.Controls.Add(Me.BtnSummaryByGreen)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(147, 248)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(831, 275)
        Me.MetroPanel2.TabIndex = 123
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'BtnDetail
        '
        Me.BtnDetail.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.BtnDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnDetail.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDetail.FlatAppearance.BorderSize = 0
        Me.BtnDetail.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnDetail.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDetail.ForeColor = System.Drawing.Color.White
        Me.BtnDetail.Image = Global.ReceivingSystem.My.Resources.Resources.Purchase1
        Me.BtnDetail.Location = New System.Drawing.Point(619, 16)
        Me.BtnDetail.Name = "BtnDetail"
        Me.BtnDetail.Size = New System.Drawing.Size(197, 240)
        Me.BtnDetail.TabIndex = 6
        Me.BtnDetail.Text = "Detail "
        Me.BtnDetail.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnDetail.UseVisualStyleBackColor = False
        '
        'BtnSummaryByClassify
        '
        Me.BtnSummaryByClassify.BackColor = System.Drawing.Color.Coral
        Me.BtnSummaryByClassify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnSummaryByClassify.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSummaryByClassify.FlatAppearance.BorderSize = 0
        Me.BtnSummaryByClassify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnSummaryByClassify.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSummaryByClassify.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSummaryByClassify.ForeColor = System.Drawing.Color.White
        Me.BtnSummaryByClassify.Image = Global.ReceivingSystem.My.Resources.Resources.Purchase1
        Me.BtnSummaryByClassify.Location = New System.Drawing.Point(416, 16)
        Me.BtnSummaryByClassify.Name = "BtnSummaryByClassify"
        Me.BtnSummaryByClassify.Size = New System.Drawing.Size(197, 240)
        Me.BtnSummaryByClassify.TabIndex = 5
        Me.BtnSummaryByClassify.Text = "Summary by Classify "
        Me.BtnSummaryByClassify.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnSummaryByClassify.UseVisualStyleBackColor = False
        '
        'BtnDetailByGreen
        '
        Me.BtnDetailByGreen.BackColor = System.Drawing.Color.YellowGreen
        Me.BtnDetailByGreen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnDetailByGreen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnDetailByGreen.FlatAppearance.BorderSize = 0
        Me.BtnDetailByGreen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnDetailByGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnDetailByGreen.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnDetailByGreen.ForeColor = System.Drawing.Color.White
        Me.BtnDetailByGreen.Image = Global.ReceivingSystem.My.Resources.Resources.Purchase1
        Me.BtnDetailByGreen.Location = New System.Drawing.Point(213, 16)
        Me.BtnDetailByGreen.Name = "BtnDetailByGreen"
        Me.BtnDetailByGreen.Size = New System.Drawing.Size(197, 240)
        Me.BtnDetailByGreen.TabIndex = 3
        Me.BtnDetailByGreen.Text = "Detail by Green"
        Me.BtnDetailByGreen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnDetailByGreen.UseVisualStyleBackColor = False
        '
        'BtnSummaryByGreen
        '
        Me.BtnSummaryByGreen.BackColor = System.Drawing.Color.DarkOrange
        Me.BtnSummaryByGreen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.BtnSummaryByGreen.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnSummaryByGreen.FlatAppearance.BorderSize = 0
        Me.BtnSummaryByGreen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.BtnSummaryByGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSummaryByGreen.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSummaryByGreen.ForeColor = System.Drawing.Color.White
        Me.BtnSummaryByGreen.Image = Global.ReceivingSystem.My.Resources.Resources.Purchase1
        Me.BtnSummaryByGreen.Location = New System.Drawing.Point(10, 16)
        Me.BtnSummaryByGreen.Name = "BtnSummaryByGreen"
        Me.BtnSummaryByGreen.Size = New System.Drawing.Size(197, 240)
        Me.BtnSummaryByGreen.TabIndex = 2
        Me.BtnSummaryByGreen.Text = "Summary by Green"
        Me.BtnSummaryByGreen.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.BtnSummaryByGreen.UseVisualStyleBackColor = False
        '
        'FrmCheckingReport
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1280, 768)
        Me.Controls.Add(Me.MetroPanel2)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmCheckingReport"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        Me.MetroPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents BtnDetail As System.Windows.Forms.Button
    Friend WithEvents BtnSummaryByClassify As System.Windows.Forms.Button
    Friend WithEvents BtnDetailByGreen As System.Windows.Forms.Button
    Friend WithEvents BtnSummaryByGreen As System.Windows.Forms.Button
End Class
