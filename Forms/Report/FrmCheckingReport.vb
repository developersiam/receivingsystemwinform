﻿Public Class FrmCheckingReport
    Inherits MetroFramework.Forms.MetroForm
    'Public XCheckingcheckerLocked As String
    Public XCheckingType As String
    Public XCheckingRcNo As String
    Private Sub FrmCheckingReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub BtnSummaryByGreen_Click(sender As Object, e As EventArgs) Handles BtnSummaryByGreen.Click
        Try
            FrmRPT_RCB003.XRptType = XCheckingType
            FrmRPT_RCB003.XRptRcNo = XCheckingRcNo
            FrmRPT_RCB003.ShowDialog()
            FrmRPT_RCB003.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnDetailByGreen_Click(sender As Object, e As EventArgs) Handles BtnDetailByGreen.Click
        Try
            FrmRPT_RCB004.XRptType = XCheckingType
            FrmRPT_RCB004.XRptRcNo = XCheckingRcNo
            FrmRPT_RCB004.ShowDialog()
            FrmRPT_RCB004.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnSummaryByClassify_Click(sender As Object, e As EventArgs) Handles BtnSummaryByClassify.Click
        Try
            FrmRPT_RCB006.XRptType = XCheckingType
            FrmRPT_RCB006.XRptRcNo = XCheckingRcNo
            FrmRPT_RCB006.ShowDialog()
            FrmRPT_RCB006.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
   
    Private Sub BtnDetail_Click(sender As Object, e As EventArgs) Handles BtnDetail.Click
        Try
            FrmRPT_RCB005._rcno = XCheckingRcNo
            FrmRPT_RCB005._type = XCheckingType
            FrmRPT_RCB005.ShowDialog()
            FrmRPT_RCB005.Dispose()
        Catch ex As Exception
            MessageBox.Show("Error : " & ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class