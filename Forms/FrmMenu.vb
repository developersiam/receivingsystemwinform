﻿Public Class FrmMenu
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            If securityRow.ItemArray.Count() < 0 Then
                MessageBox.Show("ข้อมูลการล็อคอินเข้าสู่ระบบผิดพลาด โปรดติดต่อแผนกไอที", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
            UserButton.Text = securityRow.uname
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnReceiving_Click(sender As Object, e As EventArgs) Handles BtnReceiving.Click

        Try
            If securityRow.receiving = True Or securityRow.depart = "Administer" Then
                Dim frm As New FrmCallReceivingNo
                frm.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnCheckReceiving_Click(sender As Object, e As EventArgs) Handles BtnAddDocNo.Click
        Try
            If securityRow.checking = True Or securityRow.depart = "Administer" Then
                Dim window As New FrmCallReceivingNoByChecker()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnEditRcNo_Click(sender As Object, e As EventArgs) Handles BtnUnLockRcNo.Click
        Try
            If securityRow.checking = True Or securityRow.depart = "Administer" Then
                Dim window As New FrmUnlockRcNo()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnApproveFromChecker_Click(sender As Object, e As EventArgs) Handles BtnApproveFromChecker.Click
        Try
            If (securityRow.depart = "Receiving" Or securityRow.depart = "Leaf Accouting" Or securityRow.depart = "Administer") And (securityRow.level = "Supervisor" Or securityRow.level = "Admin") Then
                Dim window As New FrmApproveFromChecker()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnCPAResult_Click(sender As Object, e As EventArgs) Handles BtnCPAResult.Click
        Try
            If securityRow.cpaResult = True Or securityRow.level = "Admin" Then
                'Dim window As New FrmCPAResult()
                Dim window As New FrmCPAMenu()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnLoadData_Click(sender As Object, e As EventArgs) Handles BtnLoadData.Click
        Try
            If securityRow.checking = True Or securityRow.depart = "Administer" Then
                Dim window As New FrmLoadDataMenu()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnReport_Click(sender As Object, e As EventArgs) Handles BtnReport.Click
        Try
            If securityRow.checking = True Or securityRow.depart = "Administer" Then
                Dim window As New FrmRECLAO004()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub SettingButton_Click(sender As Object, e As EventArgs) Handles SettingButton.Click
        Try
            If securityRow.receiving = True Or securityRow.depart = "Administer" Then
                Dim window As New FrmSetting()
                window.ShowDialog()
            Else
                MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class