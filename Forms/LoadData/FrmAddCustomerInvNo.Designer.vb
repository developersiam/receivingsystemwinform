﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAddCustomerInvNo
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.ClearTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel24 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.AddMetroTile = New MetroFramework.Controls.MetroTile()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.CompanyComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpReceivingSELCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.InvDate = New MetroFramework.Controls.MetroDateTime()
        Me.GradeTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.QuantityTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.UnitPriceTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.QtyBaleTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.InvNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.Sp_Receiving_SEL_CompanyTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_CompanyTableAdapter()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_SEL_InvoiceDocNoAllBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_InvoiceDocNoAllTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_InvoiceDocNoAllTableAdapter()
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.SpReceivingSELCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_InvoiceDocNoAllBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.ClearTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel24)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel1)
        Me.MetroPanel1.Controls.Add(Me.AddMetroTile)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(17, 34)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1213, 54)
        Me.MetroPanel1.TabIndex = 127
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'ClearTile
        '
        Me.ClearTile.ActiveControl = Nothing
        Me.ClearTile.AutoSize = True
        Me.ClearTile.BackColor = System.Drawing.Color.White
        Me.ClearTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearTile.Location = New System.Drawing.Point(699, 10)
        Me.ClearTile.Name = "ClearTile"
        Me.ClearTile.Size = New System.Drawing.Size(36, 35)
        Me.ClearTile.Style = MetroFramework.MetroColorStyle.White
        Me.ClearTile.TabIndex = 127
        Me.ClearTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CancelFile32
        Me.ClearTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.ClearTile.UseSelectable = True
        Me.ClearTile.UseTileImage = True
        '
        'MetroLabel24
        '
        Me.MetroLabel24.AutoSize = True
        Me.MetroLabel24.Location = New System.Drawing.Point(741, 17)
        Me.MetroLabel24.Name = "MetroLabel24"
        Me.MetroLabel24.Size = New System.Drawing.Size(37, 19)
        Me.MetroLabel24.TabIndex = 126
        Me.MetroLabel24.Text = "clear"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(635, 22)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(34, 19)
        Me.MetroLabel1.TabIndex = 125
        Me.MetroLabel1.Text = "save"
        '
        'AddMetroTile
        '
        Me.AddMetroTile.ActiveControl = Nothing
        Me.AddMetroTile.AutoSize = True
        Me.AddMetroTile.BackColor = System.Drawing.Color.White
        Me.AddMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddMetroTile.Location = New System.Drawing.Point(589, 7)
        Me.AddMetroTile.Name = "AddMetroTile"
        Me.AddMetroTile.Size = New System.Drawing.Size(49, 44)
        Me.AddMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.AddMetroTile.TabIndex = 124
        Me.AddMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Save32
        Me.AddMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.AddMetroTile.UseSelectable = True
        Me.AddMetroTile.UseTileImage = True
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(818, 8)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(860, 20)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(49, 19)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1126, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel5.Location = New System.Drawing.Point(115, 246)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(58, 25)
        Me.MetroLabel5.TabIndex = 152
        Me.MetroLabel5.Text = "Grade"
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel9.Location = New System.Drawing.Point(17, 208)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(153, 25)
        Me.MetroLabel9.TabIndex = 146
        Me.MetroLabel9.Text = "Packlist Invoice no."
        '
        'CompanyComboBox
        '
        Me.CompanyComboBox.DataSource = Me.SpReceivingSELCompanyBindingSource
        Me.CompanyComboBox.DisplayMember = "name"
        Me.CompanyComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.CompanyComboBox.FormattingEnabled = True
        Me.CompanyComboBox.ItemHeight = 29
        Me.CompanyComboBox.Location = New System.Drawing.Point(182, 132)
        Me.CompanyComboBox.Name = "CompanyComboBox"
        Me.CompanyComboBox.Size = New System.Drawing.Size(243, 35)
        Me.CompanyComboBox.TabIndex = 145
        Me.CompanyComboBox.UseSelectable = True
        Me.CompanyComboBox.ValueMember = "code"
        '
        'SpReceivingSELCompanyBindingSource
        '
        Me.SpReceivingSELCompanyBindingSource.DataMember = "sp_Receiving_SEL_Company"
        Me.SpReceivingSELCompanyBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel7.Location = New System.Drawing.Point(85, 132)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(85, 25)
        Me.MetroLabel7.TabIndex = 144
        Me.MetroLabel7.Text = "Company"
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(35, 170)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(135, 25)
        Me.MetroLabel2.TabIndex = 154
        Me.MetroLabel2.Text = "PackList Inv date"
        '
        'InvDate
        '
        Me.InvDate.FontSize = MetroFramework.MetroDateTimeSize.Tall
        Me.InvDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.InvDate.Location = New System.Drawing.Point(182, 172)
        Me.InvDate.MinimumSize = New System.Drawing.Size(0, 35)
        Me.InvDate.Name = "InvDate"
        Me.InvDate.Size = New System.Drawing.Size(243, 35)
        Me.InvDate.TabIndex = 155
        '
        'GradeTextbox
        '
        Me.GradeTextbox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.GradeTextbox.CustomButton.Image = Nothing
        Me.GradeTextbox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.GradeTextbox.CustomButton.Name = ""
        Me.GradeTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.GradeTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.GradeTextbox.CustomButton.TabIndex = 1
        Me.GradeTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.GradeTextbox.CustomButton.UseSelectable = True
        Me.GradeTextbox.CustomButton.Visible = False
        Me.GradeTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.GradeTextbox.Lines = New String(-1) {}
        Me.GradeTextbox.Location = New System.Drawing.Point(182, 247)
        Me.GradeTextbox.MaxLength = 32767
        Me.GradeTextbox.Name = "GradeTextbox"
        Me.GradeTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.GradeTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.GradeTextbox.SelectedText = ""
        Me.GradeTextbox.SelectionLength = 0
        Me.GradeTextbox.SelectionStart = 0
        Me.GradeTextbox.ShortcutsEnabled = True
        Me.GradeTextbox.Size = New System.Drawing.Size(243, 30)
        Me.GradeTextbox.Style = MetroFramework.MetroColorStyle.Orange
        Me.GradeTextbox.TabIndex = 156
        Me.GradeTextbox.UseCustomBackColor = True
        Me.GradeTextbox.UseSelectable = True
        Me.GradeTextbox.UseStyleColors = True
        Me.GradeTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.GradeTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'QuantityTextBox
        '
        '
        '
        '
        Me.QuantityTextBox.CustomButton.Image = Nothing
        Me.QuantityTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.QuantityTextBox.CustomButton.Name = ""
        Me.QuantityTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.QuantityTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.QuantityTextBox.CustomButton.TabIndex = 1
        Me.QuantityTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.QuantityTextBox.CustomButton.UseSelectable = True
        Me.QuantityTextBox.CustomButton.Visible = False
        Me.QuantityTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.QuantityTextBox.Lines = New String(-1) {}
        Me.QuantityTextBox.Location = New System.Drawing.Point(182, 282)
        Me.QuantityTextBox.MaxLength = 32767
        Me.QuantityTextBox.Name = "QuantityTextBox"
        Me.QuantityTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.QuantityTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.QuantityTextBox.SelectedText = ""
        Me.QuantityTextBox.SelectionLength = 0
        Me.QuantityTextBox.SelectionStart = 0
        Me.QuantityTextBox.ShortcutsEnabled = True
        Me.QuantityTextBox.Size = New System.Drawing.Size(243, 30)
        Me.QuantityTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.QuantityTextBox.TabIndex = 158
        Me.QuantityTextBox.UseCustomBackColor = True
        Me.QuantityTextBox.UseSelectable = True
        Me.QuantityTextBox.UseStyleColors = True
        Me.QuantityTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.QuantityTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel8.Location = New System.Drawing.Point(96, 284)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(77, 25)
        Me.MetroLabel8.TabIndex = 157
        Me.MetroLabel8.Text = "Quantity"
        '
        'UnitPriceTextBox
        '
        '
        '
        '
        Me.UnitPriceTextBox.CustomButton.Image = Nothing
        Me.UnitPriceTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.UnitPriceTextBox.CustomButton.Name = ""
        Me.UnitPriceTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.UnitPriceTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.UnitPriceTextBox.CustomButton.TabIndex = 1
        Me.UnitPriceTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.UnitPriceTextBox.CustomButton.UseSelectable = True
        Me.UnitPriceTextBox.CustomButton.Visible = False
        Me.UnitPriceTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.UnitPriceTextBox.Lines = New String(-1) {}
        Me.UnitPriceTextBox.Location = New System.Drawing.Point(182, 317)
        Me.UnitPriceTextBox.MaxLength = 32767
        Me.UnitPriceTextBox.Name = "UnitPriceTextBox"
        Me.UnitPriceTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.UnitPriceTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.UnitPriceTextBox.SelectedText = ""
        Me.UnitPriceTextBox.SelectionLength = 0
        Me.UnitPriceTextBox.SelectionStart = 0
        Me.UnitPriceTextBox.ShortcutsEnabled = True
        Me.UnitPriceTextBox.Size = New System.Drawing.Size(243, 30)
        Me.UnitPriceTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.UnitPriceTextBox.TabIndex = 160
        Me.UnitPriceTextBox.UseCustomBackColor = True
        Me.UnitPriceTextBox.UseSelectable = True
        Me.UnitPriceTextBox.UseStyleColors = True
        Me.UnitPriceTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.UnitPriceTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(94, 322)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(80, 25)
        Me.MetroLabel3.TabIndex = 159
        Me.MetroLabel3.Text = "UnitPrice"
        '
        'QtyBaleTextBox
        '
        '
        '
        '
        Me.QtyBaleTextBox.CustomButton.Image = Nothing
        Me.QtyBaleTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.QtyBaleTextBox.CustomButton.Name = ""
        Me.QtyBaleTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.QtyBaleTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.QtyBaleTextBox.CustomButton.TabIndex = 1
        Me.QtyBaleTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.QtyBaleTextBox.CustomButton.UseSelectable = True
        Me.QtyBaleTextBox.CustomButton.Visible = False
        Me.QtyBaleTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.QtyBaleTextBox.Lines = New String(-1) {}
        Me.QtyBaleTextBox.Location = New System.Drawing.Point(182, 352)
        Me.QtyBaleTextBox.MaxLength = 32767
        Me.QtyBaleTextBox.Name = "QtyBaleTextBox"
        Me.QtyBaleTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.QtyBaleTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.QtyBaleTextBox.SelectedText = ""
        Me.QtyBaleTextBox.SelectionLength = 0
        Me.QtyBaleTextBox.SelectionStart = 0
        Me.QtyBaleTextBox.ShortcutsEnabled = True
        Me.QtyBaleTextBox.Size = New System.Drawing.Size(243, 30)
        Me.QtyBaleTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.QtyBaleTextBox.TabIndex = 162
        Me.QtyBaleTextBox.UseCustomBackColor = True
        Me.QtyBaleTextBox.UseSelectable = True
        Me.QtyBaleTextBox.UseStyleColors = True
        Me.QtyBaleTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.QtyBaleTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel4.Location = New System.Drawing.Point(32, 360)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(139, 25)
        Me.MetroLabel4.TabIndex = 161
        Me.MetroLabel4.Text = "Number of bales"
        '
        'InvNoTextBox
        '
        Me.InvNoTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.InvNoTextBox.CustomButton.Image = Nothing
        Me.InvNoTextBox.CustomButton.Location = New System.Drawing.Point(215, 2)
        Me.InvNoTextBox.CustomButton.Name = ""
        Me.InvNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.InvNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.InvNoTextBox.CustomButton.TabIndex = 1
        Me.InvNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.InvNoTextBox.CustomButton.UseSelectable = True
        Me.InvNoTextBox.CustomButton.Visible = False
        Me.InvNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.InvNoTextBox.Lines = New String(-1) {}
        Me.InvNoTextBox.Location = New System.Drawing.Point(182, 212)
        Me.InvNoTextBox.MaxLength = 32767
        Me.InvNoTextBox.Name = "InvNoTextBox"
        Me.InvNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InvNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.InvNoTextBox.SelectedText = ""
        Me.InvNoTextBox.SelectionLength = 0
        Me.InvNoTextBox.SelectionStart = 0
        Me.InvNoTextBox.ShortcutsEnabled = True
        Me.InvNoTextBox.Size = New System.Drawing.Size(243, 30)
        Me.InvNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.InvNoTextBox.TabIndex = 163
        Me.InvNoTextBox.UseCustomBackColor = True
        Me.InvNoTextBox.UseSelectable = True
        Me.InvNoTextBox.UseStyleColors = True
        Me.InvNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.InvNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Sp_Receiving_SEL_CompanyTableAdapter
        '
        Me.Sp_Receiving_SEL_CompanyTableAdapter.ClearBeforeFill = True
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ReceivingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_SEL_InvoiceDocNoAllBindingSource
        '
        Me.Sp_Receiving_SEL_InvoiceDocNoAllBindingSource.DataMember = "sp_Receiving_SEL_InvoiceDocNoAll"
        Me.Sp_Receiving_SEL_InvoiceDocNoAllBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_InvoiceDocNoAllTableAdapter
        '
        Me.Sp_Receiving_SEL_InvoiceDocNoAllTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_InvoiceDocNoAllDataGridView
        '
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.AutoGenerateColumns = False
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9})
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.DataSource = Me.Sp_Receiving_SEL_InvoiceDocNoAllBindingSource
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.Location = New System.Drawing.Point(431, 132)
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.Name = "Sp_Receiving_SEL_InvoiceDocNoAllDataGridView"
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.RowTemplate.Height = 28
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.Size = New System.Drawing.Size(799, 558)
        Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView.TabIndex = 164
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "InvoiceNo"
        Me.DataGridViewTextBoxColumn1.HeaderText = "InvoiceNo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 81
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "InvoiceDate"
        Me.DataGridViewTextBoxColumn2.HeaderText = "InvoiceDate"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 90
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "FromCompany"
        Me.DataGridViewTextBoxColumn3.HeaderText = "FromCompany"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Width = 99
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Grade"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Grade"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 61
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Quantity"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Quantity"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Width = 71
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "UnitPrice"
        Me.DataGridViewTextBoxColumn6.HeaderText = "UnitPrice"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 75
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "QtyBales"
        Me.DataGridViewTextBoxColumn7.HeaderText = "QtyBales"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 74
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Username"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Username"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Width = 80
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "DtRecord"
        Me.DataGridViewTextBoxColumn9.HeaderText = "DtRecord"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Width = 78
        '
        'FrmAddCustomerInvNo
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1253, 730)
        Me.Controls.Add(Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView)
        Me.Controls.Add(Me.InvNoTextBox)
        Me.Controls.Add(Me.QtyBaleTextBox)
        Me.Controls.Add(Me.MetroLabel4)
        Me.Controls.Add(Me.UnitPriceTextBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.QuantityTextBox)
        Me.Controls.Add(Me.MetroLabel8)
        Me.Controls.Add(Me.GradeTextbox)
        Me.Controls.Add(Me.InvDate)
        Me.Controls.Add(Me.MetroLabel2)
        Me.Controls.Add(Me.MetroLabel5)
        Me.Controls.Add(Me.MetroLabel9)
        Me.Controls.Add(Me.CompanyComboBox)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmAddCustomerInvNo"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.SpReceivingSELCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_InvoiceDocNoAllBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_InvoiceDocNoAllDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents ClearTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel24 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents AddMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents CompanyComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvDate As MetroFramework.Controls.MetroDateTime
    Friend WithEvents GradeTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents QuantityTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents UnitPriceTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents QtyBaleTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents SpReceivingSELCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_CompanyTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_CompanyTableAdapter
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_SEL_InvoiceDocNoAllBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_InvoiceDocNoAllTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_InvoiceDocNoAllTableAdapter
    Friend WithEvents Sp_Receiving_SEL_InvoiceDocNoAllDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
