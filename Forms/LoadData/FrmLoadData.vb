﻿Imports System.Data.OleDb
Imports System.IO
Imports FastMember

Public Class FrmLoadData
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim _loadBaleList As New List(Of LTLPackingList)
    Private Sub FrmLoadData_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)

            Me.Sp_Receiving_SEL_SupplierTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Supplier)
            Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNo, _defaultCrop)

            LoadBaleMetroGrid.AutoGenerateColumns = False

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Try
            FrmLogIn.Show()
            Me.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub OpenTile_Click(sender As Object, e As EventArgs) Handles OpenTile.Click
        Try
            Dim myStream As Stream = Nothing
            Dim openFileDialog1 As New OpenFileDialog()

            OpenFileTextbox.Text = ""
            LoadBaleMetroGrid.DataSource = Nothing
            _loadBaleList.Clear()

            ' openFileDialog1.InitialDirectory = "c:\"
            openFileDialog1.Filter = "excel files (*.xls)|*.xls"
            openFileDialog1.FilterIndex = 2
            openFileDialog1.RestoreDirectory = True
            If openFileDialog1.ShowDialog() = DialogResult.OK Then
                Try
                    myStream = openFileDialog1.OpenFile()
                    If (myStream IsNot Nothing) Then
                        ' Insert code to read the stream here.
                        OpenFileTextbox.Text = openFileDialog1.FileName
                        LoadData(openFileDialog1.FileName)
                    End If
                Catch Ex As Exception
                    MessageBox.Show("ไม่สามารถเปิดไฟล์นี้ Original error: " & Ex.Message)
                Finally
                    ' Check this again, since we need to make sure we didn't throw an exception on open.
                    If (myStream IsNot Nothing) Then
                        myStream.Close()
                    End If
                End Try
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub LoadData(ByVal xlsPath As String)
        Try
            Dim connection As System.Data.OleDb.OleDbConnection
            Dim dataSet As System.Data.DataSet
            Dim command As System.Data.OleDb.OleDbDataAdapter

            connection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & xlsPath & ";Extended Properties=Excel 8.0;")
            command = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", connection)
            command.TableMappings.Add("Table", "Net-informations.com")
            dataSet = New System.Data.DataSet
            command.Fill(dataSet)
            connection.Close()

            For Each row As DataRow In dataSet.Tables(0).Rows

                Dim item As New LTLPackingList
                Dim baleFromDB = _businessLayerService.ReceivingBL().GetLTLLoadBaleByBaleBarcode(row(1))

                item.CampainID = Convert.ToInt16(row(0))
                item.BaleBarcode = row(1)
                item.FarmerID = row(2)
                item.StockMvtInDate = Convert.ToDateTime(row(3))
                item.Green = row(4)
                item.Type = row(5)
                item.QualityLTLCode = row(6)
                item.WeightBuy = Convert.ToDecimal(row(7))
                item.Price = Convert.ToDecimal(row(8))
                item.StockOutNo = row(9)
                item.PurchaserID = row(10)
                item.BaleNo = Convert.ToInt16(row(11))
                If baleFromDB Is Nothing Then
                    item.Remark = ""
                Else
                    item.Remark = "Dupplicated"
                End If

                _loadBaleList.Add(item)
            Next

            'LoadBaleMetroGrid.DataSource = dataSet.Tables(0)

            Dim dt As New DataTable()
            Dim reader = ObjectReader.Create(_loadBaleList)
            dt.Load(reader)
            LoadBaleMetroGrid.DataSource = dt

            For i As Integer = 0 To LoadBaleMetroGrid.Rows.Count - 1
                If LoadBaleMetroGrid.Rows(i).Cells(0).Value <> "" Then
                    LoadBaleMetroGrid.Rows(i).Cells(0).Style.ForeColor = Color.Red
                End If
            Next

            If _loadBaleList.Where(Function(x) x.Remark = "Dupplicated").Count > 0 Then
                If MessageBox.Show("พบรายการข้อมูลหมายเลขบาร์โค้ตซ้ำในระบบจำนวน " &
                               _loadBaleList.Where(Function(x) x.Remark = "Dupplicated").Count &
                               " รายการ ท่านต้องการล้างข้อมูลและกลับไปเลือกไฟล์ข้อมูลใหม่หรือไม่?",
                               "warning!",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Warning) = DialogResult.Yes Then
                    LoadBaleMetroGrid.DataSource = Nothing
                    OpenFileTextbox.Text = ""
                    _loadBaleList.Clear()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message & " การโหลดข้อมูลไม่สมบูรณ์ โปรดตรวจสอบไฟล์ข้อมูลให้ถูกต้องก่อนการนำเข้าระบบ", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            LoadBaleMetroGrid.DataSource = Nothing
            OpenFileTextbox.Text = ""
            _loadBaleList.Clear()
        End Try
    End Sub

    Private Sub ClearTile_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            OpenFileTextbox.Text = ""
            If Not LoadBaleMetroGrid.DataSource Is Nothing Then
                LoadBaleMetroGrid.DataSource = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Try
            If OpenFileTextbox.Text = "" Then
                MessageBox.Show("กรุณาเลือกไฟล์ Excel ที่ต้องการอัพโหลด !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If SupplierCombobox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Supplier !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If InvNoComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Pack List Inv No. !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If TruckPackingNo.Text = "" Then
                MessageBox.Show("กรุณาเลือก Truck packing No. !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If TruckNoTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ ป้ายทะเบียนรถ !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            '' ตัด invoice number ให้อยู่ในรูปแบบของ StockOutNo เพื่อตรวจสอบว่าตรงกันหรือไม่
            '' หากไม่ตรงกันจะไม่อนุญาติให้นำเข้าข้อมูล
            'Dim invoiceNoToStockOutNo = InvNoComboBox.Text.Replace("2019", "19").Replace("/STEC", "")
            'If _loadBaleList.Where(Function(x) x.StockOutNo = invoiceNoToStockOutNo).Count <= 0 Then
            '    MessageBox.Show("หมายเลข Packing List Invoice No ที่เลือก ไม่ตรงกับหมายเลข StockOutNo ในไฟล์ข้อมูล ระบบไม่อนุญาติให้นำเข้าข้อมูล โปรดตรวจสอบข้อมูลอีกครั้ง!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Return
            'End If

            If _loadBaleList.Count < 1 Then
                MessageBox.Show("ไม่พบรายการข้อมูลที่ต้องการนำเข้า โปรดตรวจสอบใหม่อีกครั้ง !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If _loadBaleList.Where(Function(x) x.Remark = "Dupplicated").Count > 0 Then
                MessageBox.Show("พบข้อมูลซ้ำในระบบจำนวน " & _loadBaleList.Where(Function(x) x.Remark = "Dupplicated").Count & " รายการ โปรดตรวจสอบข้อมูลให้ถูกต้องอีกครั้งก่อนทำการนำเข้าข้อมูล !",
                                "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            For Each item In _loadBaleList
                db.sp_Receiving_INS_LoadBales(InvNoComboBox.Text,
                                                  TruckPackingNo.Text,
                                                  item.BaleBarcode, ' BaleBarcode
                                                  item.BaleNo, 'BaleNo'
                                                  SupplierCombobox.Text,
                                                  item.Green, ' Green
                                                  item.WeightBuy, 'WeightBuy
                                                  _username,
                                                  TruckNoTextBox.Text,
                                                  "", 'BaleBarcodeFromReplace
                                                  "", 'Remark
                                                  item.CampainID, 'CampaignID
                                                  item.FarmerID, 'FarmerID
                                                  item.StockMvtInDate, 'StockMvtInDate
                                                  item.QualityLTLCode, 'QualityLTLCode
                                                  item.Price, 'Price
                                                  item.StockOutNo, 'StockOutNo
                                                  item.PurchaserID, 'PurchaserID
                                                  item.Type 'Type
                                                  )
            Next

            'For i As Integer = 0 To LoadBaleMetroGrid.RowCount - 1
            '    If (LoadBaleMetroGrid.Item(1, i).Value.ToString <> "") Then
            '        'db.sp_Receiving_INS_LoadBales(InvNoComboBox.Text, TruckPackingNo.Text, MetroGrid.Item(1, i).Value.ToString, Convert.ToInt16(MetroGrid(0, i).Value), SupplierCombobox.Text, MetroGrid.Item(2, i).Value.ToString, Convert.ToDecimal(MetroGrid(3, i).Value), XUsername, TruckNoTextBox.Text, "", "")
            '        db.sp_Receiving_INS_LoadBales(InvNoComboBox.Text,
            '                                      TruckPackingNo.Text,
            '                                      LoadBaleMetroGrid.Item(1, i).Value.ToString, ' BaleBarcode
            '                                      Convert.ToInt16(LoadBaleMetroGrid(11, i).Value), 'BaleNo'
            '                                      SupplierCombobox.Text,
            '                                      LoadBaleMetroGrid.Item(4, i).Value.ToString, ' Green
            '                                      Convert.ToDecimal(LoadBaleMetroGrid.Item(7, i).Value), 'WeightBuy
            '                                      _username,
            '                                      TruckNoTextBox.Text,
            '                                      "", 'BaleBarcodeFromReplace
            '                                      "", 'Remark
            '                                      Convert.ToInt16(LoadBaleMetroGrid.Item(0, i).Value), 'CampaignID
            '                                      LoadBaleMetroGrid.Item(2, i).Value.ToString, 'FarmerID
            '                                      Convert.ToDateTime(LoadBaleMetroGrid.Item(3, i).Value), 'StockMvtInDate
            '                                      LoadBaleMetroGrid.Item(4, i).Value.ToString, 'QualityLTLFarmerCode
            '                                      LoadBaleMetroGrid.Item(6, i).Value, 'QualityLTLCode
            '                                      Convert.ToDecimal(LoadBaleMetroGrid.Item(8, i).Value), 'Price
            '                                      LoadBaleMetroGrid.Item(9, i).Value.ToString, 'StockOutNo
            '                                      LoadBaleMetroGrid.Item(10, i).Value.ToString, 'PurchaserID
            '                                      LoadBaleMetroGrid.Item(5, i).Value.ToString 'Type
            '                                      )
            '    End If
            'Next

            MessageBox.Show("อัพโหลดข้อมูล จำนวน " & LoadBaleMetroGrid.Rows.Count & " รายการ เรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LoadBaleMetroGrid.DataSource = Nothing
            OpenFileTextbox.Text = ""
            TruckPackingNo.Text = ""
            TruckNoTextBox.Text = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Me.Sp_Receiving_SEL_SupplierTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Supplier)
            Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNo, _defaultCrop)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub


End Class