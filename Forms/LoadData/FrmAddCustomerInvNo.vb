﻿Public Class FrmAddCustomerInvNo
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmAddCustomerInvNo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)

            Me.Sp_Receiving_SEL_CompanyTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Company)
            Me.Sp_Receiving_SEL_InvoiceDocNoAllTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNoAll, _defaultCrop)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If InvNoTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Pack list Inv No. !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If InvDate.Text = "" Then
                MessageBox.Show("กรุณาเลือก Pack list Inv Date !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If CompanyComboBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Company !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If GradeTextbox.Text = "" Then
                MessageBox.Show("กรุณาคีย์Grade !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If QuantityTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์Quantiry !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If UnitPriceTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์Unit Price !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
            If QtyBaleTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์Total Bales !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            db.sp_Receiving_INS_InvoiceDocNo(InvNoTextBox.Text, InvDate.Text, CompanyComboBox.Text, GradeTextbox.Text, QuantityTextBox.Text, UnitPriceTextBox.Text, QtyBaleTextBox.Text, _username)
            Me.Sp_Receiving_SEL_InvoiceDocNoAllTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_InvoiceDocNoAll, _defaultCrop)
            GradeTextbox.Text = ""
            QuantityTextBox.Text = ""
            UnitPriceTextBox.Text = ""
            QtyBaleTextBox.Text = ""
            GradeTextbox.Focus()

            MessageBox.Show("บันทึกข้อมูล Receiving no. เรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Try
            Me.Sp_Receiving_SEL_CompanyTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_Company)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearTile_Click(sender As Object, e As EventArgs) Handles ClearTile.Click
        Try
            InvNoTextBox.Text = ""
            CompanyComboBox.Text = ""
            GradeTextbox.Text = ""
            QuantityTextBox.Text = ""
            UnitPriceTextBox.Text = ""
            QtyBaleTextBox.Text = ""
            InvNoTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Try
            FrmLogIn.Show()
            Me.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub QuantityTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QuantityTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub UnitPriceTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles UnitPriceTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub QtyBaleTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles QtyBaleTextBox.KeyPress
        Try
            If Not Char.IsNumber(e.KeyChar) AndAlso Not e.KeyChar = "." AndAlso Not Char.IsControl(e.KeyChar) Then e.KeyChar = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub InvNoTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles InvNoTextBox.KeyUp
        If e.KeyCode <> Keys.Enter Then
            Return
        End If

        GradeTextbox.Focus()
    End Sub

    Private Sub GradeTextbox_KeyUp(sender As Object, e As KeyEventArgs) Handles GradeTextbox.KeyUp
        If e.KeyCode <> Keys.Enter Then
            Return
        End If

        QuantityTextBox.Focus()
    End Sub

    Private Sub QuantityTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles QuantityTextBox.KeyUp
        If e.KeyCode <> Keys.Enter Then
            Return
        End If
        UnitPriceTextBox.Focus()
    End Sub

    Private Sub UnitPriceTextBox_KeyUp(sender As Object, e As KeyEventArgs) Handles UnitPriceTextBox.KeyUp
        If e.KeyCode <> Keys.Enter Then
            Return
        End If
        QtyBaleTextBox.Focus()
    End Sub
End Class