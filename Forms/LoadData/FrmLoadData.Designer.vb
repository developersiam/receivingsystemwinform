﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLoadData
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.OpenFileTextbox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.LoadBaleMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.Remark = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BaleBarcode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BaleNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Green = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QualityLTLCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.WeightBuy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Price = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StockOutNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CampainID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FarmerID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PurchaserID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OpenTile = New MetroFramework.Controls.MetroTile()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.MetroLabel9 = New MetroFramework.Controls.MetroLabel()
        Me.InvNoComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.SpReceivingSELInvoiceDocNoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.TruckNoTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel3 = New MetroFramework.Controls.MetroPanel()
        Me.MetroLabel26 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel25 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel23 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel22 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel14 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel13 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel8 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel10 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel11 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel12 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel15 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel16 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel17 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel18 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel19 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel20 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel21 = New MetroFramework.Controls.MetroLabel()
        Me.MetroTile4 = New MetroFramework.Controls.MetroTile()
        Me.TruckPackingNo = New MetroFramework.Controls.MetroTextBox()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.SupplierCombobox = New MetroFramework.Controls.MetroComboBox()
        Me.SpReceivingSELSupplierBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Receiving_SEL_SupplierTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_SupplierTableAdapter()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.MetroPanel2 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTile2 = New MetroFramework.Controls.MetroTile()
        Me.MetroPanel4 = New MetroFramework.Controls.MetroPanel()
        Me.MetroTile3 = New MetroFramework.Controls.MetroTile()
        Me.SpReceivingSELInvoiceDocNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_InvoiceDocNoTableAdapter()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.LoadBaleMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SpReceivingSELInvoiceDocNoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel3.SuspendLayout()
        CType(Me.SpReceivingSELSupplierBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroPanel2.SuspendLayout()
        Me.MetroPanel4.SuspendLayout()
        CType(Me.SpReceivingSELInvoiceDocNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileTextbox
        '
        '
        '
        '
        Me.OpenFileTextbox.CustomButton.Image = Nothing
        Me.OpenFileTextbox.CustomButton.Location = New System.Drawing.Point(647, 2)
        Me.OpenFileTextbox.CustomButton.Name = ""
        Me.OpenFileTextbox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.OpenFileTextbox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.OpenFileTextbox.CustomButton.TabIndex = 1
        Me.OpenFileTextbox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.OpenFileTextbox.CustomButton.UseSelectable = True
        Me.OpenFileTextbox.CustomButton.Visible = False
        Me.OpenFileTextbox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.OpenFileTextbox.Lines = New String(-1) {}
        Me.OpenFileTextbox.Location = New System.Drawing.Point(52, 47)
        Me.OpenFileTextbox.MaxLength = 32767
        Me.OpenFileTextbox.Name = "OpenFileTextbox"
        Me.OpenFileTextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.OpenFileTextbox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.OpenFileTextbox.SelectedText = ""
        Me.OpenFileTextbox.SelectionLength = 0
        Me.OpenFileTextbox.SelectionStart = 0
        Me.OpenFileTextbox.ShortcutsEnabled = True
        Me.OpenFileTextbox.Size = New System.Drawing.Size(675, 30)
        Me.OpenFileTextbox.Style = MetroFramework.MetroColorStyle.Orange
        Me.OpenFileTextbox.TabIndex = 124
        Me.OpenFileTextbox.UseSelectable = True
        Me.OpenFileTextbox.UseStyleColors = True
        Me.OpenFileTextbox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.OpenFileTextbox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel2.Location = New System.Drawing.Point(7, 47)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(37, 25)
        Me.MetroLabel2.TabIndex = 128
        Me.MetroLabel2.Text = "File"
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.Button2)
        Me.MetroPanel1.Controls.Add(Me.Button1)
        Me.MetroPanel1.Controls.Add(Me.SaveButton)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(6, 34)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1179, 54)
        Me.MetroPanel1.TabIndex = 126
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1126, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(89, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'LoadBaleMetroGrid
        '
        Me.LoadBaleMetroGrid.AllowUserToAddRows = False
        Me.LoadBaleMetroGrid.AllowUserToDeleteRows = False
        Me.LoadBaleMetroGrid.AllowUserToResizeRows = False
        Me.LoadBaleMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.LoadBaleMetroGrid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.LoadBaleMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LoadBaleMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.LoadBaleMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.LoadBaleMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LoadBaleMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.LoadBaleMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.LoadBaleMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Remark, Me.BaleBarcode, Me.BaleNo, Me.Green, Me.QualityLTLCode, Me.WeightBuy, Me.Price, Me.StockOutNo, Me.CampainID, Me.FarmerID, Me.PurchaserID})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.LoadBaleMetroGrid.DefaultCellStyle = DataGridViewCellStyle8
        Me.LoadBaleMetroGrid.EnableHeadersVisualStyles = False
        Me.LoadBaleMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.LoadBaleMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LoadBaleMetroGrid.Location = New System.Drawing.Point(3, 83)
        Me.LoadBaleMetroGrid.Name = "LoadBaleMetroGrid"
        Me.LoadBaleMetroGrid.ReadOnly = True
        Me.LoadBaleMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.LoadBaleMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.LoadBaleMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.LoadBaleMetroGrid.RowTemplate.Height = 24
        Me.LoadBaleMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.LoadBaleMetroGrid.Size = New System.Drawing.Size(763, 560)
        Me.LoadBaleMetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.LoadBaleMetroGrid.TabIndex = 127
        '
        'Remark
        '
        Me.Remark.DataPropertyName = "Remark"
        Me.Remark.HeaderText = "Remark"
        Me.Remark.Name = "Remark"
        Me.Remark.ReadOnly = True
        Me.Remark.Width = 78
        '
        'BaleBarcode
        '
        Me.BaleBarcode.DataPropertyName = "BaleBarcode"
        Me.BaleBarcode.HeaderText = "BaleBarcode"
        Me.BaleBarcode.Name = "BaleBarcode"
        Me.BaleBarcode.ReadOnly = True
        Me.BaleBarcode.Width = 108
        '
        'BaleNo
        '
        Me.BaleNo.DataPropertyName = "BaleNo"
        Me.BaleNo.HeaderText = "BaleNo"
        Me.BaleNo.Name = "BaleNo"
        Me.BaleNo.ReadOnly = True
        Me.BaleNo.Width = 76
        '
        'Green
        '
        Me.Green.DataPropertyName = "Green"
        Me.Green.HeaderText = "Green"
        Me.Green.Name = "Green"
        Me.Green.ReadOnly = True
        Me.Green.Width = 69
        '
        'QualityLTLCode
        '
        Me.QualityLTLCode.DataPropertyName = "QualityLTLCode"
        Me.QualityLTLCode.HeaderText = "QualityLTLCode"
        Me.QualityLTLCode.Name = "QualityLTLCode"
        Me.QualityLTLCode.ReadOnly = True
        Me.QualityLTLCode.Width = 126
        '
        'WeightBuy
        '
        Me.WeightBuy.DataPropertyName = "WeightBuy"
        Me.WeightBuy.HeaderText = "WeightBuy"
        Me.WeightBuy.Name = "WeightBuy"
        Me.WeightBuy.ReadOnly = True
        Me.WeightBuy.Width = 98
        '
        'Price
        '
        Me.Price.DataPropertyName = "Price"
        Me.Price.HeaderText = "Price"
        Me.Price.Name = "Price"
        Me.Price.ReadOnly = True
        Me.Price.Width = 62
        '
        'StockOutNo
        '
        Me.StockOutNo.DataPropertyName = "StockOutNo"
        Me.StockOutNo.HeaderText = "StockOutNo"
        Me.StockOutNo.Name = "StockOutNo"
        Me.StockOutNo.ReadOnly = True
        Me.StockOutNo.Width = 105
        '
        'CampainID
        '
        Me.CampainID.DataPropertyName = "CampainID"
        Me.CampainID.HeaderText = "CampainID"
        Me.CampainID.Name = "CampainID"
        Me.CampainID.ReadOnly = True
        Me.CampainID.Width = 98
        '
        'FarmerID
        '
        Me.FarmerID.DataPropertyName = "FarmerID"
        Me.FarmerID.HeaderText = "FarmerID"
        Me.FarmerID.Name = "FarmerID"
        Me.FarmerID.ReadOnly = True
        Me.FarmerID.Width = 88
        '
        'PurchaserID
        '
        Me.PurchaserID.DataPropertyName = "PurchaserID"
        Me.PurchaserID.HeaderText = "PurchaserID"
        Me.PurchaserID.Name = "PurchaserID"
        Me.PurchaserID.ReadOnly = True
        Me.PurchaserID.Width = 105
        '
        'OpenTile
        '
        Me.OpenTile.ActiveControl = Nothing
        Me.OpenTile.AutoSize = True
        Me.OpenTile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.OpenTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.OpenTile.Location = New System.Drawing.Point(733, 46)
        Me.OpenTile.Name = "OpenTile"
        Me.OpenTile.Size = New System.Drawing.Size(33, 31)
        Me.OpenTile.Style = MetroFramework.MetroColorStyle.White
        Me.OpenTile.TabIndex = 129
        Me.OpenTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.OpenTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.OpenFolder32
        Me.OpenTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.OpenTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.OpenTile.UseSelectable = True
        Me.OpenTile.UseTileImage = True
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MetroLabel9
        '
        Me.MetroLabel9.AutoSize = True
        Me.MetroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel9.Location = New System.Drawing.Point(23, 46)
        Me.MetroLabel9.Name = "MetroLabel9"
        Me.MetroLabel9.Size = New System.Drawing.Size(126, 25)
        Me.MetroLabel9.TabIndex = 134
        Me.MetroLabel9.Text = "PackList Inv no."
        '
        'InvNoComboBox
        '
        Me.InvNoComboBox.DataSource = Me.SpReceivingSELInvoiceDocNoBindingSource1
        Me.InvNoComboBox.DisplayMember = "InvoiceNo"
        Me.InvNoComboBox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.InvNoComboBox.FormattingEnabled = True
        Me.InvNoComboBox.ItemHeight = 29
        Me.InvNoComboBox.Location = New System.Drawing.Point(160, 46)
        Me.InvNoComboBox.Name = "InvNoComboBox"
        Me.InvNoComboBox.Size = New System.Drawing.Size(229, 35)
        Me.InvNoComboBox.TabIndex = 135
        Me.InvNoComboBox.UseSelectable = True
        Me.InvNoComboBox.ValueMember = "InvoiceNo"
        '
        'SpReceivingSELInvoiceDocNoBindingSource1
        '
        Me.SpReceivingSELInvoiceDocNoBindingSource1.DataMember = "sp_Receiving_SEL_InvoiceDocNo"
        Me.SpReceivingSELInvoiceDocNoBindingSource1.DataSource = Me.ReceivingDataSet
        '
        'TruckNoTextBox
        '
        '
        '
        '
        Me.TruckNoTextBox.CustomButton.Image = Nothing
        Me.TruckNoTextBox.CustomButton.Location = New System.Drawing.Point(201, 2)
        Me.TruckNoTextBox.CustomButton.Name = ""
        Me.TruckNoTextBox.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckNoTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckNoTextBox.CustomButton.TabIndex = 1
        Me.TruckNoTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckNoTextBox.CustomButton.UseSelectable = True
        Me.TruckNoTextBox.CustomButton.Visible = False
        Me.TruckNoTextBox.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TruckNoTextBox.Lines = New String(-1) {}
        Me.TruckNoTextBox.Location = New System.Drawing.Point(160, 164)
        Me.TruckNoTextBox.MaxLength = 32767
        Me.TruckNoTextBox.Name = "TruckNoTextBox"
        Me.TruckNoTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckNoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckNoTextBox.SelectedText = ""
        Me.TruckNoTextBox.SelectionLength = 0
        Me.TruckNoTextBox.SelectionStart = 0
        Me.TruckNoTextBox.ShortcutsEnabled = True
        Me.TruckNoTextBox.Size = New System.Drawing.Size(229, 30)
        Me.TruckNoTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.TruckNoTextBox.TabIndex = 137
        Me.TruckNoTextBox.UseCustomBackColor = True
        Me.TruckNoTextBox.UseSelectable = True
        Me.TruckNoTextBox.UseStyleColors = True
        Me.TruckNoTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckNoTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel3.Location = New System.Drawing.Point(72, 169)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(80, 25)
        Me.MetroLabel3.TabIndex = 136
        Me.MetroLabel3.Text = "Truck no."
        '
        'MetroPanel3
        '
        Me.MetroPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel3.Controls.Add(Me.MetroLabel26)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel25)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel23)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel22)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel14)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel13)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel8)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel10)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel11)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel12)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel7)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel15)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel16)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel17)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel18)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel19)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel20)
        Me.MetroPanel3.Controls.Add(Me.MetroLabel21)
        Me.MetroPanel3.Controls.Add(Me.MetroTile4)
        Me.MetroPanel3.HorizontalScrollbarBarColor = True
        Me.MetroPanel3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.HorizontalScrollbarSize = 10
        Me.MetroPanel3.Location = New System.Drawing.Point(7, 311)
        Me.MetroPanel3.Name = "MetroPanel3"
        Me.MetroPanel3.Size = New System.Drawing.Size(394, 434)
        Me.MetroPanel3.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel3.TabIndex = 139
        Me.MetroPanel3.VerticalScrollbarBarColor = True
        Me.MetroPanel3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel3.VerticalScrollbarSize = 10
        '
        'MetroLabel26
        '
        Me.MetroLabel26.AutoSize = True
        Me.MetroLabel26.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel26.Location = New System.Drawing.Point(159, 390)
        Me.MetroLabel26.Name = "MetroLabel26"
        Me.MetroLabel26.Size = New System.Drawing.Size(117, 19)
        Me.MetroLabel26.TabIndex = 155
        Me.MetroLabel26.Text = "คอลัมภ์ J = BaleNo"
        Me.MetroLabel26.UseCustomForeColor = True
        '
        'MetroLabel25
        '
        Me.MetroLabel25.AutoSize = True
        Me.MetroLabel25.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel25.Location = New System.Drawing.Point(159, 365)
        Me.MetroLabel25.Name = "MetroLabel25"
        Me.MetroLabel25.Size = New System.Drawing.Size(143, 19)
        Me.MetroLabel25.TabIndex = 154
        Me.MetroLabel25.Text = "คอลัมภ์ I = StockOutNo"
        Me.MetroLabel25.UseCustomForeColor = True
        '
        'MetroLabel23
        '
        Me.MetroLabel23.AutoSize = True
        Me.MetroLabel23.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel23.Location = New System.Drawing.Point(159, 340)
        Me.MetroLabel23.Name = "MetroLabel23"
        Me.MetroLabel23.Size = New System.Drawing.Size(107, 19)
        Me.MetroLabel23.TabIndex = 153
        Me.MetroLabel23.Text = "คอลัมภ์ H = Price"
        Me.MetroLabel23.UseCustomForeColor = True
        '
        'MetroLabel22
        '
        Me.MetroLabel22.AutoSize = True
        Me.MetroLabel22.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel22.Location = New System.Drawing.Point(159, 315)
        Me.MetroLabel22.Name = "MetroLabel22"
        Me.MetroLabel22.Size = New System.Drawing.Size(172, 19)
        Me.MetroLabel22.TabIndex = 152
        Me.MetroLabel22.Text = "คอลัมภ์ G = StockMvtWeight"
        Me.MetroLabel22.UseCustomForeColor = True
        '
        'MetroLabel14
        '
        Me.MetroLabel14.AutoSize = True
        Me.MetroLabel14.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel14.Location = New System.Drawing.Point(159, 290)
        Me.MetroLabel14.Name = "MetroLabel14"
        Me.MetroLabel14.Size = New System.Drawing.Size(166, 19)
        Me.MetroLabel14.TabIndex = 151
        Me.MetroLabel14.Text = "คอลัมภ์ F = QualityLTLCode"
        Me.MetroLabel14.UseCustomForeColor = True
        '
        'MetroLabel13
        '
        Me.MetroLabel13.AutoSize = True
        Me.MetroLabel13.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel13.Location = New System.Drawing.Point(159, 265)
        Me.MetroLabel13.Name = "MetroLabel13"
        Me.MetroLabel13.Size = New System.Drawing.Size(191, 19)
        Me.MetroLabel13.TabIndex = 150
        Me.MetroLabel13.Text = "คอลัมภ์ E = QualityFarmerCode"
        Me.MetroLabel13.UseCustomForeColor = True
        '
        'MetroLabel8
        '
        Me.MetroLabel8.AutoSize = True
        Me.MetroLabel8.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel8.Location = New System.Drawing.Point(159, 240)
        Me.MetroLabel8.Name = "MetroLabel8"
        Me.MetroLabel8.Size = New System.Drawing.Size(168, 19)
        Me.MetroLabel8.TabIndex = 149
        Me.MetroLabel8.Text = "คอลัมภ์ D = StockMvtInDate"
        Me.MetroLabel8.UseCustomForeColor = True
        '
        'MetroLabel10
        '
        Me.MetroLabel10.AutoSize = True
        Me.MetroLabel10.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel10.Location = New System.Drawing.Point(159, 215)
        Me.MetroLabel10.Name = "MetroLabel10"
        Me.MetroLabel10.Size = New System.Drawing.Size(132, 19)
        Me.MetroLabel10.TabIndex = 148
        Me.MetroLabel10.Text = "คอลัมภ์ C = FarmerID"
        Me.MetroLabel10.UseCustomForeColor = True
        '
        'MetroLabel11
        '
        Me.MetroLabel11.AutoSize = True
        Me.MetroLabel11.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel11.Location = New System.Drawing.Point(159, 190)
        Me.MetroLabel11.Name = "MetroLabel11"
        Me.MetroLabel11.Size = New System.Drawing.Size(159, 19)
        Me.MetroLabel11.TabIndex = 147
        Me.MetroLabel11.Text = "คอลัมภ์ B = StockMvtCBID"
        Me.MetroLabel11.UseCustomForeColor = True
        '
        'MetroLabel12
        '
        Me.MetroLabel12.AutoSize = True
        Me.MetroLabel12.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel12.Location = New System.Drawing.Point(159, 165)
        Me.MetroLabel12.Name = "MetroLabel12"
        Me.MetroLabel12.Size = New System.Drawing.Size(151, 19)
        Me.MetroLabel12.TabIndex = 146
        Me.MetroLabel12.Text = "คอลัมภ์ A = CampaignID"
        Me.MetroLabel12.UseCustomForeColor = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel7.Location = New System.Drawing.Point(9, 43)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(133, 19)
        Me.MetroLabel7.TabIndex = 145
        Me.MetroLabel7.Text = "1.แถวที่1แสดงชื่อคอลัมภ์"
        Me.MetroLabel7.UseCustomForeColor = True
        '
        'MetroLabel15
        '
        Me.MetroLabel15.AutoSize = True
        Me.MetroLabel15.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel15.Location = New System.Drawing.Point(9, 240)
        Me.MetroLabel15.Name = "MetroLabel15"
        Me.MetroLabel15.Size = New System.Drawing.Size(116, 19)
        Me.MetroLabel15.TabIndex = 144
        Me.MetroLabel15.Text = "คอลัมภ์ D = น้ำหนัก"
        Me.MetroLabel15.UseCustomForeColor = True
        Me.MetroLabel15.Visible = False
        '
        'MetroLabel16
        '
        Me.MetroLabel16.AutoSize = True
        Me.MetroLabel16.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel16.Location = New System.Drawing.Point(9, 215)
        Me.MetroLabel16.Name = "MetroLabel16"
        Me.MetroLabel16.Size = New System.Drawing.Size(104, 19)
        Me.MetroLabel16.TabIndex = 143
        Me.MetroLabel16.Text = "คอลัมภ์ C = เกรด"
        Me.MetroLabel16.UseCustomForeColor = True
        Me.MetroLabel16.Visible = False
        '
        'MetroLabel17
        '
        Me.MetroLabel17.AutoSize = True
        Me.MetroLabel17.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel17.Location = New System.Drawing.Point(9, 190)
        Me.MetroLabel17.Name = "MetroLabel17"
        Me.MetroLabel17.Size = New System.Drawing.Size(126, 19)
        Me.MetroLabel17.TabIndex = 142
        Me.MetroLabel17.Text = "คอลัมภ์ B = Barcode"
        Me.MetroLabel17.UseCustomForeColor = True
        Me.MetroLabel17.Visible = False
        '
        'MetroLabel18
        '
        Me.MetroLabel18.AutoSize = True
        Me.MetroLabel18.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel18.Location = New System.Drawing.Point(9, 165)
        Me.MetroLabel18.Name = "MetroLabel18"
        Me.MetroLabel18.Size = New System.Drawing.Size(116, 19)
        Me.MetroLabel18.TabIndex = 141
        Me.MetroLabel18.Text = "คอลัมภ์ A = ลำดับที่"
        Me.MetroLabel18.UseCustomForeColor = True
        Me.MetroLabel18.Visible = False
        '
        'MetroLabel19
        '
        Me.MetroLabel19.AutoSize = True
        Me.MetroLabel19.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel19.Location = New System.Drawing.Point(9, 118)
        Me.MetroLabel19.Name = "MetroLabel19"
        Me.MetroLabel19.Size = New System.Drawing.Size(268, 19)
        Me.MetroLabel19.TabIndex = 140
        Me.MetroLabel19.Text = "4.แถวสุดท้ายคือห่อยารายการสุดท้าย(ไม่ต้องมีสรุป)"
        Me.MetroLabel19.UseCustomForeColor = True
        '
        'MetroLabel20
        '
        Me.MetroLabel20.AutoSize = True
        Me.MetroLabel20.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel20.Location = New System.Drawing.Point(9, 93)
        Me.MetroLabel20.Name = "MetroLabel20"
        Me.MetroLabel20.Size = New System.Drawing.Size(130, 19)
        Me.MetroLabel20.TabIndex = 139
        Me.MetroLabel20.Text = "3.ชื่อ Sheet คือ Sheet1"
        Me.MetroLabel20.UseCustomForeColor = True
        '
        'MetroLabel21
        '
        Me.MetroLabel21.AutoSize = True
        Me.MetroLabel21.ForeColor = System.Drawing.Color.Purple
        Me.MetroLabel21.Location = New System.Drawing.Point(9, 68)
        Me.MetroLabel21.Name = "MetroLabel21"
        Me.MetroLabel21.Size = New System.Drawing.Size(152, 19)
        Me.MetroLabel21.TabIndex = 138
        Me.MetroLabel21.Text = "2.ห่อยาให้แสดงตั้งแต่แถวที่2"
        Me.MetroLabel21.UseCustomForeColor = True
        '
        'MetroTile4
        '
        Me.MetroTile4.ActiveControl = Nothing
        Me.MetroTile4.AutoSize = True
        Me.MetroTile4.Dock = System.Windows.Forms.DockStyle.Top
        Me.MetroTile4.Location = New System.Drawing.Point(0, 0)
        Me.MetroTile4.Name = "MetroTile4"
        Me.MetroTile4.Size = New System.Drawing.Size(392, 40)
        Me.MetroTile4.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroTile4.TabIndex = 69
        Me.MetroTile4.Text = "ข้ำกำหนด Excel ที่ใช้อัพโหลด"
        Me.MetroTile4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile4.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile4.UseSelectable = True
        '
        'TruckPackingNo
        '
        Me.TruckPackingNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.TruckPackingNo.CustomButton.Image = Nothing
        Me.TruckPackingNo.CustomButton.Location = New System.Drawing.Point(201, 2)
        Me.TruckPackingNo.CustomButton.Name = ""
        Me.TruckPackingNo.CustomButton.Size = New System.Drawing.Size(25, 25)
        Me.TruckPackingNo.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.TruckPackingNo.CustomButton.TabIndex = 1
        Me.TruckPackingNo.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.TruckPackingNo.CustomButton.UseSelectable = True
        Me.TruckPackingNo.CustomButton.Visible = False
        Me.TruckPackingNo.FontSize = MetroFramework.MetroTextBoxSize.Tall
        Me.TruckPackingNo.Lines = New String(-1) {}
        Me.TruckPackingNo.Location = New System.Drawing.Point(160, 128)
        Me.TruckPackingNo.MaxLength = 32767
        Me.TruckPackingNo.Name = "TruckPackingNo"
        Me.TruckPackingNo.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.TruckPackingNo.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.TruckPackingNo.SelectedText = ""
        Me.TruckPackingNo.SelectionLength = 0
        Me.TruckPackingNo.SelectionStart = 0
        Me.TruckPackingNo.ShortcutsEnabled = True
        Me.TruckPackingNo.Size = New System.Drawing.Size(229, 30)
        Me.TruckPackingNo.Style = MetroFramework.MetroColorStyle.Orange
        Me.TruckPackingNo.TabIndex = 141
        Me.TruckPackingNo.UseCustomBackColor = True
        Me.TruckPackingNo.UseSelectable = True
        Me.TruckPackingNo.UseStyleColors = True
        Me.TruckPackingNo.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.TruckPackingNo.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroLabel4
        '
        Me.MetroLabel4.AutoSize = True
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel4.Location = New System.Drawing.Point(5, 133)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(146, 25)
        Me.MetroLabel4.TabIndex = 140
        Me.MetroLabel4.Text = "Truck Packing No."
        '
        'SupplierCombobox
        '
        Me.SupplierCombobox.DataSource = Me.SpReceivingSELSupplierBindingSource
        Me.SupplierCombobox.DisplayMember = "code"
        Me.SupplierCombobox.FontSize = MetroFramework.MetroComboBoxSize.Tall
        Me.SupplierCombobox.FormattingEnabled = True
        Me.SupplierCombobox.ItemHeight = 29
        Me.SupplierCombobox.Location = New System.Drawing.Point(160, 87)
        Me.SupplierCombobox.Name = "SupplierCombobox"
        Me.SupplierCombobox.Size = New System.Drawing.Size(229, 35)
        Me.SupplierCombobox.TabIndex = 143
        Me.SupplierCombobox.UseSelectable = True
        Me.SupplierCombobox.ValueMember = "code"
        '
        'SpReceivingSELSupplierBindingSource
        '
        Me.SpReceivingSELSupplierBindingSource.DataMember = "sp_Receiving_SEL_Supplier"
        Me.SpReceivingSELSupplierBindingSource.DataSource = Me.ReceivingDataSet
        '
        'MetroLabel5
        '
        Me.MetroLabel5.AutoSize = True
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel5.Location = New System.Drawing.Point(76, 88)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(74, 25)
        Me.MetroLabel5.TabIndex = 142
        Me.MetroLabel5.Text = "Supplier"
        '
        'Sp_Receiving_SEL_SupplierTableAdapter
        '
        Me.Sp_Receiving_SEL_SupplierTableAdapter.ClearBeforeFill = True
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ReceivingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'MetroPanel2
        '
        Me.MetroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel2.Controls.Add(Me.MetroLabel5)
        Me.MetroPanel2.Controls.Add(Me.SupplierCombobox)
        Me.MetroPanel2.Controls.Add(Me.MetroTile2)
        Me.MetroPanel2.Controls.Add(Me.TruckPackingNo)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel9)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel4)
        Me.MetroPanel2.Controls.Add(Me.InvNoComboBox)
        Me.MetroPanel2.Controls.Add(Me.MetroLabel3)
        Me.MetroPanel2.Controls.Add(Me.TruckNoTextBox)
        Me.MetroPanel2.HorizontalScrollbarBarColor = True
        Me.MetroPanel2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.HorizontalScrollbarSize = 10
        Me.MetroPanel2.Location = New System.Drawing.Point(6, 98)
        Me.MetroPanel2.Name = "MetroPanel2"
        Me.MetroPanel2.Size = New System.Drawing.Size(394, 207)
        Me.MetroPanel2.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel2.TabIndex = 144
        Me.MetroPanel2.VerticalScrollbarBarColor = True
        Me.MetroPanel2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel2.VerticalScrollbarSize = 10
        '
        'MetroTile2
        '
        Me.MetroTile2.ActiveControl = Nothing
        Me.MetroTile2.AutoSize = True
        Me.MetroTile2.Dock = System.Windows.Forms.DockStyle.Top
        Me.MetroTile2.Location = New System.Drawing.Point(0, 0)
        Me.MetroTile2.Name = "MetroTile2"
        Me.MetroTile2.Size = New System.Drawing.Size(392, 40)
        Me.MetroTile2.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroTile2.TabIndex = 69
        Me.MetroTile2.Text = "PackList"
        Me.MetroTile2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile2.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile2.UseSelectable = True
        '
        'MetroPanel4
        '
        Me.MetroPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroPanel4.Controls.Add(Me.MetroTile3)
        Me.MetroPanel4.Controls.Add(Me.MetroLabel2)
        Me.MetroPanel4.Controls.Add(Me.OpenTile)
        Me.MetroPanel4.Controls.Add(Me.LoadBaleMetroGrid)
        Me.MetroPanel4.Controls.Add(Me.OpenFileTextbox)
        Me.MetroPanel4.HorizontalScrollbarBarColor = True
        Me.MetroPanel4.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel4.HorizontalScrollbarSize = 10
        Me.MetroPanel4.Location = New System.Drawing.Point(408, 97)
        Me.MetroPanel4.Name = "MetroPanel4"
        Me.MetroPanel4.Size = New System.Drawing.Size(777, 648)
        Me.MetroPanel4.Style = MetroFramework.MetroColorStyle.Lime
        Me.MetroPanel4.TabIndex = 145
        Me.MetroPanel4.VerticalScrollbarBarColor = True
        Me.MetroPanel4.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel4.VerticalScrollbarSize = 10
        '
        'MetroTile3
        '
        Me.MetroTile3.ActiveControl = Nothing
        Me.MetroTile3.AutoSize = True
        Me.MetroTile3.Dock = System.Windows.Forms.DockStyle.Top
        Me.MetroTile3.Location = New System.Drawing.Point(0, 0)
        Me.MetroTile3.Name = "MetroTile3"
        Me.MetroTile3.Size = New System.Drawing.Size(775, 40)
        Me.MetroTile3.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroTile3.TabIndex = 69
        Me.MetroTile3.Text = "PackList"
        Me.MetroTile3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile3.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Bold
        Me.MetroTile3.UseSelectable = True
        '
        'SpReceivingSELInvoiceDocNoBindingSource
        '
        Me.SpReceivingSELInvoiceDocNoBindingSource.DataMember = "sp_Receiving_SEL_InvoiceDocNo"
        Me.SpReceivingSELInvoiceDocNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_InvoiceDocNoTableAdapter
        '
        Me.Sp_Receiving_SEL_InvoiceDocNoTableAdapter.ClearBeforeFill = True
        '
        'SaveButton
        '
        Me.SaveButton.AutoSize = True
        Me.SaveButton.BackColor = System.Drawing.Color.White
        Me.SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.SaveButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SaveButton.FlatAppearance.BorderSize = 0
        Me.SaveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.SaveButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.SaveButton.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.SaveButton.ForeColor = System.Drawing.Color.Black
        Me.SaveButton.Image = Global.ReceivingSystem.My.Resources.Resources.Save32
        Me.SaveButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.SaveButton.Location = New System.Drawing.Point(403, 1)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(52, 51)
        Me.SaveButton.TabIndex = 178
        Me.SaveButton.Text = "save"
        Me.SaveButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.SaveButton.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.AutoSize = True
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Image = Global.ReceivingSystem.My.Resources.Resources.CancelFile32
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(459, 1)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(52, 51)
        Me.Button1.TabIndex = 179
        Me.Button1.Text = "clear"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.AutoSize = True
        Me.Button2.BackColor = System.Drawing.Color.White
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Teal
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button2.Font = New System.Drawing.Font("Tahoma", 7.0!)
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Image = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button2.Location = New System.Drawing.Point(515, 1)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(52, 51)
        Me.Button2.TabIndex = 180
        Me.Button2.Text = "refresh"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.UseVisualStyleBackColor = False
        '
        'FrmLoadData
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1198, 768)
        Me.Controls.Add(Me.MetroPanel4)
        Me.Controls.Add(Me.MetroPanel2)
        Me.Controls.Add(Me.MetroPanel3)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmLoadData"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.LoadBaleMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SpReceivingSELInvoiceDocNoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel3.ResumeLayout(False)
        Me.MetroPanel3.PerformLayout()
        CType(Me.SpReceivingSELSupplierBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroPanel2.ResumeLayout(False)
        Me.MetroPanel2.PerformLayout()
        Me.MetroPanel4.ResumeLayout(False)
        Me.MetroPanel4.PerformLayout()
        CType(Me.SpReceivingSELInvoiceDocNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileTextbox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents LoadBaleMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents OpenTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel9 As MetroFramework.Controls.MetroLabel
    Friend WithEvents InvNoComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents TruckNoTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel3 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel15 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel16 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel17 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel18 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel19 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel20 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel21 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroTile4 As MetroFramework.Controls.MetroTile
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents TruckPackingNo As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SupplierCombobox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents SpReceivingSELSupplierBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_SupplierTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_SupplierTableAdapter
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents MetroPanel2 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroTile2 As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroPanel4 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroTile3 As MetroFramework.Controls.MetroTile
    Friend WithEvents SpReceivingSELInvoiceDocNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_InvoiceDocNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_InvoiceDocNoTableAdapter
    Friend WithEvents SpReceivingSELInvoiceDocNoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel25 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel23 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel22 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel14 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel13 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel8 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel10 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel11 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel12 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel26 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Remark As DataGridViewTextBoxColumn
    Friend WithEvents BaleBarcode As DataGridViewTextBoxColumn
    Friend WithEvents BaleNo As DataGridViewTextBoxColumn
    Friend WithEvents Green As DataGridViewTextBoxColumn
    Friend WithEvents QualityLTLCode As DataGridViewTextBoxColumn
    Friend WithEvents WeightBuy As DataGridViewTextBoxColumn
    Friend WithEvents Price As DataGridViewTextBoxColumn
    Friend WithEvents StockOutNo As DataGridViewTextBoxColumn
    Friend WithEvents CampainID As DataGridViewTextBoxColumn
    Friend WithEvents FarmerID As DataGridViewTextBoxColumn
    Friend WithEvents PurchaserID As DataGridViewTextBoxColumn
    Friend WithEvents SaveButton As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
End Class
