﻿Public Class FrmLoadDataMenu
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmLoadDataMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ReceivingDataSet.security' table. You can move, or remove it, as needed.        
        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

  
    Private Sub BtnLoadData_Click(sender As Object, e As EventArgs) Handles BtnLoadData.Click
        Try
            Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim baleBarcodeRow As ReceivingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
            baleBarcodeRow = Me.ReceivingDataSet.security.FindByuname(XUsernameApprove)
            If baleBarcodeRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not baleBarcodeRow Is Nothing Then
                'เช็ค password โดยจะต้องใช้ store  decodePassword เพื่อนำ password ที่ทำการเข้ารหัสแต่ละครั้งออกมา โดยต้องเข้ารหัสจำนวน 3 ครั้งจึงจะได้ password ที่แท้จริง
                XPassword = baleBarcodeRow.pwd
                For i As Integer = 1 To 3
                    Me.Sp_Receiving_DecodePasswordTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_DecodePassword, XPassword)
                    XPassword = Me.ReceivingDataSet.sp_Receiving_DecodePassword.Rows(0).Item("PWDanswer")
                Next

                If baleBarcodeRow.uname = XUsernameApprove And (baleBarcodeRow.checking = True Or baleBarcodeRow.depart = "Administer") Then
                    FrmLoadData.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If


            'FrmAddDocNo.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BtnInputInvNo_Click(sender As Object, e As EventArgs) Handles BtnInputInvNo.Click
        Try
            Dim XPassword As String
            Dim XUsernameApprove As String
            XUsernameApprove = Trim(UsernameMetroLabel.Text)

            'Get baleBarcodeRow detail 
            Dim baleBarcodeRow As ReceivingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
            baleBarcodeRow = Me.ReceivingDataSet.security.FindByuname(XUsernameApprove)
            If baleBarcodeRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            ElseIf Not baleBarcodeRow Is Nothing Then
                'เช็ค password โดยจะต้องใช้ store  decodePassword เพื่อนำ password ที่ทำการเข้ารหัสแต่ละครั้งออกมา โดยต้องเข้ารหัสจำนวน 3 ครั้งจึงจะได้ password ที่แท้จริง
                XPassword = baleBarcodeRow.pwd
                For i As Integer = 1 To 3
                    Me.Sp_Receiving_DecodePasswordTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_DecodePassword, XPassword)
                    XPassword = Me.ReceivingDataSet.sp_Receiving_DecodePassword.Rows(0).Item("PWDanswer")
                Next

                If baleBarcodeRow.uname = XUsernameApprove And (baleBarcodeRow.checking = True Or baleBarcodeRow.depart = "Administer") Then
                    FrmAddCustomerInvNo.ShowDialog()
                Else
                    MessageBox.Show("คุณไม่มีสิทธิ์ใช้งานโมดูลนี้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Return
                End If
            End If


            'FrmAddDocNo.ShowDialog()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

   

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Try
            FrmLogIn.Show()
            Me.Dispose()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class