﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmApproveFromChecker
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.Save = New MetroFramework.Controls.MetroTile()
        Me.SaveMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.RefreshTile = New MetroFramework.Controls.MetroTile()
        Me.MetroLabel6 = New MetroFramework.Controls.MetroLabel()
        Me.BackMetroTile = New MetroFramework.Controls.MetroTile()
        Me.MetroTile1 = New MetroFramework.Controls.MetroTile()
        Me.UsernameMetroLabel = New MetroFramework.Controls.MetroLabel()
        Me.RequestApproveMetroGrid = New MetroFramework.Controls.MetroGrid()
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.RequestTypeComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.ApproveStatusComboBox = New MetroFramework.Controls.MetroComboBox()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter()
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter()
        Me.Crop = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RcNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateFromChecker = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Checker = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RemarkName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateFromApproved = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ApproveStatus = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ApproveBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RequestNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MetroPanel1.SuspendLayout()
        CType(Me.RequestApproveMetroGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroPanel1
        '
        Me.MetroPanel1.Controls.Add(Me.Save)
        Me.MetroPanel1.Controls.Add(Me.SaveMetroLabel)
        Me.MetroPanel1.Controls.Add(Me.RefreshTile)
        Me.MetroPanel1.Controls.Add(Me.MetroLabel6)
        Me.MetroPanel1.Controls.Add(Me.BackMetroTile)
        Me.MetroPanel1.Controls.Add(Me.MetroTile1)
        Me.MetroPanel1.Controls.Add(Me.UsernameMetroLabel)
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 33)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(1381, 54)
        Me.MetroPanel1.TabIndex = 119
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'Save
        '
        Me.Save.ActiveControl = Nothing
        Me.Save.AutoSize = True
        Me.Save.BackColor = System.Drawing.Color.White
        Me.Save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Save.Location = New System.Drawing.Point(854, 13)
        Me.Save.Name = "Save"
        Me.Save.Size = New System.Drawing.Size(38, 35)
        Me.Save.Style = MetroFramework.MetroColorStyle.White
        Me.Save.TabIndex = 123
        Me.Save.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Save.TileImage = Global.ReceivingSystem.My.Resources.Resources.Save32
        Me.Save.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Save.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.Save.UseSelectable = True
        Me.Save.UseTileImage = True
        '
        'SaveMetroLabel
        '
        Me.SaveMetroLabel.AutoSize = True
        Me.SaveMetroLabel.Location = New System.Drawing.Point(898, 21)
        Me.SaveMetroLabel.Name = "SaveMetroLabel"
        Me.SaveMetroLabel.Size = New System.Drawing.Size(37, 20)
        Me.SaveMetroLabel.TabIndex = 122
        Me.SaveMetroLabel.Text = "save"
        '
        'RefreshTile
        '
        Me.RefreshTile.ActiveControl = Nothing
        Me.RefreshTile.AutoSize = True
        Me.RefreshTile.BackColor = System.Drawing.Color.White
        Me.RefreshTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshTile.Location = New System.Drawing.Point(965, 13)
        Me.RefreshTile.Name = "RefreshTile"
        Me.RefreshTile.Size = New System.Drawing.Size(36, 35)
        Me.RefreshTile.Style = MetroFramework.MetroColorStyle.White
        Me.RefreshTile.TabIndex = 121
        Me.RefreshTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.Refresh32
        Me.RefreshTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.RefreshTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.RefreshTile.UseSelectable = True
        Me.RefreshTile.UseTileImage = True
        '
        'MetroLabel6
        '
        Me.MetroLabel6.AutoSize = True
        Me.MetroLabel6.Location = New System.Drawing.Point(1007, 20)
        Me.MetroLabel6.Name = "MetroLabel6"
        Me.MetroLabel6.Size = New System.Drawing.Size(53, 20)
        Me.MetroLabel6.TabIndex = 116
        Me.MetroLabel6.Text = "refresh"
        '
        'BackMetroTile
        '
        Me.BackMetroTile.ActiveControl = Nothing
        Me.BackMetroTile.AutoSize = True
        Me.BackMetroTile.BackColor = System.Drawing.Color.White
        Me.BackMetroTile.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BackMetroTile.Location = New System.Drawing.Point(1114, 3)
        Me.BackMetroTile.Name = "BackMetroTile"
        Me.BackMetroTile.Size = New System.Drawing.Size(50, 48)
        Me.BackMetroTile.Style = MetroFramework.MetroColorStyle.White
        Me.BackMetroTile.TabIndex = 114
        Me.BackMetroTile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileImage = Global.ReceivingSystem.My.Resources.Resources.CircledLeft50
        Me.BackMetroTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BackMetroTile.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.BackMetroTile.UseSelectable = True
        Me.BackMetroTile.UseTileImage = True
        '
        'MetroTile1
        '
        Me.MetroTile1.ActiveControl = Nothing
        Me.MetroTile1.AutoSize = True
        Me.MetroTile1.BackColor = System.Drawing.Color.White
        Me.MetroTile1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTile1.Location = New System.Drawing.Point(3, 1)
        Me.MetroTile1.Name = "MetroTile1"
        Me.MetroTile1.Size = New System.Drawing.Size(57, 47)
        Me.MetroTile1.Style = MetroFramework.MetroColorStyle.White
        Me.MetroTile1.TabIndex = 111
        Me.MetroTile1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroTile1.TileImage = Global.ReceivingSystem.My.Resources.Resources.GenderNeutralUser50
        Me.MetroTile1.TileImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroTile1.TileTextFontSize = MetroFramework.MetroTileTextSize.Tall
        Me.MetroTile1.UseSelectable = True
        Me.MetroTile1.UseTileImage = True
        '
        'UsernameMetroLabel
        '
        Me.UsernameMetroLabel.AutoSize = True
        Me.UsernameMetroLabel.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.UsernameMetroLabel.Location = New System.Drawing.Point(56, 20)
        Me.UsernameMetroLabel.Name = "UsernameMetroLabel"
        Me.UsernameMetroLabel.Size = New System.Drawing.Size(92, 25)
        Me.UsernameMetroLabel.Style = MetroFramework.MetroColorStyle.Lime
        Me.UsernameMetroLabel.TabIndex = 112
        Me.UsernameMetroLabel.Text = "Username"
        '
        'RequestApproveMetroGrid
        '
        Me.RequestApproveMetroGrid.AllowUserToAddRows = False
        Me.RequestApproveMetroGrid.AllowUserToDeleteRows = False
        Me.RequestApproveMetroGrid.AllowUserToResizeRows = False
        Me.RequestApproveMetroGrid.AutoGenerateColumns = False
        Me.RequestApproveMetroGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.RequestApproveMetroGrid.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.RequestApproveMetroGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RequestApproveMetroGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.RequestApproveMetroGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RequestApproveMetroGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.RequestApproveMetroGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.RequestApproveMetroGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Crop, Me.RequestType, Me.RcNo, Me.BC, Me.DateFromChecker, Me.Checker, Me.RemarkName, Me.DateFromApproved, Me.ApproveStatus, Me.ApproveBy, Me.RequestNo})
        Me.RequestApproveMetroGrid.DataSource = Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer), CType(CType(136, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.RequestApproveMetroGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.RequestApproveMetroGrid.EnableHeadersVisualStyles = False
        Me.RequestApproveMetroGrid.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.RequestApproveMetroGrid.GridColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.RequestApproveMetroGrid.Location = New System.Drawing.Point(9, 140)
        Me.RequestApproveMetroGrid.Name = "RequestApproveMetroGrid"
        Me.RequestApproveMetroGrid.ReadOnly = True
        Me.RequestApproveMetroGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(119, Byte), Integer), CType(CType(53, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(72, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.RequestApproveMetroGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.RequestApproveMetroGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.RequestApproveMetroGrid.RowTemplate.Height = 24
        Me.RequestApproveMetroGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.RequestApproveMetroGrid.Size = New System.Drawing.Size(1261, 529)
        Me.RequestApproveMetroGrid.Style = MetroFramework.MetroColorStyle.Orange
        Me.RequestApproveMetroGrid.TabIndex = 133
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource.DataMember = "sp_Receiving_SEL_ReceivingCheckerRequest"
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource.DataSource = Me.ReceivingDataSet
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RequestTypeComboBox
        '
        Me.RequestTypeComboBox.FormattingEnabled = True
        Me.RequestTypeComboBox.ItemHeight = 24
        Me.RequestTypeComboBox.Items.AddRange(New Object() {"Edit", "Delete"})
        Me.RequestTypeComboBox.Location = New System.Drawing.Point(118, 104)
        Me.RequestTypeComboBox.Name = "RequestTypeComboBox"
        Me.RequestTypeComboBox.Size = New System.Drawing.Size(158, 30)
        Me.RequestTypeComboBox.TabIndex = 136
        Me.RequestTypeComboBox.UseSelectable = True
        '
        'MetroLabel7
        '
        Me.MetroLabel7.AutoSize = True
        Me.MetroLabel7.Location = New System.Drawing.Point(12, 105)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(90, 20)
        Me.MetroLabel7.TabIndex = 135
        Me.MetroLabel7.Text = "Request type"
        '
        'ApproveStatusComboBox
        '
        Me.ApproveStatusComboBox.FormattingEnabled = True
        Me.ApproveStatusComboBox.ItemHeight = 24
        Me.ApproveStatusComboBox.Items.AddRange(New Object() {"Not approve", "Approve"})
        Me.ApproveStatusComboBox.Location = New System.Drawing.Point(428, 104)
        Me.ApproveStatusComboBox.Name = "ApproveStatusComboBox"
        Me.ApproveStatusComboBox.Size = New System.Drawing.Size(172, 30)
        Me.ApproveStatusComboBox.TabIndex = 138
        Me.ApproveStatusComboBox.UseSelectable = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.Location = New System.Drawing.Point(321, 105)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(101, 20)
        Me.MetroLabel3.TabIndex = 137
        Me.MetroLabel3.Text = "Approve status"
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.Connection = Nothing
        Me.TableAdapterManager.securityTableAdapter = Nothing
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter
        '
        Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.ClearBeforeFill = True
        '
        'Sp_Receiving_SEL_MatByRcNoBindingSource
        '
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo"
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatByRcNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.ClearBeforeFill = True
        '
        'Crop
        '
        Me.Crop.DataPropertyName = "Crop"
        Me.Crop.HeaderText = "Crop"
        Me.Crop.Name = "Crop"
        Me.Crop.ReadOnly = True
        Me.Crop.Width = 70
        '
        'RequestType
        '
        Me.RequestType.DataPropertyName = "RequestType"
        Me.RequestType.HeaderText = "RequestType"
        Me.RequestType.Name = "RequestType"
        Me.RequestType.ReadOnly = True
        Me.RequestType.Width = 131
        '
        'RcNo
        '
        Me.RcNo.DataPropertyName = "RcNo"
        Me.RcNo.HeaderText = "RcNo"
        Me.RcNo.Name = "RcNo"
        Me.RcNo.ReadOnly = True
        Me.RcNo.Width = 73
        '
        'BC
        '
        Me.BC.DataPropertyName = "BC"
        Me.BC.HeaderText = "BC"
        Me.BC.Name = "BC"
        Me.BC.ReadOnly = True
        Me.BC.Width = 53
        '
        'DateFromChecker
        '
        Me.DateFromChecker.DataPropertyName = "DateFromChecker"
        Me.DateFromChecker.HeaderText = "DateFromChecker"
        Me.DateFromChecker.Name = "DateFromChecker"
        Me.DateFromChecker.ReadOnly = True
        Me.DateFromChecker.Width = 171
        '
        'Checker
        '
        Me.Checker.DataPropertyName = "Checker"
        Me.Checker.HeaderText = "Checker"
        Me.Checker.Name = "Checker"
        Me.Checker.ReadOnly = True
        Me.Checker.Width = 94
        '
        'RemarkName
        '
        Me.RemarkName.DataPropertyName = "RemarkName"
        Me.RemarkName.HeaderText = "RemarkName"
        Me.RemarkName.Name = "RemarkName"
        Me.RemarkName.ReadOnly = True
        Me.RemarkName.Width = 137
        '
        'DateFromApproved
        '
        Me.DateFromApproved.DataPropertyName = "DateFromApproved"
        Me.DateFromApproved.HeaderText = "DateFromApproved"
        Me.DateFromApproved.Name = "DateFromApproved"
        Me.DateFromApproved.ReadOnly = True
        Me.DateFromApproved.Width = 186
        '
        'ApproveStatus
        '
        Me.ApproveStatus.DataPropertyName = "ApproveStatus"
        Me.ApproveStatus.HeaderText = "ApproveStatus"
        Me.ApproveStatus.Name = "ApproveStatus"
        Me.ApproveStatus.ReadOnly = True
        Me.ApproveStatus.Width = 128
        '
        'ApproveBy
        '
        Me.ApproveBy.DataPropertyName = "ApproveBy"
        Me.ApproveBy.HeaderText = "ApproveBy"
        Me.ApproveBy.Name = "ApproveBy"
        Me.ApproveBy.ReadOnly = True
        Me.ApproveBy.Width = 118
        '
        'RequestNo
        '
        Me.RequestNo.DataPropertyName = "RequestNo"
        Me.RequestNo.HeaderText = "RequestNo"
        Me.RequestNo.Name = "RequestNo"
        Me.RequestNo.ReadOnly = True
        Me.RequestNo.Visible = False
        Me.RequestNo.Width = 117
        '
        'FrmApproveFromChecker
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1700, 701)
        Me.Controls.Add(Me.ApproveStatusComboBox)
        Me.Controls.Add(Me.MetroLabel3)
        Me.Controls.Add(Me.RequestTypeComboBox)
        Me.Controls.Add(Me.MetroLabel7)
        Me.Controls.Add(Me.RequestApproveMetroGrid)
        Me.Controls.Add(Me.MetroPanel1)
        Me.Name = "FrmApproveFromChecker"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.MetroPanel1.ResumeLayout(False)
        Me.MetroPanel1.PerformLayout()
        CType(Me.RequestApproveMetroGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents RefreshTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroLabel6 As MetroFramework.Controls.MetroLabel
    Friend WithEvents BackMetroTile As MetroFramework.Controls.MetroTile
    Friend WithEvents MetroTile1 As MetroFramework.Controls.MetroTile
    Friend WithEvents UsernameMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents Save As MetroFramework.Controls.MetroTile
    Friend WithEvents SaveMetroLabel As MetroFramework.Controls.MetroLabel
    Friend WithEvents RequestApproveMetroGrid As MetroFramework.Controls.MetroGrid
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents RequestTypeComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ApproveStatusComboBox As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter
    Friend WithEvents Crop As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RcNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateFromChecker As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Checker As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RemarkName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateFromApproved As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ApproveStatus As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ApproveBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RequestNo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
