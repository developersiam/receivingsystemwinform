﻿Imports ReceivingSystem

Public Interface IReceivingDocumentBL
    Function GetMatRcByCropAndType(crop As Int32, type As String, checkerlock As Boolean) As List(Of sp_Receiving_SEL_MatRC_Result)
    Function GetByChecker(crop As Int32, type As String, checkerlock As Boolean) As List(Of sp_Receiving_SEL_MatRCByReceivingFinish_Result)
    Function GetMatRcByID(crop As Int32, type As String, rcno As String) As sp_Receiving_SEL_MatRCByRcNo_Result
    Function GetMatRcByRcno(rcno As String) As matrc
    Function GetMatForAddDocno(rcno As String) As List(Of sp_Receiving_SEL_MatbyRcNoGroupByField_Result)
    Function GetMatSupTypeCompanyByRcno(rcno As String) As sp_Receiving_SEL_MatSubtypeCompany_Result
    Function GetMaxRcno(crop As Int32) As String
    Function GetWarehouses() As List(Of sp_Receiving_SEL_MatWH_Result)
    Function GetClassifiers() As List(Of sp_Receiving_SEL_BuyerClassifier_Result)
    Function GetInvoices(crop As Int32) As List(Of sp_Receiving_SEL_InvoiceDocNo_Result)
    Function GetBalesByInvoiceNo(invoiceNo As String) As List(Of sp_Receiving_SEL_LoadBaleByInvoiceNo_Result)
    Sub DeleteMatRc(rcno As String)
    Sub AddMatRc(crop As Int32, type As String, subtype As String,
                        company As String, rcFrom As String, user As String, place As String,
                        truckno As String, classifier As String, buyer As String, remark As String,
                        machine As String, transportationDocumentCode As String, invoiceNo As String)
    Sub EditMatRc(matrc As matrc)
    Function GetNotReceiveBalesFromBuyingSystem(transportationCode As String, crop As Integer, rcno As String) As List(Of sp_Receiving_SEL_BCRemainingFromBuyingSystem_Result)
End Interface
Public Class ReceivingDocumentBL
    Implements IReceivingDocumentBL

    Public Sub AddMatRc(crop As Int32, type As String, subtype As String,
                        company As String, rcFrom As String, user As String, place As String,
                        truckno As String, classifier As String, buyer As String, remark As String,
                        machine As String, transportationDocumentCode As String, invoiceNo As String) Implements IReceivingDocumentBL.AddMatRc
        Try
            Using _context As New StecDBMSEntities
                'Dim matrc As New matrc
                'matrc = _context.matrcs.SingleOrDefault(Function(x) x.rcno = rcno)
                'If matrc Is Nothing Then
                '    Throw New ArgumentException("ไม่พบข้อมูล receiving number นี้ในระบบ")
                'End If

                '_context.matrcs.Remove(matrc)
                '_context.SaveChanges()

                'db.sp_Receiving_INS_MatSubtypeCompany(rcno, SubtypeComboBox.Text, CompanyComboBox.Text, RcFromComboBox.Text) 'insert into MatSubtypeCompany 
                'db.sp_Receiving_INS_MatRcNo(_defaultCrop, Convert.ToInt32(Microsoft.VisualBasic.Right(rcno, 4)), _username) 'insert into MatRCNo

                'Get Receiving number from database 
                Dim rcno As String = _businessLayerService.ReceivingDocumentBL().GetMaxRcno(_defaultCrop)

                _context.sp_Receiving_INS_MatSubtypeCompany(rcno, subtype, company, rcFrom)
                _context.sp_Receiving_INS_MatRcNo(crop, Convert.ToInt32(Microsoft.VisualBasic.Right(rcno, 4)), user)
                _context.sp_Receiving_INS_MatRC(rcno, crop, type, Now, place, truckno, buyer, classifier, Now, rcFrom, remark, machine, user, transportationDocumentCode, invoiceNo)
                _context.SaveChanges()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub DeleteMatRc(rcno As String) Implements IReceivingDocumentBL.DeleteMatRc
        Try
            Using _context As New StecDBMSEntities
                'Dim matrc As New matrc
                'matrc = _context.matrcs.SingleOrDefault(Function(x) x.rcno = rcno)
                'If matrc Is Nothing Then
                '    Throw New ArgumentException("ไม่พบข้อมูล receiving number นี้ในระบบ")
                'End If
                '_context.matrcs.Remove(matrc)
                '_context.SaveChanges()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub EditMatRc(matrc As matrc) Implements IReceivingDocumentBL.EditMatRc
        Try
            Using _context As New StecDBMSEntities
                'Dim matrc As New matrc
                'matrc = _context.matrcs.SingleOrDefault(Function(x) x.rcno = rcno)
                'If matrc Is Nothing Then
                '    Throw New ArgumentException("ไม่พบข้อมูล receiving number นี้ในระบบ")
                'End If

                '_context.matrcs.Remove(matrc)
                '_context.SaveChanges()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetBalesByInvoiceNo(invoiceNo As String) As List(Of sp_Receiving_SEL_LoadBaleByInvoiceNo_Result) Implements IReceivingDocumentBL.GetBalesByInvoiceNo
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_LoadBaleByInvoiceNo(invoiceNo).ToList()
        End Using
    End Function

    Public Function GetByChecker(crop As Integer, type As String, checkerlock As Boolean) As List(Of sp_Receiving_SEL_MatRCByReceivingFinish_Result) Implements IReceivingDocumentBL.GetByChecker
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatRCByReceivingFinish(crop, type, checkerlock).ToList()
        End Using
    End Function

    Public Function GetClassifiers() As List(Of sp_Receiving_SEL_BuyerClassifier_Result) Implements IReceivingDocumentBL.GetClassifiers
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BuyerClassifier().ToList()
        End Using
    End Function
    Public Function GetInvoices(crop As Int32) As List(Of sp_Receiving_SEL_InvoiceDocNo_Result) Implements IReceivingDocumentBL.GetInvoices
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_InvoiceDocNo(crop).ToList()
        End Using
    End Function
    Public Function GetMatForAddDocno(rcno As String) As List(Of sp_Receiving_SEL_MatbyRcNoGroupByField_Result) Implements IReceivingDocumentBL.GetMatForAddDocno
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatbyRcNoGroupByField(rcno).ToList()
        End Using
    End Function
    Public Function GetMatRcByCropAndType(crop As Integer, type As String, checkerlock As Boolean) As List(Of sp_Receiving_SEL_MatRC_Result) Implements IReceivingDocumentBL.GetMatRcByCropAndType
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatRC(crop, type, checkerlock).OrderByDescending(Function(x) x.rcno).ToList()
        End Using
    End Function
    Public Function GetMatRcByID(crop As Int32, type As String, rcno As String) As sp_Receiving_SEL_MatRCByRcNo_Result Implements IReceivingDocumentBL.GetMatRcByID
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatRCByRcNo(crop, type, rcno).SingleOrDefault()
        End Using
    End Function

    Public Function GetMatRcByRcno(rcno As String) As matrc Implements IReceivingDocumentBL.GetMatRcByRcno
        Using _context As New StecDBMSEntities
            Return _context.matrcs.SingleOrDefault(Function(x) x.rcno = rcno)
        End Using
    End Function

    Public Function GetMatSupTypeCompanyByRcno(rcno As String) As sp_Receiving_SEL_MatSubtypeCompany_Result Implements IReceivingDocumentBL.GetMatSupTypeCompanyByRcno
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatSubtypeCompany(rcno).SingleOrDefault()
        End Using
    End Function

    Public Function GetMaxRcno(crop As Integer) As String Implements IReceivingDocumentBL.GetMaxRcno
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_GETMAX_MatRcNo(crop).Single().RcNo
        End Using
    End Function

    Public Function GetNotReceiveBalesFromBuyingSystem(transportationCode As String, crop As Integer, rcno As String) As List(Of sp_Receiving_SEL_BCRemainingFromBuyingSystem_Result) Implements IReceivingDocumentBL.GetNotReceiveBalesFromBuyingSystem
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BCRemainingFromBuyingSystem(transportationCode, crop, rcno).ToList()
        End Using
    End Function

    Public Function GetWarehouses() As List(Of sp_Receiving_SEL_MatWH_Result) Implements IReceivingDocumentBL.GetWarehouses
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatWH().ToList()
        End Using
    End Function
End Class
