﻿Imports ReceivingSystem

Public Interface ISupplierBL
    Function GetSuppliers() As List(Of supplier)
    Function GetSupplierFromBUBuyingSystemByBaleBarcode(baleBarcode As String) As String
End Interface
Public Class SupplierBL
    Implements ISupplierBL
    Public Function GetSupplierFromBUBuyingSystemByBaleBarcode(baleBarcode As String) As String Implements ISupplierBL.GetSupplierFromBUBuyingSystemByBaleBarcode
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_SupplierByBaleBarcode(baleBarcode).ToString
        End Using
    End Function
    Public Function GetSuppliers() As List(Of supplier) Implements ISupplierBL.GetSuppliers
        Using _context As New StecDBMSEntities
            Return _context.suppliers.OrderBy(Function(x) x.code).ToList()
        End Using
    End Function
End Class
