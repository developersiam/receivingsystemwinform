﻿Imports ReceivingSystem

Public Interface ICompanyBL
    Function GetCompanies() As List(Of sp_Receiving_SEL_Company_Result)
    Function GetCompanyByCompanyCode(companyCode As String) As sp_Receiving_SEL_Company_Result
End Interface
Public Class CompanyBL
    Implements ICompanyBL
    Public Function GetCompanies() As List(Of sp_Receiving_SEL_Company_Result) Implements ICompanyBL.GetCompanies
        Using dbContext As New StecDBMSEntities
            Return dbContext.sp_Receiving_SEL_Company().OrderBy(Function(x) x.name).ToList()
        End Using
    End Function

    Public Function GetCompanyByCompanyCode(companyCode As String) As sp_Receiving_SEL_Company_Result Implements ICompanyBL.GetCompanyByCompanyCode
        Using dbContext As New StecDBMSEntities
            Return dbContext.sp_Receiving_SEL_Company().SingleOrDefault(Function(x) x.code = companyCode)
        End Using
    End Function
End Class