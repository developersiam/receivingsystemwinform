﻿Imports ReceivingSystem

Public Interface INTRMInspectionBL
    Function GetByBaleBarcode(bc As String) As sp_NTRM_SEL_NTRMInspectionByBC_Result
End Interface

Public Class NTRMInspectionBL
    Implements INTRMInspectionBL

    Public Function GetByBaleBarcode(bc As String) As sp_NTRM_SEL_NTRMInspectionByBC_Result Implements INTRMInspectionBL.GetByBaleBarcode
        Using _context As New StecDBMSEntities
            Dim model = _context.sp_NTRM_SEL_NTRMInspectionByBC(bc).SingleOrDefault()
            Return model
        End Using
    End Function
End Class
