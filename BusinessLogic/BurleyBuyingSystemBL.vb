﻿Imports ReceivingSystem

Public Interface IBurleyBuyingSystemBL
    Function GetTransportationDocumentByID(transportationCode As String) As sp_GetTransportationDocumentByTransportationCode_Result
    Function GetBuyingDetailByBaleBarcode(baleBarcode As String) As sp_GetBuyingDataByBaleBarcode_Result
    Function GetMoistureSummaryByFarmer(farmerCode As String) As Integer
    Function GetIncidentFarmertByBaleBarcode(baleBarcode As String) As List(Of sp_GetIncidentFarmerByBaleBarcode_Result)
End Interface
Public Class BurleyBuyingSystemBL
    Implements IBurleyBuyingSystemBL
    Public Function GetBuyingDetailByBaleBarcode(baleBarcode As String) As sp_GetBuyingDataByBaleBarcode_Result Implements IBurleyBuyingSystemBL.GetBuyingDetailByBaleBarcode
        Using _context As New BuyingSystemEntities
            Return _context.sp_GetBuyingDataByBaleBarcode(baleBarcode).SingleOrDefault()
        End Using
    End Function

    Public Function GetIncidentFarmertByBaleBarcode(baleBarcode As String) As List(Of sp_GetIncidentFarmerByBaleBarcode_Result) Implements IBurleyBuyingSystemBL.GetIncidentFarmertByBaleBarcode
        Using _context As New BuyingSystemEntities
            Return _context.sp_GetIncidentFarmerByBaleBarcode(baleBarcode).ToList()
        End Using
    End Function

    Public Function GetMoistureSummaryByFarmer(farmerCode As String) As Integer Implements IBurleyBuyingSystemBL.GetMoistureSummaryByFarmer
        Using _context As New BuyingSystemEntities
            Return _context.sp_GetCountMoistureByFarmer(farmerCode).SingleOrDefault()
        End Using
    End Function

    Public Function GetTransportationDocumentByID(transportationCode As String) As sp_GetTransportationDocumentByTransportationCode_Result Implements IBurleyBuyingSystemBL.GetTransportationDocumentByID
        Using _context As New BuyingSystemEntities
            Return _context.sp_GetTransportationDocumentByTransportationCode(transportationCode).SingleOrDefault()
        End Using
    End Function
End Class
