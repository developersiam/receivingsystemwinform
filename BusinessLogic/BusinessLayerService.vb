﻿Public Class BusinessLayerService
    Public Function TypeBL() As ITypeBL
        Dim obj As New TypeBL()
        Return obj
    End Function

    Public Function SupplierBL() As ISupplierBL
        Dim obj As New SupplierBL()
        Return obj
    End Function

    Public Function ClassifyBL() As IClassifyBL
        Dim obj As New ClassifyBL()
        Return obj
    End Function
    Public Function GreenBL() As IGreenBL
        Dim obj As New GreenBL()
        Return obj
    End Function

    Public Function ReceivingDocumentBL() As IReceivingDocumentBL
        Dim obj As New ReceivingDocumentBL()
        Return obj
    End Function

    Public Function ReceivingBL() As IReceivingBL
        Dim obj As New ReceivingBL()
        Return obj
    End Function

    Public Function CompanyBL() As ICompanyBL
        Dim obj As New CompanyBL()
        Return obj
    End Function

    Public Function BurleyBuyingSystemBL() As IBurleyBuyingSystemBL
        Dim obj As New BurleyBuyingSystemBL()
        Return obj
    End Function
    Public Function FCVBuyingBL() As IFCVBuyingBL
        Dim obj As New FCVBuyingBL()
        Return obj
    End Function
    Public Function CPABatchBL() As ICPABatchBL
        Dim obj As New CPABatchBL()
        Return obj
    End Function
    Public Function QCLabBL() As IQCLabBL
        Dim obj As New QCLabBL()
        Return obj
    End Function
    Public Function NTRMInspectionBL() As INTRMInspectionBL
        Dim obj As New NTRMInspectionBL()
        Return obj
    End Function
End Class
