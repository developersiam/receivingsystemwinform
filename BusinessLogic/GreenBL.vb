﻿Imports ReceivingSystem

Public Interface IGreenBL
    Function GetGreens(crop As Int32, type As String) As List(Of sp_Receiving_SEL_Green_Result)

End Interface
Public Class GreenBL
    Implements IGreenBL

    Public Function GetGreens(crop As Int32, type As String) As List(Of sp_Receiving_SEL_Green_Result) Implements IGreenBL.GetGreens
        Using dbContext As New StecDBMSEntities
            Return dbContext.sp_Receiving_SEL_Green(crop, type).OrderBy(Function(x) x.green).ToList()
            'Return dbContext.sp_Receiving_SEL_Green(crop, type) _
            '    .Select(Function(x) New green With {
            '        .crop = x.crop,
            '        .type = x.type,
            '        .green = x.green,
            '        .level = x.level,
            '        .p1 = x.p1,
            '        .p2 = x.p2,
            '        .p3 = x.p3,
            '        .p4 = x.p4,
            '        .p5 = x.p5,
            '        .p6 = x.p6,
            '        .p7 = x.p7,
            '        .p8 = x.p8,
            '        .p9 = x.p9,
            '        .p10 = x.p10,
            '        .p11 = x.p11,
            '        .p12 = x.p12,
            '        .p13 = x.p13,
            '        .p14 = x.p14,
            '        .p15 = x.p15,
            '        .dtrecord = x.dtrecord,
            '        .rcuser = x.rcuser,
            '        .leafuser = x.leafuser,
            '        .group = x.group}).ToList()
        End Using
    End Function
End Class
