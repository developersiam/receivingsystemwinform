﻿Imports ReceivingSystem

Public Interface ICPABatchBL
    Function GetUnfinishCPABatchByRCLine(rcno As String, rcLine As String) As List(Of UnfinishBatchNo)
    Sub Finish(batchNo As String, finishUser As String)
    Sub Add(crop As Integer, classifyGroup As String, rcLine As String, rcno As String)
End Interface

Public Class CPABatchBL
    Implements ICPABatchBL

    Public Sub Add(crop As Integer, classifyGroup As String, rcLine As String, rcno As String) Implements ICPABatchBL.Add
        Try
            If classifyGroup Is Nothing Then
                Throw New ArgumentException("โปรดระบุ classify group")
            End If
            If rcLine Is Nothing Then
                Throw New ArgumentException("โปรดระบุ rc line")
            End If
            If rcno Is Nothing Then
                Throw New ArgumentException("โปรดระบุ rcno")
            End If


            Using resource As New StecDBMSEntities
                Dim _cpaGroupList = resource.CPABatches.Where(Function(x) x.ClassifyGroup = classifyGroup And x.Crop = crop).ToList()
                Dim _runno = 1

                If _cpaGroupList.Count > 0 Then
                    _runno = _cpaGroupList.Max(Function(x) x.RunNo) + 1
                End If

                ' example batch no : 20-BT00003
                Dim _batchNo = Right(crop, 2) & "-" & classifyGroup & _runno.ToString().PadLeft(5, "0")

                resource.CPABatches.Add(New CPABatch With {.Crop = crop,
                                        .RunNo = _runno,
                                        .ClassifyGroup = classifyGroup,
                                        .RCLines = rcLine,
                                        .DtRecord = Now,
                                        .BatchNo = _batchNo,
                                        .Finished = False,
                                        .FinishedUser = Nothing,
                                        .FinishedDatetime = Nothing,
                                        .RCNo = rcno})
                resource.SaveChanges()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub Finish(batchNo As String, finishUser As String) Implements ICPABatchBL.Finish
        Try
            If batchNo Is Nothing Then
                Throw New ArgumentException("โปรดระบุ batch no")
            End If

            Using resource As New StecDBMSEntities

                Dim model = resource.CPABatches.Single(Function(x) x.BatchNo = batchNo)

                model.Finished = True
                model.FinishedDatetime = Now
                model.FinishedUser = finishUser

                resource.SaveChanges()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetUnfinishCPABatchByRCLine(rcno As String, rcLine As String) As List(Of UnfinishBatchNo) Implements ICPABatchBL.GetUnfinishCPABatchByRCLine
        Try
            Using resource As New StecDBMSEntities
                Return resource.sp_Receiving_SEL_BatchNoByRcLine(rcno, rcLine).
                    Select(Function(x) New UnfinishBatchNo With {
                    .Crop = x.Crop,
                    .ClassifyGroup = x.ClassifyGroup,
                    .RCLines = x.RCLines,
                    .BatchNo = x.BatchNo,
                    .RCNo = x.RCNo,
                    .NBales = x.NBales,
                    .Bales = x.Bales}).
                    ToList()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
