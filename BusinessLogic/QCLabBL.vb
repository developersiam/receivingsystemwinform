﻿Imports ReceivingSystem

Public Interface IQCLabBL
    Function GetCPAResultByLabCode(labCode As Integer, batchNo As String) As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result)
    Function GetCPASetup() As List(Of CPASetup)
End Interface
Public Class QCLabBL
    Implements IQCLabBL

    Public Function GetCPAResultByLabCode(labCode As Integer, batchNo As String) As List(Of sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo_Result) Implements IQCLabBL.GetCPAResultByLabCode
        Try
            Using resource As New StecDBMSEntities
                Return resource.sp_QClab_SEL_CPAResultsByLabCodeANDBatchNo(labCode, batchNo).ToList()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCPASetup() As List(Of CPASetup) Implements IQCLabBL.GetCPASetup
        Try
            Using resource As New StecDBMSEntities
                Return resource.CPASetups.ToList()
            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
