﻿Imports ReceivingSystem

Public Interface IFCVBuyingBL
    Function GetBaleBarcodeDetailByBarcode(baleBarcode As String) As sp_GetBuyingDetailByBaleBarcode_Result
    Function GetBaleBarcodeDetailByBarcodeV2(baleBarcode As String) As sp_Receiving_SEL_FCVBuyingInfoByBaleBarcode_Result
    Function GetFarmerByCrop(crop As Short) As List(Of sp_Receiving_SEL_Farmer_Result)
End Interface
Public Class FCVBuyingBL
    Implements IFCVBuyingBL
    Public Function GetBaleBarcodeDetailByBarcode(baleBarcode As String) As sp_GetBuyingDetailByBaleBarcode_Result Implements IFCVBuyingBL.GetBaleBarcodeDetailByBarcode
        Using _context As New FCBuyingSystemEntities
            Return _context.sp_GetBuyingDetailByBaleBarcode(baleBarcode).SingleOrDefault()
        End Using
    End Function

    Public Function GetBaleBarcodeDetailByBarcodeV2(baleBarcode As String) As sp_Receiving_SEL_FCVBuyingInfoByBaleBarcode_Result Implements IFCVBuyingBL.GetBaleBarcodeDetailByBarcodeV2
        Using _context As New FCVBuyingSystemEntities
            Return _context.sp_Receiving_SEL_FCVBuyingInfoByBaleBarcode(baleBarcode).SingleOrDefault()
        End Using
    End Function

    Public Function GetFarmerByCrop(crop As Short) As List(Of sp_Receiving_SEL_Farmer_Result) Implements IFCVBuyingBL.GetFarmerByCrop
        Using _context As New FCVBuyingSystemEntities
            Return _context.sp_Receiving_SEL_Farmer(crop).ToList()
        End Using
    End Function
End Class
