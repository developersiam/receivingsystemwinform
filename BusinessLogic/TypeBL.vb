﻿Imports ReceivingSystem

Public Interface ITypeBL
    Function GetTypes() As List(Of type)
    Function GetSubTypes() As List(Of subtype)
End Interface
Public Class TypeBL
    Implements ITypeBL

    Public Function GetSubTypes() As List(Of subtype) Implements ITypeBL.GetSubTypes
        Using dbContext As New StecDBMSEntities
            Return dbContext.subtypes.OrderBy(Function(x) x.subtype1).ToList()
        End Using
    End Function

    Public Function GetTypes() As List(Of type) Implements ITypeBL.GetTypes
        Using dbContext As New StecDBMSEntities
            Return dbContext.types.OrderBy(Function(x) x.type1).ToList()
        End Using
    End Function
End Class
