﻿Imports ReceivingSystem

Public Interface IClassifyBL
    Function GetClassifyByType(type As String) As List(Of classify)
    Function GetLTLClassifyByType(type As String) As List(Of classify)
End Interface
Public Class ClassifyBL
    Implements IClassifyBL
    Public Function GetClassifyByType(type As String) As List(Of classify) Implements IClassifyBL.GetClassifyByType
        Using dbContext As New StecDBMSEntities
            Return dbContext.classifies.Where(Function(x) x.type = type And x.Laos = False).OrderBy(Function(x) x.classify1).ToList()
        End Using
    End Function

    Public Function GetLTLClassifyByType(type As String) As List(Of classify) Implements IClassifyBL.GetLTLClassifyByType
        Using dbContext As New StecDBMSEntities
            Return dbContext.classifies.Where(Function(x) x.type = type And x.Laos = True).OrderBy(Function(x) x.classify1).ToList()
        End Using
    End Function
End Class
