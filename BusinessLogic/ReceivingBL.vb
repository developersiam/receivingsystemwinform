﻿Imports ReceivingSystem

Public Interface IReceivingBL
    Function GetMatByRcno(rcno As String) As List(Of sp_Receiving_SEL_MatByRcNo_Result)
    Function GetMatByBatchNo(batchNo As String) As List(Of sp_Receiving_SEL_MatByBatchNo_Result)
    Function GetMatByBaleBarcode(baleBarcode As String) As sp_Receiving_SEL_MatByBC_Result
    Function GetLTLLoadBaleByBaleBarcode(baleBarcode As String) As sp_Receiving_SEL_LoadBaleByBaleBarcode_Result
    Function GetLTLLoadBaleNotReceiveByInvoiceNo(invoiceNo As String) As List(Of sp_Receiving_SEL_LoadBaleNotReceiveByInvoiceNo_Result)
    Function GetNTRMInspectionByBaleBarcode(baleBarcode As String) As List(Of sp_NTRM_SEL_NTRMInspectionByBC_Result)
    Function GetMatByCrop(crop As Int32, baleBarcode As String) As sp_Receiving_SEL_Mat_Result
    Function GetDupplicateBaleNumberBySupplier(crop As Int32, type As String, supplier As String, baleNumber As Int32) As List(Of sp_Receiving_SEL_MatBySupplierBaleno_Result)
    Function GetNTRMTypes() As List(Of sp_NTRM_SEL_NTRMTYPE_Result)
    Function GetBatchNoByX(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByX_Result
    Function GetBatchNoByLX(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByLX_Result
    Function GetBatchNoByC(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByC_Result
    Function GetBatchNoByBT(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByBT_Result
    Function GetLTLBatchGroup() As Integer
    Function GetMaxBaleNoForNPI(crop As Int32, supplier As String) As String
    Function GetMaxBatchNo(crop As Int32, cpaGroup As String, rcLines As String, rcno As String, finishhUser As String, fromType As String) As List(Of sp_Receiving_GETMAX_BatchNo_Result)
    Function GetSupplierGrouByBatchNo(batchNo As String) As List(Of sp_Receiving_SEL_SupplierGroubyBatchNo_Result)
    Function GetRemainingBurleyReceivingBaleFromTransportationCode(transportationCode As String, crop As Int16, rcno As String) As List(Of sp_Receiving_SEL_BCRemainingFromBuyingSystem_Result)
    Function GetRemainingLTLReceivingBaleFromInvoiceNo(invoiceNo As String) As List(Of sp_Receiving_SEL_LoadBaleNotReceiveByInvoiceNo_Result)
    Function GetFcvNpiFarmers() As List(Of NPI_Farmer)
    Function GetFcvNpiFarmersV2(crop As Short) As List(Of sp_Receiving_SEL_Farmer_Result)
    Sub FcvNpiReceivingAdd(rcno As String, crop As Integer, type As String, subType As String, company As String,
                             rcfrom As String, bc As String, supplier As String, baleNo As Integer, farmerCode As String,
                             green As String, classify As String, weight As Decimal, weightbuy As Decimal, bz As Boolean,
                             wh As String, wet As Boolean, receivingUser As String, remarkChecker As String, remarkCheckerDate As DateTime,
                             remarkCheckerUser As String, classifier As String, replaceBale As Boolean)
    Sub FcvNpiReceivingEdit(bc As String, supplier As String, baleNo As Integer, farmerCode As String,
                                    green As String, classify As String, weight As Decimal, weightbuy As Decimal,
                                    greenOld As String, classifyOld As String, weightOld As Decimal, weightbuyOld As Decimal,
                                   receivingUser As String, remark As String, remarkChecker As String,
                                   remarkCheckerDate As DateTime, remarkCheckerUser As String, classifier As String, replaceBale As Boolean)
End Interface
Public Class ReceivingBL
    Implements IReceivingBL
    Public Sub FcvNpiReceivingAdd(rcno As String, crop As Integer, type As String, subType As String, company As String,
                                    rcfrom As String, bc As String, supplier As String, baleNo As Integer, farmerCode As String,
                                    green As String, classify As String, weight As Decimal, weightbuy As Decimal, bz As Boolean,
                                    wh As String, wet As Boolean, receivingUser As String, remarkChecker As String, remarkCheckerDate As DateTime,
                                    remarkCheckerUser As String, classifier As String, replaceBale As Boolean) Implements IReceivingBL.FcvNpiReceivingAdd
        Using _context As New StecDBMSEntities
            _context.sp_Receiving_INS_MatCPA_ForNPI(rcno, crop, type, subType, company, rcfrom, bc, supplier, baleNo, farmerCode, green, classify,
                                                    weight, weightbuy, bz, wh, receivingUser, remarkChecker, remarkCheckerDate, remarkCheckerUser, classifier, replaceBale)
        End Using
    End Sub
    Public Sub FcvNpiReceivingEdit(bc As String, supplier As String, baleNo As Integer, farmerCode As String,
                                    green As String, classify As String, weight As Decimal, weightbuy As Decimal,
                                    greenOld As String, classifyOld As String, weightOld As Decimal, weightbuyOld As Decimal,
                                   receivingUser As String, remark As String, remarkChecker As String,
                                   remarkCheckerDate As DateTime, remarkCheckerUser As String, classifier As String, replaceBale As Boolean) Implements IReceivingBL.FcvNpiReceivingEdit
        Using _context As New StecDBMSEntities
            _context.sp_Receiving_UPD_MatCPAForNPI(bc, supplier, baleNo, farmerCode, green, classify, weight, weightbuy, receivingUser,
                                                   greenOld, classifyOld, weightbuyOld, weightOld,
                                                   remark, remarkChecker, remarkCheckerDate, remarkCheckerUser,
                                                   classifier, replaceBale)
        End Using
    End Sub
    Public Function GetBatchNoByBT(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByBT_Result Implements IReceivingBL.GetBatchNoByBT
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BatchNoByBT(receivingLine, rcno).SingleOrDefault()
        End Using
    End Function
    Public Function GetBatchNoByC(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByC_Result Implements IReceivingBL.GetBatchNoByC
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BatchNoByC(receivingLine, rcno).SingleOrDefault()
        End Using
    End Function
    Public Function GetBatchNoByLX(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByLX_Result Implements IReceivingBL.GetBatchNoByLX
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BatchNoByLX(receivingLine, rcno).SingleOrDefault()
        End Using
    End Function
    Public Function GetBatchNoByX(receivingLine As String, rcno As String) As sp_Receiving_SEL_BatchNoByX_Result Implements IReceivingBL.GetBatchNoByX
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BatchNoByX(receivingLine, rcno).SingleOrDefault()
        End Using
    End Function
    Public Function GetDupplicateBaleNumberBySupplier(crop As Integer, type As String, supplier As String, baleNumber As Integer) As List(Of sp_Receiving_SEL_MatBySupplierBaleno_Result) Implements IReceivingBL.GetDupplicateBaleNumberBySupplier
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatBySupplierBaleno(crop, type, supplier, baleNumber).ToList()
        End Using
    End Function
    Public Function GetFcvNpiFarmers() As List(Of NPI_Farmer) Implements IReceivingBL.GetFcvNpiFarmers
        Using _context As New StecDBMSEntities
            Return _context.NPI_Farmer.ToList()
        End Using
    End Function
    Public Function GetFcvNpiFarmersV2(crop As Short) As List(Of sp_Receiving_SEL_Farmer_Result) Implements IReceivingBL.GetFcvNpiFarmersV2
        Using _context As New FCVBuyingSystemEntities
            Return _context.sp_Receiving_SEL_Farmer(crop).ToList()
        End Using
    End Function
    Public Function GetLTLBatchGroup() As Integer Implements IReceivingBL.GetLTLBatchGroup
        Using _context As New StecDBMSEntities
            Return Convert.ToInt32(_context.sp_Receiving_SEL_LTLBATCHGROUP())
        End Using
    End Function
    Public Function GetLTLLoadBaleByBaleBarcode(baleBarcode As String) As sp_Receiving_SEL_LoadBaleByBaleBarcode_Result Implements IReceivingBL.GetLTLLoadBaleByBaleBarcode
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_LoadBaleByBaleBarcode(baleBarcode).SingleOrDefault()
        End Using
    End Function
    Public Function GetLTLLoadBaleNotReceiveByInvoiceNo(invoiceNo As String) As List(Of sp_Receiving_SEL_LoadBaleNotReceiveByInvoiceNo_Result) Implements IReceivingBL.GetLTLLoadBaleNotReceiveByInvoiceNo
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_LoadBaleNotReceiveByInvoiceNo(invoiceNo).ToList()
        End Using
    End Function
    Public Function GetMatByBaleBarcode(baleBarcode As String) As sp_Receiving_SEL_MatByBC_Result Implements IReceivingBL.GetMatByBaleBarcode
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatByBC(baleBarcode).SingleOrDefault()
        End Using
    End Function

    Public Function GetMatByBatchNo(batchNo As String) As List(Of sp_Receiving_SEL_MatByBatchNo_Result) Implements IReceivingBL.GetMatByBatchNo
        Using resource As New StecDBMSEntities
            Return resource.sp_Receiving_SEL_MatByBatchNo(batchNo).ToList
        End Using
    End Function

    Public Function GetMatByCrop(crop As Integer, baleBarcode As String) As sp_Receiving_SEL_Mat_Result Implements IReceivingBL.GetMatByCrop
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_Mat(crop, baleBarcode).SingleOrDefault()
        End Using
    End Function
    Public Function GetMatByRcno(rcno As String) As List(Of sp_Receiving_SEL_MatByRcNo_Result) Implements IReceivingBL.GetMatByRcno
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MatByRcNo(rcno).ToList()
        End Using
    End Function
    Public Function GetMaxBaleNoForNPI(crop As Integer, supplier As String) As String Implements IReceivingBL.GetMaxBaleNoForNPI
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_MaxBalenoForNPI(crop, supplier).ToString()
        End Using
    End Function
    Public Function GetNTRMInspectionByBaleBarcode(baleBarcode As String) As List(Of sp_NTRM_SEL_NTRMInspectionByBC_Result) Implements IReceivingBL.GetNTRMInspectionByBaleBarcode
        Using _context As New StecDBMSEntities
            Return _context.sp_NTRM_SEL_NTRMInspectionByBC(baleBarcode).ToList()
        End Using
    End Function
    Public Function GetNTRMTypes() As List(Of sp_NTRM_SEL_NTRMTYPE_Result) Implements IReceivingBL.GetNTRMTypes
        Using _context As New StecDBMSEntities
            Return _context.sp_NTRM_SEL_NTRMTYPE().OrderBy(Function(x) x.NTRMTypeName).ToList()
        End Using
    End Function
    Public Function GetRemainingBurleyReceivingBaleFromTransportationCode(transportationCode As String, crop As Int16, rcno As String) As List(Of sp_Receiving_SEL_BCRemainingFromBuyingSystem_Result) Implements IReceivingBL.GetRemainingBurleyReceivingBaleFromTransportationCode
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_BCRemainingFromBuyingSystem(transportationCode, crop, rcno).ToList()
        End Using
    End Function
    Public Function GetRemainingLTLReceivingBaleFromInvoiceNo(invoiceNo As String) As List(Of sp_Receiving_SEL_LoadBaleNotReceiveByInvoiceNo_Result) Implements IReceivingBL.GetRemainingLTLReceivingBaleFromInvoiceNo
        Using _context As New StecDBMSEntities
            Dim list = _context.sp_Receiving_SEL_LoadBaleNotReceiveByInvoiceNo(invoiceNo)
            Return list.ToList()
        End Using
    End Function
    Public Function GetSupplierGrouByBatchNo(batchNo As String) As List(Of sp_Receiving_SEL_SupplierGroubyBatchNo_Result) Implements IReceivingBL.GetSupplierGrouByBatchNo
        Using _context As New StecDBMSEntities
            Return _context.sp_Receiving_SEL_SupplierGroubyBatchNo(batchNo).ToList()
        End Using
    End Function
    Private Function GetMaxBatchNo(crop As Integer, cpaGroup As String, rcLines As String, rcno As String, finishhUser As String, fromType As String) As List(Of sp_Receiving_GETMAX_BatchNo_Result) Implements IReceivingBL.GetMaxBatchNo
        Using _context As New StecDBMSEntities
            Dim list = _context.sp_Receiving_GETMAX_BatchNo(crop, cpaGroup, rcLines, rcno, finishhUser, fromType).ToList()
            Return list
        End Using
    End Function
End Class
