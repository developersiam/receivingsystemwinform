'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class customer
    Public Property code As String
    Public Property name As String
    Public Property address As String
    Public Property tel As String
    Public Property fax As String
    Public Property url As String
    Public Property email As String
    Public Property remark As String
    Public Property dtrecord As Nullable(Of Date)
    Public Property user As String
    Public Property UseOrders As Nullable(Of Boolean)
    Public Property rowguid As System.Guid

End Class
