﻿Public Class FrmAddRemarkRequestFromChecker
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Private Sub FrmAddRemarkRequestFromChecker_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Size = New Size(1360, 768)
            RemarkNoTextBox.Text = ""
            RemarkNameTextBox.Text = ""
            RemarkStatusCheckBox.Checked = True
            If RemarkStatusCheckBox.Checked = True Then
                RemarkStatusCheckBox.Text = "Active"
            ElseIf RemarkStatusCheckBox.Checked = False Then
                RemarkStatusCheckBox.Text = "Unactive"
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub

    Private Sub RemarkNoTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles RemarkNoTextBox.KeyDown
        If e.KeyCode = Keys.Enter Then
            RemarkNameTextBox.Focus()
        End If
    End Sub

    Private Sub AddMetroTile_Click(sender As Object, e As EventArgs) Handles AddMetroTile.Click
        Try
            If RemarkNoTextBox.Text = "" Then
                MessageBox.Show("กรุณาคีย์ Remark no. !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If RemarkNameTextBox.Text = "" Then
                MessageBox.Show("กรุณาเลือก Remark name !", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim XResult As Boolean
            If RemarkStatusCheckBox.Checked = True Then
                XResult = True
            ElseIf RemarkStatusCheckBox.Checked = False Then
                XResult = False
            End If

            db.sp_Receiving_INS_RemarkCheckerRequest(RemarkNoTextBox.Text, RemarkNameTextBox.Text, XResult)

            MessageBox.Show("บันทึกข้อมูลเรียบร้อยแล้ว !!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class