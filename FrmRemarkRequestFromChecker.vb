﻿Public Class FrmRemarkRequestFromChecker
    Inherits MetroFramework.Forms.MetroForm
    Private Sub FrmRemarkRequestFromChecker_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'ReceivingDataSet.sp_Receiving_SEL_RemarkCheckerRequest' table. You can move, or remove it, as needed.

        Try
            Me.Size = New Size(1280, 768)
            UsernameMetroLabel.Text = _username
            Me.Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_RemarkCheckerRequest)
            'RequestTypeComboBox.SelectedIndex = 0
            'ApproveStatusComboBox.SelectedIndex = 0

            'XRequestType = RequestTypeComboBox.Text
            'If ApproveStatusComboBox.SelectedIndex = 0 Then
            '    XApproveStatus = False
            'ElseIf ApproveStatusComboBox.SelectedIndex = 1 Then
            '    XApproveStatus = True
            'End If

            'If XRequestType <> "" Then
            '    Me.Sp_Receiving_SEL_ReceivingCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_ReceivingCheckerRequest, XCrop, XRequestType, XApproveStatus)
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub CreateTile_Click(sender As Object, e As EventArgs) Handles CreateTile.Click
        Try
            FrmAddRemarkRequestFromChecker.ShowDialog()
            FrmAddRemarkRequestFromChecker.Dispose()
            Me.Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_RemarkCheckerRequest)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub RefreshTile_Click(sender As Object, e As EventArgs) Handles RefreshTile.Click
        Me.Sp_Receiving_SEL_RemarkCheckerRequestTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_SEL_RemarkCheckerRequest)
    End Sub

    Private Sub BackMetroTile_Click(sender As Object, e As EventArgs) Handles BackMetroTile.Click
        Me.Dispose()
    End Sub
End Class