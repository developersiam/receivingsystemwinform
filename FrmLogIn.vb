﻿Public Class FrmLogIn
    Inherits MetroFramework.Forms.MetroForm
    Private Sub FrmLogIn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            securityAdapter.Fill(securityDataTable)
            UsernameTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub UsernameTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles UsernameTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                PasswordTextBox.Text = ""
                PasswordTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub PasswordTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles PasswordTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                LoginButton.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        Try
            UsernameTextBox.Text = ""
            PasswordTextBox.Text = ""
            UsernameTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
        Try
            Dim username = Trim(UsernameTextBox.Text)
            securityRow = securityDataTable.FindByuname(username)

            If securityRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                UsernameTextBox.Text = ""
                PasswordTextBox.Text = ""
                UsernameTextBox.Focus()
                Return
            End If

            'เช็ค password โดยจะต้องใช้ store  decodePassword เพื่อนำ password ที่ทำการเข้ารหัสแต่ละครั้งออกมา โดยต้องเข้ารหัสจำนวน 3 ครั้งจึงจะได้ password ที่แท้จริง
            Dim password = securityRow.pwd
            For i As Integer = 1 To 3
                Dim decodePasswordTableAdapter As New ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter()
                Dim decodeDataTable As New ReceivingDataSet.sp_Receiving_DecodePasswordDataTable()
                decodePasswordTableAdapter.Fill(decodeDataTable, password)
                password = decodeDataTable.Rows(0).Item("PWDanswer")
            Next

            If securityRow.uname <> username Then
                MessageBox.Show("Username ไม่ถูกต้อง , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            If password <> PasswordTextBox.Text Then
                MessageBox.Show("Password ไม่ถูกต้อง , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If

            'ตรวจสอบสิทธิ์การเข้าใช้งานระบบ
            If securityRow.receiving = True Or securityRow.cpaResult = True Or securityRow.level = "Supervisor" Or securityRow.level = "Admin" Or securityRow.level = "User" Then
                _username = username
                _password = password

                Dim form As New FrmMenu()
                form.ShowDialog()
            Else
                MessageBox.Show("User นี้ไม่มีสิทธิ์ในการใช้งานโปรแกรมนี้, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Class