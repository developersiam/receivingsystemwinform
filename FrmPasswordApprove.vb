﻿Public Class FrmPasswordApprove
    Inherits MetroFramework.Forms.MetroForm
    Dim db As New ReceivingDataClassesDataContext
    Dim _password As String
    Dim _green As String
    Dim _classify As String
    Dim _buyingWeight As Double
    Dim _receivingWeight As Double
    Dim _issued As Boolean 'ไว้สำหรับเช็คสถานะการ issued ของ barcode นั้นๆ หาก= 1 แสดงว่าถูกนำไปใช้แล้ว ไม่อนุญาตให้ทำการลบหรือแก้ไขข้อมูล
    Dim _leafLocked As Boolean 'ไว้สำหรับเช็คสถานะการ lockedของLeafAcc หาก=1 แสดงว่าใส่ราคาแล้วจะไม่อนุญาตให้ทำการลบหรือแก้ไขข้อมูล
    Public _baleBarcode As String

    Private Sub FrmPasswordApprove_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
            DiffWeightCheckBox.Checked = False

            If _approveType = "BUBuyingRejection" Then
                MixGradeRadioButton.Visible = True
                NestingRadioButton.Visible = True
            Else
                MixGradeRadioButton.Visible = False
                NestingRadioButton.Visible = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub UsernameTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles UsernameTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                PasswordTextBox.Text = ""
                PasswordTextBox.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub PasswordTextBox_KeyDown(sender As Object, e As KeyEventArgs) Handles PasswordTextBox.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                LoginButton.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub ClearButton_Click(sender As Object, e As EventArgs) Handles ClearButton.Click
        Try
            UsernameTextBox.Text = ""
            PasswordTextBox.Text = ""
            UsernameTextBox.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
        Try
            Dim username As String

            username = Trim(UsernameTextBox.Text)
            Dim securityRow As ReceivingDataSet.securityRow
            Me.SecurityTableAdapter.Fill(Me.ReceivingDataSet.security)
            securityRow = Me.ReceivingDataSet.security.FindByuname(username)

            If securityRow Is Nothing Then
                MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                UsernameTextBox.Text = ""
                PasswordTextBox.Text = ""
                UsernameTextBox.Focus()
                Return
            End If

            'เช็ค password โดยจะต้องใช้ store  decodePassword เพื่อนำ password ที่ทำการเข้ารหัสแต่ละครั้งออกมา โดยต้องเข้ารหัสจำนวน 3 ครั้งจึงจะได้ password ที่แท้จริง
            _password = securityRow.pwd
            For i As Integer = 1 To 3
                Me.Sp_Receiving_DecodePasswordTableAdapter.Fill(Me.ReceivingDataSet.sp_Receiving_DecodePassword, _password)
                _password = Me.ReceivingDataSet.sp_Receiving_DecodePassword.Rows(0).Item("PWDanswer")
            Next

            If securityRow.uname = username And
                    _password = PasswordTextBox.Text And
                    securityRow.receiving = True And
                    (securityRow.level = "Supervisor" Or securityRow.level = "Admin") Then

                ' ให้ Fill ลง sp_Receiving_SEL_MatByRcNo เพื่อ Get ค่า Green , Classify , BuyingWeight, ReceivingWeight
                Dim matRow As ReceivingDataSet.sp_Receiving_SEL_MatByRcNoRow
                Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.Fill(ReceivingDataSet.sp_Receiving_SEL_MatByRcNo, _aproveRcno)
                matRow = Me.ReceivingDataSet.sp_Receiving_SEL_MatByRcNo.FindBybc(_baleBarcode)
                If Not matRow Is Nothing Then
                    _green = matRow.green
                    _classify = matRow.classify
                    _buyingWeight = matRow.weightbuy
                    _receivingWeight = matRow.weight
                    _issued = matRow.issued
                    _leafLocked = matRow.leaflocked
                End If

                '12 May 2018 แก้ไขโดยเพิ่มให้ Reject bale from Buying ได้
                If _approveType = "BUBuyingRejection" Then 'ถ้าขอ Reject barcode จากระบบ buyingsystem ที่ห่อยาเคยซื้อมาแล้วแต่ต้องการ Reject

                    If MixGradeRadioButton.Checked = False And NestingRadioButton.Checked = False And NTRMRadioButton.Checked = False Then
                        MessageBox.Show("กรณียา BU ของ STEC จะต้องมีการระบุเหตุผลของการ reject ด้วยทุกครั้ง",
                                        "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If

                    If MessageBox.Show("ต้องการ Reject barcode " & _baleBarcode & " นี้ใช่หรือไม่?",
                                           "Question", MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Information) = DialogResult.Yes Then

                        Dim rejectReason As String
                        If MixGradeRadioButton.Checked = True Then
                            rejectReason = MixGradeRadioButton.Text
                        ElseIf NestingRadioButton.Checked = True Then
                            rejectReason = NestingRadioButton.Text
                        Else
                            rejectReason = NTRMRadioButton.Text
                        End If

                        db.sp_UPD_RejectBuyingBale(_baleBarcode, username, rejectReason)

                        MessageBox.Show("ทำการ Rejet ห่อยานี้เรียบร้อยแล้ว!! กรุณาส่งอีเมลล์พร้อมแนบหลักฐานการ Reject ห่อยา เช่นรูปถ่าย ฯลฯ และสาเหตุการ Reject ให้ผู้เกี่ยวข้องรับทราบอีกครั้ง",
                                        "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                    Me.Close()
                ElseIf _approveType = "DeleteBaleBarcode" Then  'ถ้าขอลบ barcode มาจาก form receivingweigth
                    '    'เพิ่มเช็คว่า หาก leaflocked =1 แสดงว่าใส่ราคาแล้วจาก LeafAcc จะไม่อนุญาตให้ทำการแก้ไขหรือลบ
                    If _leafLocked = True Then
                        MessageBox.Show("Barcode นี้ถูกระบุราคาโดย Green Leaf แล้วไม่สามารถแก้ไขหรือลบข้อมูลได้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If

                    'เพิ่มเช็คว่า หาก xissued = 1 แสดงว่าถูกไปใช้งานแล้วไม่อนุญาตให้ทำการแก้ไขหรือลบ
                    If _issued = True Then
                        MessageBox.Show("Barcode นี้ถูกนำไปใช้งานต่อแล้ว เช่นเข้า Blend, Regrade,Handstrip ไม่สามารถแก้ไขหรือลบข้อมูลได้ , กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                        Return
                    End If

                    If MessageBox.Show("ต้องการลบ barcode " & _baleBarcode & " นี้ใช่หรือไม่?",
                            "Question",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) = DialogResult.Yes Then

                        If DiffWeightCheckBox.Checked = True Then
                            db.sp_Receiving_DEL_MatCPA(_baleBarcode, _aproveRcno, _green, _classify, _buyingWeight, _receivingWeight, "DiffWeight", username)
                        Else
                            db.sp_Receiving_DEL_MatCPA(_baleBarcode, _aproveRcno, _green, _classify, _buyingWeight, _receivingWeight, "From FrmReceivingWeight confirmed to delete barcode :" & _baleBarcode, username)
                        End If
                        MessageBox.Show("ทำการลบเรียบร้อยแล้ว!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                    Me.Close()
                End If
            Else
                MessageBox.Show("Username ไม่ถูกต้องหรือไม่มีสิทธิ์ในการ approve, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    
End Class