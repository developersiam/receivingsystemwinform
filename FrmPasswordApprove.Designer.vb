﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPasswordApprove
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ClearButton = New MetroFramework.Controls.MetroButton()
        Me.LoginButton = New MetroFramework.Controls.MetroButton()
        Me.MetroLabel2 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.PasswordTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.UsernameTextBox = New MetroFramework.Controls.MetroTextBox()
        Me.ReceivingDataSet = New ReceivingSystem.ReceivingDataSet()
        Me.SecurityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SecurityTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter()
        Me.TableAdapterManager = New ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager()
        Me.Sp_Receiving_DecodePasswordBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_DecodePasswordTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter()
        Me.MetroLabel3 = New MetroFramework.Controls.MetroLabel()
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter = New ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter()
        Me.DiffWeightCheckBox = New MetroFramework.Controls.MetroCheckBox()
        Me.MixGradeRadioButton = New System.Windows.Forms.RadioButton()
        Me.NestingRadioButton = New System.Windows.Forms.RadioButton()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.NTRMRadioButton = New System.Windows.Forms.RadioButton()
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ClearButton
        '
        Me.ClearButton.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.ClearButton.Location = New System.Drawing.Point(99, 3)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(90, 30)
        Me.ClearButton.Style = MetroFramework.MetroColorStyle.Orange
        Me.ClearButton.TabIndex = 19
        Me.ClearButton.Text = "Clear"
        Me.ClearButton.UseSelectable = True
        '
        'LoginButton
        '
        Me.LoginButton.FontSize = MetroFramework.MetroButtonSize.Medium
        Me.LoginButton.Location = New System.Drawing.Point(3, 3)
        Me.LoginButton.Name = "LoginButton"
        Me.LoginButton.Size = New System.Drawing.Size(90, 30)
        Me.LoginButton.Style = MetroFramework.MetroColorStyle.Orange
        Me.LoginButton.TabIndex = 18
        Me.LoginButton.Text = "Submit"
        Me.LoginButton.UseSelectable = True
        '
        'MetroLabel2
        '
        Me.MetroLabel2.AutoSize = True
        Me.MetroLabel2.Location = New System.Drawing.Point(3, 54)
        Me.MetroLabel2.Name = "MetroLabel2"
        Me.MetroLabel2.Size = New System.Drawing.Size(63, 19)
        Me.MetroLabel2.TabIndex = 17
        Me.MetroLabel2.Text = "Password"
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 0)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(68, 19)
        Me.MetroLabel1.TabIndex = 16
        Me.MetroLabel1.Text = "Username"
        '
        'PasswordTextBox
        '
        '
        '
        '
        Me.PasswordTextBox.CustomButton.Image = Nothing
        Me.PasswordTextBox.CustomButton.Location = New System.Drawing.Point(164, 1)
        Me.PasswordTextBox.CustomButton.Name = ""
        Me.PasswordTextBox.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.PasswordTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.PasswordTextBox.CustomButton.TabIndex = 1
        Me.PasswordTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.PasswordTextBox.CustomButton.UseSelectable = True
        Me.PasswordTextBox.CustomButton.Visible = False
        Me.PasswordTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.PasswordTextBox.Lines = New String(-1) {}
        Me.PasswordTextBox.Location = New System.Drawing.Point(3, 76)
        Me.PasswordTextBox.MaxLength = 32767
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.PasswordTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.PasswordTextBox.SelectedText = ""
        Me.PasswordTextBox.SelectionLength = 0
        Me.PasswordTextBox.SelectionStart = 0
        Me.PasswordTextBox.ShortcutsEnabled = True
        Me.PasswordTextBox.Size = New System.Drawing.Size(192, 29)
        Me.PasswordTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.PasswordTextBox.TabIndex = 15
        Me.PasswordTextBox.UseSelectable = True
        Me.PasswordTextBox.UseStyleColors = True
        Me.PasswordTextBox.UseSystemPasswordChar = True
        Me.PasswordTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.PasswordTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'UsernameTextBox
        '
        Me.UsernameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.UsernameTextBox.CustomButton.Image = Nothing
        Me.UsernameTextBox.CustomButton.Location = New System.Drawing.Point(164, 1)
        Me.UsernameTextBox.CustomButton.Name = ""
        Me.UsernameTextBox.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.UsernameTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.UsernameTextBox.CustomButton.TabIndex = 1
        Me.UsernameTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.UsernameTextBox.CustomButton.UseSelectable = True
        Me.UsernameTextBox.CustomButton.Visible = False
        Me.UsernameTextBox.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.UsernameTextBox.Lines = New String(-1) {}
        Me.UsernameTextBox.Location = New System.Drawing.Point(3, 22)
        Me.UsernameTextBox.MaxLength = 32767
        Me.UsernameTextBox.Name = "UsernameTextBox"
        Me.UsernameTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.UsernameTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.UsernameTextBox.SelectedText = ""
        Me.UsernameTextBox.SelectionLength = 0
        Me.UsernameTextBox.SelectionStart = 0
        Me.UsernameTextBox.ShortcutsEnabled = True
        Me.UsernameTextBox.Size = New System.Drawing.Size(192, 29)
        Me.UsernameTextBox.Style = MetroFramework.MetroColorStyle.Orange
        Me.UsernameTextBox.TabIndex = 14
        Me.UsernameTextBox.UseSelectable = True
        Me.UsernameTextBox.UseStyleColors = True
        Me.UsernameTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.UsernameTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'ReceivingDataSet
        '
        Me.ReceivingDataSet.DataSetName = "ReceivingDataSet"
        Me.ReceivingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SecurityBindingSource
        '
        Me.SecurityBindingSource.DataMember = "security"
        Me.SecurityBindingSource.DataSource = Me.ReceivingDataSet
        '
        'SecurityTableAdapter
        '
        Me.SecurityTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.securityTableAdapter = Me.SecurityTableAdapter
        Me.TableAdapterManager.typeTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'Sp_Receiving_DecodePasswordBindingSource
        '
        Me.Sp_Receiving_DecodePasswordBindingSource.DataMember = "sp_Receiving_DecodePassword"
        Me.Sp_Receiving_DecodePasswordBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_DecodePasswordTableAdapter
        '
        Me.Sp_Receiving_DecodePasswordTableAdapter.ClearBeforeFill = True
        '
        'MetroLabel3
        '
        Me.MetroLabel3.AutoSize = True
        Me.MetroLabel3.ForeColor = System.Drawing.Color.Green
        Me.MetroLabel3.Location = New System.Drawing.Point(3, 293)
        Me.MetroLabel3.Margin = New System.Windows.Forms.Padding(3, 20, 3, 0)
        Me.MetroLabel3.Name = "MetroLabel3"
        Me.MetroLabel3.Size = New System.Drawing.Size(287, 19)
        Me.MetroLabel3.TabIndex = 20
        Me.MetroLabel3.Text = "Please logon  for approve deleting or rejection."
        Me.MetroLabel3.UseCustomBackColor = True
        Me.MetroLabel3.UseCustomForeColor = True
        '
        'Sp_Receiving_SEL_MatByRcNoBindingSource
        '
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataMember = "sp_Receiving_SEL_MatByRcNo"
        Me.Sp_Receiving_SEL_MatByRcNoBindingSource.DataSource = Me.ReceivingDataSet
        '
        'Sp_Receiving_SEL_MatByRcNoTableAdapter
        '
        Me.Sp_Receiving_SEL_MatByRcNoTableAdapter.ClearBeforeFill = True
        '
        'DiffWeightCheckBox
        '
        Me.DiffWeightCheckBox.AutoSize = True
        Me.DiffWeightCheckBox.BackColor = System.Drawing.Color.White
        Me.DiffWeightCheckBox.DisplayFocus = True
        Me.DiffWeightCheckBox.FontSize = MetroFramework.MetroCheckBoxSize.Medium
        Me.DiffWeightCheckBox.FontWeight = MetroFramework.MetroCheckBoxWeight.Bold
        Me.DiffWeightCheckBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.DiffWeightCheckBox.Location = New System.Drawing.Point(3, 3)
        Me.DiffWeightCheckBox.Name = "DiffWeightCheckBox"
        Me.DiffWeightCheckBox.Size = New System.Drawing.Size(101, 19)
        Me.DiffWeightCheckBox.Style = MetroFramework.MetroColorStyle.Lime
        Me.DiffWeightCheckBox.TabIndex = 129
        Me.DiffWeightCheckBox.Text = "Diff Weight"
        Me.DiffWeightCheckBox.UseCustomBackColor = True
        Me.DiffWeightCheckBox.UseCustomForeColor = True
        Me.DiffWeightCheckBox.UseSelectable = True
        '
        'MixGradeRadioButton
        '
        Me.MixGradeRadioButton.AutoSize = True
        Me.MixGradeRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.MixGradeRadioButton.Location = New System.Drawing.Point(3, 35)
        Me.MixGradeRadioButton.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.MixGradeRadioButton.Name = "MixGradeRadioButton"
        Me.MixGradeRadioButton.Size = New System.Drawing.Size(112, 20)
        Me.MixGradeRadioButton.TabIndex = 130
        Me.MixGradeRadioButton.TabStop = True
        Me.MixGradeRadioButton.Text = "Mix Grade (ปน)"
        Me.MixGradeRadioButton.UseVisualStyleBackColor = True
        '
        'NestingRadioButton
        '
        Me.NestingRadioButton.AutoSize = True
        Me.NestingRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.NestingRadioButton.Location = New System.Drawing.Point(3, 61)
        Me.NestingRadioButton.Name = "NestingRadioButton"
        Me.NestingRadioButton.Size = New System.Drawing.Size(108, 20)
        Me.NestingRadioButton.TabIndex = 131
        Me.NestingRadioButton.TabStop = True
        Me.NestingRadioButton.Text = "Nesting (ยัดไส้)"
        Me.NestingRadioButton.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.UsernameTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.MetroLabel2)
        Me.FlowLayoutPanel1.Controls.Add(Me.PasswordTextBox)
        Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 120)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(3, 10, 3, 3)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(198, 150)
        Me.FlowLayoutPanel1.TabIndex = 132
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoScroll = True
        Me.FlowLayoutPanel2.AutoSize = True
        Me.FlowLayoutPanel2.Controls.Add(Me.LoginButton)
        Me.FlowLayoutPanel2.Controls.Add(Me.ClearButton)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 111)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(192, 36)
        Me.FlowLayoutPanel2.TabIndex = 133
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = True
        Me.FlowLayoutPanel3.Controls.Add(Me.DiffWeightCheckBox)
        Me.FlowLayoutPanel3.Controls.Add(Me.MixGradeRadioButton)
        Me.FlowLayoutPanel3.Controls.Add(Me.NestingRadioButton)
        Me.FlowLayoutPanel3.Controls.Add(Me.NTRMRadioButton)
        Me.FlowLayoutPanel3.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel3.Controls.Add(Me.MetroLabel3)
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(23, 75)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(293, 312)
        Me.FlowLayoutPanel3.TabIndex = 133
        '
        'NTRMRadioButton
        '
        Me.NTRMRadioButton.AutoSize = True
        Me.NTRMRadioButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.NTRMRadioButton.Location = New System.Drawing.Point(3, 87)
        Me.NTRMRadioButton.Name = "NTRMRadioButton"
        Me.NTRMRadioButton.Size = New System.Drawing.Size(66, 20)
        Me.NTRMRadioButton.TabIndex = 133
        Me.NTRMRadioButton.TabStop = True
        Me.NTRMRadioButton.Text = "NTRM"
        Me.NTRMRadioButton.UseVisualStyleBackColor = True
        '
        'FrmPasswordApprove
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(427, 400)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Name = "FrmPasswordApprove"
        Me.Style = MetroFramework.MetroColorStyle.Orange
        Me.Text = "Deleting / Rejection"
        CType(Me.ReceivingDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SecurityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_DecodePasswordBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sp_Receiving_SEL_MatByRcNoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ClearButton As MetroFramework.Controls.MetroButton
    Friend WithEvents LoginButton As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroLabel2 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PasswordTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents UsernameTextBox As MetroFramework.Controls.MetroTextBox
    Friend WithEvents ReceivingDataSet As ReceivingSystem.ReceivingDataSet
    Friend WithEvents SecurityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SecurityTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.securityTableAdapter
    Friend WithEvents TableAdapterManager As ReceivingSystem.ReceivingDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Sp_Receiving_DecodePasswordBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_DecodePasswordTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_DecodePasswordTableAdapter
    Friend WithEvents MetroLabel3 As MetroFramework.Controls.MetroLabel
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Sp_Receiving_SEL_MatByRcNoTableAdapter As ReceivingSystem.ReceivingDataSetTableAdapters.sp_Receiving_SEL_MatByRcNoTableAdapter
    Friend WithEvents DiffWeightCheckBox As MetroFramework.Controls.MetroCheckBox
    Friend WithEvents MixGradeRadioButton As RadioButton
    Friend WithEvents NestingRadioButton As RadioButton
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents NTRMRadioButton As RadioButton
End Class
